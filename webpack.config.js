var path = require('path');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = env => {
  return {
    stats: {
      warnings:false
    },
    entry: './js/index.js',
    output: {
      path: path.resolve(__dirname, 'build'),
      filename: 'js/app.bundle.js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules\/(?!(underscore)\/).*/,
          loader: 'babel-loader',
          resolve: {
            fullySpecified: false
          },
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
          }
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            "style-loader",
            "css-loader",
            "sass-loader",
          ],
        },
        {
          test: /\.(png|jp(e*)g|svg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'images/[hash]-[name].[ext]',
              },
            },
          ],
        }
      ]
    },
    plugins: [
      new CopyWebpackPlugin({
        patterns: [
          {from: 'index.html', to: 'index.html'},
          {from: 'fonts', to: 'fonts'},
          {from: 'img', to: 'img'},
        ],
      }),
    ],
    devServer: {
      port: 1234,
      proxy: {
        "/api": {
          target: "http://localhost:8080",
          pathRewrite: {'' : ''}
        }
      }
    },
    stats: {
      colors: true
    },
  }
};
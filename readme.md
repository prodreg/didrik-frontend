# Didrik

For å starte app

- yarn build
- yarn start
- eventuelt yarn add node_sass

## Packages:

### react-transition-group

Only used one place.

### react-tooltip

Used four places.

### humanize-plus

Only used one place, for parsing a file size. Could definitively be cleaned up and removed.

### jquery

Unclear if used at all. Imported in App.js using a name, but it might still be used? Have to check further.

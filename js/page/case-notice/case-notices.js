import {Link} from "react-router-dom";
import {
  getStoreDataForAllCases,
  getStoreDataForUser
} from "../../redux/selectors";
import {getAllCases} from "../../redux/thunks/case-thunks";
import {connect} from "react-redux";

import React from "react";
import Spinner from "../../components/spinner";
import {MdChevronLeft, MdChevronRight} from "react-icons/md";
import {GrChatOption, GrInProgress} from "react-icons/gr";
import ReactPaginate from "react-paginate";
import Moment from "react-moment";
import {getAllMyNotices} from "../../redux/thunks/case-notice-thunks";
import {translateCaseNoticeType} from "../../misc/translate";
import {MdOutlineNotificationsActive} from "react-icons/md";
import Container from "../../components/ui/container/container";
import {allMyNotices} from "../../redux/reducers/case-notice-reducer";
import SearchForm from "../../components/ui/search-form/search-form";

const NoticeElement = ({
  notice,
}) => (
    <tr className="d-flex">
      <td className="col-4">
        <div>
          <strong>{notice.noticeContent.sak.produsent.navn}</strong> <small className="text-muted">({notice.noticeContent.sak.produsent.prodnr})</small>
        </div>
        <div><small className="text-muted">{notice.noticeContent.bank.name}</small></div>
      </td>
      <td className="col-4">{translateCaseNoticeType(notice.type)}</td>
      <td className="col-4">
        <div className="row">
          <div className="col-5" style={{ textAlign: 'right' }}>
            <Link to={"/case-notice/" + notice.id}>Varsel</Link>
          </div>
          <div className="col-2" style={{ textAlign: 'center' }}>
            |
          </div>
          <div className="col-5" style={{ textAlign: 'left' }}>
            {(notice.type === 'SLETTING' || notice.type === 'ENDRING') && (
                <div>
                  <span className="d-block">Tilbakemelding</span>
                  <span className="d-block text-x-small">ikke nødvendig</span>
                </div>
            )}
            {(notice.type === 'NY' && notice.tilbakemeldingHandled && notice.tilbakemeldingId !== null) && (
                <div>
                  <Link to={"/case-feedback/"
                  + notice.tilbakemeldingId}>Tilbakemelding</Link>
                  <span
                      className="d-block text-success text-x-small">mottatt</span>
                </div>
            )}
            {(notice.type === 'NY' && !notice.tilbakemeldingHandled) && (
                <div>
                  <span>Tilbakemelding</span>
                  <span
                      className="d-block text-warning text-x-small">ikke mottatt</span>
                </div>

            )}
          </div>
        </div>
      </td>
    </tr>
)

class CaseNotices extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      checkShowPendingNotices: true,
      checkShowCompletedNotices: true,

      search: '',

      rowsPerPage: 8,
      currentPage: 0,
    }

    this.handleCheckChange = this.handleCheckChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.filterNotices = this.filterNotices.bind(this);
    this.paginationSlice = this.paginationSlice.bind(this);
  }

  handleCheckChange(event) {
    const target = event.target;
    const name = target.name;

    this.setState({
      [name]: !this.state[name]
    })
  }

  handleSearchChange(event) {
    this.setState({
      search: event.target.value,
      currentPage: 0
    })
  }

  handlePageChange({ selected }) {
    this.setState({
      currentPage: selected
    });
  };

  paginationSlice(notices) {
    return notices.slice(
        this.state.currentPage * this.state.rowsPerPage,
        (this.state.currentPage * this.state.rowsPerPage)
        + this.state.rowsPerPage,
    );
  }

  filterNotices(notices) {
    const search = this.state.search.toLowerCase();

    return notices
    .filter(element => {
      if (this.state.checkShowPendingNotices && element.status !== 'SENT') {
        return true;
      }
      if (this.state.checkShowCompletedNotices && element.status === 'SENT') {
        return true;
      }

      return false;
    })
    .filter(element => {
      if (element.bankOrgNumber.includes(search)) {
        return true;
      }
      if (element.mottakerId.includes(search)) {
        return true;
      }
      if (element.noticeContent.bank.name.toLowerCase().includes(search)) {
        return true;
      }

      if (element.noticeContent.sak.produsent) {
        if ((element.noticeContent.sak.produsent.prodnr || '').includes(search)) {
          return true;
        }
        if ((element.noticeContent.sak.produsent.orgnr || '').includes(search)) {
          return true;
        }
        if ((element.noticeContent.sak.produsent.navn|| '').toLowerCase().includes(search)) {
          return true;
        }

        if(element.noticeContent.sak.produsent.allProdNr) {
          for (let index = 0; index < element.noticeContent.sak.produsent.allProdNr.length;
              index++) {
            if (element.noticeContent.sak.produsent.allProdNr[index].includes(search)) {
              return true;
            }
          }
        }

        if(element.noticeContent.sak.produsent.allOrgNr) {
          for (let index = 0;
              index < element.noticeContent.sak.produsent.allOrgNr.length;
              index++) {
            if (element.noticeContent.sak.produsent.allOrgNr[index] &&
              element.noticeContent.sak.produsent.allOrgNr[index].includes(search)) {
              return true;
            }
          }
        }
      }

      return false
    })
  }

  componentDidMount() {
    this.props.getAllMyNotices(this.props.user.data.orgNumber);
  }

  render() {
    const allMyNotices = this.props.allMyNotices;
    const allMyNoticesFiltered = this.filterNotices(allMyNotices.data);
    const paginationSlice = this.paginationSlice(allMyNoticesFiltered);

    return (
        <Container
            title={"Varsler"}
            description={"Summering av aktuelle varsler for din organisasjon"}
            icon={<MdOutlineNotificationsActive size="60px"/>}
        >
          <div className="row">
            <div className="col-lg">
                  <div className="row">
                    <div className="col-lg">
                      {allMyNotices.loading && (
                          <Spinner/>
                      )}
                      {allMyNotices.loaded && (
                          <div>
                            <div className="row">
                              <div className="col-md">
                                <div className="mb-3">
                                  <SearchForm
                                    type="text"
                                    id="searchInput"
                                    value={this.state.search}
                                    placeholder="Søk"
                                    className=""
                                    onChange={this.handleSearchChange}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md">
                                <div className="form-check">
                                  <input className="form-check-input"
                                         type="checkbox"
                                         name="checkShowPendingNotices"
                                         checked={this.state.checkShowPendingNotices}
                                         onChange={this.handleCheckChange}
                                         id="checkShowPendingNotices"/>
                                  <label className="form-check-label"
                                         htmlFor="checkShowPendingNotices">
                                    Vis saker under behandling
                                  </label>
                                </div>
                                <div className="form-check">
                                  <input className="form-check-input"
                                         type="checkbox"
                                         name="checkShowCompletedNotices"
                                         checked={this.state.checkShowCompletedNotices}
                                         onChange={this.handleCheckChange}
                                         id="checkShowCompletedNotices"/>
                                  <label className="form-check-label"
                                         htmlFor="checkShowCompletedNotices">
                                    Vis ferdigstilte saker
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md">
                                <table className="table table-sm table-striped">
                                  <tbody>
                                  {paginationSlice.map((element, index) => (
                                      <NoticeElement
                                          key={index}
                                          notice={element}/>
                                  ))}
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md">
                                <br/>
                                <ReactPaginate
                                    previousLabel={<MdChevronLeft/>}
                                    nextLabel={<MdChevronRight/>}
                                    breakLabel={"..."}
                                    forcePage={this.state.currentPage}
                                    pageCount={allMyNoticesFiltered.length
                                        / this.state.rowsPerPage}
                                    onPageChange={this.handlePageChange}
                                    marginPagesDisplayed={3}
                                    pageRangeDisplayed={3}
                                    containerClassName={"pagination"}
                                    pageClassName={"page-item"}
                                    pageLinkClassName={"page-link"}
                                    previousClassName={"page-item"}
                                    previousLinkClassName={"page-link"}
                                    nextLinkClassName={"page-link"}
                                    nextClassName={"page-item"}
                                    breakClassName={"page-item"}
                                    breakLinkClassName={"page-link"}
                                    activeClassName={"active"}
                                />
                              </div>
                            </div>
                          </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
        </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const user = getStoreDataForUser(state);
  const allMyNotices = state.allMyNotices;
  return {
    user,
    allMyNotices
  };
}

const mapDispatchToProps = {
  getAllMyNotices
}

export default connect(mapStateToProps, mapDispatchToProps)(CaseNotices)
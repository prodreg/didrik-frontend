import {
  getStoreDataFormdataNoticeTypes,
  getStoreDataFormdataProductTypes,
  getStoreDataFormdataSchemeTypes,
  getStoreDataForUser,
} from "../../redux/selectors";
import { connect } from "react-redux";

import React from "react";
import Spinner from "../../components/spinner";
import { getCaseById } from "../../redux/thunks/case-thunks";
import Swal from "sweetalert2";
import Moment from "react-moment";
import { getNoticeById } from "../../redux/thunks/case-notice-thunks";
import { formatAccountNumber, formatCurrency } from "../../misc/helpers";
import { Link } from "react-router-dom";
import { translateCaseNoticeTypeExplanation } from "../../misc/translate";
import Conversation from "../conversation/conversation";
import PrimaryButton from "../../components/ui/button/primary-button";
import InternalNote from "../../components/internal-note";

const VareleveranseElement = ({ vareleveranseList }) => (
  <div className="card mt-2">
    <div className="card-body">
      <div className="row">
        <div className="col-md-4 mb-3">
          <label className="form-label">Varemottaker</label>
          <input
            readOnly
            type="text"
            className="form-control form-control-sm"
            value={vareleveranseList[0].varemottaker.name}
          />
        </div>
        <div className="col-md-4 mb-3">
          <label className="form-label">Ordning</label>
          <input
            readOnly
            type="text"
            className="form-control form-control-sm"
            value={vareleveranseList[0].ordning}
          />
        </div>
      </div>
      {vareleveranseList.map((element, index) => (
        <div key={index}>
          <hr />
          <div className="row">
            <div className="col-md-2 mb-3">
              <label className="form-label">År</label>
              <input
                readOnly
                type="text"
                className="form-control form-control-sm"
                value={element.aar}
              />
            </div>
            <div className="col-md-3 mb-3">
              <label className="form-label">Produktslag</label>
              <input
                readOnly
                type="text"
                className="form-control form-control-sm"
                value={element.produktslagDescription}
              />
            </div>
            {!element.korrBeloep && (
              <div className="col-md-5 mb-3">
                <label className="form-label">
                  Årlig beløp overført til bank
                </label>
                <input
                  readOnly
                  type="text"
                  className="form-control form-control-sm"
                  value={element.oppgjoer}
                />
              </div>
            )}

            {element.korrBeloep && (
              <div className="col-md-5 mb-3">
                <div className="row">
                  <div className="col-md-6">
                    <label className="form-label">
                      Årlig beløp overført til bank
                    </label>

                    <input
                      readOnly
                      type="text"
                      className="form-control form-control-sm"
                      style={{ textDecoration: "line-through" }}
                      value={element.oppgjoer}
                    />
                  </div>
                  <div className="col-md-6">
                    <label className="form-label">
                      Årlig beløp overført til bank (Korrigert)
                    </label>

                    <input
                      readOnly
                      type="text"
                      className="form-control form-control-sm"
                      value={element.korrBeloep}
                    />
                  </div>
                </div>
              </div>
            )}
            {element.korrTrekk && (
              <div className="col-md-2 mb-3">
                <label className="form-label">Trekk</label>

                <input
                  readOnly
                  type="text"
                  className="form-control form-control-sm"
                  style={{ textDecoration: "line-through" }}
                  value={element.korrTrekk}
                />
              </div>
            )}
          </div>
          <div className="row">
            <div className="col-md-12 mb-3">
              <div className="form-floating">
                <textarea
                  readOnly
                  className="form-control form-control-sm"
                  id="floatingTextarea2"
                  style={{ height: "100px" }}
                  name="text"
                  value={element.text}
                ></textarea>
                <label htmlFor="floatingTextarea2">
                  Ytterligere informasjon
                </label>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  </div>
);

class CaseNotice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataLoaded: false,
      noticeDto: {},
      noticeContent: {},
      noticeLegal: {},
    };
  }

  componentDidMount() {
    this.props.getNoticeById(this.props.match.params.noticeId).then((data) => {
      const noticeContent = JSON.parse(data.varselContent);
      const noticeLegal = JSON.parse(noticeContent.legalText);

      this.setState({
        dataLoaded: true,
        noticeDto: data,
        noticeContent: noticeContent,
        noticeLegal: noticeLegal,
      });
    });
  }

  groupVareleveranseOnOrganizationNumber(vareleveranseList) {
    return vareleveranseList.reduce((payload, vareleveranse) => {
      const index = payload.findIndex((element) => {
        return element.some(
          (some) =>
            some.varemottaker.orgNumber === vareleveranse.varemottaker.orgNumber
        );
      });
      if (index === -1) {
        payload.push([vareleveranse]);
      } else {
        payload[index].push(vareleveranse);
      }
      return payload;
    }, []);
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-lg">
            <div className="card">
              <div className="card-header" style={{ fontWeight: "bold" }}>
                {translateCaseNoticeTypeExplanation(this.state.noticeDto.type)}
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg">
                    {!this.state.dataLoaded && <Spinner />}
                    {this.state.dataLoaded && (
                      <div>
                        <div className="row">
                          <div className="col-md-6">
                            <h5>Produsent</h5>
                            <hr />
                            <table className="table table-sm">
                              <tbody>
                                <tr>
                                  <th scope="row">Navn</th>
                                  <td>
                                    {
                                      this.state.noticeContent.sak.produsent
                                        .navn
                                    }
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Produsentnummer</th>
                                  <td>
                                    {
                                      this.state.noticeContent.sak.produsent
                                        .prodnr
                                    }
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Adresse</th>
                                  <td>
                                    {this.state.noticeContent.sak.produsent
                                      .adresse.adresse1 +
                                      ", " +
                                      this.state.noticeContent.sak.produsent
                                        .adresse.postnr +
                                      ", " +
                                      this.state.noticeContent.sak.produsent
                                        .adresse.poststed}
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div className="col-md-6">
                            <h5>Detaljer</h5>
                            <hr />
                            <table className="table table-sm">
                              <tbody>
                                <tr>
                                  <th scope="row">Opprettet</th>
                                  <td>
                                    <Moment
                                      unix
                                      locale="no"
                                      format="D. MMM YYYY [kl.] HH:mm"
                                    >
                                      {this.state.noticeDto.created}
                                    </Moment>
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Bank</th>
                                  <td>{this.state.noticeContent.bank.name}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Mottaker</th>
                                  <td>
                                    {formatCurrency(
                                      this.state.noticeContent.mottaker.name
                                    )}
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Driftskredittkonto</th>
                                  <td>
                                    {formatAccountNumber(
                                      this.state.noticeContent.sak.kontoNummer
                                    )}
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div>
                          <p className="m-0">
                            Banken har innvilget driftskreditt:
                          </p>
                          <p>
                            <strong>
                              {formatCurrency(
                                this.state.noticeContent.sak.bevilget
                              )}
                            </strong>
                          </p>
                          <p className="m-0">{this.state.noticeLegal.pant}</p>
                          <p>
                            <strong>
                              {formatAccountNumber(
                                this.state.noticeContent.sak.kontoNummer
                              )}
                            </strong>
                          </p>
                        </div>
                        <p>{this.state.noticeLegal.tilbakemelding}</p>
                        <h4>Grunnlag for driftskreditt</h4>
                        <hr />

                        {this.groupVareleveranseOnOrganizationNumber(
                          this.state.noticeContent.sak.vareleveranseList
                        ).map((element, index) => (
                          <VareleveranseElement
                            key={index}
                            vareleveranseList={element}
                          />
                        ))}
                        <br />
                        <div className="row">
                          {this.props.user.data.isBank && (
                            <div className="col mt-1">
                              <Link
                                to={"/case/" + this.state.noticeContent.sak.id}
                              >
                                <PrimaryButton
                                  title="Gå til sak"
                                />
                              </Link>
                            </div>
                          )}
                          {this.state.noticeDto.tilbakemeldingId && (
                            <div className="col mt-1">
                              <Link
                                to={
                                  "/case-feedback/" +
                                  this.state.noticeDto.tilbakemeldingId
                                }
                              >
                                <PrimaryButton
                                  title="Gå til tilbakemelding"
                                  className="float-md-end"
                                />

                              </Link>
                            </div>
                          )}
                        </div>
                        {this.props.user.data.isVaremottaker && (
                          <div className="row">
                            <InternalNote
                              caseId={this.state.noticeContent.sak.id}
                            />
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
        {this.state.dataLoaded && (
          <Conversation
            caseId={this.state.noticeDto.sakId}
            receiverId={this.state.noticeContent.mottaker.orgNumber}
            height="500px"
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const user = getStoreDataForUser(state);
  const oneCase = state.oneCase;
  return {
    user,
    oneCase,
  };
};

const mapDispatchToProps = {
  getNoticeById,
};

export default connect(mapStateToProps, mapDispatchToProps)(CaseNotice);

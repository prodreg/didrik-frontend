import React from "react";
import { Row, Col, Form, FloatingLabel } from "react-bootstrap";
import { resetUserPassword, getResetUserPasswordInfo } from "../../redux/thunks/user-thunks";
import { connect } from "react-redux";
import Swal from "sweetalert2";
import SecondaryButton from "../../components/ui/button/secondary-button";

class ResetPassword extends React.Component {

  state = {
    newPassword: "",
    newPasswordConfirm: "",
    validated: false,
    validNewPassword: {
      isValid: false,
      validMinLength: false,
      nonEmpty: false
    },
    validConfirmNewPassword: {
      isValid: false,
      nonEmpty: false,
      equalsNewPassword: false
    },
    validForm: false,
    saving: false
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getResetUserPasswordInfo(this.props.match.params.token)
      .then((res) => {
        if (res.error === true) {
          this.handleInvalidToken();
        }
      });
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    },() => {
      this.validateNewPassword();
      this.validateConfirmNewPassword();
    });
  }

  validateForm = () => {
    const validNewPassword = this.state.validNewPassword.isValid && this.state.validNewPassword.nonEmpty
      && this.state.validNewPassword.validMinLength;
    const validConfirmNewPassword = this.state.validConfirmNewPassword.isValid && this.state.validConfirmNewPassword.nonEmpty
      && this.state.validConfirmNewPassword.equalsNewPassword;
    const isValid = validNewPassword && validConfirmNewPassword;

    this.setState({
      validForm: isValid
    });
  }

  validateNewPassword = () => {
    const password = this.state.newPassword;
    let isValid = true;
    let nonEmpty = true;
    let validMinLength = true;

    if (!password) {
      isValid = false;
      nonEmpty = false;
    }

    if (password.length < 6) {
      isValid = false;
      validMinLength = false;
    }

    const validNewPassword = {
      isValid: isValid,
      nonEmpty: nonEmpty,
      validMinLength: validMinLength
    };

    this.setState({
      validNewPassword: validNewPassword
    }, () => {
      this.validateForm()
    });
  }

  validateConfirmNewPassword = () => {
    const password = this.state.newPasswordConfirm;
    let isValid = true;
    let nonEmpty = true;
    let equalsNewPassword = true;

    if (!password) {
      isValid = false;
      nonEmpty = false;
    }

    if (password !== this.state.newPassword) {
      isValid = false;
      equalsNewPassword = false;
    }

    const validConfirmNewPassword = {
      isValid: isValid,
      nonEmpty: nonEmpty,
      equalsNewPassword: equalsNewPassword
    };

    this.setState({
      validConfirmNewPassword: validConfirmNewPassword
    }, () => {
      this.validateForm()
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    const validForm = this.state.validForm;
    if (form.checkValidity() === false || !validForm) {
      event.stopPropagation();
    } else {
      this.setState({
        saving: true
      });

      this.props.resetUserPassword(this.props.match.params.token, this.state.newPassword).then((res) => {
        this.handleError(res.payload.error);
      })
        .finally(() => {
          this.setState({
            saving: false
          });
        });
    }

    this.setState({
      validated: true
    })
  }

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Det oppstod en feil, og passordet ditt er ikke oppdatert. Vennligst gå på login-siden og klikk 'glemt passord' lenken for å få tilsendt ny passord-reset epost.",
        "error"
      ).then(() => {
        this.props.history.push({
          pathname: "/",
        });
      });
    } else {
      Swal.fire(
        "OK",
        "Passord har blitt endret.",
        "success"
      ).then(() => {
        this.props.history.push({
          pathname: "/",
        });
      });
    }
  }

  handleInvalidToken = () => {
    Swal.fire(
      "En feil har oppstått",
      "Det oppstod en feil, og passordet ditt er ikke oppdatert: passord-reset token ble ikke funnet. Vennligst gå på login-siden og klikk 'glemt passord' lenken for å få tilsendt ny passord-reset epost.",
      "error"
    ).then(() => {
      this.props.history.push({
        pathname: "/",
      });
    });
  }

  render() {
    return (
      <div className="container" style={{ marginTop: "50px" }}>
        <div className="row" style={{ marginBottom: "10px" }}>
          <h2 className="col-md-6 offset-md-3">
            Endre passord
          </h2>
        </div>
        <Row className="mb-3">
          <div className="col-md-6 offset-md-3">
            <Form noValidate onSubmit={ this.handleSubmit }>
              <Row className="mb-3">
                <Form.Group as={Col}>
                  <FloatingLabel label="Nytt Passord">
                    <Form.Control required type="password" name="newPassword" placeholder="Nytt Passord" onChange={ this.handleChange }
                                  isInvalid={!this.state.validNewPassword.isValid && this.state.validated} />
                  </FloatingLabel>
                  {this.state.validated && (
                    <>
                      <Row>
                        {!this.state.validNewPassword.nonEmpty && (
                          <Form.Text className="text-danger text-small">
                            Du må skrive inn det nye passordet
                          </Form.Text>
                        )}
                      </Row>
                      <Row>
                        {!this.state.validNewPassword.validMinLength && (
                          <Form.Text className="text-danger text-small">
                            Passordet må være minst 6 tegn
                          </Form.Text>
                        )}
                      </Row>
                    </>
                  )}
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group>
                  <FloatingLabel label="Nytt Passord (gjenta)">
                    <Form.Control required type="password" placeholder="Nytt Passord (gjenta)" name="newPasswordConfirm"
                                  isInvalid={!this.state.validConfirmNewPassword.isValid && this.state.validated} onChange={ this.handleChange } />
                  </FloatingLabel>
                  {this.state.validated && (
                    <Row>
                      {!this.state.validConfirmNewPassword.isValid && (
                        <Form.Text className="text-danger text-small">
                          Du må skrive inn samme passord to ganger
                        </Form.Text>
                      )}
                    </Row>
                  )}
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Col>
                  <SecondaryButton
                    type="submit"
                    title="Lagre"
                    disabled={ (!this.state.validForm && this.state.validated) || this.state.saving }
                  />
                </Col>
              </Row>
            </Form>
          </div>
        </Row>
      </div>
    );
  }
}

ResetPassword.defaultProps = {
  showChangePassword: false
};

const mapStateToProps = (state) => {
  return { };
}

const mapDispatchToProps = {
  resetUserPassword,
  getResetUserPasswordInfo
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);

import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllNewsPosts,
  deleteNewsPost,
  editNewsPost,
} from "../../../redux/thunks/news-thunks";
import TextArea from "../../../components/ui/textarea/text-area";
import Spinner from "../../../components/spinner";
import Swal from "sweetalert2";
import PrimaryButton from "../../../components/ui/button/primary-button";
import InputField from "../../../components/ui/input-field/input-field";
import "./news-posts.scss";
import { Modal } from "react-bootstrap";
import TertiaryButton from "../../../components/ui/button/tertiary-button";
import SecondaryButton from "../../../components/ui/button/secondary-button";

const NewsPosts = () => {
  const newsPosts = useSelector((state) => state.newsPostsReducer);
  const [header, setHeader] = useState("");
  const [content, setContent] = useState("");
  const [order, setOrder] = useState("");
  const [newsPost, setNewsPost] = useState("");
  const [showModal, setShowModal] = useState(false);
  const dispatch = useDispatch();

  let isHeader = header !== "";
  let isContent = content !== "";
  let isOrder = order !== "";
  let disableButton = isHeader && isContent && isOrder;

  useEffect(() => {
    getNewsPosts();
  }, [dispatch]);

  const getNewsPosts = () => {
    dispatch(getAllNewsPosts()).then((response) => {
      showError(response.payload.error);
    });
  };

  const showError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke laste inn nyhetsartikler. Prøv igjen senere",
        "error"
      );
    }
  };

  const handleIncomingDeleteResponse = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke slette nyhetsartikkelen. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("Vellykket!", "Nyhetsartikkelen har blitt slettet", "success");
      dispatch(getAllNewsPosts());
    }
  };

  const handleIncomingEditResponse = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke redigere nyhetsartikkelen. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("Vellykket!", "Endringen har blitt lagret", "success");
      setShowModal(false);
    }
  };

  const handleDeletePost = (event, newsPostId) => {
    event.preventDefault();
    Swal.fire({
      title: "Er du sikker?",
      text: "Ønsker du å slette denne nyhetsartikkelen?",
      confirmButtonText: "Ja",
      showCancelButton: true,
      cancelButtonText: "Avbryt",
      type: "warning",
    }).then((result) => {
      if (result.value) {
        dispatch(deleteNewsPost(newsPostId)).then((response) => {
          handleIncomingDeleteResponse(response.payload.error);
        });
      }
    });
  };

  const handleEditPost = (event, newsPost) => {
    event.preventDefault();
    setNewsPost(newsPost);
    setHeader(newsPost.header);
    setContent(newsPost.content);
    setOrder(newsPost.order);
    setShowModal(true);
  };

  const handleSavePost = (event) => {
    event.preventDefault();
    newsPost.header = header;
    newsPost.content = content;
    newsPost.order = order;
    dispatch(editNewsPost(newsPost)).then((response) => {
      handleIncomingEditResponse(response.payload.error);
    });
  };

  const headerChangeHandler = (event) => {
    setHeader(event.target.value);
  };

  const contentChangeHandler = (event) => {
    setContent(event.target.value);
  };

  const orderChangeHandler = (event) => {
    setOrder(event.target.value);
  };

  const hideModal = (event) => {
    event.preventDefault();
    setShowModal(false);
  };

  const displayModal = () => {
    return (
      <Modal show={showModal} onHide={hideModal}>
        <Modal.Header closeButton>
          <Modal.Title className="modal-title">
            Rediger nyhetsartikkel
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="edit-container">
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control form-control-sm"
                id="headerInput"
                value={header}
                onChange={headerChangeHandler}
                placeholder="Overskrift..."
              />
              <label htmlFor="headerInput">Overskrift...</label>
              {!isHeader && (
                <p className="error-text">Overskrift kan ikke være tom</p>
              )}
            </div>
            <div className="form-floating mb-3">
              <input
                type="number"
                className="form-control form-control-sm"
                id="orderInput"
                value={order}
                min={0}
                onChange={orderChangeHandler}
                placeholder="Sortering..."
              />
              <label htmlFor="orderInput">Sortering...</label>
              {!isOrder && (
                <p className="error-text">Rekkefølge kan ikke være tom</p>
              )}
            </div>
            <div className="form-floating mb-3" style={{ height: "200px" }}>
              <textarea
                className="form-control form-control-sm"
                cols="30"
                rows="100"
                value={content}
                onChange={contentChangeHandler}
                id="textarea"
                placeholder="Innhold..."
                style={{ height: "200px", resize: "none" }}
              />
              <label htmlFor="textarea">Innhold...</label>
              {!isContent && (
                <p className="error-text">Innhold kan ikke være tom</p>
              )}
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <TertiaryButton
            title="Avbryt"
            onClick={hideModal}
          />
          <SecondaryButton
            title="Lagre"
            onClick={handleSavePost}
            disabled={!disableButton}
          />
        </Modal.Footer>
      </Modal>
    );
  };

  const showNewsPosts = () => {
    return newsPosts.data
      .sort((p1, p2) =>
        p1.order - p2.order === 0
          ? p2.createdAt - p1.createdAt
          : p1.order - p2.order
      )
      .map((newsPost, i) => {
        return (
          <div key={i} className="news-posts-container">
            <div key={i} className="card">
              <div className="card-header">{newsPost.header}</div>
              <div className="card-body">{newsPost.content}</div>
              <div className="card-footer text-muted">
                <div className="order-content">
                  Rekkefølge: {newsPost.order}
                </div>
                <div className="edit-buttons-container">
                  <PrimaryButton
                    title="Rediger"
                    onClick={(e) => handleEditPost(e, newsPost)}
                  />
                  <div className="delete-button">
                    <TertiaryButton
                      title="Slett"
                      onClick={(e) => handleDeletePost(e, newsPost.id)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
  };

  return (
    <div>
      {newsPosts.loading && newsPosts.data.length !== 0 && <Spinner />}
      {newsPosts.data && newsPosts.loaded && showNewsPosts()}
      {displayModal()}
    </div>
  );
};

export default NewsPosts;

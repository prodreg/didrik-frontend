import React from "react";
import Container from "../../../components/ui/container/container";
import { MdOutlineWysiwyg } from "react-icons/md";
import ValidateIfAdmin from "../validate-admin";
import NewsPosts from "./news-posts";
import NewsPostForm from "./news-post-form";

const News = () => {
  return (
    <Container
      title={"Adminverktøy"}
      description={"Publiser nyhetsartikkel"}
      icon={<MdOutlineWysiwyg size="62px" />}
    >
      <ValidateIfAdmin />
      <NewsPostForm />
      <NewsPosts />
    </Container>
  );
};

export default News;

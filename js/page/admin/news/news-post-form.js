import React from "react";
import { useDispatch } from "react-redux";
import { getAllNewsPosts } from "../../../redux/thunks/news-thunks";
import useInput from "../../../hooks/use-input";
import PrimaryButton from "../../../components/ui/button/primary-button";
import TextArea from "../../../components/ui/textarea/text-area";
import InputField from "../../../components/ui/input-field/input-field";
import Swal from "sweetalert2";
import "./news-posts.scss";
import { postNewsPost } from "../../../redux/thunks/news-thunks";

const NewsPostForm = () => {
  const inputCheck = (input) => input.trim() !== "";
  const dispatch = useDispatch();
  const {
    input: order,
    isValid: enteredOrderIsValid,
    hasError: orderError,
    inputChangeHandler: orderChangeHandler,
    inputBlurHandler: orderBlurHandler,
    resetForm: resetOrderInput,
  } = useInput(inputCheck);
  const {
    input: header,
    isValid: enteredHeaderIsValid,
    hasError: headerError,
    inputChangeHandler: headerChangeHandler,
    inputBlurHandler: headerBlurHandler,
    resetForm: resetHeaderInput,
  } = useInput(inputCheck);
  const {
    input: content,
    isValid: enteredContentIsValid,
    hasError: contentError,
    inputChangeHandler: contentChangeHandler,
    inputBlurHandler: contentBlurHandler,
    resetForm: resetContentInput,
  } = useInput(inputCheck);

  let formIsValid =
    enteredHeaderIsValid && enteredContentIsValid && enteredOrderIsValid;

  const postNews = () => {
    const post = {
      header: header,
      content: content,
      order: order,
    };
    Swal.fire({
      title: "Er du sikker?",
      text: "Ønsker du å opprette en nyhetsartikkel?",
      confirmButtonText: "Ja",
      showCancelButton: true,
      cancelButtonText: "Avbryt",
      type: "warning",
    }).then((result) => {
      if (result.value) {
        dispatch(postNewsPost(post)).then((response) => {
          handleIncomingResponse(response.payload.error);
        });
      }
    });
  };

  const resetForm = () => {
    resetHeaderInput();
    resetContentInput();
    resetOrderInput();
  };

  const handleIncomingResponse = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke opprette nyhetsartikkel. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("Vellykket!", "Nyhetsartikkelen er blitt opprettet", "success");
      dispatch(getAllNewsPosts());
      resetForm();
    }
  };

  const formSubmissionHandler = (event) => {
    event.preventDefault();
    if (!formIsValid) {
      return;
    }
    postNews();
  };

  return (
    <form onSubmit={formSubmissionHandler}>
      <div className="form-group">
        <label htmlFor="header" className="label-text">
          Artikkeloverskrift
        </label>
        <div className="form-floating mb-3">
          <input
            type="text"
            className="form-control form-control-sm"
            id="headerInput"
            value={header}
            onChange={headerChangeHandler}
            onBlur={headerBlurHandler}
            placeholder="Overskrift..."
          />
          <label htmlFor="headerInput">Overskrift...</label>
          {headerError && (
            <p className="error-text">Overskrift kan ikke være tom</p>
          )}
        </div>
      </div>
      <div className="number-container">
        <label htmlFor="order" className="label-text">
          Rekkefølge
        </label>
        <div className="form-group">
          <div className="form-floating mb-3">
            <input
              type="number"
              className="form-control form-control-sm"
              id="orderInput"
              value={order}
              min={0}
              onChange={orderChangeHandler}
              onBlur={orderBlurHandler}
              placeholder="Sortering..."
            />
            <label htmlFor="orderInput">Sortering...</label>
            {orderError && (
              <p className="error-text">Rekkefølge kan ikke være tom</p>
            )}
          </div>
        </div>
      </div>
      <div className="content-container">
        <div className="form-group">
          <label htmlFor="content" className="label-text">
            Artikkelinnhold
          </label>
          <div className="form-floating mb-3" style={{ height: "250px" }}>
            <textarea
              className="form-control form-control-sm"
              cols="30"
              rows="100"
              value={content}
              onChange={contentChangeHandler}
              onBlur={contentBlurHandler}
              id="textarea"
              placeholder="Innhold..."
              style={{ height: "250px", resize: "none" }}
            />
            <label htmlFor="textarea">Innhold...</label>
            {contentError && (
              <p className="error-text">Innhold kan ikke være tom</p>
            )}
          </div>
          <small id="contentHelp" className="form-text text-muted">
            Dette vises som innhold i nyhetsartikkelen
          </small>
        </div>
      </div>
      <div className="news-post-button-container">
        <PrimaryButton
          disabled={!formIsValid}
          title="Opprett"
        />
      </div>
    </form>
  );
};

export default NewsPostForm;

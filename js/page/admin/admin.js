import React from "react";
import { Link } from "react-router-dom";
import Container from "../../components/ui/container/container";
import { MdOutlineAdminPanelSettings } from "react-icons/md";
import PrimaryButton from "../../components/ui/button/primary-button";
import "./admin.scss";

const Admin = () => {
  return (
    <Container
      title="Adminverktøy"
      description="Oversikt"
      icon={<MdOutlineAdminPanelSettings size="53px" />}
    >
      <div className="buttons-container">
        <Link to="/administrator/send-event-message">
          <PrimaryButton title="Send melding" />
        </Link>
        <Link to="/administrator/news">
          <PrimaryButton title="Nyhetsartikler" />
        </Link>
        <Link to="/administrator/reporting">
          <PrimaryButton title="Innrapportering" />
        </Link>
      </div>
    </Container>
  );
};

export default Admin;

import React, { useState } from "react";
import useInput from "../../../hooks/use-input";
import { useDispatch } from "react-redux";
import PrimaryButton from "../../../components/ui/button/primary-button";
import InputField from "../../../components/ui/input-field/input-field";
import { getBankFile } from "../../../redux/thunks/admin-thunks";
import "./bank-file.scss";
import Swal from "sweetalert2";
import FileSaver from "file-saver";
import Spinner from "../../../components/spinner";

const BankFile = () => {
  const inputCheck = (input) => input !== "";
  const [radioValue, setRadioValue] = useState("FIXED_FORMAT");
  const [isDownloading, setIsDownloading] = useState(false);
  let bankFileFormIsValid = false;
  const dispatch = useDispatch();
  const {
    input: orgnr,
    isValid: orgnrIsValid,
    inputChangeHandler: orgnrChangeHandler,
    resetForm: resetOrgnrInput,
  } = useInput(inputCheck);
  const {
    input: year,
    isValid: yearIsValid,
    inputChangeHandler: yearChangeHandler,
    resetForm: resetYearInput,
  } = useInput(inputCheck);

  if (orgnrIsValid && yearIsValid) {
    bankFileFormIsValid = true;
  }

  const radioButtonHandler = (event) => {
    setRadioValue(event.target.value);
  };

  const resetForm = () => {
    resetOrgnrInput();
    resetYearInput();
  };

  const showDownloadError = () => {
    setIsDownloading(false);
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke hente fil. Prøv igjen senere",
      "error"
    );
  };

  const downloadBankFile = (data, orgnr) => {
    const blob = new Blob([data], {
      type: "text/plain;charset=utf-8",
    });
    FileSaver.saveAs(blob, "bank-returfil-" + orgnr + ".txt");
    resetForm();
    setIsDownloading(false);
  };

  const formSubmissionHandler = (event) => {
    event.preventDefault();
    if (!bankFileFormIsValid) {
      return;
    }
    setIsDownloading(true);
    dispatch(getBankFile(orgnr, year, radioValue)).then((response) => {
      if (response.payload.error) {
        showDownloadError();
      } else {
        downloadBankFile(response.payload.data, orgnr);
      }
    });
  };

  return (
    <form onSubmit={formSubmissionHandler}>
      <div className="form-group">
        <label htmlFor="orgnr" className="label-text">
          Orgnummer på bank
        </label>
        <div className="form-floating mb-3">
          <input
            type="number"
            className="form-control form-control-sm"
            id="orgnr"
            value={orgnr}
            min={0}
            onChange={orgnrChangeHandler}
            placeholder="Orgnr..."
          />
          <label htmlFor="orgnr">Orgnr...</label>
        </div>
      </div>
      <div className="form-group-year">
        <label htmlFor="year" className="label-text">
          Hent ut for år
        </label>
        <div className="form-floating mb-3">
          <input
            type="number"
            className="form-control form-control-sm"
            id="year"
            value={year}
            min={0}
            onChange={yearChangeHandler}
            placeholder="År..."
          />
          <label htmlFor="year">År...</label>
        </div>
      </div>
      <div className="form-group-format">
        <label htmlFor="send" className="label-text">
          Format
        </label>
        <div className="radio-button-container">
          <div>
            <input
              type="radio"
              value="FIXED_FORMAT"
              checked={radioValue === "FIXED_FORMAT"}
              onChange={radioButtonHandler}
            />
            <label htmlFor="fixed" className="radio-text">
              FIXED_FORMAT
            </label>
          </div>
          <div>
            <input
              type="radio"
              value="SEMI_COLON"
              checked={radioValue === "SEMI_COLON"}
              onChange={radioButtonHandler}
            />
            <label htmlFor="semi" className="radio-text">
              SEMI_COLON
            </label>
          </div>
        </div>
      </div>
      {isDownloading ? (
        <Spinner />
      ) : (
        <div className="download-button">
          <PrimaryButton
            title="Last ned"
            disabled={!bankFileFormIsValid}
          />
        </div>
      )}
    </form>
  );
};

export default BankFile;

import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Container from "../../../components/ui/container/container";
import { MdOutlineAssessment } from "react-icons/md";
import ValidateIfAdmin from "../validate-admin";
import Card from "../../../components/ui/card/card";
import BankFile from "./bank-file";
import ReportFiles from "./report-files";

const Reporting = () => {
  return (
    <Container
      title="Adminverktøy"
      description="Innrapportering"
      icon={<MdOutlineAssessment size="60px" />}
    >
      <ValidateIfAdmin />
      <Card title="Opprett returfil bank">
        <BankFile />
      </Card>
      <Card title="Oversikt over innrapportering">
        <ReportFiles />
      </Card>
    </Container>
  );
};

export default Reporting;

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PrimaryButton from "../../../components/ui/button/primary-button";
import {
  getReportedFiles,
  getReportedFile,
} from "../../../redux/thunks/admin-thunks";
import { getAllOrganizations } from "../../../redux/thunks/organization-thunks";
import useInput from "../../../hooks/use-input";
import InputField from "../../../components/ui/input-field/input-field";
import Spinner from "../../../components/spinner";
import { MdOutlineFileDownload } from "react-icons/md";
import Swal from "sweetalert2";
import "./report-files.scss";
import FileSaver from "file-saver";

const ReportFiles = () => {
  const inputCheck = (input) => input !== "";
  let reportFileFormIsValid = false;
  const organizationsData = useSelector(
    (state) => state.allOrganizationsReducer
  );
  const reportedFilesData = useSelector(
    (state) => state.adminReportedFilesReducer
  );
  const {
    input: year,
    isValid: yearIsValid,
    inputChangeHandler: yearChangeHandler,
  } = useInput(inputCheck);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getReportedFiles(""));
  }, [dispatch]);

  if (yearIsValid) {
    reportFileFormIsValid = true;
  }

  const showError = () => {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste inn innrapporteringer. Prøv igjen senere",
      "error"
    );
  };

  const showDownloadError = () => {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste ned rapport. Prøv igjen senere",
      "error"
    );
  };

  const showNoReports = () => {
    Swal.fire(
      "Ingen innrapportering ",
      "Det finnes ingen innrapportering for dette året",
      "info"
    );
  };

  const getOrganizationName = (orgnr) => {
    const organizationObject = organizationsData.data.filter(
      (o) => o.orgNumber === orgnr
    );
    if (organizationObject.length !== 0) {
      return organizationObject[0].name;
    }
  };

  const formSubmissionHandler = (event) => {
    event.preventDefault();
    if (!reportFileFormIsValid) {
      return;
    }
    dispatch(getAllOrganizations(["MOTTAKER", "BANK"]));
    dispatch(getReportedFiles(year)).then((response) => {
      if (response.payload.error) {
        showError();
      } else if (response.payload.data.length === 0) {
        showNoReports();
      }
    });
  };

  const downloadFile = (data, mottakerId, year) => {
    const blob = new Blob([data], {
      type: "text/plain;charset=utf-8",
    });
    FileSaver.saveAs(blob, "import-" + mottakerId + "-" + year + ".txt");
  };

  const downloadFileHandler = (event, report) => {
    event.preventDefault();
    dispatch(getReportedFile(report.id, report.type)).then((response) => {
      if (response.payload.error) {
        showDownloadError();
      } else {
        downloadFile(response.payload.data, report.mottakerId, report.forYear);
      }
    });
  };

  const showReportedFiles = () => {
    return (
      <div className="report-table">
        <table className="table && table-striped">
          <thead>
            <tr>
              <th>Orgnr</th>
              <th>Navn</th>
              <th>Type</th>
              <th>Mottatt</th>
              <th>Av</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {reportedFilesData.data.map((report, key) => {
              return (
                <tr key={key}>
                  <td>{report.mottakerId}</td>
                  <td>{getOrganizationName(report.mottakerId)}</td>
                  <td>{report.type.toUpperCase()}</td>
                  <td>{new Date(report.received).toLocaleDateString()}</td>
                  <td>{report.importedBy}</td>
                  <td>
                    <a
                      className="pointer"
                      onClick={(e) => downloadFileHandler(e, report)}
                    >
                      <MdOutlineFileDownload size="20px" color="black" />
                    </a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  };

  const displayForm = () => {
    return (
      <form onSubmit={formSubmissionHandler}>
        <div className="form-group">
          <label htmlFor="year" className="label-text">
            Hent ut for år
          </label>
          <div className="form-floating mb-3">
            <input
              type="number"
              className="form-control form-control-sm"
              id="year"
              value={year}
              min={0}
              onChange={yearChangeHandler}
              placeholder="År..."
            />
            <label htmlFor="year">År...</label>
          </div>
        </div>
        <div className="fetch-button">
          <PrimaryButton
            title="Hent"
            disabled={!reportFileFormIsValid}
          />
        </div>
      </form>
    );
  };

  return (
    <div>
      {displayForm()}
      {reportedFilesData.loading && <Spinner />}
      {reportedFilesData.data.length !== 0 &&
        reportedFilesData.loaded &&
        showReportedFiles()}
    </div>
  );
};

export default ReportFiles;

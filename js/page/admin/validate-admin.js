import React from "react";
import { Redirect } from "react-router-dom";
import userService from "../../service/user-service";

const ValidateIfAdmin = () => {
  const userRole = userService.getLoggedInUser().roles[0];

  return <div>{userRole !== "admin" ? <Redirect to="/" /> : null}</div>;
};

export default ValidateIfAdmin;

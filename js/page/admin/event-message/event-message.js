import React from "react";
import Container from "../../../components/ui/container/container";
import { MdSend } from "react-icons/md";
import "./event-message.scss";
import ValidateIfAdmin from "../validate-admin";
import EventMessageForm from "./event-message-form";

const EventMessage = () => {
  return (
    <Container
      title="Adminverktøy"
      description="Send melding"
      icon={<MdSend size="68px" />}
    >
      <ValidateIfAdmin />
      <EventMessageForm />
    </Container>
  );
};

export default EventMessage;

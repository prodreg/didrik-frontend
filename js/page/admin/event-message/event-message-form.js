import React from "react";
import { useDispatch } from "react-redux";
import useInput from "../../../hooks/use-input";
import InputField from "../../../components/ui/input-field/input-field";
import PrimaryButton from "../../../components/ui/button/primary-button";
import CheckboxWithLabel from "../../../components/ui/checkbox/checkbox-with-label";
import TextArea from "../../../components/ui/textarea/text-area";
import { postEventMessage } from "../../../redux/thunks/admin-thunks";
import Swal from "sweetalert2";
import "./event-message.scss";
import useCheckbox from "../../../hooks/use-checkbox";

const EventMessageForm = () => {
  const dispatch = useDispatch();
  const inputCheck = (input) => input.trim() !== "";
  const {
    input: description,
    isValid: enteredDescriptionIsValid,
    hasError: descriptionError,
    inputChangeHandler: descriptionChangeHandler,
    inputBlurHandler: descriptionBlurHandler,
    resetForm: resetDescriptionInput,
  } = useInput(inputCheck);
  const {
    input: content,
    isValid: enteredContentIsValid,
    hasError: contentError,
    inputChangeHandler: contentChangeHandler,
    inputBlurHandler: contentBlurHandler,
    resetForm: resetContentInput,
  } = useInput(inputCheck);
  const {
    checked: checkedBank,
    selected: selectedBank,
    checkboxHandler: checkboxBankHandler,
    resetCheckbox: resetBankCheckbox,
  } = useCheckbox();
  const {
    checked: checkedConsignee,
    selected: selectedConsignee,
    checkboxHandler: checkboxConsigneeHandler,
    resetCheckbox: resetConsigneeCheckbox,
  } = useCheckbox();

  const userGroups = [];

  let formIsValid =
    enteredDescriptionIsValid &&
    enteredContentIsValid &&
    (checkedBank || checkedConsignee);

  let checkboxError =
    !checkedBank &&
    !checkedConsignee &&
    (selectedBank === 1 || selectedConsignee === 1);

  if (checkedBank) {
    userGroups.push("bank");
  }
  if (checkedConsignee) {
    userGroups.push("mottaker");
  }

  const checkUserGroup =
    userGroups.length === 2
      ? userGroups[0] + " og " + userGroups[1]
      : userGroups[0];

  const swalConfirmDescription =
    "Ønsker du å sende denne meldingen til følgende grupper: " + checkUserGroup;

  const swalConfirmedDescription = "Meldingen blir sendt til " + checkUserGroup;

  const resetForm = () => {
    resetDescriptionInput();
    resetContentInput();
    resetConsigneeCheckbox();
    resetBankCheckbox();
  };

  const sendEventMessage = () => {
    const eventMessage = {
      description: description,
      userGroups: userGroups,
      content: content,
    };
    Swal.fire({
      title: "Er du sikker?",
      text: swalConfirmDescription,
      confirmButtonText: "Ja",
      showCancelButton: true,
      cancelButtonText: "Avbryt",
      type: "warning",
    }).then((result) => {
      if (result.value) {
        dispatch(postEventMessage(eventMessage)).then((response) => {
          handleIncomingResponse(response.payload.error);
        });
      } else {
        Swal.fire("Ikke sendt!", "Meldigen vil ikke bli sendt", "info");
      }
    });
  };

  const handleIncomingResponse = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke sende melding. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("Sendt!", swalConfirmedDescription, "success");
      resetForm();
    }
  };

  const formSubmissionHandler = (event) => {
    event.preventDefault();
    if (!formIsValid) {
      event.stopPropagation();
    }
    sendEventMessage();
  };
  return (
    <form onSubmit={formSubmissionHandler}>
      <div className="form-group">
        <label htmlFor="description" className="label-text">
          Beskrivelse
        </label>
        <div className="form-floating mb-3">
          <input
            type="text"
            id="description"
            onChange={descriptionChangeHandler}
            onBlur={descriptionBlurHandler}
            value={description}
            placeholder="Kort beskrivelse av meldingen..."
            className="form-control form-control-sm"
          />
          <label htmlFor="headerInput">Kort beskrivelse av meldingen...</label>
          {descriptionError && (
            <p className="error-text">Beskrivelse kan ikke være tom</p>
          )}
        </div>
      </div>
      <div className="checkbox-container">
        <div className="form-group">
          <label htmlFor="user-group" className="label-text">
            Sendes til
          </label>
          <CheckboxWithLabel
            checked={checkedBank}
            onChange={checkboxBankHandler}
            label="Banker"
          />
          <CheckboxWithLabel
            checked={checkedConsignee}
            onChange={checkboxConsigneeHandler}
            label="Varemottakere"
          />
          {checkboxError && (
            <p className="error-text">Du må krysse av på minst én</p>
          )}
        </div>
      </div>
      <div className="content-container">
        <div className="form-group">
          <label htmlFor="content" className="label-text">
            Meldingsinnhold
          </label>
          <div className="form-floating mb-3" style={{ height: "250px" }}>
            <textarea
              className="form-control form-control-sm"
              cols="30"
              rows="100"
              value={content}
              onChange={contentChangeHandler}
              onBlur={contentBlurHandler}
              id="textarea"
              placeholder="Innhold..."
              style={{ height: "250px", resize: "none" }}
            />
            <label htmlFor="textarea">Innhold...</label>
            {contentError && (
              <p className="error-text">Innhold kan ikke være tom</p>
            )}
          </div>
          <small id="contentHelp" className="form-text text-muted">
            Dette vises som innhold til meldingen som blir sendt
          </small>
        </div>
        <div className="event-message-button-container">
          <PrimaryButton
            type="submit"
            disabled={!formIsValid}
            title="Send"
          />
        </div>
      </div>
    </form>
  );
};

export default EventMessageForm;

import React from "react";
import { Modal, Button, Row, Col, Form, FloatingLabel } from "react-bootstrap";
import { addOrganization } from "../../redux/thunks/organization-thunks";
import { connect } from "react-redux";
import Swal from "sweetalert2";
import TertiaryButton from "../../components/ui/button/tertiary-button";
import SecondaryButton from "../../components/ui/button/secondary-button";

class AddOrg extends React.Component {
  state = {
    newOrg: {
      type: "",
      name: "",
      orgNumber: "",
      email: ""
    },
    validType: {
      isValid: false,
      nonEmpty: false
    },
    validName: {
      isValid: false,
      nonEmpty: false,
      unique: false
    },
    validOrgNumber: {
      isValid: false,
      nonEmpty: false,
      unique: false,
      validPattern: false
    },
    validEmail: {
      isValid: false,
      nonEmpty: false,
      validPattern: false
    },
    validated: false,
    validNewOrg: false,
    saving: false
  };

  constructor(props) {
    super(props);
  }

  closeDialog = () => {
    this.setState({
      newOrg: {
        type: "",
        name: "",
        orgNumber: "",
        email: ""
      },
      validType: {
        isValid: false,
        nonEmpty: false
      },
      validName: {
        isValid: false,
        nonEmpty: false,
        unique: false
      },
      validOrgNumber: {
        isValid: false,
        nonEmpty: false,
        unique: false,
        validPattern: false
      },
      validEmail: {
        isValid: false,
        nonEmpty: false,
        validPattern: false
      },
      validated: false,
      validNewOrg: false,
      saving: false
    });

    this.props.toggleShowAddOrg();
  }

  validateForm = () => {
    const validType = this.state.validType.isValid && this.state.validType.nonEmpty;
    const validName = this.state.validName.isValid && this.state.validName.nonEmpty && this.state.validName.unique;
    const validOrgNumber = this.state.validOrgNumber.isValid && this.state.validOrgNumber.nonEmpty 
      && this.state.validOrgNumber.unique && this.state.validOrgNumber.validPattern;
    const validEmail = this.state.validEmail.isValid && this.state.validEmail.validPattern;
    const isValid = validType && validName && validOrgNumber && validEmail;

    this.setState({
      validNewOrg: isValid
    });
  }
  
  validateType = (event) => {
    const type = event.target.value;
    var isValid = true;
    var nonEmpty = true;

    if(!type) {
      isValid = false;
      nonEmpty = false;
    }

    var newOrg = this.state.newOrg;
    newOrg.type = type;
    this.setState({
      newOrg: newOrg,
      validType: {
        isValid: isValid,
        nonEmpty: nonEmpty
      }
    }, () => {
      const event = { target: { value: this.state.newOrg.name}};
      this.validateName(event);
    });
  }

  validateName = (event) => {
    const name = event.target.value;
    var isValid = true;
    var nonEmpty = true;
    var unique = true;

    if (!name) {
      isValid = false;
      nonEmpty = false;
    }

    for(var i=0; i<this.props.organizations.length; i++) {
      var org = this.props.organizations[i];
      if(org.name === name && org.organizationType === this.state.newOrg.type) {
        isValid = false;
        unique = false;
        break;
      }
    }
    
    var newOrg = this.state.newOrg;
    newOrg.name = name;
    this.setState({
      newOrg: newOrg,
      validName: {
        isValid: isValid,
        nonEmpty: nonEmpty,
        unique: unique
      }
    }, () => {
      this.validateForm()
    });
  }

  validateOrgNumber = (event) => {
    const orgNumber = event.target.value;
    var isValid = true;
    var nonEmpty = true;
    var unique = true;
    var validPattern = true;

    if (!orgNumber) {
      isValid = false;
      nonEmpty = false;
    }

    const exisitingOrgNumbers = this.props.organizations.map(org => {
      return org.orgNumber;
    })
    if(exisitingOrgNumbers.includes(orgNumber)) {
      isValid = false;
      unique = false;
    }

    const orgNumberArray = Array.from(orgNumber);
    if(orgNumberArray.length !== 9) {
      isValid = false;
      validPattern = false;
    } else {
      const numberArray = orgNumberArray.map(num => {
        return parseInt(num);
      })
      const weights = [3, 2, 7, 6, 5, 4, 3, 2];
      var weightedSum = 0;
      for(let i=0; i < numberArray.length-1; i++) {
        weightedSum += numberArray[i]*weights[i];
      }
      const modulo = weightedSum % 11;
      const controlNumber = 11 - modulo;
      if(controlNumber !== numberArray[8]) {
        isValid = false;
        validPattern = false;
      }
    }

    var newOrg = this.state.newOrg;
    newOrg.orgNumber = orgNumber;
    this.setState({
      newOrg: newOrg,
      validOrgNumber: {
        isValid: isValid,
        nonEmpty: nonEmpty,
        unique: unique,
        validPattern: validPattern
      }
    }, () => {
      this.validateForm()
    });
  }

  validateEmail = (event) => {
    const email = event.target.value
    var isValid = true;
    var validPattern = true;

    let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(email)) {
      isValid = false;
      validPattern = false;
    }

    var newOrg = this.state.newOrg;
    newOrg.email = email;
    this.setState({
      neworg: newOrg,
      validEmail: {
        isValid: isValid,
        validPattern: validPattern
      }
    }, () => {
      this.validateForm()
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    const validForm = this.state.validNewOrg;
    if (form.checkValidity() === false || !validForm) {
      event.stopPropagation();
    } else {
      this.setState({
        saving: true
      });

      const orgToAdd = {
        name: this.state.newOrg.name,
        orgNumber: this.state.newOrg.orgNumber,
        organizationType: this.state.newOrg.type,
        email: this.state.newOrg.email,
        deactivated: false
      };

      this.props.addOrganization(orgToAdd).then((res) => {
        this.handleError(res.payload.error);
      })
      .finally(() => {
        this.setState({
          saving: false
        });
      });
    }

    this.setState({
      validated: true
    })

  }

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Organisasjonen kunne ikke bli opprettet. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("OK", "Organisasjon er opprettet", "success").then(() => {
        this.props.history.go(0);
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.showAddOrg} onHide={this.closeDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Legg til ny organisasjon</Modal.Title>
        </Modal.Header>
        <Form noValidate onSubmit={ this.handleSubmit }>
            <Modal.Body>
              <Row className="mb-3">
                <Form.Group>
                  <FloatingLabel label="Organisasjonstype">
                    <Form.Select value={this.state.newOrg.type} isInvalid={!this.state.validType.isValid && this.state.validated} onChange={this.validateType}>
                      <option key={""} value={""}></option>
                      {Object.keys(this.props.orgTypes).map((key, i) => {
                        return (<option key={i} value={key}>{this.props.orgTypes[key]}</option>)
                      })}
                    </Form.Select>
                  </FloatingLabel>
                  {this.state.validated && (
                    <Row>
                    {!this.state.validType.nonEmpty && (
                      <Form.Text className="text-danger text-small">
                        Organisasjonstype er påkrevd
                      </Form.Text>
                    )}
                    </Row>
                  )}
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group as={Col}>
                  <FloatingLabel label="Organisasjonsnavn">
                    <Form.Control required type="text" placeholder="Organisasjonsnavn" onChange={ this.validateName }
                      isInvalid={!this.state.validName.isValid && this.state.validated} />
                  </FloatingLabel>
                  {this.state.validated && (
                    <>
                      <Row>
                        {!this.state.validName.nonEmpty && (
                          <Form.Text className="text-danger text-small">
                              Organisasjonsnavn er påkrevd
                          </Form.Text>
                        )}
                        </Row>
                        <Row>
                        {!this.state.validName.unique && (
                          <Form.Text className="text-danger text-small">
                              En organisasjon med samme organisasjonstype og navn eksisterer
                          </Form.Text>
                        )}
                      </Row>
                    </>
                  )}
                </Form.Group>
                <Form.Group as={Col}>
                  <FloatingLabel label="Org. nummer">
                    <Form.Control required type="text" placeholder="Org. nummer" onChange={ this.validateOrgNumber }
                      isInvalid={!this.state.validOrgNumber.isValid && this.state.validated} />
                  </FloatingLabel>
                  {this.state.validated && (
                    <>
                      <Row>
                        {!this.state.validOrgNumber.nonEmpty && (
                          <Form.Text className="text-danger text-small">
                              Organisasjonsnummer er påkrevd
                          </Form.Text>
                        )}
                        </Row>
                        <Row>
                          {!this.state.validOrgNumber.validPattern && (
                            <Form.Text className="text-danger text-small">
                                Du må skrive inn et gyldig organisasjonsnummer
                            </Form.Text>
                          )}
                        </Row>
                        <Row>
                        {!this.state.validOrgNumber.unique && (
                          <Form.Text className="text-danger text-small">
                              En organisasjon med organisasjonsnummeret eksisterer
                          </Form.Text>
                        )}
                      </Row>
                    </>
                  )}
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group>
                  <FloatingLabel label="Epost">
                    <Form.Control required type="email" onChange={ this.validateEmail } placeholder="Epost" 
                      isInvalid={!this.state.validEmail.isValid && this.state.validated} />
                      {this.state.validated && (
                        <Row>
                          {!this.state.validEmail.validPattern && (
                            <Form.Text className="text-danger text-small">
                                Du må skrive inn en gyldig epost-adresse
                            </Form.Text>
                          )}
                        </Row>
                      )}
                  </FloatingLabel>
                </Form.Group>
              </Row>
            </Modal.Body>
            <Modal.Footer>
            <TertiaryButton
              type="reset"
              title="Avbryt"
              onClick={this.closeDialog}
              disabled={ this.state.saving }
            />
            <SecondaryButton
              type="submit"
              title="Lagre"
              disabled={ (!this.state.validNewOrg && this.state.validated) || this.state.saving }
            />
            </Modal.Footer>
        </Form>
      </Modal>
    );
  }
}

AddOrg.defaultProps = {
  showAddOrg: false,
  organizations: [],
  orgTypes: {}
};

const mapStateToProps = (state) => {
  return { };
}

const mapDispatchToProps = {
  addOrganization
}

export default connect(mapStateToProps, mapDispatchToProps)(AddOrg);
import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import Spinner from "../../components/spinner";
import Container from "../../components/ui/container/container";
import {
  getStoreDataForAllOrganizations,
  getStoreDataFormdataOrganizationTypes,
  getStoreDataForQueriedOrganizations,
} from "../../redux/selectors";
import {
  getAllOrganizations,
  queryOrganizations,
} from "../../redux/thunks/organization-thunks";
import "./organization-search.scss";
import { getOrganizationTypes } from "../../redux/thunks/formdata-thunks";
import { MdOutlineWorkOutline } from "react-icons/md";
import SearchForm from "../../components/ui/search-form/search-form";
import CheckboxWithLabel from "../../components/ui/checkbox/checkbox-with-label";
import { Table, Card, Row, Col, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import { MdChevronLeft, MdChevronRight} from "react-icons/md";
import ReactPaginate from "react-paginate";
import PrimaryButton from "../../components/ui/button/primary-button";
import AddOrg from "./add-org";

class OrganizationSearch extends React.Component {
  state = {
    orgTypes: {},
    filterSelection: {},
    searchText: "",
    organizations: [],
    loading: false,
    currentPage: 1,
    showAddOrg: false,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getOrganizationTypes().then((res) => {
      var orgTypes = {};
      var filterSelection = {};
      for (const type of res) {
        orgTypes[type.value] = type.description;
        filterSelection[type.value] = true;
      }
      this.setState({
        orgTypes: orgTypes,
        filterSelection: filterSelection,
        loading: true,
      });
      this.props.getAllOrganizations(Object.keys(orgTypes)).then((res) => {
        this.setState({
          organizations: res.payload.data,
          loading: false,
        });
      });
    });
  }

  fetchOrganizations = () => {
    this.setState({
      loading: true,
    });
    if (this.state.searchText === "") {
      this.props
        .getAllOrganizations(Object.keys(this.state.orgTypes))
        .then((res) => {
          this.setState({
            organizations: res.payload.data,
            loading: false,
          });
        });
    } else {
      this.props
        .queryOrganizations(
          this.state.searchText,
          Object.keys(this.state.orgTypes)
        )
        .then((res) => {
          if (res == null) {
            res = [];
          }
          var currentPage = this.state.currentPage;
          if (res.length > 0) {
            currentPage = 1;
          }

          this.setState({
            organizations: res,
            loading: false,
            currentPage: currentPage,
          });
        });
    }
  };

  onChange = (event) => {
    this.setState({
      searchText: event.target.value,
    }, this.fetchOrganizations);
  };

  handleFilterChange = (type, event) => {
    var filterSelection = this.state.filterSelection;
    filterSelection[type] = event.target.checked;
    this.setState({
      filterSelection: filterSelection,
    });
  };

  handlePageChange = (pageNumber) => {
    this.setState({
      currentPage: pageNumber.selected + 1,
    });
  };

  toggleShowAddOrg = () => {
    const showAddOrg = this.state.showAddOrg;
    this.setState({
      showAddOrg: !showAddOrg,
    });
  };

  fetchOrganizationDetail = (event, orgNumber) => {
    event.preventDefault();
    this.props.history.push(`/organizations/details/${orgNumber}`);
  };

  render() {
    const organizations = this.props.allOrganizations;
    const filteredOrgs = this.state.organizations.filter(
      (org) => this.state.filterSelection[org.organizationType] === true
    );
    const totalResults = filteredOrgs.length;
    const orgsPerPage = 20;

    return (
      <Container
        title={"Organisasjonssøk"}
        description={"Her kan du søke i organisasjoner"}
        icon={<MdOutlineWorkOutline size="60px" />}
      >
        {organizations.loading && (
          <>
            <br />
            <Spinner />
            Henter data
            <br />
          </>
        )}
        {organizations.loaded && organizations.error && (
          <div>Kunne ikke hente organisasjoner. Prøv senere.</div>
        )}
        {organizations.loaded && !organizations.error && (
          <>
            <SearchForm
              type={"text"}
              id="organization-search"
              value={this.state.searchText}
              placeholder={"Søk etter organisasjon..."}
              className=""
              onChange={this.onChange}
            />
            <ul style={{ listStyle: "none", paddingLeft: "0" }}>
              {Object.keys(this.state.orgTypes).map((type) => {
                return (
                  <li key={type}>
                    <CheckboxWithLabel
                      checked={this.state.filterSelection[type]}
                      onChange={this.handleFilterChange.bind(this, type)}
                      label={this.state.orgTypes[type]}
                    />
                  </li>
                );
              })}
            </ul>
            <hr />
            <PrimaryButton
              title={"Legg til"}
              onClick={this.toggleShowAddOrg}
            />
            {this.state.loading && (
              <>
                <br />
                <Spinner />
                Henter data
                <br />
              </>
            )}
            {!this.state.loading && (
              <>
                <Table hover striped>
                  <thead>
                    <tr>
                      <th>Organisasjon</th>
                      <th>Org. nr.</th>
                      <th>Type</th>
                    </tr>
                  </thead>
                  <tbody>
                    {filteredOrgs
                      .slice(
                        (this.state.currentPage - 1) * orgsPerPage,
                        (this.state.currentPage - 1) * orgsPerPage + orgsPerPage
                      )
                      .map((org) => {
                        return (
                          <tr
                            key={org.orgNumber}
                            onClick={(e) =>
                              this.fetchOrganizationDetail(e, org.orgNumber)
                            }
                            className="organization-row"
                          >
                            <td>{org.name}</td>
                            <td>{org.orgNumber}</td>
                            <td>{this.state.orgTypes[org.organizationType]}</td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
                <ReactPaginate
                  previousLabel={<MdChevronLeft />}
                  nextLabel={<MdChevronRight />}
                  breakLabel={"..."}
                  pageCount={totalResults / orgsPerPage}
                  marginPagesDisplayed={3}
                  pageRangeDisplayed={3}
                  onPageChange={this.handlePageChange}
                  containerClassName={"pagination float-start"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </>
            )}
          </>
        )}
        {organizations.loaded && !organizations.error && (
          <AddOrg
            showAddOrg={this.state.showAddOrg}
            toggleShowAddOrg={this.toggleShowAddOrg}
            history={this.props.history}
            organizations={organizations.data.orgs}
            orgTypes={this.state.orgTypes}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const allOrganizations = getStoreDataForAllOrganizations(state);
  const queriedOrganizations = getStoreDataForQueriedOrganizations(state);
  const organizationTypes = getStoreDataFormdataOrganizationTypes(state);

  return { allOrganizations, queriedOrganizations, organizationTypes };
};

const mapDispatchToProps = {
  getAllOrganizations,
  queryOrganizations,
  getOrganizationTypes,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(OrganizationSearch));

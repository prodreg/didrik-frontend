import {
  getStoreDataForUser,
} from "../../redux/selectors";
import {connect} from "react-redux";

import React from "react";
import Spinner from "../../components/spinner";
import Swal from "sweetalert2";
import Moment from "react-moment";
import {formatAccountNumber, formatCurrency, isNumberNotNegative} from "../../misc/helpers";
import {Link} from "react-router-dom";
import {
  getFeedbackById,
  sendFeedback
} from "../../redux/thunks/case-feedback-thunks";
import PrimaryButton from "../../components/ui/button/primary-button";

const radioKeys = {
  "BASIS_CORRECT": "BASIS_CORRECT",
  "BASIS_INCORRECT": "BASIS_INCORRECT",
  "WITHDRAWAL_MAX": "WITHDRAWAL_MAX",
  "WITHDRAWAL_CUSTOM": "WITHDRAWAL_CUSTOM",
}

const validationMap = {
  goodsDeliverySettlement: (settlement) => !isNumberNotNegative(settlement),
  maxWithdrawal: (maxWithdrawal) => !maxWithdrawal,
}

const Input = ({
  formClassName,
  name,
  value,
  onChange,
  disabled,
  placeholder,
  id,
  label,
  validationError,
  validationErrorText
}) => (
    <div className={formClassName}>
      <input type="number"
             id={id}
             placeholder={placeholder}
             className={"form-control form-control-sm" + (validationError ? 'is-invalid' : '')}
             onChange={onChange}
             name={name}
             disabled={disabled}
             value={value}/>
      {label && (
          <label
              htmlFor={id}>{label}</label>
      )}
      {validationError && (
          <div className="row"><small
              className="text-danger text-small form-text">{validationErrorText}</small>
          </div>
      )}
    </div>
);

const VareleveranseElement = ({
  that,
  vareleveranseList,
}) => (
    <div className="card mt-2">
      <div className="card-body">
        <div className="row">
          <div className="col-md-6 mb-3">
            <label className="form-label">Varemottaker</label>
            <input readOnly type="text"
                   className="form-control form-control-sm"
                   value={vareleveranseList[0].varemottaker.name}/>
          </div>
          <div className="col-md-6 mb-3">
            <label className="form-label">Ordning</label>
            <input readOnly type="text"
                   className="form-control form-control-sm"
                   value={vareleveranseList[0].ordning}/>
          </div>
        </div>
        {vareleveranseList.map((element, index) => (
            <div key={index}>
              <hr />
              <div className="row">
                <div className="col-md-4 mb-3">
                  <label className="form-label">Produktslag</label>
                  <input readOnly type="text"
                         className="form-control form-control-sm"
                         value={element.produktslagDescription}/>
                </div>
                {(that.state.goodsDeliverySettlementRadio[element.id]
                    === radioKeys.BASIS_INCORRECT
                    &&
                    that.orgNumberIsMatchingFeedbackOrgNumber(
                        element.varemottaker.orgNumber))
                && (
                    <div className="col-md-4 mb-3">
                      <div className="row">
                        <div className="col-md-6">
                          <label className="form-label">Årlig
                            beløp
                            overført til bank</label>
                          <input readOnly type="text"
                                 className="form-control form-control-sm"
                                 style={{ textDecoration: 'line-through' }}
                                 value={element.oppgjoer}/>
                        </div>
                        <div className="col-md-6">
                          <label className="form-label">Årlig
                            beløp
                            overført til bank</label>

                          <Input
                              disabled={that.disableAllInput()}
                              value={that.state.goodsDeliverySettlement[element.id]
                              || ''}
                              onChange={(event) => that.setSettlement(
                                  event, element.id)}
                              validationError={validationMap.goodsDeliverySettlement(that.state.goodsDeliverySettlement[element.id]
                                  || '')
                              && that.state.isFormSubmitted}
                              validationErrorText={"Beløpet kan ikke være tomt eller negativt"}
                          />
                        </div>
                      </div>
                    </div>
                )}
                {that.state.goodsDeliverySettlementRadio[element.id]
                === radioKeys.BASIS_CORRECT
                && (
                    <div className="col-md-4 mb-3">
                      <label className="form-label">Årlig
                        beløp
                        overført til bank</label>
                      <input readOnly type="text"
                             className="form-control form-control-sm"
                             value={element.oppgjoer}/>
                    </div>
                )}
                <div className="col-md-4 mb-3">
                  <label className="form-label">År</label>
                  <input readOnly type="text"
                         className="form-control form-control-sm"
                         value={element.aar}/>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 mb-3">
                  <div className="form-floating">
                                  <textarea readOnly
                                            className="form-control form-control-sm"
                                            id="floatingTextarea2"
                                            style={{ height: '100px' }}
                                            name="text"
                                            value={element.text}></textarea>
                    <label htmlFor="floatingTextarea2">Ytterligere
                      informasjon</label>
                  </div>
                </div>
              </div>
              {that.orgNumberIsMatchingFeedbackOrgNumber(
                  element.varemottaker.orgNumber) && (
                  <div className="row">
                    <div className="col-12">
                      <div className="form-check">
                        <input
                            className="form-check-input"
                            type="radio"
                            disabled={that.disableAllInput()}
                            name={"settlementRadio-"
                            + element.id}
                            checked={that.state.goodsDeliverySettlementRadio[element.id]
                            === radioKeys.BASIS_CORRECT}
                            onChange={() => that.settlementRadio(
                                element.id,
                                radioKeys.BASIS_CORRECT)}/>
                        <label
                            className="form-check-label">
                          Oppgitt grunnlag samsvarer
                          med
                          vår informasjon om årlig
                          beløp
                          overført bank etter evt.
                          trekk
                        </label>
                      </div>
                      <div className="form-check">
                        <input className="form-check-input"
                               type="radio"
                               disabled={that.disableAllInput()}
                               name={"settlementRadio-"
                               + element.id}
                               checked={that.state.goodsDeliverySettlementRadio[element.id]
                               === radioKeys.BASIS_INCORRECT}
                               onChange={() => that.settlementRadio(
                                   element.id,
                                   radioKeys.BASIS_INCORRECT)}/>
                        <label className="form-check-label">
                          Oppgitt grunnlag stemmer
                          ikke
                          overens med vår informasjon
                        </label>
                      </div>
                    </div>
                  </div>
              )}
            </div>
        ))}
      </div>
    </div>
)

class CaseFeedback extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataLoaded: false,
      feedbackDto: {},
      feedbackContent: {},
      feedbackLegal: {},

      goodsDeliverySettlementRadio: {},
      withdrawalRadio: '',

      goodsDeliverySettlement: {},
      maxWithdrawal: '',
      notes: '',

      isFormSubmitted: false
    }

    this.settlementRadio = this.settlementRadio.bind(this);
    this.setSettlement = this.setSettlement.bind(this);
    this.withdrawalRadio = this.withdrawalRadio.bind(this);
    this.setMaxWithdrawal = this.setMaxWithdrawal.bind(this);
    this.shouldSamvirkeordningenGuaranteeTextBeDisplayed = this.shouldSamvirkeordningenGuaranteeTextBeDisplayed.bind(
        this);
    this.shouldPrivatordningenGuaranteeTextBeDisplayed = this.shouldPrivatordningenGuaranteeTextBeDisplayed.bind(
        this);
    this.disableAllInput = this.disableAllInput.bind(this);
    this.isFormInvalid = this.isFormInvalid.bind(this);
    this.sendFeedback = this.sendFeedback.bind(this);
    this.loadCaseFeedback = this.loadCaseFeedback.bind(this);
    this.orgNumberIsMatchingFeedbackOrgNumber = this.orgNumberIsMatchingFeedbackOrgNumber.bind(
        this);
  }

  componentDidMount() {
    this.loadCaseFeedback();
  }

  loadCaseFeedback() {
    this.props.getFeedbackById(this.props.match.params.feedbackId)
    .then((data) => {
      const feedbackContent = JSON.parse(data.tilbakemeldingContent);
      const feedbackLegal = JSON.parse(feedbackContent.legalText);

      this.setState({
        dataLoaded: true,
        feedbackDto: data,
        feedbackContent: feedbackContent,
        feedbackLegal: feedbackLegal,
        goodsDeliverySettlementRadio: feedbackContent.sak.vareleveranseList.reduce(
            (obj, element) => {
              obj[element.id] = element.korrBeloep ? radioKeys.BASIS_INCORRECT
                  : radioKeys.BASIS_CORRECT
              return obj;
            }, {}),
        withdrawalRadio: feedbackContent.korrTrekk == null
            ? radioKeys.WITHDRAWAL_CUSTOM
            : radioKeys.WITHDRAWAL_MAX,
        goodsDeliverySettlement: feedbackContent.sak.vareleveranseList.reduce(
            (obj, element) => {
              obj[element.id] = element.korrBeloep
              return obj;
            }, {}),
        maxWithdrawal: feedbackContent.korrTrekk == null ? ''
            : feedbackContent.korrTrekk,
        notes: feedbackContent.merknader || ''
      })
    })
  }

  settlementRadio(id, value) {
    this.setState({
      goodsDeliverySettlementRadio: {
        ...this.state.goodsDeliverySettlementRadio,
        [id]: value
      },
      goodsDeliverySettlement: {
        ...this.state.goodsDeliverySettlement,
        [id]: null
      }
    })
  }

  setSettlement(event, id) {
    event.preventDefault();
    const target = event.target;
    const value = target.value;

    this.setState({
      goodsDeliverySettlement: {
        ...this.state.goodsDeliverySettlement,
        [id]: value
      }
    })
  }

  withdrawalRadio(value) {
    this.setState({
      withdrawalRadio: value,
      maxWithdrawal: ''
    })
  }

  setMaxWithdrawal(value) {
    this.setState({
      maxWithdrawal: value
    })
  }

  orgNumberIsMatchingFeedbackOrgNumber(orgNumber) {
    return orgNumber === this.state.feedbackContent.mottaker.orgNumber
  }

  feedbackOrgNumberIsMatchingLoggedInUser() {
    return this.props.user.data.orgNumber
        === this.state.feedbackContent.mottaker.orgNumber
  }

  disableAllInput() {
    return !this.feedbackOrgNumberIsMatchingLoggedInUser()
        || this.state.feedbackDto.status !== 'DRAFT'
  }

  shouldSamvirkeordningenGuaranteeTextBeDisplayed() {
    for (let i = 0; i < this.state.feedbackContent.sak.vareleveranseList.length;
        i++) {
      if (this.state.feedbackContent.sak.vareleveranseList[i].ordning
          === "SAMVIRKEORDNINGEN" &&
          (this.state.feedbackContent.sak.vareleveranseList[i].varemottaker.orgNumber
              === this.state.feedbackContent.mottaker.orgNumber)) {
        return true;
      }
    }
    return false;
  }

  shouldPrivatordningenGuaranteeTextBeDisplayed() {
    for (let i = 0; i < this.state.feedbackContent.sak.vareleveranseList.length;
        i++) {
      if (this.state.feedbackContent.sak.vareleveranseList[i].ordning
          === "PRIVATORDNINGEN" &&
          (this.state.feedbackContent.sak.vareleveranseList[i].varemottaker.orgNumber
              === this.state.feedbackContent.mottaker.orgNumber)) {
        return true;
      }
    }
    return false;
  }

  isFormInvalid() {
    for (const [key, value] of Object.entries(this.state.goodsDeliverySettlementRadio)) {
      if(value === radioKeys.BASIS_INCORRECT) {
        if(validationMap.goodsDeliverySettlement(this.state.goodsDeliverySettlement[key])) {
          return true;
        }
      }
    }

    if(this.state.withdrawalRadio === radioKeys.WITHDRAWAL_MAX) {
      if(validationMap.maxWithdrawal(this.state.maxWithdrawal)) {
        return true;
      }
    }

    return false;
  }

  sendFeedback(event) {
    event.preventDefault();

    if (this.isFormInvalid()) {
      this.setState({
        isFormSubmitted: true
      })
      Swal.fire({
        type: 'error',
        title: 'Valideringsfeil',
        text: 'Vennligst gjennomgå skjema og rett felter markert med rødt.',
      })
    } else {
      Swal.fire({
        title: 'Send tilbakemeldingen?',
        html: '<p style="display: block;">Ønsker du å sende tilbakemelding til bank? Det er ikke mulig å redigere feltene etter de er sendt inn.</p>',
        showCancelButton: true,
        confirmButtonText: 'Ja',
        cancelButtonText: 'Avbryt'
      }).then((result) => {
        if (result.value) {
          this.props.sendFeedback({
            beskrivOrdning: null,
            korrTrekk: this.state.withdrawalRadio === radioKeys.WITHDRAWAL_MAX
                ? this.state.maxWithdrawal : null,
            merknader: this.state.notes,
            tilbakemeldingId: this.state.feedbackDto.id,
            vareleveranseList: this.state.feedbackContent.sak.vareleveranseList.map(element => {
              element.korrBeloep = this.state.goodsDeliverySettlement[element.id]
              return element;
            })
          }).then(() => {
            Swal.fire({
              type: 'success',
              text: "Tilbakemelding sendt",
            })
            this.props.history.push("/case-notice");
          })
        }
      })
    }
  }

  groupVareleveranseOnOrganizationNumber(vareleveranseList) {
    return vareleveranseList.reduce((payload, vareleveranse) => {
      const index = payload.findIndex(element => {
        return element.some(some => some.varemottaker.orgNumber
            === vareleveranse.varemottaker.orgNumber);
      });
      if (index === -1) {
        payload.push([vareleveranse]);
      } else {
        payload[index].push(vareleveranse)
      }
      return payload;
    }, [])
  }

  render() {
    return (
        <div>
          <div className="row">
            <div className="col-lg">
              <div className="card">
                <div className="card-header" style={{ fontWeight: 'bold' }}>
                  Tilbakemelding
                </div>
                <div className="card-body">
                  {!this.state.dataLoaded && (
                      <Spinner/>
                  )}
                  {this.state.dataLoaded && (
                      <div>
                        <div className="row">
                          <div className="col-lg">
                            <div className="row">
                              <div className="col-md-6">
                                <h5>Produsent</h5>
                                <hr/>
                                <table className="table table-sm">
                                  <tbody>
                                  <tr>
                                    <th scope="row">Navn</th>
                                    <td>{this.state.feedbackContent.sak.produsent.navn}</td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Produsentnummer</th>
                                    <td>{this.state.feedbackContent.sak.produsent.prodnr}</td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Adresse</th>
                                    <td>{this.state.feedbackContent.sak.produsent.adresse.adresse1
                                    + ', ' +
                                    this.state.feedbackContent.sak.produsent.adresse.postnr
                                    + ', ' +
                                    this.state.feedbackContent.sak.produsent.adresse.poststed}</td>
                                  </tr>
                                  </tbody>
                                </table>
                              </div>
                              <div className="col-md-6">
                                <h5>Detaljer</h5>
                                <hr/>
                                <table className="table table-sm">
                                  <tbody>
                                  <tr>
                                    <th scope="row">Sendt</th>
                                    <td>
                                      {this.state.feedbackDto.sent && (
                                          <Moment unix locale="no"
                                                  format="D. MMM YYYY [kl.] HH:mm">
                                            {this.state.feedbackDto.sent}
                                          </Moment>
                                      )}
                                      {!this.state.feedbackDto.sent && (
                                          <span>Ikke sendt</span>
                                      )}
                                    </td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Bank</th>
                                    <td>
                                      {this.state.feedbackContent.bank.name}
                                    </td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Mottaker</th>
                                    <td>
                                      {formatCurrency(
                                          this.state.feedbackContent.mottaker.name)}
                                    </td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Driftskredittkonto</th>
                                    <td>
                                      <strong>{formatAccountNumber(
                                          this.state.feedbackContent.sak.kontoNummer)}</strong>
                                    </td>
                                  </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div>
                              <p className="m-0">Banken har innvilget
                                driftskreditt:</p>
                              <p><strong>{formatCurrency(
                                  this.state.feedbackContent.sak.bevilget)}</strong>
                              </p>
                              <p>{this.state.feedbackLegal.pant}</p>
                            </div>
                            <h4>Grunnlag for driftskreditt</h4>
                            <hr/>

                            {this.groupVareleveranseOnOrganizationNumber(
                                this.state.feedbackContent.sak.vareleveranseList).map(
                                (element, index) => (
                                    <VareleveranseElement key={index}
                                                          that={this}
                                                          vareleveranseList={element}
                                    />
                                ))}
                          </div>
                        </div>
                        <div className="row">
                          <div className="col mt-3">
                            <h5>{this.state.feedbackLegal.trekkheader}</h5>
                            <p>{this.state.feedbackLegal.forbeholditrekktext}</p>
                            <div className="form-check">
                              <input
                                  className="form-check-input"
                                  type="radio"
                                  disabled={this.disableAllInput()}
                                  name="withdrawalRadio"
                                  checked={this.state.withdrawalRadio
                                  === radioKeys.WITHDRAWAL_MAX}
                                  onChange={() => this.withdrawalRadio(
                                      radioKeys.WITHDRAWAL_MAX)}/>
                              <label
                                  className="form-check-label"
                                  style={{ width: '50%' }}>
                                Maksimalt trekk pr år:
                                  <Input
                                      formClassName="form-floating"
                                      id="withdrawalRadioInput"
                                      disabled={this.state.withdrawalRadio !== radioKeys.WITHDRAWAL_MAX || this.disableAllInput()}
                                      placeholder={"Samlet trekk kan ikke overstige oppgitt beløp."}
                                      value={this.state.maxWithdrawal}
                                      onChange={(event) => this.setMaxWithdrawal(
                                          event.target.value)}
                                      validationError={validationMap.maxWithdrawal(this.state.maxWithdrawal
                                          || '') && this.state.isFormSubmitted && this.state.withdrawalRadio === radioKeys.WITHDRAWAL_MAX}
                                      validationErrorText={"Beløpet kan ikke være tomt"}
                                      label="Samlet trekk kan ikke overstige oppgitt beløp."
                                  />
                              </label>
                            </div>
                            <div className="form-check">
                              <input
                                  className="form-check-input"
                                  type="radio"
                                  name="withdrawalRadio"
                                  disabled={this.disableAllInput()}
                                  checked={this.state.withdrawalRadio
                                  === radioKeys.WITHDRAWAL_CUSTOM}
                                  onChange={() => this.withdrawalRadio(
                                      radioKeys.WITHDRAWAL_CUSTOM)}/>
                              <label
                                  className="form-check-label">
                                Trekk for livdyr o.l. Beskriv ordningen i
                                merknadsfeltet.
                              </label>
                            </div>
                            <h5 className="mt-3">Garantier</h5>
                            {this.shouldPrivatordningenGuaranteeTextBeDisplayed()
                            && (
                                <p>{this.state.feedbackLegal.garantiprivatordningen}</p>
                            )}
                            {this.shouldSamvirkeordningenGuaranteeTextBeDisplayed()
                            && (
                                <p>{this.state.feedbackLegal.garantisamvirkeordningen}</p>
                            )}
                            <h5>Særskilte merknader:</h5>
                            <textarea
                                value={this.state.notes}
                                disabled={this.disableAllInput()}
                                style={{ width: '100%' }}
                                rows={5}
                                onChange={(event) => this.setState({
                                  notes: event.target.value
                                })}/>
                          </div>
                        </div>

                        {this.props.user.data.isBank && (
                            <div className="row">
                              <div className="col-md-6 mt-3">
                                <Link to={'/case/'
                                + this.state.feedbackContent.sak.id}>
                                  <PrimaryButton
                                    title="Gå til sak"
                                  />
                                </Link>
                              </div>
                            </div>
                        )}
                        {this.props.user.data.isVaremottaker && (
                            <div className="row">
                              <div className="col-md-6 mt-3">
                                <Link to={'/case-notice/'
                                + this.state.feedbackDto.varselId}>
                                  <PrimaryButton
                                    title="Gå til varsel"
                                  />
                                </Link>
                              </div>
                              <div className="col-md-6 mt-3">
                                <PrimaryButton
                                  title="Send tilbakemelding"
                                  disabled={this.disableAllInput()}
                                  className="float-md-end"
                                  onClick={this.sendFeedback}
                                />
                              </div>
                            </div>
                        )}
                      </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  const user = getStoreDataForUser(state);
  return { user };
};

const mapDispatchToProps = {
  getFeedbackById,
  sendFeedback,
}

export default connect(mapStateToProps,
    mapDispatchToProps)(CaseFeedback)
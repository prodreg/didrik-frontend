import Spinner from "../components/spinner";
import { connect } from "react-redux";
import {
  getStoreDataForUserInfo,
  getStoreDataForVersionInfo,
  getStoreDataForNewsPosts,
} from "../redux/selectors";
import { getAllNewsPosts } from "../redux/thunks/news-thunks";
import { getUserInfo } from "../redux/thunks/user-info-thunks";
import { getVersionInfo } from "../redux/thunks/version-info-thunks";
import React, { version } from "react";
import userService from "../service/user-service";
import { Modal, Button, Table, Card } from "react-bootstrap";
import Moment from "react-moment";
import PrimaryButton from "../components/ui/button/primary-button";

class Home extends React.Component {
  state = {
    showVersion: false,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const username = userService.getLoggedInUser().username;
    this.props.getUserInfo(username);
    this.props.getVersionInfo();
    this.props.getAllNewsPosts();
  }

  toggleVersionInfo = () => {
    this.setState({
      showVersion: !this.state.showVersion,
    });
  };

  render() {
    const userInfo = this.props.userInfo;
    const versionInfo = this.props.versionInfo;
    const newsPosts = this.props.newsPosts;

    return (
      <div>
        <div className="row">
          <div className="col-lg">
            <div className="alert alert-primary" role={"alert"} style={{ fontWeight: "bold" }}>
             Dette er det nye grensesnittet for driftskreditt. Det gamle grensesnittet kan finnes <a href="https://kreditt.prodreg.no/old" className="alert-link">her</a>.
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg">
            <div className="card">
              <div className="card-header" style={{ fontWeight: "bold" }}>
                Velkommen til Landbrukets Dataflyt sin skjematjeneste for
                driftskredittordningen
              </div>
              <div className="card-body">
                <p>
                  Dette er en internettbasert tjeneste som tilbys av Landbrukets
                  Dataflyt, i forbindelse med driftskredittordningen for
                  landbruket.
                  <br />
                  Tjenesten gir banker oversikt over registrerte produsenter og
                  varemottakere, og gjør det enklere å sende meldinger til
                  produsentens varemottakere. Meldingene som sendes fra banken
                  mottas av aktuelle varemottakere, der de behandles før svar
                  sendes tilbake til bank.
                </p>
                <p>
                  Tjenesten omfatter også innsending av årsoppgave til
                  Landbrukets Dataflyt.
                </p>
                {userInfo.loading ||
                  versionInfo.loading ||
                  (newsPosts.loading && <Spinner />)}
                {userInfo.loaded && (
                  <p>
                    Du er logget inn som{" "}
                    <strong>{userInfo.data.fullName}</strong>, og representerer
                    nå <strong>{userInfo.data.organization.name}</strong>.
                  </p>
                )}
                {versionInfo.loaded && (
                  <span
                    className="text-muted float-end"
                    onClick={this.toggleVersionInfo}
                    style={{ fontSize: "60%", cursor: "pointer" }}
                  >
                    version: {versionInfo.data.release}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
        {newsPosts.loaded && newsPosts.data.length > 0 && (
          <>
            {newsPosts.data
              .sort((p1, p2) =>
                p1.order - p2.order === 0
                  ? p2.createdAt - p1.createdAt
                  : p1.order - p2.order
              )
              .map((newsPost) => {
                return (
                  <Card key={newsPost.id} className="mt-3">
                    <Card.Header>
                      <span>{newsPost.header}</span>
                      <span className="float-end">
                        <Moment locale="no" format="D. MMM YYYY [kl.] HH:mm">
                          {newsPost.createdAt}
                        </Moment>
                      </span>
                    </Card.Header>
                    <Card.Body>
                      <p>{newsPost.content}</p>
                    </Card.Body>
                  </Card>
                );
              })}
          </>
        )}
        <Modal show={this.state.showVersion} onHide={this.toggleVersionInfo}>
          <Modal.Header closeButton>
            <Modal.Title>Version</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Table striped>
              <tbody>
                <tr>
                  <td>Release</td>
                  <td>{versionInfo.data.release}</td>
                </tr>
                <tr>
                  <td>Build</td>
                  <td>{versionInfo.data.build}</td>
                </tr>
                <tr>
                  <td>Build-date</td>
                  <td>
                    <Moment unix format="DD.MM.YYYY @ HH:mm">
                      {versionInfo.data.date}
                    </Moment>
                  </td>
                </tr>
                <tr>
                  <td>Deploy-date</td>
                  <td>
                    <Moment unix format="DD.MM.YYYY @ HH:mm">
                      {versionInfo.data.deployDate}
                    </Moment>
                  </td>
                </tr>
              </tbody>
            </Table>
          </Modal.Body>
          <Modal.Footer>
            <PrimaryButton
              title="Lukk"
              onClick={this.toggleVersionInfo}
            />
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const userInfo = getStoreDataForUserInfo(state);
  const versionInfo = getStoreDataForVersionInfo(state);
  const newsPosts = getStoreDataForNewsPosts(state);
  return { userInfo, versionInfo, newsPosts };
};

const mapDispatchToProps = {
  getUserInfo,
  getVersionInfo,
  getAllNewsPosts,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

import React from "react";
import Spinner from "../../components/spinner";
import { Link } from "react-router-dom";
import NumberFormat from "react-number-format";
import Swal from "sweetalert2";

const FarmProductionHistory = (props) => {
  const data = props.farmProductionHistoryData;
  return (
    <div>
      {data.loading && <Spinner />}
      {data.loaded && data.data.length !== 0 && (
        <table className="table && table-striped">
          <thead>
            <tr>
              <th>År</th>
              <th>Varemottaker</th>
              <th>Netto</th>
              <th>Produsent</th>
            </tr>
          </thead>
          <tbody>
            {data.data.map((history, key) => {
              return (
                <tr key={key}>
                  <td>{history.year}</td>
                  <td>{history.varemottaker ? history.varemottaker.name : "Varemottaker ikke funnet"}</td>
                  <td>
                    <NumberFormat
                      value={history.netto}
                      displayType="text"
                      thousandSeparator={true}
                      decimalScale={2}
                      fixedDecimalScale={true}
                      prefix="kr "
                    />
                  </td>
                  <td>
                    <div>{history.produsent ? history.produsent.navn : "Produsent ikke funnet"}</div>
                    <span className="small">
                      <Link to={`/producers/details/${history.prodNummer}`}>
                        {history.prodNummer}
                      </Link>
                    </span>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
      {data.loaded && data.data.length === 0 && (
        <p>Fant ingen tidligere produksjon</p>
      )}
    </div>
  );
};

export default React.memo(FarmProductionHistory);

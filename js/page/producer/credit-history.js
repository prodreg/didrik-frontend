import React from "react";
import Spinner from "../../components/spinner";
import NumberFormat from "react-number-format";

const CreditHistory = (props) => {
  const data = props.creditHistoryData;
  return (
    <div>
      {data.loading && <Spinner />}
      {data.loaded && data.data.length !== 0 && (
        <table className="table && table-striped">
          <thead>
            <tr>
              <th>År</th>
              <th>Bank</th>
              <th>Kreditt</th>
            </tr>
          </thead>
          <tbody>
            {data.data.map((history, key) => {
              return (
                <tr key={key}>
                  <td>{history.year}</td>
                  <td>{history.bank ? history.bank.name : "Bank ikke funnet"}</td>
                  <td>
                    <NumberFormat
                      value={history.bevilget}
                      displayType="text"
                      thousandSeparator={true}
                      decimalScale={2}
                      fixedDecimalScale={true}
                      prefix="kr "
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
      {data.loaded && data.data.length === 0 && (
        <p>Fant ingen tidligere kreditt</p>
      )}
    </div>
  );
};

export default React.memo(CreditHistory);

import React from "react";
import {
  MdChevronLeft,
  MdChevronRight,
  MdOutlineMailOutline
} from "react-icons/md";
import {Link} from "react-router-dom";
import Moment from "react-moment";
import ReactPaginate from "react-paginate";
import useLocalStorage from "../../hooks/use-local-storage";

const ProducerActiveCases = (props) => {
  const activeCase = props.activeCasesData.data[0];
  if (props.role === 'mottaker' && props.userInfo.loaded) {
    activeCase.varselForMottaker = activeCase.varsler.find((varsel) => {
      return varsel.status !== 'PENDING' && varsel.type !== 'SLETTING' && varsel.mottakerId === props.userInfo.data.organization.orgNumber;
    });
  }

  return (
    <div className="default-info-container">
      <ol className="message-container">
        <div className="message-info-container">
            <span className="inbox-icon-container">
              <span className="text-primary">
                {" "}
                <MdOutlineMailOutline size="13px" />
                {" "}
              </span>
            </span>
          <Link
            className="event-message-link"
            to={activeCase.varselForMottaker ? `/case-notice/${activeCase.varselForMottaker.id}` : `/case/${activeCase.id}`}
          >
            <li className="template-container">
              <div className="default-info-container">
                <span className="text-primary">
                  <b>{activeCase.produsent.navn}</b>
                </span>
                <div className="message-title-container">
                  <span className="sub-text">{activeCase.bankName}</span>
                </div>
              </div>
            </li>
          </Link>
          <Link
            className="event-message-link"
            to={activeCase.varselForMottaker ? `/case-notice/${activeCase.varselForMottaker.id}` : `/case/${activeCase.id}`}
          >
            <div className="timestamp-container">
              {"Opprettet: "}
              <Moment unix format="D MMM YYYY HH:mm">
                {activeCase.created}
              </Moment>
            </div>
            <div className="timestamp-container">
              {"Oppdatert: "}
              <Moment unix format="D MMM YYYY HH:mm">
                {activeCase.updated}
              </Moment>
            </div>
          </Link>
        </div>
      </ol>
    </div>
  );
};

export default ProducerActiveCases;

import React from "react";
import Spinner from "../../components/spinner";
import NumberFormat from "react-number-format";

const ProductionHistory = (props) => {
  const data = props.productionHistoryData;
  return (
    <div>
      {data.loading && <Spinner />}
      {data.loaded && data.data.length !== 0 && (
        <table className="table && table-striped">
          <thead>
            <tr>
              <th>År</th>
              <th>Varemottaker</th>
              <th>Netto</th>
            </tr>
          </thead>
          <tbody>
            {data.data.map((history, key) => {
              return (
                <tr key={key}>
                  <td>{history.year}</td>
                  <td>{history.varemottaker ? history.varemottaker.name : "Varemottaker ikke funnet"}</td>
                  <td>
                    <NumberFormat
                      value={history.netto}
                      displayType="text"
                      thousandSeparator={true}
                      decimalScale={2}
                      fixedDecimalScale={true}
                      prefix="kr "
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
      {data.loaded && data.data.length === 0 && (
        <p>Fant ingen tidligere produksjon</p>
      )}
    </div>
  );
};

export default React.memo(ProductionHistory);

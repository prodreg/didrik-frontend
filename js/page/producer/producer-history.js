import React, { useState, useEffect } from "react";
import {
	getAllActiveCasesByProducer,
	getOrCreateNewPreDraftAndRedirect,
} from "../../redux/thunks/case-thunks";
import { getUserInfo } from "../../redux/thunks/user-info-thunks";
import {
	getCreditHistory,
	getProductionHistory,
	getFarmProductionHistory,
} from "../../redux/thunks/producer-thunk";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "../../components/spinner";
import Card from "../../components/ui/card/card";
import "./producer-history.scss";
import CreditHistory from "./credit-history";
import ProductionHistory from "./production-history";
import FarmProductionHistory from "./farm-production-history";
import ProducerActiveCases from "./producer-active-cases";
import PrimaryButton from "../../components/ui/button/primary-button";
import userService from "../../service/user-service";
import { useHistory } from "react-router-dom";

const ProducerHistory = (props) => {
	const user = useSelector((state) => state.user);
	const userRole = userService.getLoggedInUser().roles[0];
	const username = userService.getLoggedInUser().username;
	const [showHistory, setShowHistory] = useState(false);
	const history = useHistory();

	const userInfo = useSelector((state) => state.userInfo);
	const activeCasesData = useSelector((state) => state.allActiveCases);
	const creditHistoryData = useSelector(
		(state) => state.creditHistoryReducer
	);
	const productionHistoryData = useSelector(
		(state) => state.productionHistoryReducer
	);
	const farmProductionHistoryData = useSelector(
		(state) => state.farmProductionHistoryReducer
	);

	const dispatch = useDispatch();

	useEffect(() => {
		fetchActiveCasesData();
		fetchUserInfo();
	}, [dispatch]);

	const fetchActiveCasesData = () => {
		dispatch(getAllActiveCasesByProducer(props.prodnr));
	};

	const fetchUserInfo = () => {
		dispatch(getUserInfo(username));
	};

	const getOrCreateNewPreDraft = () => {
		dispatch(
			getOrCreateNewPreDraftAndRedirect(
				user.data.orgNumber,
				props.prodnr,
				props.producer,
				history
			)
		);
	};

	const fetchHistoricData = () => {
		setShowHistory(true);
		dispatch(getCreditHistory(props.prodnr));
		dispatch(getProductionHistory(props.prodnr, ""));
		dispatch(getFarmProductionHistory(props.prodnr, ""));
	};

	const canSeeActiveCase =
		activeCasesData.data[0] &&
		((userRole === "garantiutvalget" &&
			activeCasesData.data[0].garantiutvalgetIncluded) ||
			(userRole === "mottaker" &&
				activeCasesData.data[0].varsler.find((varsel) => {
					return (
						varsel.status !== "PENDING" &&
						varsel.type !== "SLETTING" &&
						varsel.mottakerId === user.data.orgNumber
					);
				})) ||
			(userRole !== "garantiutvalget" && userRole !== "mottaker"));

	const getCardContents = () => {
		if (activeCasesData.loading ?? !activeCasesData.loaded)
			return <Spinner />;
		if (activeCasesData.data.length === 0)
			return (
				<div>
					{userRole === "bank" && (
						<PrimaryButton
							title="Opprett sak"
							onClick={getOrCreateNewPreDraft}
						/>
					)}
					<p>
						Ingen registrert digital sak -{" "}
						<span
							className="history-link"
							onClick={fetchHistoricData}
						>
							se historisk data
						</span>
					</p>
				</div>
			);
		return (
			<div>
				{canSeeActiveCase && (
					<ProducerActiveCases
						activeCasesData={activeCasesData}
						role={userRole}
						userInfo={userInfo}
					/>
				)}
			</div>
		);
	};

	return (
		<div>
			<Card title="Registrert driftskreditt">{getCardContents()}</Card>
			{showHistory && (
				<div>
					<Card title="Kreditthistorikk">
						<CreditHistory creditHistoryData={creditHistoryData} />
					</Card>
					<Card title="Produksjonshistorikk - Produsent">
						<ProductionHistory
							productionHistoryData={productionHistoryData}
						/>
					</Card>
					<Card title="Produksjonshistorikk - Gård">
						<FarmProductionHistory
							farmProductionHistoryData={
								farmProductionHistoryData
							}
						/>
					</Card>
				</div>
			)}
			<div className="historic-data-button">
				{activeCasesData.data.length !== 0 && !showHistory && (
					<PrimaryButton
						title="Hent historisk data"
						onClick={fetchHistoricData}
					/>
				)}
			</div>
		</div>
	);
};

export default ProducerHistory;

import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import "./producer-detail.scss";
import Container from "../../components/ui/container/container";
import { MdOutlineContactPage } from "react-icons/md";
import { getProducer } from "../../redux/thunks/producer-thunk";
import Spinner from "../../components/spinner";
import ProducerHistory from "./producer-history";
import Swal from "sweetalert2";
import ReactTooltip from "react-tooltip";

const ProducerDetail = (props) => {
  const prodnr = props.match.params.prodnr;
  const producer = useSelector((state) => state.producerReducer);
  const [historicProdnr, setHistoricProdnr] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    fetchProducer();
    setHistoricProdnr([]);
  }, [dispatch, prodnr]);

  const fetchProducer = () => {
    if (prodnr !== "") {
      dispatch(getProducer(prodnr)).then(
        (response) => response.payload.error && displayError()
      );
    }
  };

  if (
    producer.loaded &&
    producer.data.allProdNr.length > 1 &&
    historicProdnr.length === 0
  ) {
    const allProdnumbers = producer.data.allProdNr.filter(
      (p) => p !== producer.data.prodnr
    );
    setHistoricProdnr(allProdnumbers);
  }

  const displayError = () => {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste inn produsentdetaljer. Prøv igjen senere",
      "error"
    );
  };

  return (
    <div>
      {producer.loading && <Spinner />}
      <Container
        title="Produsentdetaljer"
        description="Detaljer om produsent"
        icon={<MdOutlineContactPage size="60px" />}
      >
        {producer.loaded && (
          <div>
            <div className="row">
              <div className="col-sm">
                <dl className="dl-horizontal">
                  <dt>Produsent</dt>
                  <dd>{producer.data.navn}</dd>
                  <dt>Produsentnummer</dt>
                  <dd>{producer.data.prodnr}</dd>
                  {historicProdnr.length !== 0 && (
                    <dd className="historic-prodnr">
                      <ReactTooltip />
                      <a data-tip={historicProdnr.join(", ")}>
                        <span className="text-secondary">
                          {historicProdnr.length > 1 ? (
                            <span>
                              {historicProdnr.length} historiske produsentnummer
                            </span>
                          ) : (
                            <span>1 historisk produsentnummer</span>
                          )}
                        </span>
                      </a>
                    </dd>
                  )}
                  <dt>Org. nr.</dt>
                  <dd>{producer.data.orgnr}</dd>
                  <dt>Adresse</dt>
                  <dd>
                    {producer.data.adresse.adresse1}
                    <br />
                    {producer.data.adresse.postnr}{" "}
                    {producer.data.adresse.poststed}
                  </dd>
                </dl>
              </div>
              <div className="col-sm">
                <dl className="dl-horizontal">
                  <dt>E-post</dt>
                  <dd>
                    <a href={`mailto:${producer.data.epost}`}></a>
                    {producer.data.epost}
                  </dd>
                  <dt>Telefon jobb</dt>
                  <dd>{producer.data.telefonJobb}</dd>
                  <dt>Telefon (privat)</dt>
                  <dd>{producer.data.telefonPrivat}</dd>
                  <dt>Telefax</dt>
                  <dd>{producer.data.telefaks}</dd>
                  <dt>Mobil</dt>
                  <dd>{producer.data.mobil}</dd>
                </dl>
              </div>
            </div>
            <ProducerHistory prodnr={prodnr} producer={producer.data} />
          </div>
        )}
      </Container>
    </div>
  );
};

export default React.memo(ProducerDetail);

import {getAllProducerNumberConnections} from "../../redux/thunks/producer-thunk";
import {connect, useDispatch, useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import Swal from "sweetalert2";
import Container from "../../components/ui/container/container";
import {MdAddCircleOutline, MdOutlineSwapHoriz} from "react-icons/md";
import Spinner from "../../components/spinner";
import PrimaryButton from "../../components/ui/button/primary-button";
import TertiaryButton from "../../components/ui/button/tertiary-button";
import NewProducerNumberConnection from "../../components/new-producer-number-connection";
import {updateProdusentIndex} from "../../redux/thunks/admin-thunks";

const ProducerNumberConnections = (props) => {

  const producerConnections = useSelector((state) => state.producerNumberConnectionReducer);
  const dispatch = useDispatch();
  const [showAddConnection, setShowAddConnection] = useState(false);
  const [reindexing, setReindexing] = useState(false);

  useEffect(() => {
    fetchProducersState();
  }, [dispatch]);

  const fetchProducersState = () => {
    dispatch(getAllProducerNumberConnections()).then(
      (response) => response.payload.error && displayError()
    );
  };

  const displayError = () => {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste inn produsentnummer-koblinger. Prøv igjen senere",
      "error"
    );
  };

  const toggleShowNewConnectionDialog = () => {
    setShowAddConnection(!showAddConnection);
  }

  const triggerReindexing = (event) => {
    event.preventDefault();
    setReindexing(true);
    props.updateProdusentIndex()
      .then((res) => {
        handleError(!res.ok);
        setReindexing(false);
      });
  }

  const handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Reindeksering feilet. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("OK", "Reindeksering var vellykket", "success");
    }
  };

  return (
    <Container
      title="Produsentnummer koblinger"
      description="Her er alle koblinger mellom produsentnummer"
      icon={<MdOutlineSwapHoriz size="60px" />}
    >
      <div>
        {producerConnections.loading && <Spinner />}
        {producerConnections.loaded && (
          <div>
            <table className="table && table-striped">
              <thead>
              <tr>
                <th scope="col">Gammelt produsennummer</th>
                <th scope="col">Nytt produsentnummer</th>
              </tr>
              </thead>
              <tbody>
              {producerConnections.data.map((p, key) => {
                return (
                  <tr key={key}>
                    <td>{p.gammeltProdusentnummer}</td>
                    <td>{p.nyttProdusentnummer}</td>
                  </tr>
                );
              })}
              </tbody>
            </table>
          </div>
        )}
        <PrimaryButton
          onClick={toggleShowNewConnectionDialog}
          title={"Ny kobling"}
          className="d-inline-block me-2"
          icon={<MdAddCircleOutline/>}
        />
        <hr/>
        <TertiaryButton
          onClick={triggerReindexing}
          title="Reindekser produsent-data"
          disabled={reindexing}
        />
        {reindexing && (
          <div>
            <div className="lds-ellipsis d-inline-block align-middle">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
            </div>
            <div className="d-inline-block align-middle me-2">Reindeksering pågår...</div>
          </div>
        )}
        <div style={{ color: "red" }}>Advarsel: Reindeksering kan ta flere minutter, og løsningens respons-tid kan affekteres mens indeksering pågår.</div>
        <div style={{ color: "red" }}>Det er normalt ikke nødvendig å kjøre manuell reindeksering, og indeksering blir kjørt automatisk hver morgen.</div>
      </div>
      <NewProducerNumberConnection
        showAddProducerNumberConnection={showAddConnection}
        toggleShowNewConnectionDialog={toggleShowNewConnectionDialog}
        history={props.history}
      />
    </Container>
  );
}

const mapStateToProps = (state) => {
  return { };
}

const mapDispatchToProps = {
  updateProdusentIndex
}

export default connect(mapStateToProps, mapDispatchToProps)(ProducerNumberConnections);

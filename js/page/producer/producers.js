import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Container from "../../components/ui/container/container";
import { getAllProducers } from "../../redux/thunks/producer-thunk";
import SearchForm from "../../components/ui/search-form/search-form";
import ProducersList from "./producers-list";
import { MdOutlineChevronLeft, MdOutlineChevronRight } from "react-icons/md";
import { MdOutlineManageSearch } from "react-icons/md";
import ReactPaginate from "react-paginate";
import "./producers.scss";
import Swal from "sweetalert2";
import useLocalStorage from "../../hooks/use-local-storage";

const Producers = () => {
  const producersState = useSelector((state) => state.producersReducer);
  const totalResults = producersState.totalResults > 8000 ? 8000 : producersState.totalResults;
  const [query, setQuery] = useLocalStorage("query", "");
  const [currentPage, setCurrentPage] = useLocalStorage(
    "page_number_producer",
    1
  );
  const producersPerPage = 10;
  const dispatch = useDispatch();

  useEffect(() => {
    fetchProducersState();
  }, [dispatch, currentPage]);

  const fetchProducersState = () => {
    fetchProducersStateWithQuery(query);
  };

  const fetchProducersStateWithQuery = (q) => {
    if (totalResults > 0 && q !== "") {
      setCurrentPage(1);
    }
    dispatch(getAllProducers(currentPage, q)).then(
      (response) => response.payload.error && displayError()
    );
  }

  const displayError = () => {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste inn produsenter. Prøv igjen senere",
      "error"
    );
  };

  const onChange = (event) => {
    setQuery(event.target.value);

    if (query.length > 1) {
      fetchProducersStateWithQuery(event.target.value);
    }
  };

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected + 1);
  };

  return (
    <Container
      title="Produsentsøk"
      description="Her kan du søke opp en produsent"
      icon={<MdOutlineManageSearch size="60px" />}
    >
      <SearchForm
        type="text"
        id="producer-search"
        value={query}
        placeholder="Produsentnummer, organisasjonsnummer eller produsentens navn..."
        className=""
        onChange={onChange}
        onClick={fetchProducersState}
        title="Søk"
      />
      <ProducersList producersState={producersState} />
      {totalResults > producersPerPage && (
        <ReactPaginate
          previousLabel={<MdOutlineChevronLeft />}
          nextLabel={<MdOutlineChevronRight />}
          breakLabel={"..."}
          pageCount={totalResults / producersPerPage}
          marginPagesDisplayed={3}
          pageRangeDisplayed={3}
          onPageChange={handlePageChange}
          containerClassName={"pagination"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextLinkClassName={"page-link"}
          nextClassName={"page-item"}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
          forcePage={currentPage - 1}
        />
      )}
    </Container>
  );
};

export default Producers;

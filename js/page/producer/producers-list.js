import React from "react";
import { withRouter } from "react-router";
import Spinner from "../../components/spinner";
import "./producers-list.scss";

const ProducersList = (props) => {
	const data = props.producersState;

	const fetchProducerDetail = (event, prodnr) => {
		event.preventDefault();
		props.history.push(`/producers/details/${prodnr}`);
	};

	return (
		<div className="producers-list-container">
			{data.loading && <Spinner />}
			{data.loaded && (
				<div style={{ overflow: "auto" }}>
					<table className="table && table-striped">
						<thead>
							<tr>
								<th scope="col">Produsentens navn</th>
								<th scope="col">Adresse</th>
								<th scope="col">Org. nr.</th>
								<th scope="col">Produsentnummer</th>
							</tr>
						</thead>
						<tbody>
							{data.data.map((producer, key) => {
								return (
									<tr
										className="producer-row"
										key={key}
										onClick={(e) =>
											fetchProducerDetail(
												e,
												producer.prodnr
											)
										}
									>
										<td>{producer.navn}</td>
										<td>
											{producer.adresse.adresse1}
											<br />
											{producer.adresse.postnr}{" "}
											{producer.adresse.poststed}
										</td>
										<td>{producer.orgnr}</td>
										<td>{producer.prodnr}</td>
									</tr>
								);
							})}
						</tbody>
					</table>
				</div>
			)}
		</div>
	);
};

export default withRouter(ProducersList);

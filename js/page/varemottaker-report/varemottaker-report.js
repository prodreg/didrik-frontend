import React from "react";
import Spinner from "../../components/spinner";
import {getStoreDataForUser} from "../../redux/selectors";
import {connect} from "react-redux";
import {MdOutlineInsertDriveFile} from "react-icons/md";
import {getVaremottakerReport} from "../../redux/thunks/report-thunks";
import Container from "../../components/ui/container/container";
import PrimaryButton from "../../components/ui/button/primary-button";
import Swal from "sweetalert2";
import FileSaver from "file-saver";

class VaremottakerReport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      loadingError: false,
      downloading: false
    };
  }

  componentDidMount() {
    this.props.getVaremottakerReport(new Date().getFullYear() - 1)
      .then((res) => {
        if (res.payload.error) {
          this.setState({
            loading: false,
            loadingError: true
          })
        } else {
          this.setState({
            loading: false,
            loadingError: false
          })
        }
      });
  }

  handleDownloadReport = (event) => {
    event.preventDefault();
    this.setState({
      downloading: true,
    });
    this.props.getVaremottakerReport(new Date().getFullYear() - 1)
      .then((res) => {
        if (res.payload.error) {
          Swal.fire(
            "En feil har oppstått",
            "Rapport kunne ikke nedlastes. Vennligst prøv igjen senere.",
            "error"
          );
        } else {
          const blob = new Blob([res.payload.data], {
            type: "application/vnd.ms-word",
          });
          FileSaver.saveAs(blob, "Rapport.doc");
        }
        this.setState({
          downloading: false
        });
      });
  };

  render() {
    return (
      <Container
        title={"Varemottakerrapport"}
        description={"Oversikt for Varemottaker"}
        icon={<MdOutlineInsertDriveFile size="60px" />}
      >
        <div className="row">
          <div className="col-lg">
            {this.state.loadingError ? (
              <div style={{ color: 'red' }}>Ingen rapport funnet</div>
            ) : (
              <div>
                {this.state.loading && <Spinner />}
                <PrimaryButton
                  title="Last ned Word rapport"
                  onClick={this.handleDownloadReport}
                  disabled={this.state.loading || this.state.downloading}
                  />
              </div>
              )}
          </div>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const user = getStoreDataForUser(state);
  return {
    user
  };
};

const mapDispatchToProps = {
  getVaremottakerReport
};

export default connect(mapStateToProps, mapDispatchToProps)(VaremottakerReport);

import React, { useState, useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import Card from "../../components/ui/card/card";
import userService from "../../service/user-service";
import { getAllChildrenWithRoles } from "../../redux/thunks/organization-thunks";
import {
  getConversationForCaseAndOrganization,
  getConversationById,
  getConversationMessages,
  postConversationMessageAs,
  postConversationMessage,
  getConversationAttachments,
} from "../../redux/thunks/conversation-thunks";
import Spinner from "../../components/spinner";
import InputField from "../../components/ui/input-field/input-field";
import PrimaryButton from "../../components/ui/button/primary-button";
import Swal from "sweetalert2";
import FileSaver from "file-saver";
import "./conversation.scss";
import Moment from "react-moment";
import "moment/locale/nb";
import { TransitionGroup, CSSTransition } from "react-transition-group";

const Conversation = (props) => {
  const dispatch = useDispatch();
  const caseId = props.caseId;
  const receiverId = props.receiverId;
  const convId = props.conversationId;
  const height = props.height;
  const userInfo = userService.getLoggedInUser();
  const userOrgNumber = userInfo.orgNumber;
  const userId = userInfo.username;
  const childOrgs = useSelector((state) => state.childrenWithRolesReducer);
  const conversationData = useSelector(
    (state) => state.conversationForCaseAndOrganizationReducer
  );
  const newConversationData = useSelector(
    (state) => state.conversationMessagesReducer
  );
  const [conversationId, setConversationId] = useState("");
  const [participantsOrgNumber, setParticipantsOrgNumber] = useState([]);
  const [participants, setParticipants] = useState("");
  const [messages, setMessages] = useState([]);
  const [postAs, setPostAs] = useState({});
  const [canPostMessage, setCanPostMessage] = useState(false);
  const [isPosting, setIsPosting] = useState(false);
  const [content, setContent] = useState("");
  const [filename, setFilename] = useState("");
  let contentIsValid = content !== "";

  /*
  useEffect(() => {
    dispatch(getAllChildrenWithRoles(userOrgNumber, ["Saksbehandling"]));
  }, [dispatch]);
  */
  const loadNewConversationMessages = (id, messages) => {
    if (id && !conversationData.error) {
      let timestamp = 0;
      if (messages.length > 0) {
        timestamp = messages[0].createdAt;
      }
      dispatch(getConversationMessages(id, timestamp)).then((response) => {
        if (!response.payload.error) {
          if (response.payload.data.length > 0) {
            const updatedData = addNewFields(response.payload.data, id);
            updatedData.forEach((d) => {
              if (!messages.find((m) => m.id === d.id)) {
                setMessages([...messages.slice(0, 0), d, ...messages.slice(0)]);
              }
            });
          }
        }
      });
    }
  };

  useEffect(() => {
    const timer = setInterval(() => {
      if (!isPosting) {
        loadNewConversationMessages(conversationId, messages);
      }
    }, 600000);
    return () => clearInterval(timer);
  }, [conversationId, messages, isPosting]);

  useEffect(() => {
    if (caseId && receiverId) {
      dispatch(getConversationForCaseAndOrganization(caseId, receiverId)).then(
        (response) =>
          !response.payload.error && populateMessages(response.payload.data)
      );
    } else if (convId) {
      dispatch(getConversationById(convId)).then(
        (response) =>
          !response.payload.error && populateMessages(response.payload.data)
      );
    }
  }, [dispatch]);

  const getAuthorType = (fromOrgNumber, fromUserId, messageObj) => {
    if (fromOrgNumber === "system") {
      messageObj.authorType = "system";
    } else if (fromUserId === userId) {
      messageObj.authorType = "current-user";
    } else if (fromOrgNumber === userOrgNumber) {
      messageObj.authorType = "current-user-org";
    } else {
      messageObj.authorType = "other-org";
    }
  };

  const addNewFields = (messages, id) => {
    messages.forEach((m) => {
      getAuthorType(m.fromOrgNumber, m.fromUserId, m);
      m.parentId = id;
    });
    return messages.reverse();
  };

  const populateMessages = (data) => {
    setConversationId(data.id);
    const participants = data.participants.map((o) => o.name).join(", ");
    setParticipants(participants);
    const updatedMessages = addNewFields(data.messages, data.id);
    setMessages(updatedMessages);
    const participantsOrgNumber = data.participants.map((o) => o.orgNumber);
    setParticipantsOrgNumber(participantsOrgNumber);
    let canPostMessage = participantsOrgNumber.indexOf(userOrgNumber) > -1;
    setCanPostMessage(canPostMessage);
    if (!canPostMessage) {
      dispatch(getAllChildrenWithRoles(userOrgNumber, ["Saksbehandling"])).then(
        (response) => {
          response.payload.data.forEach((child) => {
            if (participantsOrgNumber.indexOf(child.orgNumber) !== -1) {
              setCanPostMessage(true);
              setPostAs(child);
            }
          });
        }
      );
    }
  };

  const displayPostError = () => {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke sende melding. Prøv igjen senere",
      "error"
    );
    setContent("");
    setIsPosting(false);
  };

  const displayAttachmentSwal = () => {
    Swal.fire(
      "Filen er lastet opp",
      "Filen er nå tilgjengelig for alle i denne samtalen",
      "success"
    );
  };

  const dispatchPostMessage = (conversationId, postAs, message) => {
    scrollToTop();
    if (Object.keys(postAs).length !== 0) {
      dispatch(
        postConversationMessageAs(conversationId, postAs.orgNumber, message)
      ).then((response) => {
        if (response.payload.error) {
          displayPostError();
        } else {
          setIsPosting(false);
          const postedMessage = addNewFields(
            [response.payload.data],
            conversationId
          );
          setMessages([
            ...messages.slice(0, 0),
            postedMessage[0],
            ...messages.slice(0),
          ]);
          if (message.attachment) {
            displayAttachmentSwal();
            loadNewConversationMessages(conversationId, messages);
          } else {
            setContent("");
          }
        }
      });
    } else {
      dispatch(postConversationMessage(conversationId, message)).then(
        (response) => {
          if (response.payload.error) {
            displayPostError();
          } else {
            setIsPosting(false);
            const postedMessage = addNewFields(
              [response.payload.data],
              conversationId
            );
            setMessages([
              ...messages.slice(0, 0),
              postedMessage[0],
              ...messages.slice(0),
            ]);
            if (message.attachment) {
              displayAttachmentSwal();
              loadNewConversationMessages(conversationId, messages);
            } else {
              setContent("");
            }
          }
        }
      );
    }
  };

  const postMessage = (attachment, filename) => {
    setFilename(filename ? filename : "");
    setIsPosting(true);
    const message = {
      message: attachment ? filename : content,
      attachment: attachment
        ? {
            data: attachment ? btoa(attachment) : null,
            mimetype: "any",
          }
        : null,
    };
    dispatchPostMessage(conversationId, postAs, message);
  };

  const contentChangeHandler = (event) => {
    setContent(event.target.value);
  };

  const handleFormSubmission = (event) => {
    event.preventDefault();
    if (!contentIsValid) {
      return;
    }
    postMessage();
  };

  const downloadAttachment = (event, messageId, filename) => {
    event.preventDefault();
    dispatch(getConversationAttachments(conversationId, messageId)).then(
      (response) => {
        if (response.payload.error) {
          Swal.fire(
            "En feil har oppstått",
            "Kunne ikke laste ned fil. Prøv igjen senere",
            "error"
          );
        } else {
          FileSaver.saveAs(
            new Blob([b64toFile(response.payload.data.data)], {
              type: "charset=8-utf-8",
            }),
            filename
          );
        }
      }
    );
  };

  const b64toFile = (data, filename, contentType) => {
    const sliceSize = 512;
    const byteCharacters = atob(data);

    const byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);
      let byteNumbers = new Array(slice.length);

      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      let byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const file = new Blob(byteArrays, { type: contentType });
    const fileData = new FormData();
    fileData.append("file", file, filename);
    return file;
  };

  const handleFileInput = (event) => {
    event.preventDefault();
    const reader = new FileReader();
    const file = event.target.files[0];
    const name = file.name;
    let attachmentData = null;
    if (name === filename) {
      return;
    }
    setFilename(name);
    reader.onload = (e) => {
      try {
        if (!e) {
          attachmentData = reader.content;
        } else {
          attachmentData = e.target.result;
        }
        const size = attachmentData.length;
        if (size < 10000000) {
          postMessage(attachmentData, name);
        } else {
          Swal.fire("Fil for stor", "Filen kan maksimalt være 10MB", "error");
        }
      } catch (error) {
        Swal.fire(
          "En feil har oppstått",
          "Kunne ikke laste opp filen. Prøv igjen senere",
          "error"
        );
      }
    };
    try {
      reader.readAsBinaryString(file);
    } catch (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke laste opp filen. Prøv igjen senere",
        "error"
      );
    }
  };

  const scrollToTop = () => {
    window.document.getElementById("chat-scroll-container").scrollTop = 0;
  };

  const displayCardTitle = () => {
    return (
      <div>
        {conversationData.loading && !conversationData.error && (
          <span>Henter dialog</span>
        )}
        {(conversationData.loaded || newConversationData.loaded) &&
          !conversationData.error &&
          !newConversationData.error && (
            <div>
              {Object.keys(postAs).length !== 0 && (
                <span className="float-end">
                  Du representerer <b>{postAs.name}</b>
                </span>
              )}
              <span>
                Dialog mellom <b>{participants}</b>
              </span>
            </div>
          )}
        {(conversationData.error || newConversationData.error) && (
          <span>
            En feil oppstod under henting av dialogdata. Prøv igjen senere.
          </span>
        )}
      </div>
    );
  };

  const displayInputFields = () => {
    return (
      <div className="mb-5">
        <form onSubmit={handleFormSubmission}>
          <div className="form-floating mb-3" style={{ height: "150px" }}>
            <textarea
              className="form-control form-control-sm"
              cols="30"
              rows="100"
              value={content}
              onChange={contentChangeHandler}
              id="textarea"
              placeholder="Skriv melding..."
              style={{ height: "150px", resize: "none" }}
              disabled={isPosting}
            />
            <label htmlFor="textarea">Skriv melding...</label>
          </div>
          <PrimaryButton
            disabled={!contentIsValid || isPosting}
            title="Send"
            className="mb-3"
          />
        </form>
        <InputField
          type="file"
          name="file"
          id="file"
          disabled={isPosting}
          onChange={handleFileInput}
        />
      </div>
    );
  };

  const displayMessages = () => {
    return (
      <div className="chat-container">
        <div
          className="bubble-container"
          id="chat-scroll-container"
          style={{ height: props.height }}
        >
          <TransitionGroup className="message-list">
            {messages.map((message, key) => (
              <CSSTransition
                key={message.id}
                classNames="bubble"
                timeout={1500}
              >
                <div
                  key={key}
                  className={`${
                    message.authorType === "current-user" ||
                    message.authorType === "current-user-org"
                      ? "bubble-to-right"
                      : message.authorType === "other-org"
                      ? "bubble-to-left"
                      : "bubble-to-center"
                  }`}
                >
                  <div className="card">
                    <div
                      className={`card-header ${
                        message.authorType === "current-user-org" ||
                        message.authorType === "current-user"
                          ? "card-primary"
                          : message.authorType === "system"
                          ? "card-info"
                          : "card-primary-dark"
                      }`}
                    >
                      {message.authorType === "system" && (
                        <span className="material-icons-outlined">info</span>
                      )}
                      {message.authorType === "other-org" && (
                        <span className="material-icons">person</span>
                      )}
                      <span className="author">
                        {message.fromOrgName}
                        {message.authorType !== "system" && (
                          <span>, {message.fromUserName}</span>
                        )}{" "}
                        -{" "}
                        <Moment format="D MMM YYYY HH:mm">
                          {message.createdAt}
                        </Moment>
                      </span>
                      {(message.authorType === "current-user" ||
                        message.authorType === "current-user-org") && (
                        <span className="material-icons">person</span>
                      )}
                    </div>
                    <div className="card-body ">
                      {message.attachmentId === null && (
                        <span>{message.message}</span>
                      )}
                      {message.attachmentId !== null && (
                        <span>
                          Fil{" "}
                          <span className="text-info">{message.message}</span>{" "}
                          lastet opp.
                          <br />
                          <a
                            className="pointer"
                            style={{ textDecoration: "underline" }}
                            onClick={(e) =>
                              downloadAttachment(e, message.id, message.message)
                            }
                          >
                            Last ned
                          </a>
                        </span>
                      )}
                    </div>
                  </div>
                </div>
              </CSSTransition>
            ))}
          </TransitionGroup>
        </div>
      </div>
    );
  };

  return (
    <div>
      {conversationData.data && (
        <Card
          title={displayCardTitle()}
          style={{
            textAlign: Object.keys(postAs).length !== 0 ? "" : "center",
          }}
        >
          {conversationData.loading && !conversationData.error && <Spinner />}
          {(conversationData.loaded || newConversationData.loaded) &&
            canPostMessage &&
            displayInputFields()}
          {(conversationData.loaded || newConversationData.loaded) &&
            displayMessages()}
        </Card>
      )}
    </div>
  );
};

export default Conversation;

import {
  getStoreDataFormdataNoticeTypes,
  getStoreDataFormdataProductTypes,
  getStoreDataFormdataSchemeTypes,
  getStoreDataForUser,
} from "../../redux/selectors";
import { connect } from "react-redux";

import React from "react";
import Spinner from "../../components/spinner";
import {
  addGarantiutvalgetToCase, approveCaseGarantiutvalget,
  cancelCase,
  completeCase,
  endCase,
  getCaseById,
  getGarantiutvalgetDataForCase, rejectCaseGarantiutvalget, signOffCaseGarantiutvalget,
} from "../../redux/thunks/case-thunks";
import Swal from "sweetalert2";
import Moment from "react-moment";
import { getNoticesForCase } from "../../redux/thunks/case-notice-thunks";
import { formatAccountNumber, formatCurrency } from "../../misc/helpers";
import { MdOutlineChatBubbleOutline } from "react-icons/md";
import { MdOutlineCheckCircle, MdOutlineRecordVoiceOver } from "react-icons/md";
import { Link } from "react-router-dom";
import {
  translateCaseGarantiutvalgetStatus,
  translateCaseNoticeStatus,
  translateCaseNoticeType,
  translateLongCaseGarantiutvalgetStatus,
  translateShortCaseGarantiutvalgetStatus,
} from "../../misc/translate";
import {
  getTextColorForCaseGarantiutvalgetStatus,
  getTextColorForCaseStatus,
} from "../../misc/interpret";
import PrimaryButton from "../../components/ui/button/primary-button";
import SecondaryButton from "../../components/ui/button/secondary-button";
import TertiaryButton from "../../components/ui/button/tertiary-button";
import GarantiutvalgetResponse from "../../components/garantiutvalget-response";
import TextArea from "../../components/ui/textarea/text-area";
import InternalNote from "../../components/internal-note";

const VareleveranseElement = ({ vareleveranseList }) => (
  <div className="card mt-2">
    <div className="card-body">
      <div className="row">
        <div className="col-md-4 mb-3">
          <label className="form-label">Varemottaker</label>
          <input
            readOnly
            type="text"
            className="form-control form-control-sm"
            value={vareleveranseList[0].varemottaker.name}
          />
        </div>
        <div className="col-md-4 mb-3">
          <label className="form-label">Ordning</label>
          <input
            readOnly
            type="text"
            className="form-control form-control-sm"
            value={vareleveranseList[0].ordning}
          />
        </div>
      </div>
      {vareleveranseList.map((element, index) => (
        <div key={index}>
          <hr />
          <div className="row">
            <div className="col-md-2 mb-3">
              <label className="form-label">År</label>
              <input
                readOnly
                type="text"
                className="form-control form-control-sm"
                value={element.aar}
              />
            </div>
            <div className="col-md-3 mb-3">
              <label className="form-label">Produktslag</label>
              <input
                readOnly
                type="text"
                className="form-control form-control-sm"
                value={element.produktslagDescription}
              />
            </div>
            {!element.korrBeloep && (
              <div className="col-md-5 mb-3">
                <label className="form-label">
                  Årlig beløp overført til bank
                </label>
                <input
                  readOnly
                  type="text"
                  className="form-control form-control-sm"
                  value={element.oppgjoer}
                />
              </div>
            )}

            {element.korrBeloep && (
              <div className="col-md-5 mb-3">
                <div className="row">
                  <div className="col-md-6">
                    <label className="form-label">
                      Årlig beløp overført til bank
                    </label>

                    <input
                      readOnly
                      type="text"
                      className="form-control form-control-sm"
                      style={{ textDecoration: "line-through" }}
                      value={element.oppgjoer}
                    />
                  </div>
                  <div className="col-md-6">
                    <label className="form-label">
                      Årlig beløp overført til bank (Korrigert)
                    </label>

                    <input
                      readOnly
                      type="text"
                      className="form-control form-control-sm"
                      value={element.korrBeloep}
                    />
                  </div>
                </div>
              </div>
            )}
            {element.korrTrekk && (
              <div className="col-md-2 mb-3">
                <label className="form-label">Trekk</label>

                <input
                  readOnly
                  type="text"
                  className="form-control form-control-sm"
                  style={{ textDecoration: "line-through" }}
                  value={element.korrTrekk}
                />
              </div>
            )}
          </div>
          <div className="row">
            <div className="col-md-12 mb-3">
              <div className="form-floating">
                <textarea
                  readOnly
                  className="form-control form-control-sm"
                  id="floatingTextarea2"
                  style={{ height: "100px" }}
                  name="text"
                  value={element.text != null ? element.text : ""}
                ></textarea>
                <label htmlFor="floatingTextarea2">
                  Ytterligere informasjon
                </label>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  </div>
);

const NoticeElement = ({ notice }) => (
  <tr className="d-flex">
    <td className="col-2">
      <div>
        <strong>{notice.noticeContent.mottaker.name}</strong>
      </div>
      <div>{notice.mottakerId}</div>
    </td>
    <td className="col-2">{translateCaseNoticeType(notice.type)}</td>
    <td className="col-5">
      <div className="row">
        <div className="col-5" style={{ textAlign: "right" }}>
          <Link to={"/case-notice/" + notice.id}>Varsel</Link>
        </div>
        <div className="col-2" style={{ textAlign: "center" }}>
          |
        </div>
        <div className="col-5" style={{ textAlign: "left" }}>
          {(notice.type === "SLETTING" || notice.type === "ENDRING") && (
            <div>
              <span className="d-block">Tilbakemelding</span>
              <span className="d-block text-x-small">ikke nødvendig</span>
            </div>
          )}
          {notice.type === "NY" && notice.tilbakemeldingHandled && (
            <div>
              <Link to={"/case-feedback/" + notice.tilbakemeldingId}>
                Tilbakemelding
              </Link>
              <span className="d-block text-success text-x-small">mottatt</span>
            </div>
          )}
          {notice.type === "NY" && !notice.tilbakemeldingHandled && (
            <div>
              <span>Tilbakemelding</span>
              <span className="d-block text-warning text-x-small">
                ikke mottatt
              </span>
            </div>
          )}
        </div>
      </div>
    </td>
    <td className="col-3" style={{ textAlign: "right" }}>
      <Link to={"/case-notice/" + notice.id}>
        <MdOutlineChatBubbleOutline/>
      </Link>
    </td>
  </tr>
);

class Case extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataLoaded: false,
      soeknadDto: {},
      garantiutvalgetDto: {},
      notices: [],
      showSignedOffGarantiuvalgDialog: false,
      showRejectedGarantiuvalgDialog: false,
      showAcceptedGarantiuvalgDialog: false,
    };

    this.caseBankOrgNumberIsMatchingLoggedInUser =
      this.caseBankOrgNumberIsMatchingLoggedInUser.bind(this);
    this.cancelCase = this.cancelCase.bind(this);
    this.endCase = this.endCase.bind(this);
    this.completeCase = this.completeCase.bind(this);
    this.addGarantiutvalgetToCase = this.addGarantiutvalgetToCase.bind(this);
  }

  componentDidMount() {
    Promise.all([
      this.props.getCaseById(this.props.match.params.caseId),
      this.props.getNoticesForCase(this.props.match.params.caseId),
    ]).then((results) => {
      if (
        results[0].garantiutvalgetIncluded &&
        results[0].garantiutvalgetStatus !== "PENDING"
      ) {
        this.props
          .getGarantiutvalgetDataForCase(this.props.match.params.caseId)
          .then((result) => {
            this.setState({
              dataLoaded: true,
              soeknadDto: results[0],
              notices: results[1].map((element) => ({
                ...element,
                noticeContent: JSON.parse(element.varselContent),
              })),
              garantiutvalgetDto: result,
            });
          });
      } else {
        this.setState({
          dataLoaded: true,
          soeknadDto: results[0],
          notices: results[1].map((element) => ({
            ...element,
            noticeContent: JSON.parse(element.varselContent),
          })),
          garantiutvalgetDto: null,
        });
      }
    });
  }

  groupVareleveranseOnOrganizationNumber(vareleveranseList) {
    if (!vareleveranseList) {
      return [];
    }

    return vareleveranseList.reduce((payload, vareleveranse) => {
      const index = payload.findIndex((element) => {
        return element.some(
          (some) =>
            some.varemottaker.orgNumber === vareleveranse.varemottaker.orgNumber
        );
      });
      if (index === -1) {
        payload.push([vareleveranse]);
      } else {
        payload[index].push(vareleveranse);
      }
      return payload;
    }, []);
  }

  caseBankOrgNumberIsMatchingLoggedInUser() {
    return this.props.user.data.orgNumber === this.state.soeknadDto.bankId;
  }

  cancelCase(event) {
    event.preventDefault();
    Swal.fire({
      type: "question",
      title: "Kansellere sak?",
      showCancelButton: true,
      confirmButtonText: "Ja",
      cancelButtonText: "Avbryt",
    }).then((result) => {
      if (result.value) {
        this.props.cancelCase(this.state.soeknadDto.id).then((res) => {
          if (res.ok) {
            Swal.fire({
              type: "success",
              title: "Vellykket",
              text: "Saken er kansellert",
            });
            this.componentDidMount();
          } else {
            Swal.fire({
              type: "error",
              title: "Feil",
              text: "Kunne ikke kansellere saken, vennligst prøv igjen senere.",
            });
          }
        });
      }
    });
  }

  endCase(event) {
    event.preventDefault();
    Swal.fire({
      type: "question",
      title: "Avslutte sak?",
      showCancelButton: true,
      confirmButtonText: "Ja",
      cancelButtonText: "Avbryt",
    }).then((result) => {
      if (result.value) {
        this.props.endCase(this.state.soeknadDto.id).then((res) => {
          if (res.ok) {
            Swal.fire({
              type: "success",
              title: "Vellykket",
              text: "Saken er avsluttet",
            });
            this.componentDidMount();
          } else {
            Swal.fire({
              type: "error",
              title: "Feil",
              text: "Kunne ikke avslutte saken, vennligst prøv igjen senere.",
            });
          }
        });
      }
    });
  }

  completeCase(event) {
    event.preventDefault();
    this.props.completeCase(this.state.soeknadDto.id).then((res) => {
      if (res.ok) {
        Swal.fire({
          type: "success",
          title: "Vellykket",
          text: "Saken er ferdigstilt",
        }).then(() => {
          this.props.history.push("/case");
        });
      } else {
        Swal.fire({
          type: "error",
          title: "Feil",
          text: "Kunne ikke ferdigstille saken, vennligst prøv igjen senere.",
        });
      }
    });
  }

  addGarantiutvalgetToCase(event) {
    event.preventDefault();
    Swal.fire({
      type: "question",
      title: "Koble inn Garantiutvalget?",
      showCancelButton: true,
      confirmButtonText: "Ja",
      cancelButtonText: "Avbryt",
    }).then((result) => {
      if (result.value) {
        this.props
          .addGarantiutvalgetToCase(this.state.soeknadDto.id)
          .then((res) => {
            if (res.ok) {
              Swal.fire({
                type: "success",
                title: "Vellykket",
                text: "Garantiutvalget er koblet inn",
              });
              this.componentDidMount();
            } else {
              Swal.fire({
                type: "error",
                title: "Feil",
                text: "Kunne ikke koble inn Garantiutvalget, vennligst prøv igjen senere.",
              });
            }
          });
      }
    });
  }

  toggleShowSignedOffGarantiutvalgDialog = () => {
    this.setState({
      showSignedOffGarantiuvalgDialog: !this.state.showSignedOffGarantiuvalgDialog,
    });
  }

  toggleShowRejectedGarantiutvalgDialog = () => {
    this.setState({
      showRejectedGarantiuvalgDialog: !this.state.showRejectedGarantiuvalgDialog,
    });
  }

  toggleShowAcceptedGarantiutvalgDialog = () => {
    this.setState({
      showAcceptedGarantiuvalgDialog: !this.state.showAcceptedGarantiuvalgDialog,
    });
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-lg">
            <div className="card">
              <div className="card-header" style={{ fontWeight: "bold" }}>
                Sak
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg">
                    {!this.state.dataLoaded && <Spinner />}
                    {this.state.dataLoaded && (
                      <div>
                        <div className="row">
                          <div className="col-md-6">
                            <h5>Produsent</h5>
                            <hr />
                            {this.state.soeknadDto.produsent ? (
                              <table className="table table-sm">
                                <tbody>
                                <tr>
                                  <th scope="row">Navn</th>
                                  <td>
                                    {this.state.soeknadDto.produsent.navn}
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Produsentnummer</th>
                                  <td>
                                    <Link to={"/producers/details/" + this.state.soeknadDto.produsent.prodnr}>
                                      {this.state.soeknadDto.produsent.prodnr}
                                    </Link>
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Adresse</th>
                                  <td>
                                    {this.state.soeknadDto.produsent.adresse
                                        .adresse1 +
                                      ", " +
                                      this.state.soeknadDto.produsent.adresse
                                        .postnr +
                                      ", " +
                                      this.state.soeknadDto.produsent.adresse
                                        .poststed}
                                  </td>
                                </tr>
                                </tbody>
                              </table>
                            ) : (
                              <h5 style={{color: 'red'}}>Produsent ikke funnet</h5>
                            )}
                          </div>
                          <div className="col-md-6">
                            <h5>Sak detaljer</h5>
                            <hr />
                            <table className="table table-sm">
                              <tbody>
                                <tr>
                                  <th scope="row">Opprettet</th>
                                  <td>
                                    <Moment
                                      unix
                                      locale="no"
                                      format="D. MMM YYYY [kl.] HH:mm"
                                    >
                                      {this.state.soeknadDto.created}
                                    </Moment>{" "}
                                    av {this.state.soeknadDto.createdBy}
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Sist endret</th>
                                  <td>
                                    <Moment
                                      unix
                                      locale="no"
                                      format="D. MMM YYYY [kl.] HH:mm"
                                    >
                                      {this.state.soeknadDto.updated}
                                    </Moment>
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Bank</th>
                                  <td>{this.state.soeknadDto.bankName}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Status</th>
                                  <td
                                    className={getTextColorForCaseStatus(
                                      this.state.soeknadDto.status
                                    )}
                                  >
                                    {this.state.soeknadDto.statusDescription}
                                  </td>
                                </tr>
                                {this.state.soeknadDto
                                  .garantiutvalgetIncluded && (
                                  <tr>
                                    <th scope="row">Garantiutvalget status</th>
                                    <td
                                      className={getTextColorForCaseGarantiutvalgetStatus(
                                        this.state.soeknadDto
                                          .garantiutvalgetStatus
                                      )}
                                    >
                                      {translateShortCaseGarantiutvalgetStatus(
                                        this.state.soeknadDto
                                          .garantiutvalgetStatus
                                      )}
                                    </td>
                                  </tr>
                                )}
                                <tr>
                                  <th scope="row">Driftskredittkonto</th>
                                  <td>
                                    {formatAccountNumber(
                                      this.state.soeknadDto.kontoNummer
                                    )}
                                  </td>
                                </tr>
                                <tr>
                                  <th scope="row">Innvilget driftskreditt</th>
                                  <td>
                                    {formatCurrency(
                                      this.state.soeknadDto.bevilget
                                    )}
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <h4>Vareleveranser</h4>
                        {this.groupVareleveranseOnOrganizationNumber(
                          this.state.soeknadDto.vareleveranseList
                        ).map((element, index) => (
                          <VareleveranseElement
                            key={index}
                            vareleveranseList={element}
                          />
                        ))}
                        <br />
                        <h4>Varsler</h4>
                        <table className="table table-striped">
                          <tbody>
                            {this.state.notices.map((element, index) => (
                              <NoticeElement key={index} notice={element} />
                            ))}
                          </tbody>
                        </table>
                        <br />
                        <div>
                          <InternalNote
                            caseId={this.state.soeknadDto.id}
                          />
                        </div>
                        <br/>
                        {this.state.soeknadDto.garantiutvalgetIncluded && (
                            <div>
                              <h4>Garantiutvalget</h4>
                              <p
                                className={getTextColorForCaseGarantiutvalgetStatus(
                                  this.state.soeknadDto.garantiutvalgetStatus
                                )}
                              >
                                {translateLongCaseGarantiutvalgetStatus(
                                  this.state.soeknadDto.garantiutvalgetStatus
                                )}
                              </p>
                            </div>
                        )}
                        {this.state.garantiutvalgetDto &&
                          this.state.garantiutvalgetDto.text && (
                            <textarea
                              className="form-control form-control-sm"
                              readOnly
                              rows={5}
                              value={this.state.garantiutvalgetDto.text}
                            />
                          )}
                        <br />
                        {this.caseBankOrgNumberIsMatchingLoggedInUser() && (
                          <div>
                            <br />
                            {this.state.soeknadDto.status === "PENDING" && (
                              <TertiaryButton
                                onClick={this.cancelCase}
                                title={"Avbryt sak"}
                                className="d-inline-block me-2"
                              />
                            )}
                            {this.state.soeknadDto.status ===
                              "ALL_VARSEL_HANDLED" && (
                                <PrimaryButton
                                  onClick={this.completeCase}
                                  title={"Ferdigstill"}
                                  className="d-inline-block me-2"
                                  icon={<MdOutlineCheckCircle/>}
                                />
                            )}
                            {(this.state.soeknadDto.status ===
                              "ALL_VARSEL_HANDLED" ||
                              this.state.soeknadDto.status === "PENDING") &&
                              !this.state.soeknadDto
                                .garantiutvalgetIncluded && (
                                <PrimaryButton
                                  onClick={this.addGarantiutvalgetToCase}
                                  title={"Koble inn Garantiutvalget"}
                                  className="d-inline-block me-2"
                                  icon={<MdOutlineRecordVoiceOver/>}
                                />
                              )}
                            {this.state.soeknadDto.status === "COMPLETED" && (
                              <TertiaryButton
                                onClick={this.endCase}
                                title={"Avslutt sak"}
                                className="d-inline-block me-2"
                              />
                            )}
                            {this.state.soeknadDto.status === "COMPLETED" && (
                              <Link to={"/edit-case/" + this.state.soeknadDto.id}>
                                <SecondaryButton
                                  title={"Rediger sak"}
                                  className="d-inline-block me-2"
                                />
                              </Link>
                            )}
                          </div>
                        )}
                        {this.props.user.data.isGarantiutvalget &&
                          this.state.soeknadDto.garantiutvalgetIncluded &&
                          this.state.soeknadDto.garantiutvalgetStatus ===
                            "PENDING" && (
                            <div>
                              <TertiaryButton
                                onClick={this.toggleShowSignedOffGarantiutvalgDialog}
                                title={"Ikke særskilt garanti"}
                                className="d-inline-block me-2"
                              />
                              <TertiaryButton
                                onClick={this.toggleShowRejectedGarantiutvalgDialog}
                                title={"Avslå sak"}
                                className="d-inline-block me-2"
                              />
                              <SecondaryButton
                                onClick={this.toggleShowAcceptedGarantiutvalgDialog}
                                title={"Godkjenn sak"}
                                className="d-inline-block me-2"
                              />
                            </div>
                          )}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Henter vel conversation id fra notices array, der notices som har subType === null:
         <Conversation conversationId={blah} height={400px}/>
          Hvis sakId og mottakerId er tilgjengelig:
          <Conversation caseId={sakId} receiverId={mottakerId} height={400px}/> */}
        <GarantiutvalgetResponse
          title="Ikke særskilt garanti"
          buttonTitle="Send avmelding"
          caseId={this.state.soeknadDto.id}
          sendGarantiutvalgetResponse={this.props.signOffCaseGarantiutvalget}
          showGarantiutvalgetResponseDialog={this.state.showSignedOffGarantiuvalgDialog}
          toggleShowGarantiutvalgetResponseDialog={this.toggleShowSignedOffGarantiutvalgDialog}
          history={this.props.history}
        />
        <GarantiutvalgetResponse
          title="Avslå sak"
          buttonTitle="Send avslag"
          caseId={this.state.soeknadDto.id}
          sendGarantiutvalgetResponse={this.props.rejectCaseGarantiutvalget}
          showGarantiutvalgetResponseDialog={this.state.showRejectedGarantiuvalgDialog}
          toggleShowGarantiutvalgetResponseDialog={this.toggleShowRejectedGarantiutvalgDialog}
          history={this.props.history}
        />
        <GarantiutvalgetResponse
          title="Godkjenn sak"
          buttonTitle="Send garantibrev"
          caseId={this.state.soeknadDto.id}
          sendGarantiutvalgetResponse={this.props.approveCaseGarantiutvalget}
          showGarantiutvalgetResponseDialog={this.state.showAcceptedGarantiuvalgDialog}
          toggleShowGarantiutvalgetResponseDialog={this.toggleShowAcceptedGarantiutvalgDialog}
          history={this.props.history}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const user = getStoreDataForUser(state);
  const oneCase = state.oneCase;
  return {
    user,
    oneCase,
  };
};

const mapDispatchToProps = {
  getCaseById,
  getNoticesForCase,
  cancelCase,
  endCase,
  completeCase,
  addGarantiutvalgetToCase,
  getGarantiutvalgetDataForCase,
  approveCaseGarantiutvalget,
  signOffCaseGarantiutvalget,
  rejectCaseGarantiutvalget,
};

export default connect(mapStateToProps, mapDispatchToProps)(Case);

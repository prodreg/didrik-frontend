import {
  getStoreDataFormdataNoticeTypes,
  getStoreDataFormdataProductTypes,
  getStoreDataFormdataSchemeTypes
} from "../../redux/selectors";
import {connect} from "react-redux";
import {MdInfoOutline} from "react-icons/md";

import React from "react";
import Spinner from "../../components/spinner";
import {
  getFormdataNoticeTypes,
  getFormdataProductTypes, getFormdataSchemeTypes
} from "../../redux/thunks/formdata-thunks";
import {
  createCase, editCase,
  getCaseById
} from "../../redux/thunks/case-thunks";
import {getAllOrganizations} from "../../redux/thunks/organization-thunks";
import {allOrganizationsReducer} from "../../redux/reducers/organization-detail-reducer";
import Swal from "sweetalert2";
import produce from "immer";
import {MdOutlineCancel, MdOutlineEditNote, MdOutlineModeEditOutline} from "react-icons/md";
import PrimaryButton from "../../components/ui/button/primary-button";
import TertiaryButton from "../../components/ui/button/tertiary-button";
import {isNumberNotNegative, validateAccountNumber} from "../../misc/helpers";

const Input = ({
  name,
  value,
  onChange,
  validationError,
  validationErrorText
}) => (
    <div>
      <input type="number"
             className={"form-control form-control-sm " + (validationError ? 'is-invalid' : '')}
             onChange={onChange}
             name={name}
             value={value}/>
      {validationError && (
          <div className="row"><small
              className="text-danger text-small form-text">{validationErrorText}</small>
          </div>
      )}
    </div>
);

const Select = ({
  defaultValue,
  name,
  values,
  onChange,
  validationError,
  validationErrorText
}) => (
    <div>
      <select
          className={"form-select " + (validationError ? 'is-invalid' : '')}
          name={name}
          defaultValue={defaultValue || ''}
          onChange={onChange}>
        <option disabled value={''}
                style={{ display: 'none' }}></option>
        {values.map(
            element => (
                <option
                    key={element.value}
                    value={element.value}
                    disabled={element.disabled}
                >{element.description}</option>
            ))}
      </select>
      {validationError && (
          <div className="row"><small
              className="text-danger text-small form-text">{validationErrorText}</small>
          </div>
      )}
    </div>
);

const VaremottakerDeliveryElement = ({
  that,
  varemottaker,
  varemottakerIndex,
}) => (
    <div className="card bg-light mt-2">
      <div className="card-body">
        <div className="row">
          <div className="col-md-2 mb-3">
            <label className="form-label">Varemottaker</label>
            <Select name="organizationNumber"
                    defaultValue={varemottaker.organizationNumber}
                    onChange={(event) =>
                        that.updateVaremottakerDeliveryField(
                            event,
                            varemottakerIndex)}
                    values={
                      that.props.allOrganizations.data
                      .sort((o1, o2) => o1.name
                      > o2.name ? 1 : -1)
                      .map(o => ({
                        value: o.orgNumber,
                        description: o.name,
                        disabled: that.state.varemottakerDelivery.some(vd => vd.organizationNumber === o.orgNumber)
                      }))
                    }
                    validationError={validationMap.organizationNumber(
                        varemottaker.organizationNumber)
                    && that.state.isFormSubmitted}
                    validationErrorText={"Varemottaker må velges"}
            />
          </div>
          <div className="col-md-6 mb-3">
            <label className="form-label">Ordning</label>
            <Select name="schemeType"
                    defaultValue={varemottaker.schemeType}
                    onChange={(event) =>
                        that.updateVaremottakerDeliveryField(
                            event,
                            varemottakerIndex)}
                    values={
                      that.props.formdataSchemeTypes.data
                      .map(o => ({
                        value: o.value,
                        description: o.description
                      }))
                    }
                    validationError={validationMap.schemeType(
                        varemottaker.schemeType)
                    && that.state.isFormSubmitted}
                    validationErrorText={"Ordning må velges"}
            />
          </div>
          <div className="col-md-2 mb-3">
            <label className="form-label">Varseltype</label>
            <Select name="noticeType"
                    selectValue={varemottaker.noticeType}
                    validationError={validationMap.noticeType(
                        varemottaker.noticeType)
                      && that.state.isFormSubmitted}
                    validationErrorText={"Varseltype må velges"}
                    onChange={(event) =>
                      that.updateVaremottakerDeliveryField(
                        event,
                        varemottakerIndex)}
                    values={
                      that.props.formdataNoticeTypes.data
                        .filter(o => {
                          if (o.value !== "SLETTING") {
                            return {
                              value: o.value,
                              description: o.description
                            }
                          }
                        })
                    }
            />
          </div>
          <div className="col-md-2 mb-3 ">
            <div
                className="d-flex justify-content-center align-items-end h-100">
              <TertiaryButton
                title="Fjern mottaker"
                onClick={(e) => that.removeVaremottakerDelivery(e, varemottakerIndex)}
              />
            </div>
          </div>
        </div>
        <hr/>
        {varemottaker.goodsDelivery.map(
            (goods, goodsIndex) => (
                <GoodsDeliveryElement key={goodsIndex} that={that}
                                      varemottaker={varemottaker}
                                      varemottakerIndex={varemottakerIndex}
                                      goods={goods} goodsIndex={goodsIndex}/>
            )
        )}
        <div className="row">
          <div className="col">
            <PrimaryButton
              title="Legg til vareleveranse"
              className="btn-sm float-end"
              onClick={(e) => that.addGoodsDelivery(e,varemottakerIndex)}
            />
          </div>
        </div>
      </div>
    </div>
)

const GoodsDeliveryElement = ({
  that,
  varemottaker,
  varemottakerIndex,
  goods,
  goodsIndex
}) => (
    <div className="row">
      <div className="col-md-1">
        <div className="row h-100">
          <div
              className="d-flex justify-content-center align-items-center h-100">
            <TertiaryButton
              title="Fjern"
              onClick={(e) => that.removeGoodsDelivery(e, varemottakerIndex, goodsIndex)}
            />
          </div>
        </div>
      </div>
      <div className="col-md-11">
        <div className="row">
          <div className="col-md-4 mb-3">
            <label className="form-label">Produktslag</label>
            <Select name="productType"
                    defaultValue={goods.productType}
                    onChange={(event) =>
                        that.updateGoodsDeliveryField(
                            event,
                            varemottakerIndex, goodsIndex)}
                    values={
                      that.props.formdataProductTypes.data
                      .map(o => ({
                        value: o.value,
                        description: o.description
                      }))
                    }
                    validationError={validationMap.productType(
                        goods.productType)
                    && that.state.isFormSubmitted}
                    validationErrorText={"Produktslag må velges"}
            />
          </div>
          <div className="col-md-4 mb-3">
            <label className="form-label">Årlig beløp overført til bank</label>
            <Input
                name="settlement"
                value={goods.settlement}
                onChange={(event) => that.updateGoodsDeliveryField(event,
                    varemottakerIndex, goodsIndex)}
                validationError={validationMap.settlement(
                    goods.settlement)
                && that.state.isFormSubmitted}
                validationErrorText={"Årlig beløp overført til bank kan ikke være tom"}
            />
          </div>
          <div className="col-md-4 mb-3">
            <label className="form-label">År</label>
            <Input
                name="year"
                value={goods.year}
                onChange={(event) => that.updateGoodsDeliveryField(event,
                    varemottakerIndex, goodsIndex)}
                validationError={validationMap.year(goods.year)
                && that.state.isFormSubmitted}
                validationErrorText={"År kan ikke være tom"}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 mb-3">
            <div className="form-floating">
              <textarea className="form-control form-control-sm"
                        id="floatingTextarea2"
                        style={{ height: '100px' }}
                        name="text"
                        value={varemottaker.text}
                        onChange={(event) =>
                            that.updateGoodsDeliveryField(
                                event,
                                varemottakerIndex,
                                goodsIndex)}></textarea>
              <label htmlFor="floatingTextarea2">Ytterligere informasjon</label>
            </div>
          </div>
        </div>
      </div>
    </div>
)

const validationMap = {
  accountNumber: (accountNumber) => !validateAccountNumber(accountNumber),
  granted: (granted) => !isNumberNotNegative(granted),

  organizationNumber: (organizationNumber) => !organizationNumber,
  schemeType: (schemeType) => !schemeType,
  productType: (productType) => !productType,
  settlement: (settlement) => !isNumberNotNegative(settlement),
  year: (year) => !isNumberNotNegative(year),
  noticeType: (noticeType) => !noticeType,
}

class EditCase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formdataLoaded: false,
      isFormSubmitted: false,

      soeknadDto: {},

      granted: '',
      accountNumber: '',
      status: 'DRAFT',
      processedByGarantiutvalaget: false,

      varemottakerDelivery: [],
    }

    this.garantiutvalgetText = this.garantiutvalgetText.bind(this);
    this.garantiutvlagetCheckbox = this.garantiutvlagetCheckbox.bind(this);
    this.addVaremottakerDelivery = this.addVaremottakerDelivery.bind(this);
    this.addGoodsDelivery = this.addGoodsDelivery.bind(this);
    this.removeVaremottakerDelivery = this.removeVaremottakerDelivery.bind(
        this);
    this.removeGoodsDelivery = this.removeGoodsDelivery.bind(this);
    this.updateGoodsDeliveryField = this.updateGoodsDeliveryField.bind(this);
    this.updateVaremottakerDeliveryField = this.updateVaremottakerDeliveryField.bind(
        this);
    this.updateField = this.updateField.bind(this);
    this.getOrganizationNameForOrganizationNumber = this.getOrganizationNameForOrganizationNumber.bind(this);
    this.calculateNoticeTypes = this.calculateNoticeTypes.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.isFormInvalid = this.isFormInvalid.bind(this);
  }

  componentDidMount() {
    Promise.all([
      this.props.getAllOrganizations(['mottaker']),
      this.props.getFormdataNoticeTypes(),
      this.props.getFormdataProductTypes(),
      this.props.getFormdataSchemeTypes(),
      this.props.getCaseById(this.props.match.params.caseId),
    ]).then((results) => {
      const soeknadDto = results[4];

      const varemottakerDelivery = []
      for(let i = 0; i < soeknadDto.vareleveranseList.length; i++) {
        const vareleveranse = soeknadDto.vareleveranseList[i];
        const foundIndex = varemottakerDelivery.findIndex(e => e.organizationNumber === vareleveranse.varemottaker.orgNumber);
        if(foundIndex === -1) {
          varemottakerDelivery.push({
            organizationNumber: vareleveranse.varemottaker.orgNumber,
            schemeType: vareleveranse.ordning,
            goodsDelivery: [{
                productType: vareleveranse.produktslag,
                settlement: vareleveranse.oppgjoer,
                year: vareleveranse.aar,
                text: vareleveranse.text,
              }
            ]
          })
        }
        else {
          varemottakerDelivery[foundIndex].goodsDelivery.push({
            productType: vareleveranse.produktslag,
            settlement: vareleveranse.oppgjoer,
            year: vareleveranse.aar,
            text: vareleveranse.text,
          })
        }

      }

      this.setState({
        formdataLoaded: true,
        soeknadDto: soeknadDto,
        granted: soeknadDto.bevilget,
        accountNumber: soeknadDto.kontoNummer,
        varemottakerDelivery: varemottakerDelivery,
      })
    })
  }

  garantiutvalgetText() {
    Swal.fire({
      text: "Garantiutvalget behandler kun søknader om særskilt garanti (fremtidig leveranse, konkurs etc.) og kun for samvirkeordningen.",
    })
  }

  garantiutvlagetCheckbox() {
    if (!this.state.processedByGarantiutvalaget) {
      this.garantiutvalgetText();
    }
    this.setState({
      processedByGarantiutvalaget: !this.state.processedByGarantiutvalaget
    })
  }

  addVaremottakerDelivery(event) {
    event.preventDefault();
    this.setState(
        produce(draft => {
          draft.varemottakerDelivery.push({
            organizationNumber: '',
            schemeType: '',
            noticeType: 'NY',
            goodsDelivery: [
              {
                productType: '',
                settlement: '',
                year: '',
                text: '',
              }
            ]
          })
        })
    )
  }

  addGoodsDelivery(event, indexVaremottakerDelivery) {
    event.preventDefault();
    this.setState(
        produce(draft => {
          draft.varemottakerDelivery[indexVaremottakerDelivery].goodsDelivery.push(
              {
                productType: '',
                settlement: '',
                year: '',
                text: '',
              }
          )
        })
    )
  }

  removeVaremottakerDelivery(event, index) {
    event.preventDefault();
    this.setState(
        produce(draft => {
          draft.varemottakerDelivery.splice(index, 1)
        })
    )
  }

  removeGoodsDelivery(event, indexVaremottakerDelivery, indexGoodsDelivery) {
    event.preventDefault();
    if (this.state.varemottakerDelivery[indexVaremottakerDelivery].goodsDelivery.length
        === 1) {
      this.removeVaremottakerDelivery(event, indexVaremottakerDelivery);
    } else {
      this.setState(
          produce(draft => {
            draft.varemottakerDelivery[indexVaremottakerDelivery].goodsDelivery.splice(
                indexGoodsDelivery, 1)
          })
      )
    }
  }

  updateField(event) {
    event.preventDefault();
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  updateVaremottakerDeliveryField(event, index) {
    event.preventDefault();
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState(
        produce(draft => {
          draft.varemottakerDelivery[index][name] = value
        })
    )
  }

  updateGoodsDeliveryField(event, indexVaremottakerDelivery,
      indexGoodsDelivery) {
    event.preventDefault();
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState(
        produce(draft => {
          draft.varemottakerDelivery[indexVaremottakerDelivery].goodsDelivery[indexGoodsDelivery][name] = value
        })
    )
  }

  isFormInvalid() {
    if (validationMap.accountNumber(this.state.accountNumber)
        || validationMap.accountNumber(this.state.accountNumber)) {
      return true;
    }
    if (this.state.varemottakerDelivery.length === 0) {
      return true;
    }

    for (let varemottakerIndex = 0;
        varemottakerIndex < this.state.varemottakerDelivery.length;
        varemottakerIndex++) {
      if (validationMap.organizationNumber(this.state.varemottakerDelivery[varemottakerIndex].organizationNumber) ||
          validationMap.schemeType(this.state.varemottakerDelivery[varemottakerIndex].schemeType) ||
          validationMap.noticeType(this.state.varemottakerDelivery[varemottakerIndex].noticeType)
      ) {
        return true;
      }

      for (let goodsIndex = 0; goodsIndex < this.state.varemottakerDelivery[varemottakerIndex].goodsDelivery.length; goodsIndex++) {
        if (validationMap.productType(this.state.varemottakerDelivery[varemottakerIndex].goodsDelivery[goodsIndex].productType) ||
            validationMap.settlement(this.state.varemottakerDelivery[varemottakerIndex].goodsDelivery[goodsIndex].settlement) ||
            validationMap.year(this.state.varemottakerDelivery[varemottakerIndex].goodsDelivery[goodsIndex].year)
        ) {
          return true;
        }
      }
    }

    return false;
  }

  getOrganizationNameForOrganizationNumber(organizationNumber) {
    if(!organizationNumber) {
      return "";
    }
    return this.props.allOrganizations.data.filter(
        organization =>
            organization.orgNumber === organizationNumber
    )[0].name;
  }

  calculateNoticeTypes() {
    const notices = [];

    this.state.varemottakerDelivery
    .filter(varemottakerDelivery => varemottakerDelivery.organizationNumber)
    .forEach(varemottakerDelivery => {
      //ENDRING
      if (this.state.soeknadDto.vareleveranseList.find(vareleveranse =>
          vareleveranse.varemottaker.orgNumber
          === varemottakerDelivery.organizationNumber)) {
        notices.push({
          organizationNumber: varemottakerDelivery.organizationNumber,
          organizationName: this.getOrganizationNameForOrganizationNumber(varemottakerDelivery.organizationNumber),
          noticeType: 'ENDRING'
        })
      }
      //NY
      else {
        notices.push({
          organizationNumber: varemottakerDelivery.organizationNumber,
          organizationName: this.getOrganizationNameForOrganizationNumber(varemottakerDelivery.organizationNumber),
          noticeType: 'NY'
        })
      }
    });

    //SLETTING
    this.state.soeknadDto.vareleveranseList.forEach(vareleveranse => {
      if(!this.state.varemottakerDelivery.find(varemottakerDelivery =>vareleveranse.varemottaker.orgNumber
          === varemottakerDelivery.organizationNumber)) {
        notices.push({
          organizationNumber: vareleveranse.varemottaker.orgNumber,
          organizationName: this.getOrganizationNameForOrganizationNumber(vareleveranse.varemottaker.orgNumber),
          noticeType: 'SLETTING'
        })
      }
    });

    return notices;
  }

  submitForm(event) {
    event.preventDefault();

    if (this.isFormInvalid()) {
      this.setState({
        isFormSubmitted: true
      })
      Swal.fire({
        type: 'error',
        title: 'Valideringsfeil',
        text: 'Vennligst gjennomgå skjema og rett felter markert med rødt. Merk at du må ha minst en vareleveranse.',
      })
    } else {
      Swal.fire({
        title: 'Rediger sak?',
        html: '<p style="display: block;">-Varseltype «Ny» brukes ved registrering av nye leveranser. Varemottaker må gi tilbakemelding før banken kan fullføre.<br><br>-Varseltype «Endring» skal kun brukes ved endring av leveranse i eksisterende notifikasjon. Varemottaker blir orientert, men er ikke påkrevd å svare.<br><br>Husk å krysse av hvis saken skal ha særskilt garanti. Se boks nede til venstre.<br><br>Vil du fortsette?</p>',
        showCancelButton: true,
        confirmButtonText: 'Ja',
        cancelButtonText: 'Avbryt'
      }).then((result) => {
        if (result.value) {
          const vareleveranseList = [];
          this.state.varemottakerDelivery.forEach(varemottakerDelivery => {
            varemottakerDelivery.goodsDelivery.forEach(goodsDelivery => {
                  vareleveranseList.push({
                    varemottaker: this.props.allOrganizations.data.filter(
                        organization =>
                            organization.orgNumber === varemottakerDelivery.organizationNumber
                    )[0],
                    ordning: varemottakerDelivery.schemeType,
                    produktslag: goodsDelivery.productType,
                    oppgjoer: goodsDelivery.settlement,
                    aar: goodsDelivery.year,
                    text: goodsDelivery.text,
                  })
                }
            )
          })

          const editCase = {
            ...this.state.soeknadDto,
            bevilget: this.state.granted,
            kontoNummer: this.state.accountNumber,
            status: 'DRAFT',
            parentId: this.state.soeknadDto.id,
            garantiutvalgetIncluded: this.state.processedByGarantiutvalaget,
            vareleveranseList: vareleveranseList,
            varselTypePerMottakerMap: this.calculateNoticeTypes().reduce(
                (map, element) => {
                  map[element.organizationNumber] = element.noticeType;
                  return map;
                }, {}),
          };
          this.props.editCase(editCase).then(() => {
            Swal.fire(
                'Vellykket',
                'Sak er oppdatert',
                'success'
            )
            this.props.history.push({
              pathname: "/",
            });
          });
        }
      })
    }
  }

  render() {
    return (
        <div>
          <div className="row">
            <div className="col-lg">
              <div className="card">
                <div className="card-header" style={{ fontWeight: 'bold' }}>
                  Rediger sak
                </div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-lg">
                      {!this.state.formdataLoaded && (
                          <Spinner/>
                      )}
                      {this.state.formdataLoaded && (
                          <form>
                            <div className="mt-2">
                              <div className="row">
                                <div className="col-md-4 mb-3">
                                  <label className="form-label">
                                    Produsentens navn
                                  </label>
                                  <input readOnly type="text"
                                         className="form-control form-control-sm"
                                         value={this.state.soeknadDto.produsent.navn}/>
                                </div>
                                <div className="col-md-4 mb-3">
                                  <label className="form-label">
                                    Produsentnummer
                                  </label>
                                  <input readOnly type="text"
                                         className="form-control form-control-sm"
                                         value={this.state.soeknadDto.produsent.prodnr}/>
                                </div>
                                <div className="col-md-4 mb-3">
                                  <label className="form-label">
                                    Bankkontonummer
                                  </label>
                                  <Input
                                      name="accountNumber"
                                      value={this.state.accountNumber}
                                      onChange={this.updateField}
                                      validationError={validationMap.accountNumber(
                                          this.state.accountNumber)
                                      && this.state.isFormSubmitted}
                                      validationErrorText={"Kontonummer kan ikke være tomt"}
                                  />
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-6 mb-3">
                                  <label className="form-label">
                                    Adresse
                                  </label>
                                  <input readOnly type="text"
                                         className="form-control form-control-sm"
                                         value={this.state.soeknadDto.produsent.adresse.adresse1}/>
                                </div>
                                <div className="col-md-3 mb-3">
                                  <label className="form-label">
                                    Postnummer
                                  </label>
                                  <input readOnly type="text"
                                         className="form-control form-control-sm"
                                         value={this.state.soeknadDto.produsent.adresse.postnr}/>
                                </div>
                                <div className="col-md-3 mb-3">
                                  <label className="form-label">
                                    Poststed
                                  </label>
                                  <input readOnly type="text"
                                         className="form-control form-control-sm"
                                         value={this.state.soeknadDto.produsent.adresse.poststed}/>
                                </div>
                              </div>
                            </div>
                            <div className="card mt-2">
                              <div className="card-header">
                                Vareleveranser
                              </div>
                              <div className="card-body bg-white">
                                {this.state.varemottakerDelivery.map(
                                    (varemottaker, varemottakerIndex) => (
                                        <VaremottakerDeliveryElement
                                            key={varemottakerIndex}
                                            that={this}
                                            varemottaker={varemottaker}
                                            varemottakerIndex={varemottakerIndex}/>
                                    )
                                )}
                                <PrimaryButton
                                  title="Legg til varemottaker"
                                  className="float-end mt-4"
                                  onClick={(e) => this.addVaremottakerDelivery(e)}
                                />
                              </div>
                            </div>
                            <div className="card mt-2">
                              <div className="card-header">
                                Varsler
                              </div>
                              <div className="card-body bg-white">
                                {this.calculateNoticeTypes().map((varsel, varselIndex) => (
                                    <div className="row mt-2" key={varselIndex}>
                                      <div className="card-body bg-light">
                                        <div className="row col">
                                          {varsel.noticeType === 'ENDRING' && (
                                              <div className="d-flex">
                                                <MdOutlineEditNote className="me-1" style={{width:"40px"}} size="40px"/>
                                                <span className="ms-1 flex-grow-1" style={{lineHeight:'40px'}}>{varsel.organizationName}: Varsel om endring av kreditt</span>
                                              </div>
                                          )}
                                          {varsel.noticeType === 'NY' && (
                                              <div className="d-flex">
                                                <MdOutlineModeEditOutline className="me-1" style={{width:"40px"}} size="40px"/>
                                                <span className="ms-1 flex-grow-1" style={{lineHeight:'40px'}}>{varsel.organizationName}: Varsel om opprettelse av kreditt</span>
                                              </div>
                                          )}
                                          {varsel.noticeType === 'SLETTING' && (
                                              <div className="d-flex">
                                                <MdOutlineCancel className="me-1" style={{width:"40px"}} size="40px"/>
                                                <span className="ms-1 flex-grow-1" style={{lineHeight:'40px'}}>{varsel.organizationName}: Varsel om sletting av kreditt</span>
                                              </div>
                                          )}
                                        </div>
                                      </div>
                                    </div>
                                ))}
                              </div>
                            </div>
                            <div className="card bg-light mt-2">
                              <div className="card-body">
                                <div className="row">
                                  <div className="col">
                                    <label className="form-label">
                                      Bevilget kroner
                                    </label>
                                    <Input
                                        name="granted"
                                        value={this.state.granted}
                                        onChange={this.updateField}
                                        validationError={validationMap.granted(
                                            this.state.granted)
                                        && this.state.isFormSubmitted}
                                        validationErrorText={"Bevilget kroner kan ikke være tom"}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="card bg-light mt-2">
                              <div className="card-body">
                                <div className="row">
                                  <div className="col-md-12">
                                    <div className="form-check">
                                      <input className="form-check-input"
                                             type="checkbox"
                                             checked={this.state.behandleAvGaratiutvalget}
                                             onChange={this.garantiutvlagetCheckbox}/>
                                      <label className="form-check-label">
                                        Saken skal også behandles av
                                        Garantiutvalget {' '}
                                        <MdInfoOutline className="info-button" style={{ cursor: 'pointer' }}
                                                onClick={(e) => this.garantiutvalgetText(e)}/>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md-12 mt-3">
                                <PrimaryButton
                                  title="Send endringer"
                                  onClick={this.submitForm}
                                />
                              </div>
                            </div>

                          </form>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  const formdataSchemeTypes = getStoreDataFormdataSchemeTypes(state);
  const formdataProductTypes = getStoreDataFormdataProductTypes(state);
  const formdataNoticeTypes = getStoreDataFormdataNoticeTypes(state);
  const allOrganizations = state.allOrganizations;
  return {
    formdataSchemeTypes,
    formdataProductTypes,
    formdataNoticeTypes,
    allOrganizations
  };
}

const mapDispatchToProps = {
  getFormdataNoticeTypes,
  getFormdataProductTypes,
  getFormdataSchemeTypes,
  getCaseById,
  editCase,
  getAllOrganizations
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCase)
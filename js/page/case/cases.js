import {Link} from "react-router-dom";
import {
  getStoreDataForAllCases,
  getStoreDataForUser
} from "../../redux/selectors";
import {getAllCases} from "../../redux/thunks/case-thunks";
import {connect} from "react-redux";

import React from "react";
import Spinner from "../../components/spinner";
import {MdOutlineChevronLeft, MdOutlineChevronRight} from "react-icons/md";
import {MdOutlineCheckCircleOutline, MdOutlineClose, MdOutlineHourglassTop} from "react-icons/md";
import ReactPaginate from "react-paginate";
import Moment from "react-moment";
import {MdOutlineFormatListBulleted} from "react-icons/md";
import Container from "../../components/ui/container/container";
import SearchForm from "../../components/ui/search-form/search-form";

class Cases extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      //Bank and admin
      checkShowPendingCases: localStorage.getItem("checkShowPendingCases") === null ? true : localStorage.getItem("checkShowPendingCases") === 'true',
      checkShowActiveCases: localStorage.getItem("checkShowActiveCases") === 'true',
      checkShowCompletedCases: localStorage.getItem("checkShowCompletedCases") === 'true',

      //Garantiutvalget
      checkShowPendingGarantiutvalgetCases: localStorage.getItem("checkShowPendingGarantiutvalgetCases") === null ? true : localStorage.getItem("checkShowPendingGarantiutvalgetCases") === 'true',
      checkShowCompletedGarantiutvalgetCases: localStorage.getItem("checkShowCompletedGarantiutvalgetCases") === 'true',


      search: localStorage.getItem("search_case") || '',

      rowsPerPage: 8,
      currentPage: parseInt(localStorage.getItem("page_number_case")) || 0,
      scrollPosition: parseInt(localStorage.getItem("scroll_cases")) || 0
    }

    this.handleCheckChange = this.handleCheckChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.filterCases = this.filterCases.bind(this);
    this.paginationSlice = this.paginationSlice.bind(this);
  }

  handleCheckChange(event) {
    const target = event.target;
    const name = target.name;
    localStorage.setItem(name, !this.state[name]);
    localStorage.setItem("page_number_case", 0);

    this.setState({
      [name]: !this.state[name],
      currentPage: 0
    });
  }

  handleSearchChange(event) {
    const query = event.target.value;
    localStorage.setItem("page_number_case", 0);
    localStorage.setItem("search_case", query);
    this.setState({
      search: query,
      currentPage: 0
    })
  }

  handlePageChange({ selected }) {
    localStorage.setItem("page_number_case", selected);
    this.setState({
      currentPage: selected
    })
  };

  paginationSlice(cases) {
    return cases.slice(
        this.state.currentPage * this.state.rowsPerPage,
        (this.state.currentPage * this.state.rowsPerPage)
        + this.state.rowsPerPage,
    );
  }

  filterCases(cases) {
    const search = this.state.search.toLowerCase();

    return cases
    .filter(element => {
      if(this.props.user.data.isBank || this.props.user.data.isAdmin) {
        if (this.state.checkShowPendingCases && (element.status === 'PENDING' ||
            element.status === 'ALL_VARSEL_HANDLED')) {
          return true;
        }
        if (this.state.checkShowActiveCases && element.status === 'COMPLETED') {
          return true;
        }
        if (this.state.checkShowCompletedCases && (element.status === 'CLOSED'
            ||
            element.status === 'CANCELLED')) {
          return true;
        }
      }
      if(this.props.user.data.isGarantiutvalget) {
        if (this.state.checkShowPendingGarantiutvalgetCases && element.garantiutvalgetStatus === 'PENDING') {
          return true;
        }
        if (this.state.checkShowCompletedGarantiutvalgetCases && (element.garantiutvalgetStatus === 'ACCEPTED' ||
            element.garantiutvalgetStatus === 'REJECTED')) {
          return true;
        }
      }

      return false;
    })
    .filter(element => {
      if ((element.kontoNummer || '').includes(search)) {
        return true;
      }
      if (element.bankName.toLowerCase().includes(search)) {
        return true;
      }
      if (element.bankId.includes(search)) {
        return true;
      }

      if (element.produsent) {
        if (element.produsent.prodnr && element.produsent.prodnr.includes(search)) {
          return true;
        }
        if (element.produsent.orgnr && element.produsent.orgnr.includes(search)) {
          return true;
        }
        if (element.produsent.navn && element.produsent.navn.toLowerCase().includes(search)) {
          return true;
        }

        for (let index = 0; index < element.produsent.allProdNr.length;
            index++) {
          if (element.produsent.allProdNr[index] && element.produsent.allProdNr[index].includes(search)) {
            return true;
          }
        }
        for (let index = 0; index < element.produsent.allOrgNr.length;
            index++) {
          if (element.produsent.allOrgNr[index] && element.produsent.allOrgNr[index].includes(search)) {
            return true;
          }
        }
      }

      return false
    })
  }

  handleScroll() {
    localStorage.setItem("scroll_cases", window.scrollY);
  }

  componentDidMount() {
    this.props.getAllCases();
    window.scrollTo(0, this.state.scrollPosition);
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render() {
    const allCases = this.props.allCases;
    const allCasesFiltered = this.filterCases(allCases.data);
    allCasesFiltered.sort((a, b) => {
      return b.created - a.created;
    });
    const paginationSlice = this.paginationSlice(allCasesFiltered)

    return (
        <Container
            title={"Saker"}
            description={"Oversikt over alle saker"}
            icon={<MdOutlineFormatListBulleted size="60px"/>}
        >
          <div className="row">
            <div className="col-lg">
              <div className="row">
                <div className="col-lg">
                  {allCases.loading && (
                      <Spinner/>
                  )}
                  {allCases.loaded && (
                      <div>
                        <div className="row">
                          <div className="col-md">
                            <div className="mb-3">
                              <SearchForm
                                type="text"
                                id="searchInput"
                                value={this.state.search}
                                placeholder="Søk"
                                className=""
                                onChange={this.handleSearchChange}
                              />
                            </div>
                          </div>
                        </div>
                        {(this.props.user.data.isBank || this.props.user.data.isAdmin) && (
                            <div className="row">
                              <div className="col-md">
                                <div className="form-check">
                                  <input className="form-check-input"
                                         type="checkbox"
                                         name="checkShowPendingCases"
                                         checked={this.state.checkShowPendingCases}
                                         onChange={this.handleCheckChange}
                                         id="checkShowPendingCases"/>
                                  <label className="form-check-label"
                                         htmlFor="checkShowPendingCases">
                                    Vis saker under behandling
                                  </label>
                                </div>
                                <div className="form-check">
                                  <input className="form-check-input"
                                         type="checkbox"
                                         name="checkShowActiveCases"
                                         checked={this.state.checkShowActiveCases}
                                         onChange={this.handleCheckChange}
                                         id="checkShowActiveCases"/>
                                  <label className="form-check-label"
                                         htmlFor="checkShowActiveCases">
                                    Vis ferdigstilte saker
                                  </label>
                                </div>
                                <div className="form-check">
                                  <input className="form-check-input"
                                         type="checkbox"
                                         name="checkShowCompletedCases"
                                         checked={this.state.checkShowCompletedCases}
                                         onChange={this.handleCheckChange}
                                         id="checkShowCompletedCases"/>
                                  <label className="form-check-label"
                                         htmlFor="checkShowCompletedCases">
                                    Vis avsluttede saker
                                  </label>
                                </div>
                              </div>
                            </div>
                        )}
                        {(this.props.user.data.isGarantiutvalget) && (
                            <div className="row">
                              <div className="col-md">
                                <div className="form-check">
                                  <input className="form-check-input"
                                         type="checkbox"
                                         name="checkShowPendingGarantiutvalgetCases"
                                         checked={this.state.checkShowPendingGarantiutvalgetCases}
                                         onChange={this.handleCheckChange}
                                         id="checkShowPendingGarantiutvalgetCases"/>
                                  <label className="form-check-label"
                                         htmlFor="checkShowPendingGarantiutvalgetCases">
                                    Vis saker under behandling hos Garantiutvalget
                                  </label>
                                </div>
                                <div className="form-check">
                                  <input className="form-check-input"
                                         type="checkbox"
                                         name="checkShowCompletedGarantiutvalgetCases"
                                         checked={this.state.checkShowCompletedGarantiutvalgetCases}
                                         onChange={this.handleCheckChange}
                                         id="checkShowCompletedGarantiutvalgetCases"/>
                                  <label className="form-check-label"
                                         htmlFor="checkShowCompletedGarantiutvalgetCases">
                                    Vis saker som er ferdig behandlet i Garantiutvalget
                                  </label>
                                </div>
                              </div>
                            </div>
                        )}
                        <div className="row">
                          <div className="col-md">
                            <br/>
                            <table className="table table-sm table-striped">
                              <tbody>
                              {paginationSlice.map(
                                  caseElement => {
                                    return (
                                        <tr key={caseElement.id}>
                                          <td className="col-2">
                                            {(this.props.user.data.isBank || this.props.user.data.isAdmin) && (
                                                <span className="ms-2">
                                                  {caseElement.status === 'PENDING' && (
                                                      <MdOutlineHourglassTop/>
                                                  )}
                                                  {(caseElement.status === 'COMPLETED' || caseElement.status === 'ALL_VARSEL_HANDLED') && (
                                                      <MdOutlineCheckCircleOutline/>
                                                  )}
                                                  {(caseElement.status === 'CLOSED' || caseElement.status === 'CANCELLED') && (
                                                      <MdOutlineClose/>
                                                  )}
                                                </span>
                                              )}
                                              {(this.props.user.data.isGarantiutvalget) && (
                                                  <span className="ms-2">
                                                    {(caseElement.garantiutvalgetStatus === 'PENDING') && (
                                                        <MdOutlineHourglassTop/>
                                                    )}
                                                    {(caseElement.garantiutvalgetStatus === 'ACCEPTED' || caseElement.garantiutvalgetStatus === 'REJECTED') && (
                                                        <MdOutlineCheckCircleOutline/>
                                                    )}
                                                  </span>
                                              )}
                                            <small className="ms-3">{caseElement.statusDescription}</small>
                                          </td>
                                          <td className="col-8">
                                            <Link to={"/case/"
                                            + caseElement.id}>
                                              {caseElement.produsent
                                                  ? caseElement.produsent.navn
                                                  + " - "
                                                  + caseElement.produsent.prodnr
                                                  : 'Produsent ikke funnet'}
                                            </Link>
                                            <small
                                                className="d-block text-muted">{caseElement.bankName}</small>
                                          </td>
                                          <td className="col-2"
                                              style={{ textAlign: 'right' }}>
                                            <small>
                                              <span className="text-muted">Sendt til varemottakere </span>
                                              <Moment unix locale="no"
                                                      format="D. MMM YYYY [kl.] HH:mm">
                                                {caseElement.created}
                                              </Moment>
                                            </small>
                                          </td>
                                        </tr>
                                    );
                                  })}
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md">
                            <ReactPaginate
                                previousLabel={<MdOutlineChevronLeft/>}
                                nextLabel={<MdOutlineChevronRight/>}
                                breakLabel={"..."}
                                forcePage={this.state.currentPage}
                                pageCount={allCasesFiltered.length
                                    / this.state.rowsPerPage}
                                onPageChange={this.handlePageChange}
                                marginPagesDisplayed={3}
                                pageRangeDisplayed={3}
                                containerClassName={"pagination"}
                                pageClassName={"page-item"}
                                pageLinkClassName={"page-link"}
                                previousClassName={"page-item"}
                                previousLinkClassName={"page-link"}
                                nextLinkClassName={"page-link"}
                                nextClassName={"page-item"}
                                breakClassName={"page-item"}
                                breakLinkClassName={"page-link"}
                                activeClassName={"active"}
                            />
                          </div>
                        </div>
                      </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const allCases = getStoreDataForAllCases(state);
  const user = getStoreDataForUser(state);
  return {
    user,
    allCases
  };
}

const mapDispatchToProps = {
  getAllCases
}

export default connect(mapStateToProps, mapDispatchToProps)(Cases)
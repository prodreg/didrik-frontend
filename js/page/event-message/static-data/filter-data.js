export default [
  {
    value: "Alle meldingstyper",
    type: "ALLE",
  },
  { value: "Garantiutvalget behandlet", type: "GARANTIUTVALGET_BEHANDLET" },
  { value: "Garantiutvalget ny", type: "GARANTIUTVALGET_NY" },
  { value: "Sak kansellert", type: "SAK_KANSELLERT" },
  { value: "Varsel endring", type: "VARSEL_ENDRING" },
  { value: "Varsel kansellert", type: "VARSEL_KANSELLERT" },
  { value: "Varsel ny", type: "VARSEL" },
  { value: "Varsel sletting", type: "VARSEL_SLETTING" },
];

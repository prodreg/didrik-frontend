import React from "react";
import Container from "../../components/ui/container/container";
import { MdOutlineMessage } from "react-icons/md";
import EventMessageList from "./event-message-list";

const EventMessages = () => {
  return (
    <Container
      title="Meldinger"
      description="Viser meldinger"
      icon={<MdOutlineMessage size="58px" />}
    >
      <EventMessageList />
    </Container>
  );
};

export default React.memo(EventMessages);

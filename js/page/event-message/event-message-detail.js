import React, { useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  getEventMessageById,
  setEventMessageIsRead,
  softDeleteEventMessage,
  setMessageToWorkInProgress,
} from "../../redux/thunks/event-message-thunks";
import Swal from "sweetalert2";
import Plain from "./template/message-detail-template/plain";
import AllVarselHandledDetail from "./template/message-detail-template/all_varsel_handled_detail";
import GarantiutvalgetBehandletDetail from "./template/message-detail-template/garantiutvalget_behandlet_detail";
import GarantiutvalgetNyDetail from "./template/message-detail-template/garantiutvalget_ny_detail";
import NewMessagePostedInConversationDetail from "./template/message-detail-template/new_message_posted_in_conversation_detail";
import SakKansellertDetail from "./template/message-detail-template/sak_kansellert_detail";
import SystemDetail from "./template/message-detail-template/system_detail";
import VarselDetail from "./template/message-detail-template/varsel_detail";
import VarselEndringDetail from "./template/message-detail-template/varsel_endring_detail";
import VarselKansellertDetail from "./template/message-detail-template/varsel_kansellert_detail";
import VarselSlettingDetail from "./template/message-detail-template/varsel_sletting_detail";
import Container from "../../components/ui/container/container";
import { MdOutlineMailOutline } from "react-icons/md";
import Actions from "../../redux/actions/event-message-actions";
import "./event-message-detail.scss";
import SecondaryButton from "../../components/ui/button/secondary-button";

const EventMessageDetail = (props) => {
  const messageId = props.match.params.id;
  const eventMessage = useSelector((state) => state.eventMessageIdReducer);
  const eventMessageCounter = useSelector((state) => state.counterReducer);
  const eventMessages = useSelector((state) => state.eventMessageReducer);
  const [messageDetail, setMessageDetail] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getEventMessageById(messageId)).then((response) => {
      handleEventMessageResponse(response, messageId);
    });
  }, [dispatch]);

  useEffect(() => setMessageDetail(eventMessage.data), [eventMessage]);

  const forceUpdate = useCallback(
    () => setMessageDetail([...eventMessage.data]),
    [eventMessage.data]
  );

  const handleEventMessageResponse = (response, messageId) => {
    if (response.payload.error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke laste inn meldingsdetaljer. Prøv igjen senere",
        "error"
      );
    } else if (!response.payload.data.read) {
      if (!response.payload.data.softDeleted) {
        dispatch(Actions.setNumberOfUnreadMessages(eventMessageCounter - 1));
      }
      dispatch(setEventMessageIsRead(messageId, true)).then(
        () =>
          (eventMessages.data.filter((e) => e.id === messageId)[0].read = true)
      );
    }
  };

  const displayTemplate = () => {
    if (eventMessage.data.length !== 0) {
      switch (eventMessage.data[0].type) {
        case "PLAIN": {
          return <Plain eventMessage={eventMessage.data[0]} />;
        }
        case "VARSEL": {
          return <VarselDetail eventMessage={eventMessage.data[0]} />;
        }
        case "SYSTEM": {
          return <SystemDetail eventMessage={eventMessage.data[0]} />;
        }
        case "VARSEL_ENDRING": {
          return <VarselEndringDetail eventMessage={eventMessage.data[0]} />;
        }
        case "VARSEL_SLETTING": {
          return <VarselSlettingDetail eventMessage={eventMessage.data[0]} />;
        }
        case "VARSEL_KANSELLERT": {
          return <VarselKansellertDetail eventMessage={eventMessage.data[0]} />;
        }
        case "GARANTIUTVALGET_NY": {
          return (
            <GarantiutvalgetNyDetail eventMessage={eventMessage.data[0]} />
          );
        }
        case "GARANTIUTVALGET_BEHANDLET": {
          return (
            <GarantiutvalgetBehandletDetail
              eventMessage={eventMessage.data[0]}
            />
          );
        }
        case "ALL_VARSEL_HANDLED": {
          return <AllVarselHandledDetail eventMessage={eventMessage.data[0]} />;
        }
        case "NEW_MESSAGE_POSTED_IN_CONVERSATION": {
          return (
            <NewMessagePostedInConversationDetail
              eventMessage={eventMessage.data[0]}
            />
          );
        }
        case "SAK_KANSELLERT": {
          return <SakKansellertDetail eventMessage={eventMessage.data[0]} />;
        }
      }
    }
  };

  const handleArchive = () => {
    Swal.fire({
      type: "warning",
      title: "Er du sikker?",
      text: "Ønsker du å arkivere denne meldingen?",
      cancelButtonText: "Avbryt",
      showCancelButton: true,
      confirmButtonText: "Arkiver",
    }).then((result) => {
      if (result.value) {
        dispatch(softDeleteEventMessage(eventMessage.data[0].id, true)).then(
          (response) => {
            if (response.payload.error) {
              Swal.fire(
                "En feil har oppstått",
                "Kunne ikke arkivere meldingen. Prøv igjen senere",
                "error"
              );
            } else {
              eventMessages.data.filter(
                (e) => e.id === eventMessage.data[0].id
              )[0].softDeleted = true;
              props.history.push("/event-message");
            }
          }
        );
      }
    });
  };

  const handleRestore = () => {
    Swal.fire({
      type: "warning",
      title: "Er du sikker?",
      text: "Ønsker du å gjenopprette denne meldingen?",
      cancelButtonText: "Avbryt",
      showCancelButton: true,
      confirmButtonText: "Ja",
    }).then((result) => {
      if (result.value) {
        dispatch(softDeleteEventMessage(eventMessage.data[0].id, false)).then(
          (response) => handleRestoreResponse(response)
        );
      }
    });
  };

  const handleRestoreResponse = (response) => {
    if (response.payload.error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke gjenopprette meldingen. Prøv igjen senere",
        "error"
      );
    } else {
      Swal.fire(
        "Melding gjenopprettet!",
        "Meldingen vil være synlig i innboks",
        "success"
      ).then(() => {
        messageDetail[0].softDeleted = false;
        eventMessages.data.filter(
          (m) => m.id === messageDetail[0].id
        )[0].softDeleted = false;
        forceUpdate();
      });
    }
  };

  const handleInProgress = (event) => {
    event.preventDefault();
    if (eventMessage.loaded && !eventMessage.data[0].workInProgress) {
      Swal.fire({
        type: "info",
        title: "Er du sikker?",
        text: "Ønsker du å endre meldingsstatus?",
        cancelButtonText: "Avbryt",
        showCancelButton: true,
        confirmButtonText: "Ja",
      }).then((result) => {
        if (result.value) {
          dispatch(
            setMessageToWorkInProgress(eventMessage.data[0].id, true)
          ).then((response) => {
            if (response.payload.error) {
              Swal.fire(
                "En feil har oppstått",
                "Kunne ikke endre meldingsstatus. Prøv igjen senere",
                "error"
              );
            } else {
              eventMessages.data.filter(
                (m) => m.id === messageDetail[0].id
              )[0].workInProgress = true;
              props.history.push("/event-message");
            }
          });
        }
      });
    } else {
      Swal.fire({
        type: "info",
        title: "Er du sikker?",
        text: "Er arbeidet ferdig?",
        cancelButtonText: "Avbryt",
        showCancelButton: true,
        confirmButtonText: "Ja",
      }).then((result) => {
        if (result.value) {
          dispatch(
            setMessageToWorkInProgress(eventMessage.data[0].id, false)
          ).then((response) => {
            if (response.payload.error) {
              Swal.fire(
                "En feil har oppstått",
                "Kunne ikke sette arbeid til ferdig. Prøv igjen senere",
                "error"
              );
            } else {
              eventMessages.data.filter(
                (m) => m.id === messageDetail[0].id
              )[0].workInProgress = false;
              eventMessages.data.filter(
                (m) => m.id === messageDetail[0].id
              )[0].softDeleted = true;
              props.history.push("/event-message");
            }
          });
        }
      });
    }
  };

  const displayTitle = () => {
    const archiveTitle =
      eventMessage.loaded && eventMessage.data[0].softDeleted
        ? "Gjenopprett"
        : "Arkiver";
    const handleClick =
      eventMessage.loaded && eventMessage.data[0].softDeleted
        ? handleRestore
        : handleArchive;
    const inProgressTitle =
      eventMessage.loaded && eventMessage.data[0].workInProgress
        ? "Arbeid ferdig"
        : "Under arbeid";
    return (
      <div className="title-for-container">
        Meldingsdetaljer
        <div className="title-button-container">
          <SecondaryButton
            title={archiveTitle}
            onClick={handleClick}
          />
          <SecondaryButton
            title={inProgressTitle}
            onClick={handleInProgress}
          />
        </div>
      </div>
    );
  };

  return (
    <Container
      title={displayTitle()}
      description={
        eventMessage.loaded && eventMessage.data[0].softDeleted ? (
          <span>
            Viser <span className="text-danger">arkivert</span> melding
          </span>
        ) : (
          "Viser detaljer"
        )
      }
      icon={<MdOutlineMailOutline size="55px" />}
    >
      {eventMessage.loaded && displayTemplate()}
    </Container>
  );
};

export default React.memo(withRouter(EventMessageDetail));

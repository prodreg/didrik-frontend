import React, { useState, useEffect, useCallback, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import {
  setEventMessageIsRead,
  softDeleteEventMessages,
} from "../../redux/thunks/event-message-thunks";
import Swal from "sweetalert2";
import Spinner from "../../components/spinner";
import InputField from "../../components/ui/input-field/input-field";
import "./event-message-list.scss";
import Default from "./template/default";
import VarselNy from "./template/varsel_ny";
import VarselEndring from "./template/varsel_endring";
import VarselSletting from "./template/varsel_sletting";
import VarselKansellert from "./template/varsel_kansellert";
import SakKansellert from "./template/sak_kansellert";
import GarantiutvalgetNy from "./template/garantiutvalget_ny";
import GarantiutvalgetBehandlet from "./template/garantiutvalget_behandlet";
import { MdInfoOutline } from "react-icons/md";
import { MdOutlineMarkEmailRead, MdOutlineMailOutline } from "react-icons/md";
import ReactPaginate from "react-paginate";
import { MdChevronLeft, MdChevronRight } from "react-icons/md";
import PrimaryButton from "../../components/ui/button/primary-button";
import useLocalStorage from "../../hooks/use-local-storage";
import Actions from "../../redux/actions/event-message-actions";
import DropdownMenu from "../../components/ui/dropdown/dropdown-menu";
import filterData from "./static-data/filter-data";
import Moment from "react-moment";
import "moment/locale/nb";
import SearchForm from "../../components/ui/search-form/search-form";

const EventMessageList = () => {
  const dispatch = useDispatch();
  const [radioValue, setRadioValue] = useLocalStorage(
    "radio_button_state",
    "innboks"
  );
  const [query, setQuery] = useState("");
  const [searchParam] = useState([
    "description",
    "message",
    "orgNumber",
    "sender",
    "title",
  ]);
  const [pageNumber, setPageNumber] = useLocalStorage("page_number", 0);
  const messagesPerPage = 7;
  const pagesVisited = pageNumber * messagesPerPage;
  const eventMessages = useSelector((state) => state.eventMessageReducer);
  const [checked, setChecked] = useState([]);
  const [type, setType] = useState(filterData[0].type);
  const [allMessages, setAllMessages] = useState([]);
  const [scrollPosition, setScrollPosition] = useLocalStorage(
    "scroll_messages",
    0
  );

  useEffect(() => {
    setAllMessages(eventMessages.data);
  }, [eventMessages]);

  const handleScroll = () => {
    const position = window.scrollY;
    setScrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });

  const forceUpdate = useCallback(
    () => setAllMessages([...eventMessages.data]),
    [eventMessages.data]
  );

  if (eventMessages.error) {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste inn meldinger. Prøv igjen senere",
      "error"
    );
  }

  const getEventMessageCount = () => {
    let counter = 0;
    eventMessages.data.forEach((m) => {
      if (!m.read && !m.softDeleted) {
        counter++;
      }
    });
    return counter;
  };

  const handleSearchQuery = (event) => {
    setQuery(event.target.value);
  };

  const radioButtonHandler = (event) => {
    setRadioValue(event.target.value);
    setPageNumber(0);
    setType(filterData[0].type);
  };

  const handlePageChange = ({ selected }) => {
    setChecked([]);
    setPageNumber(selected);
  };

  const filterByRadioButtons = (messages) => {
    if (radioValue === "innboks") {
      return messages
        .filter((message) => !message.softDeleted)
        .filter((message) => !message.workInProgress);
    }
    if (radioValue === "arkiv") {
      return messages.filter((message) => message.softDeleted);
    }
    if (radioValue === "under_arbeid") {
      return messages.filter((message) => message.workInProgress);
    }
  };

  const filterByType = (data, type) => {
    if (type === filterData[0].type) {
      return filterByRadioButtons(data);
    }
    return filterByRadioButtons(data.filter((e) => e.type === type));
  };

  let sliced = filterByType(eventMessages.data, type).slice(
    pagesVisited,
    pagesVisited + messagesPerPage
  );

  const onFilterChange = (obj) => {
    setType(obj.type);
  };

  const handleCheckboxChange = (event) => {
    if (event.target.checked) {
      setChecked([...checked, event.target.value]);
    } else {
      setChecked(checked.filter((c) => c !== event.target.value));
    }
  };

  const filterMessages = (messages) => {
    const filtered = messages.filter((message) => {
      return searchParam.some((param) => {
        return (
          message[param].toString().toLowerCase().indexOf(query.toLowerCase()) >
          -1
        );
      });
    });
    if (filtered.length > messagesPerPage) {
      return [
        filtered.slice(pagesVisited, pagesVisited + messagesPerPage),
        filtered.length,
      ];
    }
    return [filtered.slice(0, messagesPerPage), filtered.length];
  };

  const getFilteredMessages = (messages) => {
    if (query !== "") {
      const filterType = filterByType(eventMessages.data, type);
      const filterRadio = filterByRadioButtons(filterType);
      return filterMessages(filterRadio);
    }
    return [messages, messages.length];
  };

  const handleArchive = () => {
    dispatch(softDeleteEventMessages(checked.join(","), true)).then(
      (response) => {
        if (response.payload.error) {
          Swal.fire(
            "En feil har oppstått",
            "Kunne ikke arkivere meldinger. Prøv igjen senere",
            "error"
          );
        } else {
          checked.forEach(
            (c) => (allMessages.filter((m) => m.id === c)[0].softDeleted = true)
          );
          const counter = getEventMessageCount();
          dispatch(Actions.setNumberOfUnreadMessages(counter));
          forceUpdate();
        }
      }
    );
    setChecked([]);
  };

  const handleChangeReadState = () => {
    checked.forEach((c) => {
      const isRead = allMessages.filter((m) => m.id === c);
      if (isRead[0].read) {
        dispatch(setEventMessageIsRead(c, false)).then((response) => {
          if (response.payload.error) {
            Swal.fire(
              "En feil har oppstått",
              "Kunne ikke sette melding til ulest. Prøv igjen senere",
              "error"
            );
          } else {
            allMessages.filter((m) => m.id === c)[0].read = false;
            const counter = getEventMessageCount();
            dispatch(Actions.setNumberOfUnreadMessages(counter));
            forceUpdate();
          }
        });
      }
    });
    setChecked([]);
  };

  const displaySearchBar = () => {
    return (
      <SearchForm
        type="text"
        id="searchMessage"
        value={query}
        placeholder="Søk etter melding..."
        className=""
        onChange={handleSearchQuery}
      />
    );
  };

  const displayTemplate = (message) => {
    switch (message.type) {
      case "VARSEL": {
        return <VarselNy message={message} />;
      }
      case "VARSEL_ENDRING": {
        return <VarselEndring message={message} />;
      }
      case "VARSEL_SLETTING": {
        return <VarselSletting message={message} />;
      }
      case "VARSEL_KANSELLERT": {
        return <VarselKansellert message={message} />;
      }
      case "GARANTIUTVALGET_NY": {
        return <GarantiutvalgetNy message={message} />;
      }
      case "GARANTIUTVALGET_BEHANDLET": {
        return <GarantiutvalgetBehandlet message={message} />;
      }
      case "SAK_KANSELLERT": {
        return <SakKansellert message={message} />;
      }
      default:
        return <Default message={message} />;
    }
  };

  const displayRadioButtons = () => {
    return (
      <div className="event-messages-radio">
        <div>
          <input
            type="radio"
            value="innboks"
            checked={radioValue === "innboks"}
            onChange={radioButtonHandler}
          />
          <label htmlFor="inbox" className="radio-text">
            Innboks
          </label>
        </div>
        <div>
          <input
            type="radio"
            value="arkiv"
            checked={radioValue === "arkiv"}
            onChange={radioButtonHandler}
          />
          <label htmlFor="archive" className="radio-text">
            Arkiv
          </label>
        </div>
        <div>
          <input
            type="radio"
            value="under_arbeid"
            checked={radioValue === "under_arbeid"}
            onChange={radioButtonHandler}
          />
          <label htmlFor="draft" className="radio-text">
            Under arbeid
          </label>
        </div>
      </div>
    );
  };

  const displayFilterByType = () => {
    return (
      <div className="filter-type-container">
        <DropdownMenu
          options={filterData}
          onChange={onFilterChange}
          optionLabel={(filterData) => filterData.value}
          optionValue={(filterData) => filterData.type}
          value={filterData.filter((f) => f.type === type)}
        />
      </div>
    );
  };

  const displayButtons = () => {
    return (
      <div className="inbox-buttons">
        <PrimaryButton
          title="Arkiver valgte meldinger"
          onClick={handleArchive}
        />{" "}
        <PrimaryButton
          title=" Marker valgte meldinger som ulest"
          onClick={handleChangeReadState}
        />
      </div>
    );
  };

  const filterType = filterByType(eventMessages.data, type);
  const filterRadio = filterByRadioButtons(filterType);
  const filteredMessagesLength = getFilteredMessages(filterRadio)[1];

  const displayInbox = () => {
    if (eventMessages.data.length === 0) {
      return (
        <div className="no-messages-text">
          <p>Ingen meldinger i innboks</p>
        </div>
      );
    }
    return (
      <div>
        <ol className="message-container">
          {getFilteredMessages(sliced)[0].map((message, key) => {
            const index = key + pageNumber + messagesPerPage;
            return (
              <div className="message-info-container" key={index}>
                <span className="inbox-icon-container">
                  {radioValue !== "arkiv" && radioValue !== "under_arbeid" && (
                    <input
                      className="message-checkbox-container"
                      type="checkbox"
                      value={message.id}
                      key={key}
                      checked={checked.indexOf(message.id) > -1}
                      onChange={handleCheckboxChange}
                    />
                  )}
                  <span className="text-primary">
                    {" "}
                    {message.read ? (
                      <MdOutlineMarkEmailRead size="13px" />
                    ) : (
                      <MdOutlineMailOutline size="13px" />
                    )}{" "}
                  </span>
                  <span className="text-danger">
                    {" "}
                    {message.important && (
                      <MdInfoOutline
                        className={`${message.read ? "blur-if-read" : ""}`}
                        size="12px"
                      />
                    )}
                  </span>
                </span>
                <Link
                  className="event-message-link"
                  to={`/event-message/${message.id}`}
                >
                  <li key={key} className="template-container">
                    {displayTemplate(message)}
                  </li>
                </Link>
                <Link
                  className="event-message-link"
                  to={`/event-message/${message.id}`}
                >
                  <div className="timestamp-container">
                    {" "}
                    <Moment format="D MMM YYYY HH:mm">
                      {message.timestampCreated}
                    </Moment>
                  </div>
                </Link>
              </div>
            );
          })}
        </ol>
        {filteredMessagesLength > messagesPerPage && (
            <ReactPaginate
                previousLabel={<MdChevronLeft />}
                nextLabel={<MdChevronRight />}
                breakLabel={"..."}
                pageCount={Math.ceil(filteredMessagesLength / messagesPerPage)}
                onPageChange={handlePageChange}
                marginPagesDisplayed={3}
                pageRangeDisplayed={3}
                containerClassName={"pagination"}
                pageClassName={"page-item"}
                pageLinkClassName={"page-link"}
                previousClassName={"page-item"}
                previousLinkClassName={"page-link"}
                nextLinkClassName={"page-link"}
                nextClassName={"page-item"}
                breakClassName={"page-item"}
                breakLinkClassName={"page-link"}
                activeClassName={"active"}
                forcePage={parseInt(pageNumber)}
            />
        )}
      </div>
    );
  };

  return (
    <div>
      {eventMessages.data.length !== 0 && displaySearchBar()}
      {eventMessages.data.length !== 0 && displayRadioButtons()}
      {eventMessages.data.length !== 0 &&
        radioValue !== "under_arbeid" &&
        displayFilterByType()}
      {eventMessages.data.length !== 0 &&
        radioValue !== "arkiv" &&
        radioValue !== "under_arbeid" &&
        displayButtons()}
      {eventMessages.loading && <Spinner />}
      {eventMessages.loaded && displayInbox()}
    </div>
  );
};

export default React.memo(withRouter(EventMessageList));

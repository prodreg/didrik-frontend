import React from "react";
import "./template.scss";

const SakKansellert = (props) => {
  const message = JSON.parse(props.message.message);

  return (
    <div className="default-info-container">
      <span className="text-primary">
        <b>
          Sak er blitt <span className="text-danger">kansellert</span>
        </b>
      </span>
      <div className="message-title-container">
        <span className="sub-text">
          Søknad fra{" "}
          <span className="text-info">
            <b>{message.sak.produsent ? message.sak.produsent.navn : "Produsent ikke funnet"}</b>
          </span>{" "}
          om driftskreditt hos{" "}
          <span className="text-info">
            <b>{message.sak.bankName}</b>
          </span>{" "}
          er blitt kansellert
        </span>
      </div>
    </div>
  );
};

export default React.memo(SakKansellert);

import React from "react";
import "./template.scss";

const VarselNy = (props) => {
  return (
    <div className="default-info-container">
      <span className="text-primary">
        <b>
          Varsel om <span className="text-success">innvilget</span> kreditt
        </b>
      </span>
      <div className="message-title-container">
        <span className="sub-text">{props.message.title}</span>
      </div>
    </div>
  );
};

export default React.memo(VarselNy);

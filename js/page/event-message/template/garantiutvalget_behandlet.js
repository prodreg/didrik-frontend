import React from "react";
import "./template.scss";

const GarantiutvalgetBehandlet = (props) => {
  const message = JSON.parse(props.message.message);
  return (
    <div className="default-info-container">
      <span className="text-primary">
        <b>
          Sak for {message.sak.produsent ? message.sak.produsent.navn : "PRODUSENT IKKE FUNNET"} ferdigbehandlet av
          Garantiutvalget
        </b>
      </span>
      <div className="message-title-container">
        <span className="sub-text">
          {" "}
          {message.garantiutvalgetDto.status === "PENDING" && <span></span>}
          {message.garantiutvalgetDto.status === "ACCEPTED" && (
            <span>
              Garantiutvalget har{" "}
              <span className="text-success">
                <b>godkjent</b>
              </span>{" "}
              saken
            </span>
          )}
          {message.garantiutvalgetDto.status === "REJECTED" && (
            <span>
              {" "}
              Garantiutvalget har{" "}
              <span className="text-warning">
                <b>avslått</b>
              </span>{" "}
              saken
            </span>
          )}
          {message.garantiutvalgetDto.status === "SIGNED_OFF" && (
            <span>
              {" "}
              Garantiutvalget har{" "}
              <span className="text-warning">
                <b>meldt seg ut</b>
              </span>{" "}
              av saken
            </span>
          )}
        </span>
      </div>
    </div>
  );
};

export default React.memo(GarantiutvalgetBehandlet);

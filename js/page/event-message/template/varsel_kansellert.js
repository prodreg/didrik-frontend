import React from "react";
import "./template.scss";

const VarselKansellert = (props) => {
  const message = JSON.parse(props.message.message);
  return (
    <div className="default-info-container">
      <span className="text-primary">
        <b>
          Varsel om innvilget kreditt er blitt{" "}
          <span className="text-danger">kansellert</span>
        </b>
      </span>
      <div className="message-title-container">
        <span className="sub-text">
          Bank: <b>{message.bankName}</b>. Produsent:{" "}
          <b>{message.produsent.navn}</b>
        </span>
      </div>
    </div>
  );
};

export default React.memo(VarselKansellert);

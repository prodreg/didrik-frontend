import React from "react";
import "./template.scss";

const GarantiutvalgetNy = (props) => {
  const message = JSON.parse(props.message.message);
  return (
    <div className="default-info-container">
      <span className="text-primary">
        <b>Varsel om sak til behandling hos Garantiutvalget</b>
      </span>
      <div className="message-title-container">
        <span className="sub-text">
          {" "}
          <span className="text-info">
            <b>{message.sak.produsent ? message.sak.produsent.navn : "Produsent ikke funnet"}</b>
          </span>{" "}
          har søkt om driftskreditt hos{" "}
          <span className="text-info">
            <b>{message.sak.bankName}</b>
          </span>
        </span>
      </div>
    </div>
  );
};

export default React.memo(GarantiutvalgetNy);

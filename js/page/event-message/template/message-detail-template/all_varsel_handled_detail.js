import React from "react";
import Card from "../../../../components/ui/card/card";
import { Link } from "react-router-dom";
import PrimaryButton from "../../../../components/ui/button/primary-button";
import Moment from "react-moment";
import "moment/locale/nb";
import "./message-detail-template.scss";
import NumberFormat from "react-number-format";

const AllVarselHandledDetail = (props) => {
  const message = JSON.parse(props.eventMessage.message);
  let settlement = 0;
  let deduct = 0;
  let generated = message.sak.generatedRef !== null;

  message.sak.vareleveranseList.forEach((v) => {
    if (v.korrBeloep) {
      settlement += v.korrBeloep;
    } else if (v.oppgjoer) {
      settlement += v.oppgjoer;
    }
    if (v.korrTrekk) {
      deduct += v.korrTrekk;
    } else if (v.trekk) {
      deduct += v.trekk;
    }
  });

  const displayTitle = () => {
    return (
      <div className="title-container">
        <span>{props.eventMessage.title}</span>
        <Moment format="D MMM YYYY HH:mm">
          {props.eventMessage.timestampCreated}
        </Moment>
      </div>
    );
  };

  return (
    <Card title={displayTitle()}>
      <h3>Melding fra {props.eventMessage.sender}</h3>
      <dl className="dl-horizontal">
        <dt>Produsent</dt>
        <dd>{message.sak.produsent.navn}</dd>
        <dt>Produsentnummer</dt>
        <dd>{message.sak.produsent.prodnr}</dd>
        <dt>Adresse</dt>
        <dd>
          <div>
            {message.sak.produsent.adresse.adresse1}
            <br />
            {message.sak.produsent.adresse.adresse2}
            <br />
            {message.sak.produsent.adresse.adresse3}
          </div>
          <div>
            {message.sak.produsent.adresse.postnr}{" "}
            {message.sak.produsent.adresse.poststed}
          </div>
        </dd>
        <dt>Bankkontonummer</dt>
        <dd>
          <NumberFormat
            value={message.sak.kontoNummer}
            format="#### ## #####"
            displayType="text"
          />
        </dd>
        <dt>Bevilget kroner</dt>
        <dd>
          <NumberFormat
            value={message.sak.bevilget}
            thousandSeparator={true}
            displayType="text"
            prefix="kr "
            decimalScale={2}
            fixedDecimalScale={true}
          />
        </dd>
        <dt>Årlig beløp overført til bank</dt>
        <dd>
          <NumberFormat
            value={settlement}
            thousandSeparator={true}
            displayType="text"
            prefix="kr "
            decimalScale={2}
            fixedDecimalScale={true}
          />
        </dd>
        {generated && <dt>Trekk</dt>}
        {generated && (
          <dd>
            <NumberFormat
              value={deduct}
              thousandSeparator={true}
              displayType="text"
              prefix="kr "
              decimalScale={2}
              fixedDecimalScale={true}
            />
          </dd>
        )}
        <br />
        <dd>
          <Link to={`/case/${message.sak.id}`}>
            <PrimaryButton title="Til sak" />
          </Link>
        </dd>
      </dl>
    </Card>
  );
};

export default React.memo(AllVarselHandledDetail);

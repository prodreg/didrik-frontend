import React from "react";
import Card from "../../../../components/ui/card/card";
import Moment from "react-moment";
import "moment/locale/nb";
import "./message-detail-template.scss";
import { Link } from "react-router-dom";
import PrimaryButton from "../../../../components/ui/button/primary-button";

const VarselKansellertDetail = (props) => {
  const message = JSON.parse(props.eventMessage.message);

  const displayTitle = () => {
    return (
      <div className="title-container">
        <span>
          <span>Varsel om innvilget kreditt er blitt kansellert</span>
          <br />
          <span>Tilbakemelding kan ikke lenger gjøres.</span>
        </span>
        <Moment format="D MMM YYYY HH:mm">
          {props.eventMessage.timestampCreated}
        </Moment>
      </div>
    );
  };

  return (
    <Card title={displayTitle()}>
      <h3>Fra {message.bankName}</h3>
      <dl className="dl-horizontal">
        <dt>Produsent</dt>
        <dd>{message.produsent.navn}</dd>
        <dt>Produsentnummer</dt>
        <dd>{message.produsent.prodnr}</dd>
        <dt>Adresse</dt>
        <dd>
          <div>
            {message.produsent.adresse.adresse1}
            <br />
            {message.produsent.adresse.adresse2}
            <br />
            {message.produsent.adresse.adresse3}
          </div>
          <div>
            {message.produsent.adresse.postnr}{" "}
            {message.produsent.adresse.poststed}
          </div>
        </dd>
        <dd>
          <Link to={`/case-notice/${message.varselId}`}>
            <PrimaryButton title="Til varsel" />
          </Link>
        </dd>
      </dl>
    </Card>
  );
};

export default React.memo(VarselKansellertDetail);

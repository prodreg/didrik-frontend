import React from "react";
import Card from "../../../../components/ui/card/card";
import Moment from "react-moment";
import "moment/locale/nb";
import "./message-detail-template.scss";
import { Link } from "react-router-dom";
import PrimaryButton from "../../../../components/ui/button/primary-button";
import NumberFormat from "react-number-format";

const GarantiutvalgetNyDetail = (props) => {
  const message = JSON.parse(props.eventMessage.message);

  const displayTitle = () => {
    return (
      <div className="title-container">
        <span>Sak til behandling hos Garantiutvalget</span>
        <Moment format="D MMM YYYY HH:mm">
          {props.eventMessage.timestampCreated}
        </Moment>
      </div>
    );
  };

  return (
    <Card title={displayTitle()}>
      <h3>Fra {message.sak.bankName}</h3>
      <dl className="dl-horizontal">
        <dt>Produsent</dt>
        <dd>{message.sak.produsent ? message.sak.produsent.navn : "Produsent ikke funnet"}</dd>
        <dt>Produsentnummer</dt>
        <dd>{message.sak.produsent ? message.sak.produsent.prodnr : ""}</dd>
        <dt>Adresse</dt>
        <dd>
          <div>
            {message.sak.produsent ? message.sak.produsent.adresse.adresse1 : ""}
            <br />
            {message.sak.produsent ? message.sak.produsent.adresse.adresse2 : ""}
            <br />
            {message.sak.produsent ? message.sak.produsent.adresse.adresse3 : ""}
          </div>
          <div>
            {message.sak.produsent ? message.sak.produsent.adresse.postnr : ""}{" "}
            {message.sak.produsent ? message.sak.produsent.adresse.poststed : ""}
          </div>
        </dd>
        <dt>Bankkontonummer</dt>
        <dd>
          <NumberFormat
            value={message.sak.kontoNummer}
            format="#### ## #####"
            displayType="text"
          />
        </dd>
        <dt>Bevilget</dt>
        <dd>
          <NumberFormat
            value={message.sak.bevilget}
            thousandSeparator={true}
            displayType="text"
            prefix="kr "
            decimalScale={2}
            fixedDecimalScale={true}
          />
        </dd>
        <dd>
          <Link to={`/case/${message.sak.id}`}>
            <PrimaryButton title="Til sak" />
          </Link>
        </dd>
      </dl>
    </Card>
  );
};

export default React.memo(GarantiutvalgetNyDetail);

import React from "react";
import Card from "../../../../components/ui/card/card";
import Moment from "react-moment";
import "moment/locale/nb";
import "./message-detail-template.scss";
import NumberFormat from "react-number-format";
import TextArea from "../../../../components/ui/textarea/text-area";
import PrimaryButton from "../../../../components/ui/button/primary-button";
import { Link } from "react-router-dom";

const GarantiutvalgetBehandletDetail = (props) => {
  const message = JSON.parse(props.eventMessage.message);

  const displayTitle = () => {
    return (
      <div className="title-container">
        <span>Sak behandlet av Garantiutvalget</span>
        <Moment format="D MMM YYYY HH:mm">
          {props.eventMessage.timestampCreated}
        </Moment>
      </div>
    );
  };

  return (
    <Card title={displayTitle()}>
      <h3>Fra Garantiutvalget</h3>
      <dl className="dl-horizontal">
        <dt>Produsent</dt>
        <dd>{message.sak.produsent.navn}</dd>
        <dt>Produsentnummer</dt>
        <dd>{message.sak.produsent.prodnr}</dd>
        <dt>Adresse</dt>
        <dd>
          <div>
            {message.sak.produsent.adresse.adresse1}
            <br />
            {message.sak.produsent.adresse.adresse2}
            <br />
            {message.sak.produsent.adresse.adresse3}
          </div>
          <div>
            {message.sak.produsent.adresse.postnr}{" "}
            {message.sak.produsent.adresse.poststed}
          </div>
        </dd>
        <dt>Bankkontonummer</dt>
        <dd>
          <NumberFormat
            value={message.sak.kontoNummer}
            format="#### ## #####"
            displayType="text"
          />
        </dd>
        <dt>Bevilget</dt>
        <dd>
          <NumberFormat
            value={message.sak.bevilget}
            thousandSeparator={true}
            displayType="text"
            prefix="kr "
            decimalScale={2}
            fixedDecimalScale={true}
          />
        </dd>
        <dt>Status</dt>
        {message.garantiutvalgetDto.status === "ACCEPTED" && (
          <dd className="text-success">Godkjent</dd>
        )}
        {message.garantiutvalgetDto.status === "REJECTED" && (
          <dd className="text-danger">Avslått</dd>
        )}
        {message.garantiutvalgetDto.status === "SIGNED_OFF" && (
          <dd className="text-danger">Meldt av</dd>
        )}
      </dl>
      {message.garantiutvalgetDto.status === "ACCEPTED" && <h2>Garantibrev</h2>}
      {message.garantiutvalgetDto.status === "REJECTED" && <h2>Begrunnelse</h2>}
      {message.garantiutvalgetDto.status === "SIGNED_OFF" && (
        <h2>Begrunnelse</h2>
      )}
      <TextArea value={message.garantiutvalgetDto.text} disabled={true} />
      <Link to={`/case/${message.sak.id}`}>
        <PrimaryButton title="Til sak" />
      </Link>
    </Card>
  );
};

export default React.memo(GarantiutvalgetBehandletDetail);

import React from "react";
import Card from "../../../../components/ui/card/card";
import Moment from "react-moment";
import "moment/locale/nb";
import "./message-detail-template.scss";

const Plain = (props) => {
  const content = JSON.parse(props.eventMessage.message).content;
  const displayTitle = () => {
    return (
      <div className="title-container">
        {props.eventMessage.title}
        <Moment format="D MMM YYYY HH:mm">
          {props.eventMessage.timestampCreated}
        </Moment>
      </div>
    );
  };

  return <Card title={displayTitle()}>{content}</Card>;
};

export default React.memo(Plain);

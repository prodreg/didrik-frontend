import React from "react";
import Card from "../../../../components/ui/card/card";
import Moment from "react-moment";
import "moment/locale/nb";
import "./message-detail-template.scss";

const SystemDetail = (props) => {
  const message = JSON.parse(props.eventMessage.message);

  const displayTitle = () => {
    return (
      <div className="title-container">
        {props.eventMessage.title}
        <Moment format="D MMM YYYY HH:mm">
          {props.eventMessage.timestampCreated}
        </Moment>
      </div>
    );
  };
  return (
    <Card title={displayTitle()}>
      <h3>{props.eventMessage.sender}</h3>
      <p>{message.text1}</p>
      <p>{message.text2}</p>
      <p>{message.text3}</p>
      <p>{message.text4}</p>
    </Card>
  );
};

export default React.memo(SystemDetail);

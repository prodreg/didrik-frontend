import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import Card from "../../../../components/ui/card/card";
import Moment from "react-moment";
import "moment/locale/nb";
import "./message-detail-template.scss";
import PrimaryButton from "../../../../components/ui/button/primary-button";
import userService from "../../../../service/user-service";
import { getNoticeIdForOrganization } from "../../../../redux/thunks/case-notice-thunks";

const NewMessagePostedInConversationDetail = (props) => {
  const dispatch = useDispatch();
  const message = JSON.parse(props.eventMessage.message);
  const userInfo = userService.getLoggedInUser();
  const userRoles = userInfo.roles;
  const orgNumber = userInfo.orgNumber;
  const [noticeId, setNoticeId] = useState("");

  let isOrganization = userRoles.indexOf("mottaker") > -1;

  const getNoticeId = () => {
    if (isOrganization) {
      dispatch(getNoticeIdForOrganization(message.sakId, orgNumber)).then(
        (response) => setNoticeId(response.payload.data)
      );
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      getNoticeId();
    });
    return () => {
      clearTimeout(timer);
    };
  }, []);

  const displayTitle = () => {
    return (
      <div className="title-container">
        {props.eventMessage.title}
        <Moment format="D MMM YYYY HH:mm">
          {props.eventMessage.timestampCreated}
        </Moment>
      </div>
    );
  };

  return (
    <Card title={displayTitle()}>
      <h3>Melding fra {message.sender}</h3>
      <br />
      <p>{message.content}</p>
      <br />
      {isOrganization && (
        <Link to={`/case-notice/${noticeId}`}>
          <PrimaryButton
            title="Til siden med dialogen"
          />
        </Link>
      )}
      {!isOrganization && (
        <Link
          to={`/case/${message.sakId}?conversationOrg=${
            message.mottakerId
              ? message.mottakerId
              : message.senderOrganization.orgNumber
          }`}
        >
          <PrimaryButton
            title="Til siden med dialogen"
          />
        </Link>
      )}
    </Card>
  );
};

export default React.memo(NewMessagePostedInConversationDetail);

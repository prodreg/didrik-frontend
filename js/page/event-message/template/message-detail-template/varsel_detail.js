import React from "react";
import Card from "../../../../components/ui/card/card";
import Moment from "react-moment";
import "moment/locale/nb";
import "./message-detail-template.scss";
import { Link } from "react-router-dom";
import PrimaryButton from "../../../../components/ui/button/primary-button";
import NumberFormat from "react-number-format";

const VarselDetail = (props) => {
  const message = JSON.parse(props.eventMessage.message);

  const displayTitle = () => {
    return (
      <div className="title-container">
        <span>{props.eventMessage.title}</span>
        <Moment format="D MMM YYYY HH:mm">
          {props.eventMessage.timestampCreated}
        </Moment>
      </div>
    );
  };

  return (
    <Card title={displayTitle()}>
      <h3>
        Fra {message.bankName}, {props.eventMessage.sender}
      </h3>
      <dl className="dl-horizontal">
        <dt>Produsent</dt>
        <dd>{message.produsent.navn}</dd>
        <dt>Produsentnummer</dt>
        <dd>{message.produsent.prodnr}</dd>
        <dt>Adresse</dt>
        <dd>
          <div>
            {message.produsent.adresse.adresse1}
            <br />
            {message.produsent.adresse.adresse2}
            <br />
            {message.produsent.adresse.adresse3}
          </div>
          <div>
            {message.produsent.adresse.postnr}{" "}
            {message.produsent.adresse.poststed}
          </div>
        </dd>
        <dt>Bankkontonummer</dt>
        <dd>
          <NumberFormat
            value={message.kontoNummer}
            format="#### ## #####"
            displayType="text"
          />
        </dd>
        <dt>Bevilget</dt>
        <dd>
          <NumberFormat
            value={message.bevilget}
            thousandSeparator={true}
            displayType="text"
            prefix="kr "
            decimalScale={2}
            fixedDecimalScale={true}
          />
        </dd>
        <dd>
          <Link to={`/case-notice/${message.varselId}`}>
            <PrimaryButton title="Til varsel" />
          </Link>
        </dd>
      </dl>
    </Card>
  );
};

export default React.memo(VarselDetail);

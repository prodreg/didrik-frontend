import React from "react";
import "./template.scss";

const Default = (props) => {
  return (
    <div className="default-info-container">
      <span className="text-primary">
        <b>{props.message.description}</b>
      </span>
      <div className="message-title-container">
        <span className="sub-text">{props.message.title}</span>
      </div>
    </div>
  );
};

export default React.memo(Default);

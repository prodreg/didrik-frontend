import React from "react";
import "./template.scss";
import Moment from "react-moment";
import "moment/locale/nb";

const VarselEndring = (props) => {
  return (
    <div className="default-info-container">
      <span className="text-primary">
        <b>
          Varsel om <span className="text-info">endring</span> av innvilget
          kreditt
        </b>
      </span>
      <div className="message-title-container">
        <span className="sub-text">{props.message.title}</span>
      </div>
    </div>
  );
};

export default React.memo(VarselEndring);

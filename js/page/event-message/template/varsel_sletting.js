import React from "react";
import "./template.scss";

const VarselSletting = (props) => {
  return (
    <div className="default-info-container">
      <span className="text-primary">
        <b>
          Varsel om <span className="text-danger">opphør</span> av garanti
        </b>
      </span>
      <div className="message-title-container">
        <span className="sub-text">{props.message.title}</span>
      </div>
    </div>
  );
};

export default React.memo(VarselSletting);

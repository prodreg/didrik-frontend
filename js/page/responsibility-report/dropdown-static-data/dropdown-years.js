export default Array.from(new Array(20), (val, i) => ({
  id: new Date().getFullYear() - 1 - i,
  value: new Date().getFullYear() - 1 - i,
}));

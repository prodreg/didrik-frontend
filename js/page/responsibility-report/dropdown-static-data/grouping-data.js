export default [
  {
    navn: "Etter Fylke",
    value: "EtterFylke",
    valueType: "EtterFylke",
    columnName: "Fylkesnr",
  },
  {
    navn: "Etter Organisasjon (Bank)",
    value: "EtterOrganisasjon",
    valueType: "EtterOrganisasjonBank",
    perOrganisasjon: true,
    columnName: "Konsernnr",
  },
  {
    navn: "Etter Organisasjon (Org)",
    value: "EtterOrganisasjon",
    valueType: "EtterOrganisasjonOrg",
    perOrganisasjon: true,
    columnName: "Org.nr",
  },
];

export default [
  {
    navn: "Bevilget per Fylke",
    id: "BevilgetPerFylke",
    type: "Bevilget",
    filter: "AlleVaremottakere",
    gruppering: "EtterFylke",
  },
  {
    navn: "Bevilget Sparebank per Fylke",
    id: "BevilgetSpareBankPerFylke",
    type: "Bevilget",
    filter: "Sparebanker",
    gruppering: "EtterFylke",
  },
  {
    navn: "Bevilget Forretningsbank per Fylke",
    id: "BevilgetForetningsBankPerFylke",
    type: "Bevilget",
    filter: "Forretningsbanker",
    gruppering: "EtterFylke",
  },
  {
    navn: "Bevilget Sparebank per Bank",
    id: "BevilgetSpareBankPerBank",
    type: "Bevilget",
    filter: "Sparebanker",
    gruppering: "EtterOrganisasjonBank",
  },
  {
    navn: "Bevilget Forretningsbank per Bank",
    id: "BevilgetForetningsBankPerBank",
    type: "Bevilget",
    filter: "Forretningsbanker",
    gruppering: "EtterOrganisasjonBank",
  },
  {
    navn: "Bevilget Samvirket",
    id: "BevilgetSamvirke",
    type: "Bevilget",
    filter: "Samvirkeordningen",
    gruppering: "EtterOrganisasjonOrg",
  },
  {
    navn: "Bevilget Private",
    id: "BevilgetPrivat",
    type: "Bevilget",
    filter: "Privatordningen",
    gruppering: "EtterOrganisasjonOrg",
  },
  {
    navn: "Saldo per Fylke",
    id: "SaldoPerFylke",
    type: "Saldo",
    filter: "AlleVaremottakere",
    gruppering: "EtterFylke",
  },
  {
    navn: "Saldo Sparebank per Fylke",
    id: "SaldoSpareBankPerFylke",
    type: "Saldo",
    filter: "Sparebanker",
    gruppering: "EtterFylke",
  },
  {
    navn: "Saldo Forretningsbank per Fylke",
    id: "SaldoForetningsBankPerFylke",
    type: "Saldo",
    filter: "Forretningsbanker",
    gruppering: "EtterFylke",
  },
  {
    navn: "Saldo Sparebank per Bank",
    id: "SaldoSpareBankPerBank",
    type: "Saldo",
    filter: "Sparebanker",
    gruppering: "EtterOrganisasjonBank",
  },
  {
    navn: "Saldo Forretningsbank per Bank",
    id: "SaldoForetningsBankPerBank",
    type: "Saldo",
    filter: "Forretningsbanker",
    gruppering: "EtterOrganisasjonBank",
  },
  {
    navn: "Saldo Samvirket",
    id: "SaldoSamvirke",
    type: "Saldo",
    filter: "Samvirkeordningen",
    gruppering: "EtterOrganisasjonOrg",
  },
  {
    navn: "Saldo Private",
    id: "SaldoPrivat",
    type: "Saldo",
    filter: "Privatordningen",
    gruppering: "EtterOrganisasjonOrg",
  },
  {
    navn: "Rapport til varemottaker, privat",
    id: "WordPrivat",
  },
  {
    navn: "Rapport til varemottaker, samvirket",
    id: "WordSamvirke",
  },
];

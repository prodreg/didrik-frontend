export default [
  { navn: "Alle", value: "Alle" },
  { navn: "Banker", value: "AlleBanker" },
  { navn: "Sparebanker", value: "Sparebanker" },
  { navn: "Forretningsbanker", value: "Forretningsbanker" },
  { navn: "Varemottakere", value: "AlleVaremottakere", varemottakere: true },
  { navn: "Privatordningen", value: "Privatordningen", varemottakere: true },
  {
    navn: "Samvirkeordningen",
    value: "Samvirkeordningen",
    varemottakere: true,
  },
  { navn: "ERROR", value: "error" },
];

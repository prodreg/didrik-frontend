import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import dropdownData from "./dropdown-static-data/dropdown-data";
import dropdownYears from "./dropdown-static-data/dropdown-years";
import groupingData from "./dropdown-static-data/grouping-data";
import wordTypes from "./dropdown-static-data/word-type";
import DropdownMenu from "../../components/ui/dropdown/dropdown-menu";
import Spinner from "../../components/spinner";
import { getResponsibilityReport } from "../../redux/thunks/report-thunks";
import { getAllOrdning } from "../../redux/thunks/organization-ordning-thunks";
import { getAllOrganizations } from "../../redux/thunks/organization-thunks";
import "./dropdown-content.scss";
import BalanceTemplate from "./template/balance-template";
import GrantedTemplate from "./template/granted-template";
import WordTemplate from "./template/word-template";
import Swal from "sweetalert2";

const DropdownContent = () => {
  const reportData = useSelector((state) => state.responsibilityReducer);
  const allOrdningData = useSelector((state) => state.allOrdning);
  const organizationsData = useSelector(
    (state) => state.allOrganizationsReducer
  );
  const [year, setYear] = useState("");
  const [objectId, setObjectId] = useState("");
  const [grouping, setGrouping] = useState("");
  const [filter, setFilter] = useState("");
  const [type, setType] = useState("");
  const [grantedName, setGrantedName] = useState("");
  const [columnName, setColumnName] = useState("");
  const dispatch = useDispatch();

  let isValid = year !== "" && (type !== "" || objectId !== "");
  let idInWordTypes = wordTypes.indexOf(objectId) > -1;

  useEffect(() => {
    getReports();
  }, [dispatch, year, objectId]);

  const getReports = () => {
    if (isValid && wordTypes.indexOf(objectId) === -1) {
      dispatch(getResponsibilityReport(year, filter, grouping, type)).then((response) => {
        showError(response.payload.error);
      });
    } else if (isValid) {
      dispatch(getAllOrdning());
      dispatch(getAllOrganizations(["MOTTAKER", "BANK"]));
    }
  };

  const onAlternativeChange = (obj) => {
    if (wordTypes.indexOf(obj.id) === -1) {
      const groupObject = groupingData.filter(
        (g) => g.valueType === obj.gruppering
      );
      setGrouping(groupObject[0].value);
      setColumnName(groupObject[0].columnName);
      setFilter(obj.filter);
      setType(obj.type);
      setGrantedName(obj.navn);
    }
    setObjectId(obj.id);
  };

  const onYearChange = (obj) => {
    setYear(obj.value);
  };

  const showError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke laste inn ansvarsrapporter. Prøv igjen senere",
        "error"
      );
    }
  };

  const displayDropdown = () => {
    return (
      <div className="dropdown-container">
        <div className="col">
          <label htmlFor="predefined" className="predefined-label">
            Predefinerte rapporter:
          </label>
          <div className="row-md-6">
            <div className="dropdown-one-container">
              <DropdownMenu
                options={dropdownData}
                placeholder="Velg et alternativ..."
                onChange={onAlternativeChange}
                optionLabel={(dropdownData) => dropdownData.navn}
                optionValue={(dropdownData) => dropdownData.id}
              />
            </div>
          </div>
          <div className="row-md-4">
            <div className="dropdown-two-container">
              <DropdownMenu
                options={dropdownYears}
                placeholder="Velg år..."
                onChange={onYearChange}
                optionLabel={(dropdownYears) => dropdownYears.id}
                optionValue={(dropdownYears) => dropdownYears.value}
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  const displayReportTable = () => {
    return (
      <div className="report-table-container">
        {type === "Bevilget" &&
          isValid &&
          !idInWordTypes &&
          reportData.loaded && (
            <GrantedTemplate
              grantedName={grantedName}
              columnName={columnName}
              grouping={grouping}
              year={year}
              filter={filter}
              type={type}
              objectId={objectId}
              reportData={reportData}
            />
          )}
        {type === "Saldo" && isValid && !idInWordTypes && reportData.loaded && (
          <BalanceTemplate
            columnName={columnName}
            grouping={grouping}
            year={year}
            filter={filter}
            type={type}
            objectId={objectId}
            reportData={reportData}
          />
        )}
        {isValid &&
          idInWordTypes &&
          allOrdningData.loaded &&
          organizationsData.loaded && (
            <WordTemplate
              ordning={allOrdningData}
              organizations={organizationsData}
              year={year}
              type={
                objectId === wordTypes[0]
                  ? "PRIVATORDNINGEN"
                  : "SAMVIRKEORDNINGEN"
              }
              wordType={objectId}
            />
          )}
      </div>
    );
  };

  return (
    <div>
      {displayDropdown()}
      {reportData.loading && isValid && <Spinner />}
      {displayReportTable()}
    </div>
  );
};

export default DropdownContent;

import React, { useState } from "react";
import { useDispatch } from "react-redux";
import Spinner from "../../../components/spinner";
import ReactPaginate from "react-paginate";
import "./templates.scss";
import { MdOutlineFileDownload } from "react-icons/md";
import { MdOutlineChevronLeft, MdOutlineChevronRight } from "react-icons/md";
import {getWordResponsibilityReport} from "../../../redux/thunks/report-thunks";
import FileSaver from "file-saver";
import Swal from "sweetalert2";

const WordTemplate = (props) => {
  const [isDownloading, setIsDownloading] = useState(false);
  const [pageNumber, setPageNumber] = useState(0);
  const orgPerPage = 8;
  const pagesVisited = pageNumber * orgPerPage;
  const dispatch = useDispatch();
  const orgNr = [];
  let tableData = [];

  if (props.ordning.loaded && props.organizations.loaded) {
    Object.entries(props.ordning.data.ordninger).forEach((o) => {
      if (o[1] === props.type) {
        orgNr.push(o[0]);
      }
    });
    tableData = props.organizations.data
      .sort((a, b) => a.orgNumber - b.orgNumber)
      .filter((org) => orgNr.indexOf(org.orgNumber) > -1);
  }

  const slicedTableData = tableData.slice(
    pagesVisited,
    pagesVisited + orgPerPage
  );

  const handlePageChange = ({ selected }) => {
    setPageNumber(selected);
  };

  const showWordDownloadError = () => {
    setIsDownloading(false);
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste ned word-dokumentet. Prøv igjen senere",
      "error"
    );
  };

  const downloadWordReport = (data, type, orgnr) => {
    const blob = new Blob([data], {
      type: "application/vnd.ms-word",
    });
    FileSaver.saveAs(blob, "Rapport-" + type + "-" + orgnr + ".doc");
    setIsDownloading(false);
  };

  const downloadWordHandler = (event, orgnr) => {
    event.preventDefault();
    setIsDownloading(true);
    dispatch(getWordResponsibilityReport(props.year, props.wordType, orgnr)).then(
      (response) => {
        if (response.payload.error) {
          showWordDownloadError();
        } else {
          downloadWordReport(response.payload.data, props.wordType, orgnr);
        }
      }
    );
  };

  const displayWordTable = () => {
    return (
      <div className="responsibility-table-container">
        <table className="table && table-striped">
          <thead>
            <tr>
              <th>Organisasjon</th>
              <th>Org.nr</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {slicedTableData.map((data, key) => {
              return (
                <tr key={key}>
                  <td>{data.name}</td>
                  <td>{data.orgNumber}</td>
                  <td>
                    <a
                      className="pointer"
                      onClick={(e) => downloadWordHandler(e, data.orgNumber)}
                    >
                      <MdOutlineFileDownload size="20px" color="black" />
                    </a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <ReactPaginate
            previousLabel={<MdOutlineChevronLeft />}
            nextLabel={<MdOutlineChevronRight />}
            breakLabel={"..."}
            pageCount={tableData.length / orgPerPage}
            onPageChange={handlePageChange}
            marginPagesDisplayed={3}
            pageRangeDisplayed={3}
            containerClassName={"pagination"}
            pageClassName={"page-item"}
            pageLinkClassName={"page-link"}
            previousClassName={"page-item"}
            previousLinkClassName={"page-link"}
            nextLinkClassName={"page-link"}
            nextClassName={"page-item"}
            breakClassName={"page-item"}
            breakLinkClassName={"page-link"}
            activeClassName={"active"}
        />
      </div>
    );
  };
  return (
    <div>
      {(props.organizations.loading ||
        props.ordning.loading ||
        isDownloading) && <Spinner />}
      {props.organizations.loaded && props.ordning.loaded && displayWordTable()}
    </div>
  );
};

export default WordTemplate;

import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import "./templates.scss";
import PrimaryButton from "../../../components/ui/button/primary-button";
import Spinner from "../../../components/spinner";
import {getExcelResponsibilityReport} from "../../../redux/thunks/report-thunks";
import { MdOutlineChevronLeft, MdOutlineChevronRight } from "react-icons/md";
import Swal from "sweetalert2";
import FileSaver from "file-saver";
import ReactPaginate from "react-paginate";
import NumberFormat from "react-number-format";

const GrantedTemplate = (props) => {
  const [isDownloading, setIsDownloading] = useState(false);
  const [sortType, setSortType] = useState("ASC");
  const [pageNumber, setPageNumber] = useState(0);
  const rowsPerPage = 10;
  const pagesVisited = pageNumber * rowsPerPage;
  let slicedData = props.reportData.data.slice(
    pagesVisited,
    pagesVisited + rowsPerPage
  );
  const [sortedData, setSortedData] = useState(slicedData);
  const dispatch = useDispatch();
  const grantedPrivate = "Bevilget Private";
  const grantedCooperative = "Bevilget Samvirket";
  const groupingName =
    props.grouping === "EtterOrganisasjon" ? "Organisasjon" : "Fylke";

  useEffect(() => {
    setSortedData(slicedData);
  }, [pagesVisited]);

  const showExcelDownloadError = () => {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste ned excel-dokumentet. Prøv igjen senere",
      "error"
    );
  };

  const downloadExcelReport = (data, id) => {
    const blob = new Blob([data], {
      type: "application/vnd.ms-excel",
    });
    FileSaver.saveAs(blob, "Rapport-" + id + ".xls");
    setIsDownloading(false);
  };

  const handleExcelReport = () => {
    dispatch(
      getExcelResponsibilityReport(
        props.year,
        props.filter,
        props.grouping,
        props.type,
        props.objectId
      )
    ).then((response) => {
      if (response.payload.error) {
        showExcelDownloadError();
      } else {
        setIsDownloading(true);
        downloadExcelReport(response.payload.data, props.objectId);
      }
    });
  };

  const handlePageChange = ({ selected }) => {
    setPageNumber(selected);
  };

  const sortBy = (event, col) => {
    event.preventDefault();
    let sorted = [];
    if (sortType === "ASC") {
      sorted = [...sortedData].sort((a, b) => (a[col] > b[col] ? 1 : -1));
      setSortType("DSC");
    }
    if (sortType === "DSC") {
      sorted = [...sortedData].sort((a, b) => (a[col] < b[col] ? 1 : -1));
      setSortType("ASC");
    }
    setSortedData(sorted);
  };

  const sortByNested = (event, col) => {
    event.preventDefault();
    const object = col.substr(0, col.indexOf("."));
    const objectProperty = col.substr(col.indexOf(".") + 1, col.length);
    let sorted = [];
    if (sortType === "ASC") {
      if (object === "total") {
        sorted = [...sortedData].sort((a, b) =>
          a.total[objectProperty] > b.total[objectProperty] ? 1 : -1
        );
      } else if (object === "egen") {
        sorted = [...sortedData].sort((a, b) =>
          a.egen[objectProperty] > b.egen[objectProperty] ? 1 : -1
        );
      } else if (object === "ansvar") {
        sorted = [...sortedData].sort((a, b) =>
          a.ansvar[objectProperty] > b.ansvar[objectProperty] ? 1 : -1
        );
      }
      setSortType("DSC");
    }
    if (sortType === "DSC") {
      if (object === "total") {
        sorted = [...sortedData].sort((a, b) =>
          a.total[objectProperty] < b.total[objectProperty] ? 1 : -1
        );
      } else if (object === "egen") {
        sorted = [...sortedData].sort((a, b) =>
          a.egen[objectProperty] < b.egen[objectProperty] ? 1 : -1
        );
      } else if (object === "ansvar") {
        sorted = [...sortedData].sort((a, b) =>
          a.ansvar[objectProperty] < b.ansvar[objectProperty] ? 1 : -1
        );
      }
      setSortType("ASC");
    }
    setSortedData(sorted);
  };

  const getOrganizationName = (orgnr) => {
    const organizationObject = props.reportData.data.filter(
      (o) => o.orgNumber === orgnr
    );
    if (organizationObject.length !== 0) {
      return organizationObject[0].name;
    }
  };

  return (
    <div className="responsibility-table-container">
      <div className="excel-report-button-container">
        {isDownloading ? (
          <Spinner />
        ) : (
          <PrimaryButton
            title="Excel-rapport"
            onClick={handleExcelReport}
          />
        )}
      </div>
      <table className="table && table-striped">
        <thead>
          <tr>
            {props.grantedName !== grantedPrivate && (
              <>
                <th className="table-left-column-name" colSpan="4">
                  Fra banken
                </th>
                <th className="table-right-column-name" colSpan="5">
                  Garantiansvar
                </th>
              </>
            )}
            {props.grantedName === grantedPrivate && (
              <>
                <th className="table-left-column-name" colSpan="4">
                  Omsetning
                </th>
                <th className="table-right-column-name" colSpan="4">
                  Organisasjonens garantiansvar, basert på %-andel av netto
                  omsetning
                </th>
              </>
            )}
          </tr>
        </thead>
        <thead>
          <tr>
            <th onClick={(e) => sortBy(e, "keyId")} className="clickable-column">
              {props.columnName}
            </th>
            <th onClick={(e) => sortBy(e, "key")} className="clickable-column">
              {groupingName}
            </th>
            <th
              onClick={(e) => sortByNested(e, "total.antallKonti")}
              className="clickable-column"
            >
              Antall konti
            </th>
            {props.grantedName !== grantedPrivate &&
              props.grantedName !== grantedCooperative && (
                <th
                  onClick={(e) => sortByNested(e, "total.bevilget")}
                  className="clickable-column"
                >
                  Bevilget
                </th>
              )}
            {(props.grantedName === grantedPrivate ||
              props.grantedName === grantedCooperative) && (
              <th
                onClick={(e) => sortByNested(e, "egen.omsetning")}
                className="clickable-column"
              >
                Netto omsetning
              </th>
            )}
            <th
              onClick={(e) => sortByNested(e, "ansvar.basertPaaBevilgning")}
              className="based-granted-column"
            >
              Basert på bevilgning (50%)
            </th>
            <th
              onClick={(e) => sortByNested(e, "ansvar.basertPaaOmsetning")}
              className="clickable-column"
            >
              Basert på omsetning (20%)
            </th>
            <th
              onClick={(e) =>
                sortByNested(e, "ansvar.minimumAvBevilgningOgOmsetning")
              }
              className="clickable-column"
            >
              Minimum av bevilgning (50%) og oms. (20%) per konto
            </th>
          </tr>
        </thead>
        <tbody>
          {sortedData.map((data, key) => {
            return (
              <tr key={key}>
                <td>{data.keyId}</td>
                <td>{data.key ? data.key : getOrganizationName(data.keyId)}</td>
                <td>{data.total.antallKonti}</td>
                {props.grantedName !== grantedPrivate &&
                  props.grantedName !== grantedCooperative && (
                    <td>
                      <NumberFormat
                        value={data.total.bevilget}
                        thousandSeparator={true}
                        displayType="text"
                        decimalScale={2}
                        fixedDecimalScale={true}
                        prefix="kr "
                      />
                    </td>
                  )}
                {(props.grantedName === grantedPrivate ||
                  props.grantedName === grantedCooperative) && (
                  <td>
                    <NumberFormat
                      value={data.egen.omsetning}
                      thousandSeparator={true}
                      displayType="text"
                      decimalScale={2}
                      fixedDecimalScale={true}
                      prefix="kr "
                    />
                  </td>
                )}
                <td>
                  <NumberFormat
                    value={data.ansvar.basertPaaBevilgning}
                    thousandSeparator={true}
                    displayType="text"
                    decimalScale={2}
                    fixedDecimalScale={true}
                    prefix="kr "
                  />
                </td>
                <td>
                  <NumberFormat
                    value={data.ansvar.basertPaaOmsetning}
                    thousandSeparator={true}
                    displayType="text"
                    decimalScale={2}
                    fixedDecimalScale={true}
                    prefix="kr "
                  />
                </td>
                <td>
                  <NumberFormat
                    value={data.ansvar.minimumAvBevilgningOgOmsetning}
                    thousandSeparator={true}
                    displayType="text"
                    decimalScale={2}
                    fixedDecimalScale={true}
                    prefix="kr "
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <ReactPaginate
          previousLabel={<MdOutlineChevronLeft />}
          nextLabel={<MdOutlineChevronRight />}
          breakLabel={"..."}
          pageCount={props.reportData.data.length / rowsPerPage}
          onPageChange={handlePageChange}
          marginPagesDisplayed={3}
          pageRangeDisplayed={3}
          containerClassName={"pagination"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextLinkClassName={"page-link"}
          nextClassName={"page-item"}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
      />
    </div>
  );
};

export default GrantedTemplate;

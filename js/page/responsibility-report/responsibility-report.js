import React from "react";
import Container from "../../components/ui/container/container";
import { MdOutlineDescription } from "react-icons/md";
import DropdownContent from "./dropdown-content";

const ResponsibilityReport = () => {
  return (
    <Container
      title="Ansvarsrapporter"
      description="Rapporter om garantiansvar"
      icon={<MdOutlineDescription size="53px" />}
    >
      <DropdownContent />
    </Container>
  );
};

export default ResponsibilityReport;

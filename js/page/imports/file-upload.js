import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import InputField from "../../components/ui/input-field/input-field";
import {
  createImportForBank,
  createImportForOrganization,
} from "../../redux/thunks/import-thunk";
import Papa from "papaparse";
import { fileSize } from "humanize-plus";
import "./file-upload.scss";
import PrimaryButton from "../../components/ui/button/primary-button";
import Spinner from "../../components/spinner";
import Swal from "sweetalert2";

const FileUpload = (props) => {
  const dispatch = useDispatch();
  const [selectedFile, setSelectedFile] = useState({});
  const [localValidationResult, setLocalValidationResult] = useState({});
  const [scanResult, setScanResult] = useState({});
  const [fileContent, setFileContent] = useState("");
  const [isValidating, setIsValidating] = useState(false);
  const [isUnprocessableEntity, setIsUnprocessableEntity] = useState(false);
  const [validationResult, setValidationResult] = useState({});
  const [hasApiError, setHasApiError] = useState(false);

  const handleFileInput = (event) => {
    event.preventDefault();
    const file = event.target.files[0];
    if (file !== null && file !== undefined) {
      setSelectedFile(file);
      readFile(file).then((response) => {
        setFileContent(response);
        scanFile(response).then((response) => {
          setScanResult(response);
          if (props.type === "BANK") {
            setLocalValidationResult(validateFileLocallyBank(response.data));
          } else {
            setLocalValidationResult(
              validateFileLocallyOrganization(response.data)
            );
          }
        });
      });
    }
  };

  const readFile = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.onerror = reject;
      reader.readAsText(file);
    });
  };

  const scanFile = (content) => {
    return new Promise((resolve, reject) => {
      Papa.parse(content, {
        skipEmptyLines: true,
        delimiter: ";",
        complete: (result) => {
          resolve(result);
        },
      });
    });
  };

  const validateFileLocallyOrganization = (scanResult) => {
    let okCount = 0;
    let errorCount = 0;

    scanResult.forEach((row) => {
      if (row.length === 5) {
        okCount++;
      } else {
        errorCount++;
      }
    });

    return {
      ok: okCount,
      errors: errorCount,
      rows: okCount + errorCount,
    };
  };

  const validateFileLocallyBank = (scanResult) => {
    let okCount = 0;
    let errorCount = 0;
    const regex = /^\d{35}(\+|-)/;

    scanResult.forEach((row) => {
      if (row.length !== 1) {
        errorCount++;
        return;
      }
      if (regex.test(row[0].trim())) {
        okCount++;
      } else {
        errorCount++;
      }
    });
    return {
      ok: okCount,
      errors: errorCount,
      rows: okCount + errorCount,
    };
  };

  const displayImportError = () => {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste opp import. Prøv igjen senere",
      "error"
    );
  };

  const validateImport = () => {
    setIsValidating(true);
    let filename = selectedFile.name || "unknown";
    props.type === "BANK"
      ? validateBankImport(fileContent, props.orgnr, filename, props.year)
      : validateOrganizationImport(
          fileContent,
          props.orgnr,
          filename,
          props.year
        );
  };

  const handleImportResponse = (response) => {
    if (response.payload.error) {
      if (response.payload.error.message === "422") {
        setIsUnprocessableEntity(true);
        setIsValidating(false);
      } else {
        setIsValidating(false);
        setHasApiError(true);
        displayImportError();
      }
    } else {
      setValidationResult(response.payload.data);
      setIsValidating(false);
    }
  };

  const validateBankImport = (data, orgnr, filename, year) => {
    dispatch(createImportForBank(data, orgnr, filename, year)).then(
      (response) => {
        handleImportResponse(response);
      }
    );
  };

  const validateOrganizationImport = (data, orgnr, filename, year) => {
    dispatch(createImportForOrganization(data, orgnr, filename, year)).then(
      (response) => {
        handleImportResponse(response);
      }
    );
  };

  return (
    <div>
      <h3>
        Innrapporter data for <b>{props.orgName}</b> - <em>{props.year}</em>
      </h3>
      <h1></h1>
      <div className="file-input-container">
        <InputField
          type="file"
          name="file"
          id="file"
          onChange={handleFileInput}
        />
      </div>
      <div className="file-table-container">
        {scanResult.data && (
          <table className="table && table-striped">
            <thead>
              <tr>
                <th>Filstørrelse</th>
                <th>Rader</th>
              </tr>
            </thead>
            <tbody>
              {selectedFile.size !== undefined && scanResult.data && (
                <tr>
                  <td>{fileSize(selectedFile.size)}</td>
                  <td>{scanResult.data.length}</td>
                </tr>
              )}
            </tbody>
          </table>
        )}
      </div>
      <div className="validation-container">
        {localValidationResult.errors > 0 && (
          <>
            <p>Dette ser ikke ut til å være en korrekt formattert importfil.</p>
            <p>
              Av <span className="text-info">{localValidationResult.rows}</span>{" "}
              rader har{" "}
              <span className="text-danger">
                {localValidationResult.errors}
              </span>{" "}
              rader ugyldig format.
            </p>
          </>
        )}
        {localValidationResult.errors === 0 && scanResult.data && (
          <div className="import-button-container">
            {isValidating && <Spinner />}
            {!validationResult.parseResult && (
              <PrimaryButton
                title="Importer data"
                onClick={validateImport}
                disabled={hasApiError}
              />
            )}
            {isUnprocessableEntity && (
              <p className="text-danger">
                Det oppstod en feil når filen ble importert.
              </p>
            )}
          </div>
        )}
        {validationResult.parseResult && (
          <div>
            {validationResult.parseResult.rowsWithValidationErrors.length ===
              0 && (
              <div className="validation-result-container">
                <p className="text-success">
                  Importerte <b>{validationResult.parseResult.rows.length}</b>{" "}
                  rader.
                </p>
                <div className="import-action-buttons">
                  <Link to={`/reporting/imports`}>
                    <PrimaryButton
                      title="Tilbake til oversikt"
                    />
                  </Link>
                  <Link
                    to={`/reporting/imports/details/${props.type}/${validationResult.mottakerImport.id}`}
                  >
                    <PrimaryButton title="Se detaljer" />
                  </Link>
                </div>
              </div>
            )}
            {validationResult.parseResult.rowsWithValidationErrors.length >
              0 && (
              <div className="validation-with-errors">
                <p className="text-danger">
                  Fant{" "}
                  <b>
                    {
                      validationResult.parseResult.rowsWithValidationErrors
                        .length
                    }
                  </b>{" "}
                  rader med valideringsfeil
                </p>
                <p>
                  Dataene er blitt importert, men kan ikke brukes til årsrapport
                  før problemene er rettet eller akseptert.
                </p>
                <p>
                  {" "}
                  - Klikk på <b>SE DETALJER</b> for mer informasjon om hva som
                  har feilet
                </p>
                <p> - For å rette dataene må du importere en oppdatert fil</p>
                <p>
                  {" "}
                  - Ved å ikke importere en ny fil aksepterer du opplysningene
                  slik de er lest inn
                </p>
                <Link
                  to={`/reporting/imports/details/${props.type}/${validationResult.mottakerImport.id}`}
                >
                  <PrimaryButton title="Se detaljer" />
                </Link>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default FileUpload;

import React, { useState } from "react";
import { useDispatch } from "react-redux";
import ErrorTypes from "../static-data/error-types";
import "../import-detail.scss";
import NumberFormat from "react-number-format";
import ReactTooltip from "react-tooltip";
import Card from "../../../components/ui/card/card";
import ReactPaginate from "react-paginate";
import {
  deleteBankErrorRowsByType,
  deleteOrganizationErrorRowsByType,
} from "../../../redux/thunks/import-thunk";
import { getImportForOrganization } from "../../../redux/thunks/import-thunk";
import { MdOutlineChevronLeft, MdOutlineChevronRight, MdDeleteOutline } from "react-icons/md";
import "./import-error.scss";
import Swal from "sweetalert2";

const ImportError = (props) => {
  const dispatch = useDispatch();
  const importDetail = props.importDetail;
  const errorType = ErrorTypes[props.type];
  const [pageNumber, setPageNumber] = useState(0);
  const rowsPerPage = 10;
  const pagesVisited = pageNumber * rowsPerPage;
  const filtered = importDetail.errors.list.filter(
    (e) => e.message === props.type
  );
  filtered.forEach((e) => {
    e.row = importDetail.rows.list.find((r) => r.index === e.rowIndex);
  });

  let sliced = filtered.slice(pagesVisited, pagesVisited + rowsPerPage);

  const handlePageChange = ({ selected }) => {
    setPageNumber(selected);
  };

  const handleDeleteOrganizationErrorRowsResponse = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke slette radene. Prøv igjen senere",
        "error"
      );
    } else {
      Swal.fire({
        type: "success",
        title: "Vellykket!",
        text: "Radene har blitt slettet.",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.value) {
          dispatch(getImportForOrganization(importDetail.id));
        }
      });
    }
  };

  const handleDeleteErrorRows = () => {
    Swal.fire({
      type: "warning",
      title: "Er du sikker?",
      text: `Dette vil slette ${filtered.length} rader`,
      cancelButtonText: "Avbryt",
      showCancelButton: true,
      confirmButtonText: "Ja",
    }).then((result) => {
      if (result.value) {
        dispatch(
          deleteOrganizationErrorRowsByType(importDetail.id, props.type)
        ).then((response) => {
          handleDeleteOrganizationErrorRowsResponse(response.payload.error);
        });
      }
    });
  };

  const displayCardHeader = () => {
    return (
      <div className="import-error">
        <span>
          <b>{errorType}</b>
        </span>
        <span className="float-end">
          <span className="dropdown">
            <a
              className="dropdown-toggle"
              data-bs-toggle="dropdown"
              data-bs-target="#"
            >
              <span className="text-muted">
                {filtered.length} rader <span className="caret" />
              </span>
            </a>
            <ul className="dropdown-menu">
              <li>
                <a onClick={handleDeleteErrorRows} className="pointer">
                  <span className="text-danger">
                    <MdDeleteOutline />{" "}
                  </span>
                  Slett
                </a>
              </li>
            </ul>
          </span>
        </span>
      </div>
    );
  };

  return (
    <Card title={displayCardHeader()}>
      <table className="table && table-striped">
        <thead>
          <tr>
            <td>Linjenummer</td>
            <td>Orgnummer</td>
            <td>Prodnummer</td>
            <td className="float-right">Netto</td>
            <td className="float-right">Brutto</td>
            <td className="float-right">Kontonummer</td>
          </tr>
        </thead>
        <tbody>
          {sliced.map((error, key) => {
            return (
              <tr key={key}>
                <td>
                  <ReactTooltip />
                  <a data-tip={error.row.rawText}>
                    <span className="text-success">{error.row.index}</span>
                  </a>
                </td>
                <td>{error.row.orgNummer}</td>
                <td>{error.row.prodNummer}</td>
                <td className="float-right">
                  {" "}
                  <NumberFormat
                    value={error.row.netto}
                    thousandSeparator={true}
                    displayType="text"
                    prefix="kr "
                    decimalScale={2}
                    fixedDecimalScale={true}
                  />
                </td>
                <td className="float-right">
                  <NumberFormat
                    value={error.row.brutto}
                    thousandSeparator={true}
                    displayType="text"
                    prefix="kr "
                    decimalScale={2}
                    fixedDecimalScale={true}
                  />
                </td>
                <td className="float-right">
                  <NumberFormat
                    value={error.row.kontoNummer}
                    format="#### ## #####"
                    displayType="text"
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      {filtered.length > rowsPerPage && (
        <ReactPaginate
          previousLabel={<MdOutlineChevronLeft />}
          nextLabel={<MdOutlineChevronRight />}
          breakLabel={"..."}
          pageCount={Math.ceil(filtered.length / rowsPerPage)}
          onPageChange={handlePageChange}
          marginPagesDisplayed={3}
          pageRangeDisplayed={3}
          containerClassName={"pagination"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextLinkClassName={"page-link"}
          nextClassName={"page-item"}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      )}
    </Card>
  );
};

export default React.memo(ImportError);

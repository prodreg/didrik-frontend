import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Spinner from "../../../components/spinner";
import Swal from "sweetalert2";
import { getOrganizationDetails } from "../../../redux/thunks/organization-thunks";
import { deleteOrganizationImport } from "../../../redux/thunks/import-thunk";
import Moment from "react-moment";
import "moment/locale/nb";
import "../import-detail.scss";
import PrimaryButton from "../../../components/ui/button/primary-button";
import NumberFormat from "react-number-format";
import ReactPaginate from "react-paginate";
import ReactTooltip from "react-tooltip";
import ImportError from "../import-error/import-error";
import { MdChevronLeft, MdChevronRight } from "react-icons/md";
import TertiaryButton from "../../../components/ui/button/tertiary-button";
import SecondaryButton from "../../../components/ui/button/secondary-button";

const OrganizationImportDetail = (props) => {
  const importOrg = props.importOrg;
  const organizationDetail = useSelector((state) => state.organizationDetails);
  const [showTable, setShowTable] = useState(true);
  const [showErrors, setShowErrors] = useState(true);
  const [pageNumber, setPageNumber] = useState(0);
  const rowsPerPage = 10;
  const pagesVisited = pageNumber * rowsPerPage;
  const sliced =
    importOrg.data.length !== 0 &&
    importOrg.data.rows.list.slice(pagesVisited, pagesVisited + rowsPerPage);
  const showRows = "Vis rader";
  const hideRows = "Skjul rader";
  const showError = "Vis feildetaljer";
  const hideError = "Skjul feildetaljer";
  const dispatch = useDispatch();

  const errorTypes = importOrg.data.length !== 0 && [
    ...new Set(importOrg.data.errors.list.map((e) => e.message)),
  ];

  useEffect(() => {
    if (importOrg.data.length !== 0) {
      dispatch(getOrganizationDetails(importOrg.data.mottakerId));
    }
  }, [dispatch, importOrg]);

  if (organizationDetail.data.length !== 0) {
    if (organizationDetail.data.details.message) {
      Swal.fire({
        type: "error",
        title: "En feil har oppstått",
        text: "Fant ikke organisasjonsdetaljer. Sender deg tilbake oversikt.",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.value) {
          props.history.push("/reporting/imports");
        }
      });
    }
  }

  const handleDeleteOrganizationImportResponse = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke slette import. Prøv igjen senere",
        "error"
      );
    } else {
      Swal.fire({
        type: "success",
        title: "Vellykket!",
        text: "Importen har blitt slettet. Sender deg tilbake til oversikt",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.value) {
          props.history.push("/reporting/imports");
        }
      });
    }
  };

  const handleDeleteImport = () => {
    Swal.fire({
      type: "warning",
      title: "Er du sikker?",
      text: "Dette vil slette hele importen og all dataen i den",
      cancelButtonText: "Avbryt",
      showCancelButton: true,
      confirmButtonText: "Ja",
    }).then((result) => {
      if (result.value) {
        dispatch(deleteOrganizationImport(importOrg.data.id)).then(
          (response) => {
            handleDeleteOrganizationImportResponse(response.payload.error);
          }
        );
      }
    });
  };

  const handleRowsDisplay = () => {
    setShowTable((prevState) => !prevState);
  };

  const handleErrorsDisplay = () => {
    setShowErrors((prevState) => !prevState);
  };

  const handlePageChange = ({ selected }) => {
    setPageNumber(selected);
  };

  return (
    <div>
      {importOrg.loading && organizationDetail.loading && <Spinner />}
      {importOrg.data.length !== 0 && organizationDetail.loaded && (
        <div>
          <span>
            <h3>
              Innrapportert data for{" "}
              <b>{organizationDetail.data.details.name}</b> -{" "}
              <em>{importOrg.data.forYear}</em>
            </h3>
          </span>
          <div className="details-container">
            <span className="text-muted float-end">
              (importert av <b>{importOrg.data.importedBy}</b>
              {" for "}
              <Moment locale="nb" fromNow>
                {importOrg.data.received}
              </Moment>
              )
            </span>
            <h4>{importOrg.data.filename}</h4>
            <hr></hr>
            {importOrg.data.errors.list.length === 0 && (
              <div>
                <p>
                  <span className="text-success">
                    <b>{importOrg.data.rows.length}</b>
                  </span>{" "}
                  rader, <span className="text-success">0</span> feil.
                </p>
                <div className="float-end">
                  <TertiaryButton
                    title="Slett import"
                    onClick={handleDeleteImport}
                  />
                </div>
                <div className="toggle-table-container">
                  <SecondaryButton
                    title={showTable ? hideRows : showRows}
                    onClick={handleRowsDisplay}
                  />
                </div>
                {showTable && (
                  <div className="rows-container">
                    <table className="table && table-striped">
                      <thead>
                        <tr>
                          <th>Linje</th>
                          <th>Orgnummer</th>
                          <th>Prodnummer</th>
                          <th className="float-right">Netto</th>
                          <th className="float-right">Brutto</th>
                          <th className="float-right">Kontonummer</th>
                        </tr>
                      </thead>
                      <tbody>
                        {sliced.map((row, key) => {
                          return (
                            <tr key={key}>
                              <td>
                                <ReactTooltip />
                                <a data-tip={row.rawText}>
                                  <span className="text-success">
                                    {row.index}
                                  </span>
                                </a>
                              </td>
                              <td>{row.orgNummer}</td>
                              <td>{row.prodNummer}</td>
                              <td className="float-right">
                                <NumberFormat
                                  value={row.netto}
                                  thousandSeparator={true}
                                  displayType="text"
                                  prefix="kr "
                                  decimalScale={2}
                                  fixedDecimalScale={true}
                                />
                              </td>
                              <td className="float-right">
                                <NumberFormat
                                  value={row.brutto}
                                  thousandSeparator={true}
                                  displayType="text"
                                  prefix="kr "
                                  decimalScale={2}
                                  fixedDecimalScale={true}
                                />
                              </td>
                              <td className="float-right">
                                <NumberFormat
                                  value={row.kontoNummer}
                                  format="#### ## #####"
                                  displayType="text"
                                />
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                    {importOrg.data.rows.list.length > rowsPerPage && (
                      <ReactPaginate
                        previousLabel={<MdChevronLeft />}
                        nextLabel={<MdChevronRight />}
                        breakLabel={"..."}
                        pageCount={Math.ceil(
                          importOrg.data.rows.list.length / rowsPerPage
                        )}
                        onPageChange={handlePageChange}
                        marginPagesDisplayed={3}
                        pageRangeDisplayed={3}
                        containerClassName={"pagination"}
                        pageClassName={"page-item"}
                        pageLinkClassName={"page-link"}
                        previousClassName={"page-item"}
                        previousLinkClassName={"page-link"}
                        nextLinkClassName={"page-link"}
                        nextClassName={"page-item"}
                        breakClassName={"page-item"}
                        breakLinkClassName={"page-link"}
                        activeClassName={"active"}
                      />
                    )}
                  </div>
                )}
              </div>
            )}
            {importOrg.data.errors.list.length > 0 && (
              <div>
                <p>
                  {" "}
                  <span className="text-info">
                    <b>{importOrg.data.rows.length}</b>
                  </span>{" "}
                  rader, med{" "}
                  <span className="text-danger">
                    <b>{importOrg.data.errors.length}</b>
                  </span>{" "}
                  feil
                  <br></br>
                </p>
                <div className="float-end">
                  <TertiaryButton
                    title="Slett import"
                    onClick={handleDeleteImport}
                  />
                </div>
                <div className="toggle-table-container">
                  <SecondaryButton
                    title={showErrors ? hideError : showError}
                    onClick={handleErrorsDisplay}
                  />
                </div>
                {showErrors &&
                  errorTypes.map((type, key) => {
                    return (
                      <div className="rows-container" key={key}>
                        <ImportError
                          importDetail={importOrg.data}
                          type={type}
                        />
                      </div>
                    );
                  })}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default withRouter(OrganizationImportDetail);

import React, { useEffect } from "react";
import Container from "../../../components/ui/container/container";
import { useDispatch, useSelector } from "react-redux";
import ValidateUser from "../validate-user";
import { getImportForOrganization } from "../../../redux/thunks/import-thunk";
import OrganizationImportDetail from "./organization-import-detail";
import Swal from "sweetalert2";
import { MdOutlineNotes } from "react-icons/md";

const OrganizationImportDetailContainer = (props) => {
  const importId = props.match.params.importId;
  const importForOrganization = useSelector(
    (state) => state.importForOrganizationReducer
  );
  const dispatch = useDispatch();

  const displayImportDetailError = () => {
    Swal.fire({
      type: "error",
      title: "En feil har oppstått",
      text: "Kunne ikke laste inn importdetaljer. Prøv igjen senere. Sender deg tilbake.",
      confirmButtonText: "Ok",
    }).then((result) => {
      if (result.value) {
        props.history.push("/reporting/imports");
      }
    });
  };

  useEffect(() => {
    dispatch(getImportForOrganization(importId)).then((response) => {
      if (response.payload.error) {
        displayImportDetailError();
      }
    });
  }, [dispatch, importId]);

  return (
    <Container
      title="Importdetaljer"
      description="Detaljer for innrapportert data for mottaker"
      icon={<MdOutlineNotes size="60px" />}
    >
      <ValidateUser roles={["mottaker", "admin", "bank"]} />
      <OrganizationImportDetail importOrg={importForOrganization} />
    </Container>
  );
};

export default OrganizationImportDetailContainer;

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Container from "../../../components/ui/container/container";
import { MdOutlineUploadFile } from "react-icons/md";
import FileUpload from "../file-upload";
import { getOrganizationDetails } from "../../../redux/thunks/organization-thunks";
import Swal from "sweetalert2";
import ValidateUser from "../validate-user";

const OrganizationImport = (props) => {
  const params = new URLSearchParams(props.history.location.search);
  const orgNr = params.get("orgId");
  const year = params.get("forYear");
  const organizationDetail = useSelector((state) => state.organizationDetails);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getOrganizationDetails(orgNr));
  }, [dispatch]);

  if (organizationDetail.data.length !== 0) {
    if (organizationDetail.data.details.message) {
      Swal.fire({
        type: "error",
        title: "En feil har oppstått",
        text: "Fant ikke organisasjonen. Sender deg tilbake.",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.value) {
          props.history.push("/reporting/imports");
        }
      });
    }
  }

  return (
    <Container
      title="Opplastning"
      description={`Opplastning av data for ${
        organizationDetail.data.details && organizationDetail.data.details.name
      }`}
      icon={<MdOutlineUploadFile size="50px" />}
    >
      <ValidateUser roles={["admin", "mottaker"]} />
      {organizationDetail.loaded && (
        <FileUpload
          orgName={organizationDetail.data.details.name}
          type={organizationDetail.data.details.organizationType}
          orgnr={organizationDetail.data.details.orgNumber}
          year={year}
        />
      )}
    </Container>
  );
};

export default OrganizationImport;

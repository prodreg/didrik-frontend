import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Spinner from "../../../components/spinner";
import Swal from "sweetalert2";
import { getOrganizationDetails } from "../../../redux/thunks/organization-thunks";
import { deleteBankImport } from "../../../redux/thunks/import-thunk";
import Moment from "react-moment";
import "moment/locale/nb";
import "../import-detail.scss";
import PrimaryButton from "../../../components/ui/button/primary-button";
import NumberFormat from "react-number-format";
import ReactPaginate from "react-paginate";
import ReactTooltip from "react-tooltip";
import { MdOutlineChevronLeft, MdOutlineChevronRight } from "react-icons/md";
import TertiaryButton from "../../../components/ui/button/tertiary-button";
import SecondaryButton from "../../../components/ui/button/secondary-button";

const BankImportDetail = (props) => {
  const importBank = props.importBank;
  const organizationDetail = useSelector((state) => state.organizationDetails);
  const [showTable, setShowTable] = useState(true);
  const [pageNumber, setPageNumber] = useState(0);
  const rowsPerPage = 10;
  const pagesVisited = pageNumber * rowsPerPage;
  const sliced =
    importBank.data.length !== 0 &&
    importBank.data.rows.list.slice(pagesVisited, pagesVisited + rowsPerPage);
  const showRows = "Vis rader";
  const hideRows = "Skjul rader";
  const dispatch = useDispatch();

  useEffect(() => {
    if (importBank.data.length !== 0) {
      dispatch(getOrganizationDetails(importBank.data.mottakerId));
    }
  }, [dispatch, importBank]);

  if (organizationDetail.data.length !== 0) {
    if (organizationDetail.data.details.message) {
      Swal.fire({
        type: "error",
        title: "En feil har oppstått",
        text: "Fant ikke organisasjonsdetaljer. Sender deg tilbake til oversikt.",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.value) {
          props.history.push("/reporting/imports");
        }
      });
    }
  }

  const handleDeleteBankImportResponse = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Kunne ikke slette import. Prøv igjen senere",
        "error"
      );
    } else {
      Swal.fire({
        type: "success",
        title: "Vellykket!",
        text: "Importen har blitt slettet. Sender deg tilbake til oversikt",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.value) {
          props.history.push("/reporting/imports");
        }
      });
    }
  };

  const handleDeleteImport = () => {
    Swal.fire({
      type: "warning",
      title: "Er du sikker?",
      text: "Dette vil slette hele importen og all dataen i den",
      confirmButtonText: "Ja",
      showCancelButton: true,
      cancelButtonText: "Avbryt",
    }).then((result) => {
      if (result.value) {
        dispatch(deleteBankImport(importBank.data.id)).then((response) => {
          handleDeleteBankImportResponse(response.payload.error);
        });
      }
    });
  };

  const handleRowsDisplay = (event) => {
    event.preventDefault();
    setShowTable((prevState) => !prevState);
  };

  const handlePageChange = ({ selected }) => {
    setPageNumber(selected);
  };

  return (
    <div>
      {importBank.loading && organizationDetail.loading && <Spinner />}
      {importBank.data.length !== 0 && organizationDetail.loaded && (
        <div>
          <span>
            <h3>
              Innrapportert data for{" "}
              <b>{organizationDetail.data.details.name}</b> -{" "}
              <em>{importBank.data.forYear}</em>
            </h3>
          </span>
          <div className="details-container">
            <span className="text-muted float-end">
              (importert av <b>{importBank.data.importedBy}</b>
              {" for "}
              <Moment locale="nb" fromNow>
                {importBank.data.received}
              </Moment>
              )
            </span>
            <h4>{importBank.data.filename}</h4>
            <hr></hr>
            {importBank.data.errors.list.length === 0 && (
              <div>
                <p>
                  <span className="text-success">
                    <b>{importBank.data.rows.length}</b>
                  </span>{" "}
                  rader, <span className="text-success">0</span> feil.
                </p>
                <div className="float-end">
                  <TertiaryButton
                    title="Slett import"
                    onClick={handleDeleteImport}
                  />
                </div>
                <div className="toggle-table-container">
                  <SecondaryButton
                    title={showTable ? hideRows : showRows}
                    onClick={handleRowsDisplay}
                  />
                </div>
                {showTable && (
                  <div className="rows-container">
                    <table className="table && table-striped">
                      <thead>
                        <tr>
                          <th>Linje</th>
                          <th>Konsern</th>
                          <th>Kontonummer</th>
                          <th className="float-right">Bevilget</th>
                          <th className="float-right">Saldo</th>
                        </tr>
                      </thead>
                      <tbody>
                        {sliced.map((row, key) => {
                          return (
                            <tr key={key}>
                              <td>
                                <ReactTooltip />
                                <a data-tip={row.rawText}>
                                  <span className="text-success">
                                    {row.rowIndex}
                                  </span>
                                </a>
                              </td>
                              <td>{row.konsern}</td>
                              <td>
                                <NumberFormat
                                  value={row.kontonummer}
                                  format="#### ## #####"
                                  displayType="text"
                                />
                              </td>
                              <td className="float-right">
                                <NumberFormat
                                  value={row.bevilget}
                                  thousandSeparator={true}
                                  displayType="text"
                                  prefix="kr "
                                  decimalScale={2}
                                  fixedDecimalScale={true}
                                />
                              </td>
                              <td className="float-right">
                                <NumberFormat
                                  value={row.saldo}
                                  thousandSeparator={true}
                                  displayType="text"
                                  prefix="kr "
                                  decimalScale={2}
                                  fixedDecimalScale={true}
                                />
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                    {importBank.data.rows.list.length > rowsPerPage && (
                      <ReactPaginate
                        previousLabel={<MdOutlineChevronLeft />}
                        nextLabel={<MdOutlineChevronRight />}
                        breakLabel={"..."}
                        pageCount={Math.ceil(
                          importBank.data.rows.list.length / rowsPerPage
                        )}
                        onPageChange={handlePageChange}
                        marginPagesDisplayed={3}
                        pageRangeDisplayed={3}
                        containerClassName={"pagination"}
                        pageClassName={"page-item"}
                        pageLinkClassName={"page-link"}
                        previousClassName={"page-item"}
                        previousLinkClassName={"page-link"}
                        nextLinkClassName={"page-link"}
                        nextClassName={"page-item"}
                        breakClassName={"page-item"}
                        breakLinkClassName={"page-link"}
                        activeClassName={"active"}
                      />
                    )}
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default withRouter(BankImportDetail);

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Container from "../../../components/ui/container/container";
import ValidateUser from "../validate-user";
import { getImportForBank } from "../../../redux/thunks/import-thunk";
import BankImportDetail from "./bank-import-detail";
import { MdOutlineNotes } from "react-icons/md";
import Swal from "sweetalert2";

const BankImportDetailContainer = (props) => {
  const importId = props.match.params.importId;
  const importForBank = useSelector((state) => state.importForBankReducer);
  const dispatch = useDispatch();

  const displayImportDetailError = () => {
    Swal.fire({
      type: "error",
      title: "En feil har oppstått",
      text: "Kunne ikke laste inn importdetaljer. Prøv igjen senere. Sender deg tilbake.",
      confirmButtonText: "Ok",
    }).then((result) => {
      if (result.value) {
        props.history.push("/reporting/imports");
      }
    });
  };

  useEffect(() => {
    dispatch(getImportForBank(importId)).then((response) => {
      if (response.payload.error) {
        displayImportDetailError();
      }
    });
  }, [dispatch, importId]);

  return (
    <Container
      title="Importdetaljer"
      description="Detaljer for innrapportert data for bank"
      icon={<MdOutlineNotes size="60px" />}
    >
      <ValidateUser roles={["admin", "bank"]} />
      <BankImportDetail importBank={importForBank} />
    </Container>
  );
};

export default BankImportDetailContainer;

import React from "react";
import Container from "../../components/ui/container/container";
import { MdOutlineAssessment } from "react-icons/md";
import ImportList from "./import-list";

const Imports = () => {
  return (
    <Container
      title="Innrapportering"
      description="Oversikt over innrapportert data"
      icon={<MdOutlineAssessment size="54px" />}
    >
      <ImportList />
    </Container>
  );
};

export default Imports;

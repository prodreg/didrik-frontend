import React from "react";
import { Redirect } from "react-router-dom";
import userService from "../../service/user-service";

const ValidateUser = (props) => {
  const userRole = userService.getLoggedInUser().roles[0];
  const authorizedRoles = props.roles;
  return (
    <div>
      {authorizedRoles.indexOf(userRole) === -1 ? <Redirect to="/" /> : null}
    </div>
  );
};

export default ValidateUser;

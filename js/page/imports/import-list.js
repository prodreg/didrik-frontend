import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  getAllChildrenForOrganization,
  getAllOrganizations,
} from "../../redux/thunks/organization-thunks";
import { getAllImports } from "../../redux/thunks/import-thunk";
import ValidateUser from "./validate-user";
import years from "./static-data/years";
import { MdOutlineCheckCircleOutline } from "react-icons/md";
import { MdOutlineWarningAmber } from "react-icons/md";
import { MdChevronLeft, MdChevronRight } from "react-icons/md";
import userService from "../../service/user-service";
import Spinner from "../../components/spinner";
import InputField from "../../components/ui/input-field/input-field";
import Swal from "sweetalert2";
import ReactPaginate from "react-paginate";
import "./import-list.scss";

const ImportList = () => {
  const user = userService.getLoggedInUser();
  const userRole = user.roles[0];
  const userOrganizationNr = user.orgNumber;
  const organizationData = useSelector((state) => state.allOrganizations);
  const importData = useSelector((state) => state.importReducer);
  const childrenOfOrganizationData = useSelector(
    (state) => state.allChildrenForOrganizationReducer
  );
  const dispatch = useDispatch();
  let orgImports = {};
  let childrenOrgImports = {};
  let importsForUser = [];
  const [query, setQuery] = useState("");
  const [searchParam] = useState(["name"]);
  const [pageNumber, setPageNumber] = useState(0);
  const importsPerPage = 20;
  const pagesVisited = importsPerPage * pageNumber;
  let slicedImports = organizationData.data.slice(
    pagesVisited,
    pagesVisited + importsPerPage
  );
  const roles = ["mottaker", "bank"];

  useEffect(() => {
    dispatch(getAllOrganizations(roles)).then((response) => {
      if (response.payload.error) {
        displayFetchingDataError();
      }
    });
    dispatch(getAllImports()).then((response) => {
      if (response.payload.error) {
        displayFetchingDataError();
      }
    });
    dispatchChildrenForOrganization();
  }, [dispatch]);

  const dispatchChildrenForOrganization = () => {
    if (userRole !== "admin") {
      dispatch(getAllChildrenForOrganization(userOrganizationNr)).then(
        (response) => {
          if (response.payload.error) {
            displayFetchingDataError();
          }
        }
      );
    }
  };

  // If the list of children organizations are not zero, populate data with imports data
  if (
    childrenOfOrganizationData.data.length !== 0 &&
    childrenOfOrganizationData.loaded &&
    organizationData.loaded &&
    roles.indexOf(userRole) > -1
  ) {
    const userImport = organizationData.data.filter(
      (o) => o.orgNumber === userOrganizationNr
    );
    if (!childrenOfOrganizationData.data.includes(userImport[0])) {
      childrenOfOrganizationData.data.push(userImport[0]);
    }
    childrenOfOrganizationData.data.forEach(
      (i) => (childrenOrgImports[i.orgNumber] = i)
    );
    importData.data.forEach((i) => {
      if (childrenOrgImports.hasOwnProperty(i.mottakerId)) {
        childrenOrgImports[i.mottakerId][i.forYear] = i;
      }
    });
    slicedImports = childrenOfOrganizationData.data.slice(
      pagesVisited,
      pagesVisited + importsPerPage
    );
    // if the list of children of organizations is empty, but the user is either mottaker or bank,
    // filter organization data to get that particular organization
  } else if (
    roles.indexOf(userRole) > -1 &&
    childrenOfOrganizationData.data.length === 0 &&
    childrenOfOrganizationData.loaded
  ) {
    importsForUser = organizationData.data.filter(
      (o) => o.orgNumber === userOrganizationNr
    );
    slicedImports = importsForUser.slice(
      pagesVisited,
      pagesVisited + importsPerPage
    );
  }

  // Populates organization data with import data by checking if the given organizations have imports data
  if (organizationData.loaded && importData.loaded) {
    organizationData.data.forEach((i) => (orgImports[i.orgNumber] = i));
    importData.data.forEach((i) => {
      if (orgImports.hasOwnProperty(i.mottakerId)) {
        orgImports[i.mottakerId][i.forYear] = i;
      }
    });
  }

  const displayFetchingDataError = () => {
    Swal.fire(
      "En feil har oppstått",
      "Kunne ikke laste inn importoversikt. Prøv igjen senere",
      "error"
    );
  };

  const handlePageChange = ({ selected }) => {
    setPageNumber(selected);
  };

  const handleSearchQuery = (event) => {
    setQuery(event.target.value);
  };

  const displaySearchBar = () => {
    return (
      <div className="search-organization-container">
        <div className="form-floating mb-3">
          <input
            type="search"
            className="form-control form-control-sm"
            id="searchInput"
            value={query}
            onChange={handleSearchQuery}
            placeholder="Søk etter organisasjon..."
          />
          <label htmlFor="searchInput">Søk etter organisasjon...</label>
        </div>
      </div>
    );
  };

  // Filter data by searching, where the search param is the name of the organization
  // In case of searching when not on page 1, set slice index to 0
  // Returns filtered data and length of filtered data to use for pagination
  const filterData = (data) => {
    const filtered = data.filter((i) => {
      return searchParam.some((p) => {
        return i[p].toString().toLowerCase().indexOf(query.toLowerCase()) > -1;
      });
    });
    if (filtered.length > importsPerPage) {
      return [
        filtered.slice(pagesVisited, pagesVisited + importsPerPage),
        filtered.length,
      ];
    }
    return [filtered.slice(0, importsPerPage), filtered.length];
  };

  // Filter data depending on the user role
  // If query is equal to "", just return the input data as-is
  const getFilteredOrganization = (data) => {
    if (
      organizationData.data.length !== 0 &&
      query !== "" &&
      userRole === "admin"
    ) {
      return filterData(organizationData.data);
    }
    if (roles.indexOf(userRole) > -1) {
      if (childrenOfOrganizationData.data.length !== 0 && query !== "") {
        return filterData(childrenOfOrganizationData.data);
      }
      if (importsForUser.length !== 0 && query !== "") {
        return filterData(importsForUser);
      }
    }
    return [data, data.length];
  };

  // Calculate page count depending on the user that is logged in
  // and the data the user has acccess to
  const calculatePageCount = () => {
    if (roles.indexOf(userRole) > -1) {
      if (childrenOfOrganizationData.data.length !== 0) {
        return Math.ceil(
          getFilteredOrganization(childrenOfOrganizationData.data)[1] /
            importsPerPage
        );
      }
      if (importsForUser.length !== 0) {
        return Math.ceil(
          getFilteredOrganization(importsForUser)[1] / importsPerPage
        );
      }
    }
    return Math.ceil(
      getFilteredOrganization(organizationData.data)[1] / importsPerPage
    );
  };

  // Show pagination depending on length of data
  const showPagination = () => {
    if (userRole === "admin") {
      return (
        organizationData.data.length > importsPerPage &&
        getFilteredOrganization(organizationData.data)[1] > importsPerPage
      );
    }
    return (
      childrenOfOrganizationData.data.length > importsPerPage &&
      getFilteredOrganization(childrenOfOrganizationData.data)[1] >
        importsPerPage &&
      importsForUser.length > importsPerPage &&
      getFilteredOrganization(importsForUser)[1] > importsPerPage
    );
  };

  const displayImportTable = () => {
    return (
      <div className="imports-list-container">
        <table className="table && table-striped">
          <thead>
            <tr>
              <th>Organisasjon</th>
              {years.map((year, key) => {
                return <th key={key}>{year}</th>;
              })}
            </tr>
          </thead>
          <tbody>
            {getFilteredOrganization(slicedImports)[0].map((o, key) => {
              return (
                <tr key={key}>
                  <td>{o.name}</td>
                  {years.map((year, key) => {
                    return (
                      <td key={key}>
                        {!o[year] && (
                          <div>
                            {o.organizationType === "BANK" && (
                              <Link
                                className="import-link"
                                to={`/reporting/imports/bank?orgId=${o.orgNumber}&forYear=${year}`}
                              >
                                <span className="text-info small">
                                  Ikke levert
                                </span>
                              </Link>
                            )}
                            {o.organizationType === "MOTTAKER" && (
                              <Link
                                className="import-link"
                                to={`/reporting/imports/mottaker?orgId=${o.orgNumber}&forYear=${year}`}
                              >
                                <span className="text-info small">
                                  Ikke levert
                                </span>
                              </Link>
                            )}
                          </div>
                        )}
                        {o[year] && o[year].errors.length === 0 && (
                          <div>
                            {o.organizationType === "BANK" && (
                              <Link
                                className="import-link"
                                to={`/reporting/imports/details/bank/${o[year].id}`}
                              >
                                <span className="text-success small">
                                  <MdOutlineCheckCircleOutline /> {o[year].rows.length}
                                </span>
                              </Link>
                            )}
                            {o.organizationType === "MOTTAKER" && (
                              <Link
                                className="import-link"
                                to={`/reporting/imports/details/mottaker/${o[year].id}`}
                              >
                                <span className="text-success small">
                                  <MdOutlineCheckCircleOutline /> {o[year].rows.length}
                                </span>
                              </Link>
                            )}
                          </div>
                        )}
                        {o[year] && o[year].errors.length > 0 && (
                          <div>
                            {o.organizationType === "BANK" && (
                              <Link
                                className="import-link"
                                to={`/reporting/imports/details/bank/${o[year].id}`}
                              >
                                <span className="text-danger small">
                                  <MdOutlineWarningAmber /> {o[year].errors.length}
                                </span>
                                <span className="small"> / </span>
                                <span className="text-danger small">
                                  {o[year].rows.length}
                                </span>
                              </Link>
                            )}
                            {o.organizationType === "MOTTAKER" && (
                              <Link
                                className="import-link"
                                to={`/reporting/imports/details/mottaker/${o[year].id}`}
                              >
                                <span className="text-danger small">
                                  <MdOutlineWarningAmber /> {o[year].errors.length}
                                </span>
                                <span className="small"> / </span>
                                <span className="text-danger small">
                                  {o[year].rows.length}
                                </span>
                              </Link>
                            )}
                          </div>
                        )}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        {showPagination() && (
          <ReactPaginate
            previousLabel={<MdChevronLeft />}
            nextLabel={<MdChevronRight />}
            breakLabel={"..."}
            pageCount={calculatePageCount()}
            onPageChange={handlePageChange}
            marginPagesDisplayed={3}
            pageRangeDisplayed={3}
            containerClassName={"pagination"}
            pageClassName={"page-item"}
            pageLinkClassName={"page-link"}
            previousClassName={"page-item"}
            previousLinkClassName={"page-link"}
            nextLinkClassName={"page-link"}
            nextClassName={"page-item"}
            breakClassName={"page-item"}
            breakLinkClassName={"page-link"}
            activeClassName={"active"}
            forcePage={pageNumber}
          />
        )}
      </div>
    );
  };

  return (
    <div>
      <ValidateUser roles={["mottaker", "admin", "bank"]} />
      {(organizationData.loading ||
        importData.loading ||
        childrenOfOrganizationData.loading) && <Spinner />}
      {displaySearchBar()}
      {userRole === "admin" &&
        organizationData.loaded &&
        importData.loaded &&
        displayImportTable()}
      {roles.indexOf(userRole) > -1 &&
        childrenOfOrganizationData.loaded &&
        organizationData.loaded &&
        importData.loaded &&
        displayImportTable()}
    </div>
  );
};

export default ImportList;

export default {
  "netto-ugyldig-format": "Netto ugyldig format",
  "brutto-ugyldig-format": "Brutto ugyldig format",
  "netto-0": "0,- Netto",
  "netto-less-than-0": "Netto mindre enn 0",
  "brutto-0": "0,- Brutto",
  "brutto-less-than-0": "Brutto mindre enn 0",

  "produsentnummer-size-10": "Produsentnummer består av 10 siffer",
  "produsentnummer-00": "Produsentnummer starter med 00",
  "produsentnummer-invalid": "Produsentnummer består av 10 siffer",

  "orgnummer-size-9": "Organisasjonsnummer består av 9 siffer",
  "orgnummer-invalid": "Organisasjonsnummer består av 9 siffer",

  "kontonummer-0": "Kontonummer er 0",
  "kontonummer-invalid": "Kontonummer ugyldig format",
};

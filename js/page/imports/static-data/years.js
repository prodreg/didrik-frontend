export default Array.from(
  new Array(2),
  (val, i) => new Date().getFullYear() - 1 - i
);

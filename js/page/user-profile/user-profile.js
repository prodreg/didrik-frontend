import React from "react";
import { connect } from "react-redux";
import Spinner from "../../components/spinner";
import userService from "../../service/user-service";
import Container from "../../components/ui/container/container";
import { Row, Col } from "react-bootstrap";
import {
  updateUserEmail,
  subscribeUserToEmail,
} from "../../redux/thunks/user-thunks";
import { getUserInfo } from "../../redux/thunks/user-info-thunks";
import { getStoreDataForUserInfo } from "../../redux/selectors";
import { MdOutlinePersonOutline } from "react-icons/md";
import { Link } from "react-router-dom";
import "./user-profile.scss";
import NewEmail from "../../components/new-email";
import ChangePassword from "./change-password";
import CheckboxWithLabel from "../../components/ui/checkbox/checkbox-with-label";

class UserProfile extends React.Component {
  state = {
    isSelf: false,
    updatingSubscriber: false,
    showChangeEmail: false,
    showChangePassword: false,
    isSubscribedToEmail: false,
  };

  constructor(props) {
    super(props);
    this.username = props.match.params.username;
  }

  componentDidMount() {
    const loggedInUser = userService.getLoggedInUser().username;
    if (loggedInUser === this.username) {
      this.setState({ isSelf: true });
    }
    this.props.getUserInfo(this.username).then((res) => {
      this.setState({ isSubscribedToEmail: res.subscriber });
    });
  }

  toggleChangeEmailDialog = () => {
    this.setState({
      showChangeEmail: !this.state.showChangeEmail,
    });
  };

  toggleChangePasswordDialog = () => {
    this.setState({
      showChangePassword: !this.state.showChangePassword,
    });
  };

  handleSubscriptionToEmail = (event) => {
    const subscribed = event.target.checked;
    this.setState({
      updatingSubscriber: true,
    });

    this.props.subscribeUserToEmail(this.username, subscribed).then(() => {
      this.setState({
        updatingSubscriber: false,
        isSubscribedToEmail: subscribed,
      });
    });
  };

  render() {
    const userInfo = this.props.userInfo;

    return (
      <Container
        title={"Brukerprofil"}
        description={"Informasjon om bruker og brukers organisasjon"}
        icon={<MdOutlinePersonOutline size="60px" />}
      >
        <Row>
          <Col fluid="lg">
            {userInfo.loading && <Spinner />}
            {userInfo.loaded && (
              <>
                {userInfo.error && (
                  <div>
                    Fant ikke noen informasjon om bruker {this.username}
                  </div>
                )}
                {!userInfo.error && (
                  <div className="user-profile">
                    <h3>Bruker</h3>
                    <dl className="dl-horizontal">
                      <dt>Navn</dt>
                      <dd id="produsent-navn">{userInfo.data.fullName}</dd>
                      <dt>Brukernavn</dt>
                      <dd>{userInfo.data.id}</dd>
                      <dt>E-post</dt>
                      <dd>{userInfo.data.email}</dd>
                      {this.state.isSelf && (
                        <>
                          <dd>
                            <a
                              style={{ cursor: "pointer" }}
                              onClick={this.toggleChangeEmailDialog}
                              className="text-small"
                            >
                              Endre e-post adresse
                            </a>
                          </dd>
                          <dd>
                            <a
                              style={{ cursor: "pointer" }}
                              onClick={this.toggleChangePasswordDialog}
                              className="text-small"
                            >
                              Endre passord
                            </a>
                          </dd>
                          <dd>
                            <CheckboxWithLabel
                              checked={this.state.isSubscribedToEmail}
                              disabled={this.state.updatingSubscriber}
                              onChange={this.handleSubscriptionToEmail.bind(
                                this
                              )}
                              label="Abonner på e-post"
                            />
                            {this.state.updatingSubscriber && <Spinner />}
                          </dd>
                        </>
                      )}
                    </dl>
                    <hr />
                    <h3>Organisasjon</h3>
                    <dl className="dl-horizontal">
                      <dt>Organisasjon</dt>
                      <dd>
                        <Link
                          to={
                            "/organizations/details/" +
                            userInfo.data.organization.orgNumber
                          }
                        >
                          {userInfo.data.organization.name}
                        </Link>
                      </dd>
                      <dt>Organisasjonsnummer</dt>
                      <dd>{userInfo.data.organization.orgNumber}</dd>
                      <dt>Organisasjonstype</dt>
                      <dd>{userInfo.data.organization.organizationType}</dd>
                    </dl>
                  </div>
                )}
              </>
            )}
          </Col>
        </Row>
        <NewEmail
          updateTarget={this.props.updateUserEmail}
          targetId={this.username}
          showChangeEmail={this.state.showChangeEmail}
          toggleShowChangeEmail={this.toggleChangeEmailDialog}
          history={this.props.history}
        />
        <ChangePassword
          username={this.username}
          toggleShowChangePassword={this.toggleChangePasswordDialog}
          history={this.props.history}
          showChangePassword={this.state.showChangePassword}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const userInfo = getStoreDataForUserInfo(state);

  return { userInfo };
};

const mapDispatchToProps = {
  getUserInfo,
  updateUserEmail,
  subscribeUserToEmail,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);

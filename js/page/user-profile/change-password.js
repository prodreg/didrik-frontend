import React from "react";
import { Modal, Button, Row, Col, Form, FloatingLabel } from "react-bootstrap";
import { changeUserPassword } from "../../redux/thunks/user-thunks";
import { connect } from "react-redux";
import Swal from "sweetalert2";
import TertiaryButton from "../../components/ui/button/tertiary-button";
import SecondaryButton from "../../components/ui/button/secondary-button";

class ChangePassword extends React.Component {

  state = {
    oldPassword: "",
    newPassword: "",
    newPasswordConfirm: "",
    validated: false,
    validCurrentPassword: {
      isValid: false,
      nonEmpty: false
    },
    validNewPassword: {
      isValid: false,
      validMinLength: false,
      nonEmpty: false
    },
    validConfirmNewPassword: {
      isValid: false,
      nonEmpty: false,
      equalsNewPassword: false
    },
    validForm: false,
    saving: false
  };

  constructor(props) {
    super(props);
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    },() => {
      this.validateCurrentPassword();
      this.validateNewPassword();
      this.validateConfirmNewPassword();
    });
  }

  closeDialog = () => {
    this.setState({
      oldPassword: "",
      newPassword: "",
      newPasswordConfirm: "",
      validated: false,
      validCurrentPassword: {
        isValid: false,
        nonEmpty: false
      },
      validNewPassword: {
        isValid: false,
        validMinLength: false,
        nonEmpty: false
      },
      validConfirmNewPassword: {
        isValid: false,
        nonEmpty: false,
        equalsNewPassword: false
      },
      validForm: false,
      saving: false
    });

    this.props.toggleShowChangePassword();
  }

  validateForm = () => {
    const validCurrentPassword = this.state.validCurrentPassword.isValid && this.state.validCurrentPassword.nonEmpty;
    const validNewPassword = this.state.validNewPassword.isValid && this.state.validNewPassword.nonEmpty
      && this.state.validNewPassword.validMinLength;
    const validConfirmNewPassword = this.state.validConfirmNewPassword.isValid && this.state.validConfirmNewPassword.nonEmpty
      && this.state.validConfirmNewPassword.equalsNewPassword;
    const isValid = validCurrentPassword && validNewPassword && validConfirmNewPassword;

    this.setState({
      validForm: isValid
    });
  }

  validateCurrentPassword = () => {
    const password = this.state.oldPassword;
    var isValid = true;
    var nonEmpty = true;

    if (!password) {
      isValid = false;
      nonEmpty = false;
    }

    const validCurrentPassword = {
      isValid: isValid,
      nonEmpty: nonEmpty
    };

    this.setState({
      validCurrentPassword: validCurrentPassword
    }, () => {
      this.validateForm()
    });
  }

  validateNewPassword = () => {
    const password = this.state.newPassword;
    var isValid = true;
    var nonEmpty = true;
    var validMinLength = true;

    if (!password) {
      isValid = false;
      nonEmpty = false;
    }

    if (password.length < 6) {
      isValid = false;
      validMinLength = false;
    }

    const validNewPassword = {
      isValid: isValid,
      nonEmpty: nonEmpty,
      validMinLength: validMinLength
    };

    this.setState({
      validNewPassword: validNewPassword
    }, () => {
      this.validateForm()
    });
  }

  validateConfirmNewPassword = () => {
    const password = this.state.newPasswordConfirm;
    var isValid = true;
    var nonEmpty = true;
    var equalsNewPassword = true;

    if (!password) {
      isValid = false;
      nonEmpty = false;
    }

    if (password !== this.state.newPassword) {
      isValid = false;
      equalsNewPassword = false;
    }

    const validConfirmNewPassword = {
      isValid: isValid,
      nonEmpty: nonEmpty,
      equalsNewPassword: equalsNewPassword
    };

    this.setState({
      validConfirmNewPassword: validConfirmNewPassword
    }, () => {
      this.validateForm()
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    const validForm = this.state.validForm;
    if (form.checkValidity() === false || !validForm) {
      event.stopPropagation();
    } else {
      this.setState({
        saving: true
      });

      this.props.changeUserPassword(this.props.username, this.state.oldPassword, this.state.newPassword).then((res) => {
        this.handleError(res.payload.error);
      })
      .finally(() => {
        this.setState({
          saving: false
        });
      });
    }

    this.setState({
      validated: true
    })

  }

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Passord kunne ikke bli endret. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire(
        "OK",
        "Passord har blitt endret.",
        "success"
      ).then(() => {
        this.props.history.go(0);
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.showChangePassword} onHide={this.closeDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Endre Passord</Modal.Title>
        </Modal.Header>
        <Form noValidate onSubmit={ this.handleSubmit }>
            <Modal.Body>
              <Row className="mb-3">
                <Form.Group>
                  <FloatingLabel label="Nåværende Passord">
                    <Form.Control required type="password" placeholder="Nåværende Passord" name="oldPassword"
                      isInvalid={!this.state.validCurrentPassword.isValid && this.state.validated} onChange={ this.handleChange } />
                  </FloatingLabel>
                  {this.state.validated && (
                    <Row>
                    {!this.state.validCurrentPassword.nonEmpty && (
                      <Form.Text className="text-danger text-small">
                        Du må skrive inn det nåværende passordet
                      </Form.Text>
                    )}
                    </Row>
                  )}
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group as={Col}>
                  <FloatingLabel label="Nytt Passord">
                    <Form.Control required type="password" name="newPassword" placeholder="Nytt Passord" onChange={ this.handleChange }
                      isInvalid={!this.state.validNewPassword.isValid && this.state.validated} />
                  </FloatingLabel>
                  {this.state.validated && (
                    <>
                    <Row>
                    {!this.state.validNewPassword.nonEmpty && (
                      <Form.Text className="text-danger text-small">
                        Du må skrive inn det nye passordet
                      </Form.Text>
                    )}
                    </Row>
                    <Row>
                    {!this.state.validNewPassword.validMinLength && (
                      <Form.Text className="text-danger text-small">
                        Passordet må være minst 6 tegn
                      </Form.Text>
                    )}
                    </Row>
                    </>
                  )}
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group>
                  <FloatingLabel label="Nytt Passord (gjenta)">
                    <Form.Control required type="password" placeholder="Nytt Passord (gjenta)" name="newPasswordConfirm"
                      isInvalid={!this.state.validConfirmNewPassword.isValid && this.state.validated} onChange={ this.handleChange } />
                  </FloatingLabel>
                  {this.state.validated && (
                    <Row>
                    {!this.state.validConfirmNewPassword.isValid && (
                      <Form.Text className="text-danger text-small">
                        Du må skrive inn samme passord to ganger
                      </Form.Text>
                    )}
                    </Row>
                  )}
                </Form.Group>
              </Row>
            </Modal.Body>
            <Modal.Footer>
            <TertiaryButton
              type="reset"
              title="Avbryt"
              onClick={this.closeDialog}
              disabled={ this.state.saving }
            />
            <SecondaryButton
              type="submit"
              title="Lagre"
              disabled={ (!this.state.validForm && this.state.validated) || this.state.saving }
            />
            </Modal.Footer>
        </Form>
      </Modal>
    );
  }


}

ChangePassword.defaultProps = {
  showChangePassword: false
};

const mapStateToProps = (state) => {
  return { };
}

const mapDispatchToProps = {
  changeUserPassword
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);

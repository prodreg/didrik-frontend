import { withRouter, Redirect } from "react-router-dom";
import React from "react";
import { getStoreDataForUser } from "../redux/selectors";
import { login } from "../redux/thunks/user-thunks";
import { connect } from "react-redux";
import userService from "../service/user-service";
import "./login.scss";
import Logo from "../../img/logo.svg";
import ForgotPassword from "../components/forgot-password";

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			username: "",
			password: "",
			error: false,
			showForgotPassword: false,
		};

		this.handleChange = this.handleChange.bind(this);
		this.loginSubmit = this.loginSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value,
		});
	}

	loginSubmit(event) {
		event.preventDefault();
		this.props
			.login(this.state.username, this.state.password)
			.then((res) => {
				this.handleError(res);
			});
	}

	handleError = (response) => {
		if (response.error) {
			this.setState({
				error: true,
			});
		} else {
			this.login(response.payload.user);
		}
	};

	login = (user) => {
		userService.loginUser(user);
		this.props.history.push({
			pathname: "/",
		});
	};

	toggleShowForgottenPasswordDialog = () => {
		this.setState({
			showForgotPassword: !this.state.showForgotPassword,
		});
	};

	render() {
		return (
			<div
				className="container"
				style={{ marginTop: "10px", paddingInline: 10 }}
			>
				<div className="row" style={{ marginBottom: "10px" }}>
					<div className="col-md-8 offset-md-2">
						<div className="card">
							<div
								className="card-header"
								style={{ fontWeight: "bold" }}
							>
								Velkommen til Landbrukets Dataflyt sin
								skjematjeneste for driftskredittordningen
							</div>
							<div className="card-body">
								<h4>Hva gjør skjematjenesten</h4>
								<p>
									Dette er en internettbasert tjeneste som
									tilbys av Landbrukets Dataflyt, i
									forbindelse med driftskredittordningen for
									landbruket.
									<br />
									Tjenesten gir banker oversikt over
									registrerte produsenter og varemottakere, og
									gjør det enklere å sende meldinger til
									produsentens varemottakere. Meldingene som
									sendes fra banken mottas av aktuelle
									varemottakere, der de behandles før svar
									sendes tilbake til bank.
								</p>
								<p>
									Tjenesten omfatter også innsending av
									årsoppgave til Landbrukets Dataflyt.
								</p>
								<h4>Hvordan ta i bruk tjenesten</h4>
								<p>
									Tjenesten er tilgjengelig for alle banker og
									varemottakere. For å få tilgang må det
									tegnes en avtale med Landbrukets Dataflyt,
									kontakt oss på telefon 22 05 47 30.
								</p>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-6 offset-md-3">
						<form onSubmit={this.loginSubmit}>
							<div className="form-floating mt-6">
								<input
									type="text"
									className="form-control"
									id="floatingInput"
									placeholder="Brukernavn"
									name="username"
									value={this.state.username}
									onChange={this.handleChange}
									required
									autoFocus
									autoComplete="off"
								></input>
								<label htmlFor="floatingInput">
									Brukernavn
								</label>
							</div>
							<div className="form-floating mt-3 mb-3">
								<input
									type="password"
									className="form-control"
									id="floatingPassword"
									placeholder="Passord"
									name="password"
									value={this.state.password}
									onChange={this.handleChange}
									required
									autoComplete="off"
								></input>
								<label htmlFor="floatingPassword">
									Passord
								</label>
							</div>
							<div className="row">
								<div className="col">
									<button
										type="submit"
										id="login-button"
										className="btn-primary"
									>
										<img src={Logo} alt="Driftskreditt" />
										Logg inn
									</button>
								</div>
								<div className="col-3 text-small">
									<a
										className="float-end"
										id="forgotten-password-button"
										onClick={
											this
												.toggleShowForgottenPasswordDialog
										}
									>
										Glemt passord
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
				{this.state.error
					? "has-error" && (
							<div className="row">
								<div
									className="col-md"
									style={{ marginTop: "10px" }}
								>
									<div
										className="alert alert-danger d-flex align-items-center"
										role="alert"
									>
										<svg
											xmlns="http://www.w3.org/2000/svg"
											width="24"
											height="24"
											fill="currentColor"
											className="bi bi-exclamation-triangle-fill flex-shrink-0 me-2"
											viewBox="0 0 16 16"
											role="img"
											aria-label="Warning:"
										>
											<path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
										</svg>
										<div>
											Ugyldig brukernavn eller passord
										</div>
									</div>
								</div>
							</div>
					  )
					: null}
				<ForgotPassword
					showForgotPassword={this.state.showForgotPassword}
					toggleShowForgottenPasswordDialog={
						this.toggleShowForgottenPasswordDialog
					}
					history={this.props.history}
				/>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	const user = getStoreDataForUser(state);
	return { user };
};

const mapDispatchToProps = {
	login,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

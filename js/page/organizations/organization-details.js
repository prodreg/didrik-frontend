import React from "react";
import { connect } from "react-redux";
import Spinner from "../../components/spinner";
import userService from "../../service/user-service";
import {
  getStoreDataForUserInfo,
  getStoreDataForOrganizationDetails,
  getStoreDataForUsersInOrganization,
  getStoreDataForOrganizationOrdning,
  getStoreDataForAllOrganizationOrdning,
  getStoreDataFormdataSchemeTypes,
} from "../../redux/selectors";
import {
  getOrganizationDetails,
  getUsersInOrganization,
  updateOrganizationName,
  updateOrganizationEmail,
  updateOrganizationUserEmail,
} from "../../redux/thunks/organization-thunks";
import {
  getOrganizationOrdning,
  updateOrdning,
} from "../../redux/thunks/organization-ordning-thunks";
import { getFormdataSchemeTypes } from "../../redux/thunks/formdata-thunks";
import {
  updateUserStatus,
} from "../../redux/thunks/user-thunks";
import { getUserInfo } from "../../redux/thunks/user-info-thunks";
import { Button, Table, Card, Row, Col, Form } from "react-bootstrap";
import Checkbox from "../../components/ui/checkbox/checkbox";
import AddUser from "./add-user";
import NewEmail from "../../components/new-email";
import NewName from "../../components/new-name";
import { MdOutlinePersonAddAlt1 } from "react-icons/md";
import { MdWorkOutline } from "react-icons/md";
import "./organization-details.scss";
import Container from "../../components/ui/container/container";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";
import PrimaryButton from "../../components/ui/button/primary-button";
import TertiaryButton from "../../components/ui/button/tertiary-button";
import SecondaryButton from "../../components/ui/button/secondary-button";

class OrganizationDetails extends React.Component {
  state = {
    showChangeOrgName: false,
    showChangeOrgEmail: false,
    showChangeUserEmail: false,
    userSelection: {},
    selectAllUsers: false,
    showAddUser: false,
    updatingUser: false,
    selectedUser: "",
    showHierarchy: false,
    ordning: "",
  };

  constructor(props) {
    super(props);
    this.orgNumber = props.match.params.orgNumber;
    this.handleSelectAllUsersChange =
      this.handleSelectAllUsersChange.bind(this);
  }

  componentDidMount() {
    const username = userService.getLoggedInUser().username;
    this.props.getUserInfo(username);
    this.props.getOrganizationDetails(this.orgNumber).then((res) => {
      if (res.organizationType === "MOTTAKER") {
        this.props.getOrganizationOrdning(this.orgNumber).then((res) => {
          this.setState({
            ordning: res,
          });
        });
      }
    });
    this.props.getFormdataSchemeTypes();
    this.props.getUsersInOrganization(this.orgNumber).then((res) => {
      const newUserSelection = {};
      for (const user of res) {
        newUserSelection[user.username] = false;
      }
      this.setState({ userSelection: newUserSelection });
    });
  }

  toggleShowChangeOrgName = () => {
    this.setState({
      showChangeOrgName: !this.state.showChangeOrgName,
    });
  };

  toggleShowChangeOrgEmail = () => {
    this.setState({
      showChangeOrgEmail: !this.state.showChangeOrgEmail,
    });
  };

  toggleShowChangeUserEmail = (username) => {
    this.setState({
      showChangeUserEmail: !this.state.showChangeUserEmail,
      selectedUser: username,
    });
  };

  canUpdateOrganizationName = () => {
    if (this.props.userInfo.loading || this.props.organizationDetails.loading) {
      return false;
    }
    return userService.getLoggedInUser().roles.includes("admin");
  };

  canUpdateOrganizationEmail = () => {
    if (this.props.userInfo.loading || this.props.organizationDetails.loading) {
      return false;
    }
    const isAdmin = userService.getLoggedInUser().roles.includes("admin");
    const isUserOrg =
      this.props.userInfo.data.organization.orgNumber ===
      this.props.organizationDetails.data.details.orgNumber;
    return isAdmin || isUserOrg;
  };

  canUpdateOrdning = () => {
    if (this.props.userInfo.loading || this.props.organizationDetails.loading) {
      return false;
    }
    const isAdmin = userService.getLoggedInUser().roles.includes("admin");
    const isUserOrg =
      this.props.userInfo.data.organization.orgNumber ===
      this.props.organizationDetails.data.details.orgNumber;
    const isMottaker = this.props.userInfo.data.roles.includes("mottaker");
    return isAdmin || (isUserOrg && isMottaker);
  };

  canAddUser = () => {
    const isAdmin = userService.getLoggedInUser().roles.includes("admin");
    return isAdmin;
  };

  handleUserSelectChange = (username, event) => {
    var userSelection = this.state.userSelection;
    userSelection[username] = event.target.checked;

    var allChecked = true;
    for (const user of Object.keys(this.state.userSelection)) {
      if (this.state.userSelection[user] === false) {
        allChecked = false;
        break;
      }
    }

    this.setState({
      userSelection: userSelection,
      selectAllUsers: allChecked,
    });
  };

  handleSelectAllUsersChange = (event) => {
    const newUserSelection = {};
    for (const user of Object.keys(this.state.userSelection)) {
      newUserSelection[user] = event.target.checked;
    }
    this.setState({
      userSelection: newUserSelection,
      selectAllUsers: event.target.checked,
    });
  };

  hasSelectedUsers = () => {
    for (const user of Object.keys(this.state.userSelection)) {
      if (this.state.userSelection[user] === true) {
        return true;
      }
    }
  };

  numberOfSelectedUsers = () => {
    var selected = 0;
    for (const user of Object.keys(this.state.userSelection)) {
      if (this.state.userSelection[user] === true) {
        selected++;
      }
    }
    return selected;
  };

  deactivateSelectedUsers = () => {
    this.setState({
      updatingUser: true,
    });

    var promises = [];
    for (const user of Object.keys(this.state.userSelection)) {
      if (this.state.userSelection[user] === true) {
        promises.push(this.props.updateUserStatus(user, "INACTIVE"));
      }
    }

    var hasFailed = false;
    Promise.allSettled(promises)
      .then((results) =>
        results.map((res) => {
          if (res.value.payload.error) hasFailed = true;
        })
      )
      .then(() => {
        this.handleStatusUpdateError(hasFailed, promises.length);
      })
      .finally(() => {
        this.setState({
          updatingUser: false,
        });
      });
  };

  activateSelectedUsers = () => {
    this.setState({
      updatingUser: true,
    });

    var promises = [];
    for (const user of Object.keys(this.state.userSelection)) {
      if (this.state.userSelection[user] === true) {
        promises.push(this.props.updateUserStatus(user, "ACTIVE"));
      }
    }

    var hasFailed = false;
    Promise.allSettled(promises)
      .then((results) =>
        results.map((res) => {
          if (res.value.payload.error) hasFailed = true;
        })
      )
      .then(() => {
        this.handleStatusUpdateError(hasFailed, promises.length);
      })
      .finally(() => {
        this.setState({
          updatingUser: false,
        });
      });
  };

  handleStatusUpdateError = (error, numberOfUsers) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "En eller flere brukere fikk ikke endret status. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire(
        "OK",
        "Status er endret for " + numberOfUsers + " brukere",
        "success"
      ).then(() => {
        this.props.history.go(0);
      });
    }
  };

  toggleShowAddUser = () => {
    this.setState({
      showAddUser: !this.state.showAddUser,
      validEmail: true,
    });
  };

  handleChangeOrdning = (event) => {
    const newOrdning = event.target.value;
    this.setState({
      ordning: newOrdning,
    });
    this.props.updateOrdning(this.orgNumber, newOrdning);
  };

  render() {
    const userInfo = this.props.userInfo;
    const organizationDetails = this.props.organizationDetails;
    const organizationUsers = this.props.organizationUsers;
    const organizationOrdning = this.props.organizationOrdning;
    const allOrganizationOrdning = this.props.allOrganizationOrdning;

    return (
      <Container
        title={"Organisasjon"}
        description={"Organisasjonsinformasjon"}
        icon={<MdWorkOutline size="60px" />}
      >
        <Row>
          <Col fluid="lg">
            {userInfo.loading || (organizationDetails.loading && <Spinner />)}
            {organizationDetails.error && (
              <div>
                Fant ikke noen informasjon om organisasjonen {this.orgNumber}
              </div>
            )}
            {organizationDetails.loaded && !organizationDetails.error && (
              <div className="organization-details">
                <Row>
                  <dl className="dl-horizontal">
                    <dt>Navn</dt>
                    <dd>{organizationDetails.data.details.name}</dd>
                    {userInfo.loaded && organizationDetails.loaded &&
                      this.canUpdateOrganizationName() && (
                        <dd>
                          <a
                            style={{ cursor: "pointer" }}
                            onClick={this.toggleShowChangeOrgName}
                            className="text-small"
                          >
                            Endre navn
                          </a>
                        </dd>
                      )}

                    <dt>Organisasjonsnummer</dt>
                    <dd>{organizationDetails.data.details.orgNumber}</dd>

                    <dt>Organisasjonstype</dt>
                    <dd>{organizationDetails.data.details.organizationType}</dd>

                    <dt>E-post</dt>
                    <dd>{organizationDetails.data.details.email}</dd>
                    {userInfo.loaded && organizationDetails.loaded &&
                      this.canUpdateOrganizationEmail() && (
                        <dd>
                          <a
                            style={{ cursor: "pointer" }}
                            onClick={this.toggleShowChangeOrgEmail}
                            className="text-small"
                          >
                            Endre e-post adresse
                          </a>
                        </dd>
                      )}
                    {organizationDetails.data.details.organizationType ===
                      "MOTTAKER" &&
                      organizationOrdning.loaded &&
                      !organizationOrdning.error &&
                      allOrganizationOrdning.loaded &&
                      !allOrganizationOrdning.error && (
                        <span>
                          <dt>Ordning</dt>
                          <dd>
                            <Form.Select
                              value={this.state.ordning}
                              onChange={this.handleChangeOrdning}
                              disabled={!this.canUpdateOrdning()}
                            >
                              {Object.keys(allOrganizationOrdning.data).map(
                                (key, i) => {
                                  return (
                                    <option
                                      key={i}
                                      value={
                                        allOrganizationOrdning.data[key].value
                                      }
                                    >
                                      {
                                        allOrganizationOrdning.data[key]
                                          .description
                                      }
                                    </option>
                                  );
                                }
                              )}
                            </Form.Select>
                          </dd>
                        </span>
                      )}
                  </dl>
                </Row>
                <Card>
                  <Card.Header>Organisasjonens brukere</Card.Header>
                  <Card.Body>
                    {organizationUsers.loading && <Spinner />}
                    {organizationUsers.loaded && (
                      <>
                        {this.canAddUser() && (
                          <>
                            {!this.hasSelectedUsers() && (
                              <PrimaryButton
                                title="Legg til bruker"
                                onClick={this.toggleShowAddUser}
                                icon={<MdOutlinePersonAddAlt1 />}
                              />
                            )}
                            {this.hasSelectedUsers() && (
                              <>
                                {this.numberOfSelectedUsers() === 1 && (
                                  <div className="float-start">
                                    {this.numberOfSelectedUsers()} bruker er
                                    valgt
                                  </div>
                                )}
                                {this.numberOfSelectedUsers() > 1 && (
                                  <div className="float-start">
                                    {this.numberOfSelectedUsers()} brukere er
                                    valgt
                                  </div>
                                )}
                                <div className="float-end">
                                  <TertiaryButton
                                    title="Inaktiver"
                                    onClick={this.deactivateSelectedUsers}
                                    disabled={this.state.updatingUser}
                                  />
                                  {" "}
                                  <SecondaryButton
                                    title="Aktiver"
                                    onClick={this.activateSelectedUsers}
                                    disabled={this.state.updatingUser}
                                  />
                                </div>
                              </>
                            )}
                          </>
                        )}
                        <Table hover striped>
                          <thead>
                            <tr>
                              {this.canAddUser() &&
                                organizationUsers.data.length !== 0 && (
                                  <th>
                                    <Checkbox
                                      checked={this.state.selectAllUsers}
                                      onChange={this.handleSelectAllUsersChange}
                                    />
                                  </th>
                                )}
                              <th>Navn</th>
                              <th>Brukernavn</th>
                              <th>Epost</th>
                              <th>Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            {organizationUsers.data.map((user) => {
                              return (
                                <tr key={user.id} className={user.status === "ACTIVE" ? "user-active" : "user-inactive"}>
                                  {this.canAddUser() && (
                                    <td>
                                      <Checkbox
                                        checked={
                                          this.state.userSelection[
                                            user.username
                                          ]
                                        }
                                        onChange={this.handleUserSelectChange.bind(
                                          this,
                                          user.username
                                        )}
                                      />
                                    </td>
                                  )}
                                  <td>{user.fullName}</td>
                                  <td>
                                    <Link to={"/user/" + user.username}>
                                      {user.username}
                                    </Link>
                                  </td>
                                  <td>
                                    <a href={"mailto:" + user.email}>
                                      {user.email}
                                    </a>{" "}
                                    {this.canAddUser() && (
                                      <a
                                        style={{ cursor: "pointer" }}
                                        onClick={this.toggleShowChangeUserEmail.bind(
                                          this,
                                          user.username
                                        )}
                                        className="text-x-small"
                                      >
                                        {" "}
                                        Endre e-post adresse
                                      </a>
                                    )}
                                  </td>
                                  <td>{user.status}</td>
                                </tr>
                              );
                            })}
                          </tbody>
                        </Table>
                      </>
                    )}
                  </Card.Body>
                </Card>
              </div>
            )}
          </Col>
        </Row>
        <NewName
          updateTarget={this.props.updateOrganizationName}
          targetId={this.orgNumber}
          showChangeName={this.state.showChangeOrgName}
          toggleShowChangeName={this.toggleShowChangeOrgName}
          history={this.props.history}
        />
        <NewEmail
          updateTarget={this.props.updateOrganizationEmail}
          targetId={this.orgNumber}
          showChangeEmail={this.state.showChangeOrgEmail}
          toggleShowChangeEmail={this.toggleShowChangeOrgEmail}
          history={this.props.history}
        />
        <NewEmail
          updateTarget={this.props.updateOrganizationUserEmail}
          targetId={this.state.selectedUser}
          orgNumber={this.orgNumber}
          showChangeEmail={this.state.showChangeUserEmail}
          toggleShowChangeEmail={this.toggleShowChangeUserEmail}
          history={this.props.history}
        />
        <AddUser
          orgNumber={this.orgNumber}
          showAddUser={this.state.showAddUser}
          toggleShowAddUser={this.toggleShowAddUser}
          history={this.props.history}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const userInfo = getStoreDataForUserInfo(state);
  const organizationDetails = getStoreDataForOrganizationDetails(state);
  const organizationUsers = getStoreDataForUsersInOrganization(state);
  const organizationOrdning = getStoreDataForOrganizationOrdning(state);
  const allOrganizationOrdning = getStoreDataFormdataSchemeTypes(state);
  return {
    userInfo,
    organizationDetails,
    organizationUsers,
    organizationOrdning,
    allOrganizationOrdning,
  };
};

const mapDispatchToProps = {
  getUserInfo,
  getOrganizationDetails,
  getUsersInOrganization,
  updateUserStatus,
  updateOrganizationName,
  updateOrganizationUserEmail,
  updateOrganizationEmail,
  getOrganizationOrdning,
  getFormdataSchemeTypes,
  updateOrdning,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrganizationDetails);

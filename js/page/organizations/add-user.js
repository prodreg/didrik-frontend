import React from "react";
import { Modal, Button, Row, Col, Form, FloatingLabel } from "react-bootstrap";
import { addUserToOrganization } from "../../redux/thunks/organization-thunks";
import { connect } from "react-redux";
import Swal from "sweetalert2";
import TertiaryButton from "../../components/ui/button/tertiary-button";
import SecondaryButton from "../../components/ui/button/secondary-button";

class AddUser extends React.Component {
  state = {
    user: {
      username: "",
      firstname: "",
      lastname: "",
      email: "",
      mobile: "",
      phoneNumber: ""
    },
    validated: false,
    validEmail: {
      isValid: false,
      nonEmpty: false,
      validPattern: false
    },
    validUsername: {
      isValid: false,
      nonEmpty: false,
      validMinLength: false,
      validMaxLength: true,
      validPattern: false
    },
    validFirstname: false,
    validLastname: false,
    validNewUser: false,
    saving: false
  };

  constructor(props) {
    super(props);
  }

  closeDialog = () => {
    this.setState({
      user: {
        username: "",
        firstname: "",
        lastname: "",
        email: "",
        mobile: "",
        phoneNumber: ""
      },
      validated: false,
      validEmail: {
        isValid: false,
        nonEmpty: false,
        validPattern: false
      },
      validUsername: {
        isValid: false,
        nonEmpty: false,
        validMinLength: false,
        validMaxLength: true,
        validPattern: false
      },
      validFirstname: false,
      validLastname: false,
      validNewUser: false,
      saving: false
    });

    this.props.toggleShowAddUser();
  }

  validateForm = () => {
    const validEmail = this.state.validEmail.isValid && this.state.validEmail.nonEmpty && this.state.validEmail.validPattern;
    const validUsername = this.state.validUsername.isValid && this.state.validUsername.nonEmpty 
      && this.state.validUsername.validMinLength && this.state.validUsername.validMaxLength && this.state.validUsername.validPattern;
    const validFirstname = this.state.validFirstname;
    const validLastname = this.state.validLastname;
    const isValid = validEmail && validUsername && validFirstname && validLastname;

    this.setState({
      validNewUser: isValid
    });
  }

  validateUsername = (event) => {
    const username = event.target.value;
    var isValid = true;
    var nonEmpty = true;
    var validMinLength = true;
    var validMaxLength = true;
    var validPattern = true;

    if (!username) {
      isValid = false;
      nonEmpty = false;
    }

    if (username.length < 5) {
      isValid = false;
      validMinLength = false;
    }

    if (username.length > 50) {
      isValid = false;
      validMaxLength = false;
    }

    let re = /^[a-zA-Z0-9\-\_\@\.]+$/;
    if (!re.test(username)){
      isValid = false;
      validPattern = false;
    }

    const validUsername = {
      isValid: isValid,
      nonEmpty: nonEmpty,
      validMinLength: validMinLength,
      validMaxLength: validMaxLength,
      validPattern: validPattern
    };

    var user = this.state.user;
    user.username = username;
    this.setState({
      validUsername: validUsername,
      user: user
    }, () => {
      this.validateForm()
    });

  }
  
  validateFirstname = (event) => {
    const firstname = event.target.value;
    var isValid = true;
    if (!firstname) {
      isValid = false;
    }

    var user = this.state.user;
    user.firstname = firstname;
    this.setState({
      user: user,
      validFirstname: isValid
    }, () => {
      this.validateForm()
    });
  }

  validateLastname = (event) => {
    const lastname = event.target.value;
    var isValid = true;
    if (!lastname) {
      isValid = false;
    }

    var user = this.state.user;
    user.lastname = lastname;
    this.setState({
      user: user,
      validLastname: isValid
    }, () => {
      this.validateForm()
    });
  }

  validateEmail = (event) => {
    const email = event.target.value
    var isValid = true;
    var nonEmpty = true;
    var validPattern = true;

    if (!email) {
      isValid = false;
      nonEmpty = false;
    }

    let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(email)) {
      isValid = false;
      validPattern = false;
    }

    var user = this.state.user;
    user.email = email;
    this.setState({
      user: user,
      validEmail: {
        isValid: isValid,
        nonEmpty: nonEmpty,
        validPattern: validPattern
      }
    }, () => {
      this.validateForm()
    });
  }

  handleMobile = (event) => {
    const mobile = event.target.value;
    var user = this.state.user;
    user.mobile = mobile;
    this.setState({
      user: user
    });
  }

  handlePhoneNumber = (event) => {
    const phoneNumber = event.target.value;
    var user = this.state.user;
    user.phoneNumber = phoneNumber;
    this.setState({
      user: user
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    const validForm = this.state.validNewUser;
    if (form.checkValidity() === false || !validForm) {
      event.stopPropagation();
    } else {
      this.setState({
        saving: true
      });

      const userToAdd = {
        username: this.state.user.username,
        givenName: this.state.user.firstname,
        surname: this.state.user.lastname,
        email: this.state.user.email,
        mobile: this.state.user.mobile,
        telephone: this.state.user.phoneNumber
      };

      this.props.addUserToOrganization(this.props.orgNumber, userToAdd).then((res) => {
        this.handleError(res.payload.error);
      })
      .finally(() => {
        this.setState({
          saving: false
        });
      });
    }

    this.setState({
      validated: true
    })

  }

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Brukeren kunne ikke bli opprettet. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("OK", "Bruker er opprettet", "success").then(() => {
        this.props.history.go(0);
      });
    }
  }

  render() {
    return (
      <Modal show={this.props.showAddUser} onHide={this.closeDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Legg til ny bruker</Modal.Title>
        </Modal.Header>
        <Form noValidate onSubmit={ this.handleSubmit }>
            <Modal.Body>
              <Row className="mb-3">
                <Form.Group controlId="newUserBrukernavn">
                  <FloatingLabel label="Brukernavn">
                    <Form.Control required type="text" placeholder="Brukernavn" 
                      isInvalid={!this.state.validUsername.isValid && this.state.validated} onChange={ this.validateUsername } />
                  </FloatingLabel>
                  {this.state.validated && (
                    <>
                    <Row>
                    {!this.state.validUsername.nonEmpty && (
                      <Form.Text className="text-danger text-small">
                        Brukernavn er påkrevd
                      </Form.Text>
                    )}
                    </Row>
                    <Row>
                    {!this.state.validUsername.validMinLength && (
                      <Form.Text className="text-danger text-small">
                        Brukernavn må bestå av minst 5 tegn
                      </Form.Text>
                    )}
                    </Row>
                    <Row>
                    {!this.state.validUsername.validMaxLength && (
                      <Form.Text className="text-danger text-small">
                        Brukernavn kan ikke bestå av mer enn 50 tegn
                      </Form.Text>
                    )}
                    </Row>
                    <Row>
                    {!this.state.validUsername.validPattern && (
                      <Form.Text className="text-danger text-small">
                        Brukernavn kan kun bestå av bokstaver (ikke æøå), tall, bindestrek, underscore, @, og punktum
                      </Form.Text>
                    )}
                    </Row>
                    </>
                  )}
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group as={Col}>
                  <FloatingLabel label="Fornavn">
                    <Form.Control required type="text" placeholder="Fornavn" onChange={ this.validateFirstname }
                      isInvalid={!this.state.validFirstname && this.state.validated} />
                  </FloatingLabel>
                  {!this.state.validFirstname && this.state.validated && (
                    <Form.Text className="text-danger text-small">
                      Fornavn er påkrevd
                    </Form.Text>
                  )}
                </Form.Group>

                <Form.Group as={Col}>
                  <FloatingLabel label="Etternavn">
                    <Form.Control required type="text" placeholder="Etternavn" onChange={ this.validateLastname }
                      isInvalid={!this.state.validLastname && this.state.validated} />
                  </FloatingLabel>
                  {!this.state.validLastname && this.state.validated && (
                    <Form.Text className="text-danger text-small">
                      Etternavn er påkrevd
                    </Form.Text>
                  )}
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group>
                  <FloatingLabel label="Epost">
                    <Form.Control required type="email" onChange={ this.validateEmail } placeholder="Epost" 
                      isInvalid={!this.state.validEmail.isValid && this.state.validated} />
                      {this.state.validated && (
                        <>
                        <Row>
                        {!this.state.validEmail.nonEmpty && (
                          <Form.Text className="text-danger text-small">
                              Epost er påkrevd
                          </Form.Text>
                        )}
                        </Row>
                        <Row>
                        {!this.state.validEmail.validPattern && (
                          <Form.Text className="text-danger text-small">
                              Du må skrive inn en gyldig e-post adresse
                          </Form.Text>
                        )}
                        </Row>
                        </>
                      )}
                  </FloatingLabel>
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group as={Col}>
                  <FloatingLabel label="Mobil">
                    <Form.Control type="text" placeholder="Mobil" onChange={ this.handleMobile } />
                  </FloatingLabel>
                </Form.Group>

                <Form.Group as={Col}>
                  <FloatingLabel label="Telefonnummer">
                    <Form.Control type="text" placeholder="Telefonnummer" onChange={ this.handlePhoneNumber } />
                  </FloatingLabel>
                </Form.Group>
              </Row>
            </Modal.Body>
            <Modal.Footer>
            <TertiaryButton
              type="reset"
              title="Avbryt"
              onClick={this.closeDialog} disabled={ this.state.saving }
            />
            <SecondaryButton
              type="submit"
              title="Lagre"
              disabled={ (!this.state.validNewUser && this.state.validated) || this.state.saving }
            />
            </Modal.Footer>
        </Form>
      </Modal>
    );
  }
}

AddUser.defaultProps = {
  showAddUser: false
};

const mapStateToProps = (state) => {
  return { };
}

const mapDispatchToProps = {
  addUserToOrganization
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUser);
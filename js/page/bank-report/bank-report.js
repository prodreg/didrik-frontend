import React, { useEffect, useState } from "react";
import { Button, Card, Table } from "react-bootstrap";
import { MdOutlineManageSearch } from "react-icons/md";
import { useSelector } from "react-redux";

import { addAuth, proxy } from "../../api/api";
import Container from "../../components/ui/container/container";
import { DetailedList } from "./detailed-list";

export const BankReportPage = () => {
	const [data, setData] = useState();
	const [error, setError] = useState();
	const orgNumber = useSelector((state) => state?.user?.data?.orgNumber);
	const orgName = useSelector(
		(state) => state?.userInfo?.data?.organization?.name
	);

	useEffect(() => {
		//fetch report for given id
		if (!orgNumber) return;
		fetch(
			`${proxy}/reports/bank/${orgNumber}`,
			addAuth({
				method: "GET",
				//headers: {
				//	"Content-Type": "application/json;charset=UTF-8",
				//	Accept: "application/json, text/plain, */*",
				//},
			})
		)
			.then((res) => {
				if (!res.ok) {
					if (res.status == "404") {
						res.json().then((res) => {
							setError(res.message);
						});
					}
					throw Error(`${res.status} - ${res.statusText}`);
				}
				return res.json();
			})
			.then((res) => {
				setData(res);
			})
			.catch((error) => {});
	}, [orgNumber]);

	const {
		konsern,
		antallProdusenterMedKreditt,
		totalKredittTilProdusenter,
		totalSaldo,
		totaltGarantiAnsvar,
		year,
	} = data ?? {};

	if (error)
		return (
			<Card>
				<Card.Header>Kunne ikke finne noen rapporter</Card.Header>
				<Card.Body>Det er antagelig en god grunn!</Card.Body>
			</Card>
		);

	//I don't want to fetch the organization name unneccessarily, but if we already have, we should use it!
	const orgNameText = orgName ? `${orgName} (${konsern})` : konsern;

	return (
		<Container
			title="Bankrapporter"
			description={`for ${orgNameText}`}
			icon={<MdOutlineManageSearch size="60px" />}
		>
			<Card className="mb-3">
				<Card.Header>Sammendrag for år {year}</Card.Header>
				<Card.Body>
					<Table hover striped>
						<thead>
							<tr>
								<th>Konsern</th>
								<th>Produsenter med kreditt</th>
								<th>Total kreditt til Produsenter</th>
								<th>Total saldo</th>
								<th>Totalt garantiansvar</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{konsern}</td>
								<td>{antallProdusenterMedKreditt}</td>
								<td>{totalKredittTilProdusenter}</td>
								<td>{totalSaldo}</td>
								<td>{totaltGarantiAnsvar}</td>
							</tr>
						</tbody>
					</Table>
				</Card.Body>
			</Card>
			<DetailedList />
		</Container>
	);
};

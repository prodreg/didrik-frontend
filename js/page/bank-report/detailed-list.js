import React, { useState } from "react";
import { Card, Table } from "react-bootstrap";
import { useSelector } from "react-redux";
import Swal from "sweetalert2";
import FileSaver from "file-saver";
import Papa from "papaparse";

import { addAuth, proxy } from "../../api/api";
import { DidrikPagination } from "../../components/DidrikPagination";

import { PrimaryButton } from "../../components/ui/button/primary-button";
import { SecondaryButton } from "../../components/ui/button/secondary-button";

export const DetailedList = () => {
	const [data, setData] = useState();
	const [error, setError] = useState();
	const [page, setPage] = useState(0);
	const [downloading, setDownloading] = useState(false);
	const orgNumber = useSelector((state) => state?.user?.data?.orgNumber);
	const rowsPerPage = 10;

	const clickHandler = () => {
		//fetching detailed list for given org number
		if (!orgNumber) return;
		fetch(
			`${proxy}/reports/bank/${orgNumber}/list`,
			addAuth({
				method: "GET",
			})
		)
			.then((res) => {
				if (!res.ok) {
					if (res.status == "404") {
						res.json().then((res) => {
							setError(res.message);
						});
					}
					throw Error(`${res.status} - ${res.statusText}`);
				}
				return res.json();
			})
			.then((res) => {
				setData([...res, ...res, ...res]);
			})
			.catch((error) => {});
	};

	const handleDownload = () => {
		setDownloading(true);
		fetch(
			`${proxy}/reports/bank/${orgNumber}/export`,
			addAuth({
				method: "GET",
			})
		)
			.then((res) => res.blob())
			.then((res) => {
				const csv = Papa.parse(res);
				/*const blob = new Blob([res], {
					type: "application/vnd.ms-word",
				});*/
				FileSaver.saveAs(res, "Bankrapport.csv");
				if (res?.payload?.error) {
					return Swal.fire(
						"En feil har oppstått",
						"Rapport kunne ikke nedlastes. Vennligst prøv igjen senere.",
						"error"
					);
				} else {
					/*const blob = new Blob([res.payload.data], {
						type: "application/vnd.ms-word",
					});
					FileSaver.saveAs(blob, "Bankrapport.doc");*/
				}
				setDownloading(false);
			});
	};

	if (error)
		return (
			<Card>
				<Card.Header>Kunne ikke finne noen rapporter</Card.Header>
				<Card.Body>Det er antagelig en god grunn!</Card.Body>
			</Card>
		);

	if (!data)
		return (
			<div>
				<PrimaryButton onClick={clickHandler}>
					Hent detaljert liste
				</PrimaryButton>
			</div>
		);

	const currentPageData = data.slice(page * 10, page * 10 + 10);

	return (
		<Card>
			<Card.Header className="d-flex justify-content-between">
				<span>Detaljert liste</span>
				<SecondaryButton
					onClick={handleDownload}
					title="Eksporter til regneark"
				/>
			</Card.Header>
			<Card.Body>
				<Table striped>
					<thead>
						<tr>
							<th>Produsent</th>
							<th>Kontonummer</th>
							<th>Bevilget</th>
							<th>Garantiansvar</th>
							<th>Saldo</th>
							<th>Omsetning</th>
						</tr>
					</thead>
					<tbody>
						{currentPageData.map(
							({ organisasjonsAnsvar }, index) => {
								const {
									prodNr,
									bevilget,
									garantiAnsvar,
									kontoNr,
									saldo,
									totalOmsetning,
								} = organisasjonsAnsvar ?? {};
								return (
									<tr key={index}>
										<td>{prodNr}</td>
										<td>{kontoNr}</td>
										<td>{bevilget}</td>
										<td>{garantiAnsvar}</td>
										<td>{saldo}</td>
										<td>{totalOmsetning}</td>
									</tr>
								);
							}
						)}
					</tbody>
				</Table>
				<DidrikPagination
					pageCount={Math.ceil(data.length / rowsPerPage)}
					onPageChange={({ selected }) => setPage(selected)}
					marginPagesDisplayed={3}
					pageRangeDisplayed={3}
				/>
			</Card.Body>
		</Card>
	);
};

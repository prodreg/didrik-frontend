export const getStoreDataFormdataSchemeTypes = (store) =>
  store ? store.formdataSchemeTypes : {};

export const getStoreDataFormdataProductTypes = (store) =>
  store ? store.formdataProductTypes : {};

export const getStoreDataFormdataNoticeTypes = (store) =>
  store ? store.formdataNoticeTypes : {};

export const getStoreDataFormdataOrganizationTypes = store =>
    store ? store.formdataOrganizationTypes : {};

export const getStoreDataForUser = store =>
    store ? store.user : {};

export const getStoreDataForCases = (store) => (store ? store.cases : {});

export const getStoreDataForAllCases = (store) => (store ? store.allCases : {});

export const getStoreDataForEventMessages = (store) =>
  store ? store.eventMessageReducer : {};

export const getStoreDataForUserInfo = (store) => (store ? store.userInfo : {});

export const getStoreDataForVersionInfo = (store) =>
  store ? store.versionInfo : {};

export const getStoreDataForNewsPosts = (store) =>
  store ? store.newsPostsReducer : {};

export const getStoreDataForOrganizationDetails = (store) =>
  store ? store.organizationDetails : {};

export const getStoreDataForUsersInOrganization = (store) =>
  store ? store.organizationUsers : {};

export const getStoreDataForAllOrganizationOrdning = (store) =>
  store ? store.allOrdning : {};

export const getStoreDataForOrganizationOrdning = store =>
    store ? store.organizationOrdning : {};

export const getStoreDataForAllOrganizations = store =>
    store ? store.allOrganizations : {};

export const getStoreDataForQueriedOrganizations = store =>
    store ? store.queryOrganizations : {};

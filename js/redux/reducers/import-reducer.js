import ImportActionTypes from "../actions/import-action-type";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const importReducer = (state = initialState, action) => {
  switch (action.type) {
    case ImportActionTypes.GET_ALL_IMPORTS_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ImportActionTypes.GET_ALL_IMPORTS_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ImportActionTypes.GET_ALL_IMPORTS_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const importForBankReducer = (state = initialState, action) => {
  switch (action.type) {
    case ImportActionTypes.GET_IMPORT_FOR_BANK_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ImportActionTypes.GET_IMPORT_FOR_BANK_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ImportActionTypes.GET_IMPORT_FOR_BANK_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const importForOrganizationReducer = (state = initialState, action) => {
  switch (action.type) {
    case ImportActionTypes.GET_IMPORT_FOR_ORGANIZATION_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ImportActionTypes.GET_IMPORT_FOR_ORGANIZATION_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ImportActionTypes.GET_IMPORT_FOR_ORGANIZATION_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

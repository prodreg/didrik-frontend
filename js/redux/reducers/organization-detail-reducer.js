import ActionsTypes from "../actions/organization-action-types";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const organizationDetails = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.GET_ORGANIZATION_DETAILS_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ORGANIZATION_DETAILS_SUCCESS: {
      const details = action.payload;
      return {
        ...state,
        data: details,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ORGANIZATION_DETAILS_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

export const updateOrganizationEmail = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.UPDATE_ORG_EMAIL_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.UPDATE_ORG_EMAIL_SUCCESS: {
      return {
        ...state,
        data: [],
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.UPDATE_ORG_EMAIL_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: error,
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

export const allOrganizationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.GET_ALL_ORGANIZATIONS_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_ORGANIZATIONS_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_ORGANIZATIONS_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const allChildrenForOrganizationReducer = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case ActionsTypes.GET_ALL_CHILDREN_FOR_ORGANIZATION_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ActionsTypes.GET_ALL_CHILDREN_FOR_ORGANIZATION_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_CHILDREN_FOR_ORGANIZATION_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

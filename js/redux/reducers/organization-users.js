import ActionsTypes from "../actions/organization-action-types";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false
};

export const organizationUsers = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.GET_USERS_IN_ORGANIZATION_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false
      };
    }
    case ActionsTypes.GET_USERS_IN_ORGANIZATION_SUCCESS: {
      const { users } = action.payload;
      return {
        ...state,
        data: users ,
        loading: false,
        loaded: true,
        error: false
      };
    }
    case ActionsTypes.GET_USERS_IN_ORGANIZATION_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true
      };
    }
    default:
      return state;
  }
}

export const addUser = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.ADD_USER_TO_ORGANIZATION_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false
      };
    }
    case ActionsTypes.ADD_USER_TO_ORGANIZATION_SUCCESS: {
      return {
        ...state,
        data: [],
        loading: false,
        loaded: true,
        error: false
      };
    }
    case ActionsTypes.ADD_USER_TO_ORGANIZATION_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: error,
        loading: false,
        loaded: false,
        error: true
      };
    }
    default:
      return state;
  }
}

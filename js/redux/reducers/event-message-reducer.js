import ActionsTypes from "../actions/event-message-action-types";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const eventMessageReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.GET_ALL_EVENT_MESSAGES_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_EVENT_MESSAGES_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_EVENT_MESSAGES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const eventMessageIdReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.GET_EVENT_MESSAGE_BY_ID_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ActionsTypes.GET_EVENT_MESSAGE_BY_ID_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: [data],
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_EVENT_MESSAGE_BY_ID_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const counterReducer = (state = 0, action) => {
  switch (action.type) {
    case ActionsTypes.SET_NUMBER_OF_UNREAD_MESSAGES: {
      const { counter } = action.payload;
      return (state = counter);
    }
    default:
      return state;
  }
};

import { combineReducers } from "redux";
import {
  producersReducer,
  producerReducer,
  creditHistoryReducer,
  productionHistoryReducer,
  farmProductionHistoryReducer,
  producerNumberConnectionReducer
} from "./producer-reducer";
import { adminReportedFilesReducer } from "./admin-reducer";
import { newsPostsReducer } from "./news-post-reducer";
import { allCases, allActiveCases, oneCase } from "./case-reducer";
import {
  formdataNoticeTypes,
  formdataProductTypes,
  formdataSchemeTypes,
  formdataOrganizationTypes,
} from "./formdata-reducer";
import {
  eventMessageReducer,
  eventMessageIdReducer,
  counterReducer,
} from "./event-message-reducer";
import userInfo from "./user-info-reducer";
import user from "./user-reducer";
import versionInfo from "./version-info-reducer";
import {
  organizationDetails,
  updateOrganizationEmail,
  allOrganizationsReducer,
  allChildrenForOrganizationReducer,
} from "./organization-detail-reducer";
import {
  importReducer,
  importForBankReducer,
  importForOrganizationReducer,
} from "./import-reducer";
import { organizationUsers, addUser } from "./organization-users";
import {
  allOrdning,
  organizationOrdning,
  updateOrdning,
} from "./organization-ordning";
import {
  allOrganizations,
  childrenWithRolesReducer,
} from "./organization-reducer";
import { responsibilityReducer } from "./report-reducer";
import { allMyNotices } from "./case-notice-reducer";
import {
  conversationByIdReducer,
  conversationForCaseAndOrganizationReducer,
  conversationMessagesReducer,
} from "./conversation-reducer";

export default combineReducers({
  oneCase,
  allCases,
  allActiveCases,
  eventMessageReducer,
  eventMessageIdReducer,
  counterReducer,
  producersReducer,
  producerReducer,
  creditHistoryReducer,
  productionHistoryReducer,
  farmProductionHistoryReducer,
  producerNumberConnectionReducer,
  adminReportedFilesReducer,
  responsibilityReducer,
  importReducer,
  importForBankReducer,
  importForOrganizationReducer,
  user,
  userInfo,
  versionInfo,
  newsPostsReducer,
  organizationDetails,
  organizationUsers,
  addUser,
  updateOrganizationEmail,
  allOrganizationsReducer,
  allChildrenForOrganizationReducer,
  allOrdning,
  organizationOrdning,
  updateOrdning,
  formdataNoticeTypes,
  formdataProductTypes,
  formdataSchemeTypes,
  formdataOrganizationTypes,
  allOrganizations,
  childrenWithRolesReducer,
  allMyNotices,
  conversationByIdReducer,
  conversationForCaseAndOrganizationReducer,
  conversationMessagesReducer,
});

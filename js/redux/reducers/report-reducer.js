import ReportActionTypes from "../actions/report-action-types";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const responsibilityReducer = (state = initialState, action) => {
  switch (action.type) {
    case ReportActionTypes.GET_RESPONSIBILITY_REPORT_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ReportActionTypes.GET_RESPONSIBILITY_REPORT_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ReportActionTypes.GET_RESPONSIBILITY_REPORT_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

import ActionsTypes from "../actions/user-action-types";
import userService from "../../service/user-service";

const isLoggedIn = userService.isLoggedIn();
const loggedInUser = userService.getLoggedInUser() || {};

const initialState = {
  data: {
    username: 'ukjent',
    orgNumber: null,
    orgType: null,
    roles: [],
    isAdmin: false,
    isBank: false,
    isVaremottaker: false,
    isGarantiutvalget: false,
    token: null,
  },
  isLoggedIn: false,
  loading: false,
  error: false
};

const loadedInitialState = {
  data: {
    username: loggedInUser.username || 'ukjent',
    orgNumber: loggedInUser.orgNumber || null,
    orgType: loggedInUser.orgType || null,
    roles: loggedInUser.roles || [],
    isAdmin: loggedInUser.isAdmin || false,
    isBank: loggedInUser.isBank || false,
    isVaremottaker: loggedInUser.isVaremottaker || false,
    isGarantiutvalget: loggedInUser.isGarantiutvalget || false,
    token: loggedInUser.token || null
  },
  isLoggedIn: isLoggedIn || false,
  loading: false,
  error: false
};

export default function(state = loadedInitialState, action) {
  switch (action.type) {
    case ActionsTypes.LOGIN_REQUEST: {
      return {
        ...state,
        data: {},
        isLoggedIn: false,
        loading: true,
        error: false
      };
    }
    case ActionsTypes.LOGIN_SUCCESS: {
      return {
        ...state,
        data: action.payload.user,
        isLoggedIn: true,
        loading: false,
        loaded: true,
        error: false
      };
    }
    case ActionsTypes.LOGIN_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        isLoggedIn: false,
        loading: false,
        error: true
      };
    }
    case ActionsTypes.LOGOUT_REQUEST: {
      return {
        ...initialState
      }
    }
    default:
      return state;
  }
}

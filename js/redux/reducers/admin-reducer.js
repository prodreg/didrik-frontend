import AdminActionTypes from "../actions/admin-action-type";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const adminReportedFilesReducer = (state = initialState, action) => {
  switch (action.type) {
    case AdminActionTypes.GET_REPORTED_FILES_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case AdminActionTypes.GET_REPORTED_FILES_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case AdminActionTypes.GET_REPORTED_FILES_FAILURE: {
      return { ...state, data: [], loading: false, loaded: false, error: true };
    }
    default:
      return state;
  }
};

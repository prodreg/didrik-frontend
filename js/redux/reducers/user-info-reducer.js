import ActionsTypes from "../actions/user-info-action-types";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ActionsTypes.GET_USER_INFO_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false
      };
    }
    case ActionsTypes.GET_USER_INFO_SUCCESS: {
      const info = action.payload.userInfo;
      return {
        ...state,
        data: info,
        loading: false,
        loaded: true,
        error: false
      };
    }
    case ActionsTypes.GET_USER_INFO_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true
      };
    }
    default:
      return state;
  }
}

import ActionsTypes from "../actions/organization-ordning-action-type";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const allOrdning = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.GET_ALL_ORDNING_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_ORDNING_SUCCESS: {
      const ordninger = action.payload;
      return {
        ...state,
        data: ordninger,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_ORDNING_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: error,
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

export const organizationOrdning = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.GET_ORDNING_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ORDNING_SUCCESS: {
      const ordning = action.payload;
      return {
        ...state,
        data: ordning,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ORDNING_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: error,
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

export const updateOrdning = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.UPDATE_ORDNING_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.UPDATE_ORDNING_SUCCESS: {
      return {
        ...state,
        data: [],
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.UPDATE_ORDNING_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: error,
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

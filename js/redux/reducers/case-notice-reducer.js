import ActionsTypes from "../actions/case-notice-action-types";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const allMyNotices = function (state = initialState, action) {
  switch (action.type) {
    case ActionsTypes.GET_ALL_MY_NOTICES_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_MY_NOTICES_SUCCESS: {
      const { notices } = action.payload;
      return {
        ...state,
        data: notices,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_MY_NOTICES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
}

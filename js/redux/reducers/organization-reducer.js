import ActionsTypes from "../actions/organization-action-types";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const allOrganizations = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.GET_ALL_ORGANIZATIONS_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_ORGANIZATIONS_SUCCESS: {
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_ORGANIZATIONS_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

export const queryOrganizations = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.QUERY_ORGANIZATIONS_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.QUERY_ORGANIZATIONS_SUCCESS: {
      const orgs = action.payload;
      return {
        ...state,
        data: orgs,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.QUERY_ORGANIZATIONS_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

export const childrenWithRolesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionsTypes.GET_ALL_CHILDREN_WITH_ROLES_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ActionsTypes.GET_ALL_CHILDREN_WITH_ROLES_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_CHILDREN_WITH_ROLES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

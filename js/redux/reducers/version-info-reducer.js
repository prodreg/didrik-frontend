import ActionsTypes from "../actions/version-info-actions";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ActionsTypes.GET_VERSION_INFO_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false
      };
    }
    case ActionsTypes.GET_VERSION_INFO_SUCCESS: {
      const info = action.payload.versionInfo;
      return {
        ...state,
        data: info,
        loading: false,
        loaded: true,
        error: false
      };
    }
    case ActionsTypes.GET_VERSION_INFO_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true
      };
    }
    default:
      return state;
  }
}

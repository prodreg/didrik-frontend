import ActionsTypes from "../actions/case-action-types";

const initialStateAllCases = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const allCases = function (state = initialStateAllCases, action) {
  switch (action.type) {
    case ActionsTypes.GET_ALL_CASES_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_CASES_SUCCESS: {
      const { cases } = action.payload;
      return {
        ...state,
        data: cases,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_CASES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

const initialStateOneCase = {
  data: {},
  loading: false,
  loaded: false,
  error: false,
};

export const oneCase = function (state = initialStateOneCase, action) {
  switch (action.type) {
    case ActionsTypes.GET_ONE_CASES_REQUEST: {
      return {
        ...state,
        data: {},
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ONE_CASES_SUCCESS: {
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ONE_CASES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: {},
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

const initialStateAllActiveCases = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const allActiveCases = (state = initialStateAllActiveCases, action) => {
  switch (action.type) {
    case ActionsTypes.GET_ALL_ACTIVE_CASES_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_ACTIVE_CASES_SUCCESS: {
      const { cases } = action.payload;
      return {
        ...state,
        data: cases,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ALL_ACTIVE_CASES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

import ActionsTypes from "../actions/formdata-action-types";

const initialStateNoticeTypes = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const formdataNoticeTypes = function (state = initialStateNoticeTypes, action) {
  switch (action.type) {
    case ActionsTypes.GET_NOTICE_TYPES_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_NOTICE_TYPES_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_NOTICE_TYPES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
}

const initialStateProductTypes = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const formdataProductTypes = function (state = initialStateProductTypes, action) {
  switch (action.type) {
    case ActionsTypes.GET_PRODUCT_TYPES_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_PRODUCT_TYPES_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_PRODUCT_TYPES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
}

const initialStateSchemeTypes = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const formdataSchemeTypes = function (state = initialStateSchemeTypes, action) {
  switch (action.type) {
    case ActionsTypes.GET_SCHEME_TYPES_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_SCHEME_TYPES_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_SCHEME_TYPES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
}

const initialStateOrganizationTypes = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const formdataOrganizationTypes = function (state = initialStateOrganizationTypes, action) {
  switch (action.type) {
    case ActionsTypes.GET_ORGANIZATION_TYPES_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ActionsTypes.GET_ORGANIZATION_TYPES_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ActionsTypes.GET_ORGANIZATION_TYPES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
}
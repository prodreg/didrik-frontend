import ProducerActionTypes from "../actions/producer-action-types";

const initialStateProducers = {
  data: [],
  loading: false,
  loaded: false,
  totalResults: 0,
  error: false,
};

export const producersReducer = (state = initialStateProducers, action) => {
  switch (action.type) {
    case ProducerActionTypes.GET_ALL_PRODUCERS_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ProducerActionTypes.GET_ALL_PRODUCERS_SUCCESS: {
      const { producers, totalResults } = action.payload;
      return {
        ...state,
        data: producers,
        loading: false,
        loaded: true,
        totalResults: totalResults,
        error: false,
      };
    }
    case ProducerActionTypes.GET_ALL_PRODUCERS_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const producerReducer = (state = initialState, action) => {
  switch (action.type) {
    case ProducerActionTypes.GET_PRODUCER_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ProducerActionTypes.GET_PRODUCER_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ProducerActionTypes.GET_PRODUCER_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

const initialStateHistory = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const creditHistoryReducer = (state = initialStateHistory, action) => {
  switch (action.type) {
    case ProducerActionTypes.GET_CREDIT_HISTORY_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ProducerActionTypes.GET_CREDIT_HISTORY_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ProducerActionTypes.GET_CREDIT_HISTORY_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const productionHistoryReducer = (
  state = initialStateHistory,
  action
) => {
  switch (action.type) {
    case ProducerActionTypes.GET_PRODUCTION_HISTORY_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ProducerActionTypes.GET_PRODUCTION_HISTORY_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ProducerActionTypes.GET_PRODUCTION_HISTORY_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const farmProductionHistoryReducer = (
  state = initialStateHistory,
  action
) => {
  switch (action.type) {
    case ProducerActionTypes.GET_FARM_PRODUCTION_HISTORY_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ProducerActionTypes.GET_FARM_PRODUCTION_HISTORY_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ProducerActionTypes.GET_FARM_PRODUCTION_HISTORY_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const producerNumberConnectionReducer = (
  state = initialStateHistory,
  action
) => {
  switch (action.type) {
    case ProducerActionTypes.GET_ALL_PRODUCER_NUMBER_CONNECTIONS_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case ProducerActionTypes.GET_ALL_PRODUCER_NUMBER_CONNECTIONS_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ProducerActionTypes.GET_ALL_PRODUCER_NUMBER_CONNECTIONS_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

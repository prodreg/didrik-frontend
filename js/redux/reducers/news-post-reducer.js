import NewsActionTypes from "../actions/news-action-type";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const newsPostsReducer = (state = initialState, action) => {
  switch (action.type) {
    case NewsActionTypes.GET_ALL_NEWS_POSTS_REQUEST: {
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: false,
      };
    }
    case NewsActionTypes.GET_ALL_NEWS_POSTS_SUCCESS: {
      return {
        ...state,
        data: action.payload.posts,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case NewsActionTypes.GET_ALL_NEWS_POSTS_FAILURE: {
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: true,
      };
    }
    default:
      return state;
  }
};

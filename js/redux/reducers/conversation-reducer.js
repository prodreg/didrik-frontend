import ConversationActionTypes from "../actions/conversation-action-type";

const initialState = {
  data: [],
  loading: false,
  loaded: false,
  error: false,
};

export const conversationByIdReducer = (state = initialState, action) => {
  switch (action.type) {
    case ConversationActionTypes.GET_CONVERSATION_BY_ID_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ConversationActionTypes.GET_CONVERSATION_BY_ID_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ConversationActionTypes.GET_CONVERSATION_BY_ID_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const conversationForCaseAndOrganizationReducer = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case ConversationActionTypes.GET_CONVERSATION_FOR_CASE_AND_ORGANIZATION_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ConversationActionTypes.GET_CONVERSATION_FOR_CASE_AND_ORGANIZATION_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ConversationActionTypes.GET_CONVERSATION_FOR_CASE_AND_ORGANIZATION_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

export const conversationMessagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ConversationActionTypes.GET_CONVERSATION_MESSAGES_REQUEST: {
      return { ...state, data: [], loading: true, loaded: false, error: false };
    }
    case ConversationActionTypes.GET_CONVERSATION_MESSAGES_SUCCESS: {
      const { data } = action.payload;
      return {
        ...state,
        data: data,
        loading: false,
        loaded: true,
        error: false,
      };
    }
    case ConversationActionTypes.GET_CONVERSATION_MESSAGES_FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        data: [],
        loading: false,
        loaded: false,
        error: error,
      };
    }
    default:
      return state;
  }
};

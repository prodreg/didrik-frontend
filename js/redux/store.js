import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducers";
import thunk from "redux-thunk";
import producerApi from "../api/producer-api";
import adminApi from "../api/admin-api";
import newsApi from "../api/news-api";
import caseApi from "../api/case-api";
import caseNoticeApi from "../api/case-notice-api";
import caseFeedbackApi from "../api/case-feedback-api";
import organizationApi from "../api/organization-api";
import formdataApi from "../api/formdata-api";
import eventMessageApi from "../api/event-message-api";
import userApi from "../api/user-api";
import userInfoApi from "../api/user-info-api";
import versionInfoApi from "../api/version-info-api";
import organizationOrdningApi from "../api/organization-ordning-api";
import reportApi from "../api/report-api";
import importApi from "../api/import-api";
import conversationApi from "../api/conversation-api";

const apis = {
  producerApi,
  adminApi,
  newsApi,
  caseApi,
  caseNoticeApi,
  caseFeedbackApi,
  organizationApi,
  formdataApi,
  eventMessageApi,
  userApi,
  userInfoApi,
  versionInfoApi,
  organizationOrdningApi,
  reportApi,
  importApi,
  conversationApi,
};

export default createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk.withExtraArgument(apis)))
);

import AdminActionTypes from "./admin-action-type";

const sendMessageToUsersActionRequest = () => ({
  type: AdminActionTypes.SEND_MESSAGE_TO_USERS_REQUEST,
});

const sendMessageToUsersActionSuccess = () => ({
  type: AdminActionTypes.SEND_MESSAGE_TO_USERS_SUCCESS,
  payload: { error: false },
});

const sendMessageToUsersActionFailure = (error) => ({
  type: AdminActionTypes.SEND_MESSAGE_TO_USERS_FAILURE,
  payload: {
    error,
  },
});

const getBankFileActionRequest = () => ({
  type: AdminActionTypes.GET_BANK_FILE_REQUEST,
});

const getBankFileActionSuccess = (data) => ({
  type: AdminActionTypes.GET_BANK_FILE_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getBankFileActionFailure = (error) => ({
  type: AdminActionTypes.GET_BANK_FILE_FAILURE,
  payload: {
    error,
  },
});

const getReportedFilesActionRequest = () => ({
  type: AdminActionTypes.GET_REPORTED_FILES_REQUEST,
});

const getReportedFilesActionSuccess = (data) => ({
  type: AdminActionTypes.GET_REPORTED_FILES_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getReportedFilesActionFailure = (error) => ({
  type: AdminActionTypes.GET_REPORTED_FILES_FAILURE,
  payload: {
    error,
  },
});

const getReportedFileActionRequest = () => ({
  type: AdminActionTypes.GET_REPORTED_FILE_REQUEST,
});

const getReportedFileActionSuccess = (data) => ({
  type: AdminActionTypes.GET_REPORTED_FILE_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getReportedFileActionFailure = (error) => ({
  type: AdminActionTypes.GET_REPORTED_FILE_FAILURE,
  payload: {
    error,
  },
});

export default {
  sendMessageToUsersActionRequest,
  sendMessageToUsersActionSuccess,
  sendMessageToUsersActionFailure,
  getBankFileActionRequest,
  getBankFileActionSuccess,
  getBankFileActionFailure,
  getReportedFilesActionRequest,
  getReportedFilesActionSuccess,
  getReportedFilesActionFailure,
  getReportedFileActionRequest,
  getReportedFileActionSuccess,
  getReportedFileActionFailure,
};

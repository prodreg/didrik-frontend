import ResponsibilityActionTypes from "./report-action-types";

const getResponsibilityReportActionRequest = () => ({
  type: ResponsibilityActionTypes.GET_RESPONSIBILITY_REPORT_REQUEST,
});

const getResponsibilityReportActionSuccess = (data) => ({
  type: ResponsibilityActionTypes.GET_RESPONSIBILITY_REPORT_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getResponsibilityReportActionFailure = (error) => ({
  type: ResponsibilityActionTypes.GET_RESPONSIBILITY_REPORT_FAILURE,
  payload: {
    error,
  },
});

const getWordFileResponsibilityActionRequest = () => ({
  type: ResponsibilityActionTypes.GET_WORD_FILE_RESPONSIBILITY_REQUEST,
});

const getWordFileResponsibilityActionSuccess = (data) => ({
  type: ResponsibilityActionTypes.GET_WORD_FILE_RESPONSIBILITY_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getWordFileResponsibilityActionFailure = (error) => ({
  type: ResponsibilityActionTypes.GET_WORD_FILE_RESPONSIBILITY_FAILURE,
  payload: {
    error,
  },
});

const getExcelFileResponsibilityActionRequest = () => ({
  type: ResponsibilityActionTypes.GET_EXCEL_FILE_RESPONSIBILITY_REQUEST,
});

const getExcelFileResponsibilityActionSuccess = (data) => ({
  type: ResponsibilityActionTypes.GET_EXCEL_FILE_RESPONSIBILITY_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getExcelFileResponsibilityActionFailure = (error) => ({
  type: ResponsibilityActionTypes.GET_EXCEL_FILE_RESPONSIBILITY_FAILURE,
  payload: {
    error,
  },
});

const getVaremottakerReportActionRequest = () => ({
  type: ResponsibilityActionTypes.GET_VAREMOTTAKER_REPORT_REQUEST,
});

const getVaremottakerReportActionSuccess = (data) => ({
  type: ResponsibilityActionTypes.GET_VAREMOTTAKER_REPORT_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getVaremottakerReportActionFailure = (error) => ({
  type: ResponsibilityActionTypes.GET_VAREMOTTAKER_REPORT_FAILURE,
  payload: {
    error,
  },
});

export default {
  getResponsibilityReportActionRequest,
  getResponsibilityReportActionSuccess,
  getResponsibilityReportActionFailure,
  getWordFileResponsibilityActionRequest,
  getWordFileResponsibilityActionSuccess,
  getWordFileResponsibilityActionFailure,
  getExcelFileResponsibilityActionRequest,
  getExcelFileResponsibilityActionSuccess,
  getExcelFileResponsibilityActionFailure,
  getVaremottakerReportActionRequest,
  getVaremottakerReportActionSuccess,
  getVaremottakerReportActionFailure
};

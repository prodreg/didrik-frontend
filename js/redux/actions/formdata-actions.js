import FormdataActionTypes from "./formdata-action-types";

const getNoticeTypesActionRequest = () => {
  return {
    type: FormdataActionTypes.GET_NOTICE_TYPES_REQUEST,
  };
};

const getNoticeTypesActionSuccess = (data) => {
  return {
    type: FormdataActionTypes.GET_NOTICE_TYPES_SUCCESS,
    payload: {
      data,
    },
  };
};

const getNoticeTypesActionFailure = (error) => {
  return {
    type: FormdataActionTypes.GET_NOTICE_TYPES_FAILURE,
    payload: {
      error,
    },
  };
};

const getProductTypesRequest = () => {
  return {
    type: FormdataActionTypes.GET_PRODUCT_TYPES_REQUEST,
  };
};

const getProductTypesSuccess = (data) => {
  return {
    type: FormdataActionTypes.GET_PRODUCT_TYPES_SUCCESS,
    payload: {
      data,
    },
  };
};

const getProductTypesFailure = (error) => {
  return {
    type: FormdataActionTypes.GET_PRODUCT_TYPES_FAILURE,
    payload: {
      error,
    },
  };
};

const getSchemeTypesRequest = () => {
  return {
    type: FormdataActionTypes.GET_SCHEME_TYPES_REQUEST,
  };
};

const getSchemeTypesSuccess = (data) => {
  return {
    type: FormdataActionTypes.GET_SCHEME_TYPES_SUCCESS,
    payload: {
      data,
    },
  };
};

const getSchemeTypesFailure = (error) => {
  return {
    type: FormdataActionTypes.GET_SCHEME_TYPES_FAILURE,
    payload: {
      error,
    },
  };
};

const getOrganizationTypesRequest = () => {
  return {
    type: FormdataActionTypes.GET_ORGANIZATION_TYPES_REQUEST,
  };
};

const getOrganizationTypesSuccess = (data) => {
  return {
    type: FormdataActionTypes.GET_ORGANIZATION_TYPES_SUCCESS,
    payload: {
      data,
    },
  };
};

const getOrganizationTypesFailure = (error) => {
  return {
    type: FormdataActionTypes.GET_ORGANIZATION_TYPES_FAILURE,
    payload: {
      error,
    },
  };
};

export default {
  getNoticeTypesActionRequest,
  getNoticeTypesActionSuccess,
  getNoticeTypesActionFailure,

  getProductTypesRequest,
  getProductTypesSuccess,
  getProductTypesFailure,

  getSchemeTypesRequest,
  getSchemeTypesSuccess,
  getSchemeTypesFailure,

  getOrganizationTypesRequest,
  getOrganizationTypesSuccess,
  getOrganizationTypesFailure,
};

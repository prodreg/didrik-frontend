const OrganizationOrdningActionTypes = {
  GET_ALL_ORDNING_REQUEST: 'GET_ALL_ORDNING_REQUEST',
  GET_ALL_ORDNING_SUCCESS: 'GET_ALL_ORDNING_SUCCESS',
  GET_ALL_ORDNING_FAILURE: 'GET_ALL_ORDNING_FAILURE',

  GET_ORDNING_REQUEST: 'GET_ORDNING_REQUEST',
  GET_ORDNING_SUCCESS: 'GET_ORDNING_SUCCESS',
  GET_ORDNING_FAILURE: 'GET_ORDNING_FAILURE',

  UPDATE_ORDNING_REQUEST: 'UPDATE_ORDNING_REQUEST',
  UPDATE_ORDNING_SUCCESS: 'UPDATE_ORDNING_SUCCESS',
  UPDATE_ORDNING_FAILURE: 'UPDATE_ORDNING_FAILURE'
}

export default OrganizationOrdningActionTypes;
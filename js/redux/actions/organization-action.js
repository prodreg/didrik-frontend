import ActionTypes from "./organization-action-types";

const getAllOrganizationsActionRequest = () => ({
  type: ActionTypes.GET_ALL_ORGANIZATIONS_REQUEST,
});

const getAllOrganizationsActionSuccess = (data) => ({
  type: ActionTypes.GET_ALL_ORGANIZATIONS_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getAllOrganizationsActionFailure = (error) => ({
  type: ActionTypes.GET_ALL_ORGANIZATIONS_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const queryOrganizationsActionRequest = () => ({
  type: ActionTypes.QUERY_ORGANIZATIONS_REQUEST,
});

const queryOrganizationsActionSuccess = (orgs) => ({
  type: ActionTypes.QUERY_ORGANIZATIONS_SUCCESS,
  payload: {
    orgs,
  },
});

const queryOrganizationsActionFailure = (error) => ({
  type: ActionTypes.QUERY_ORGANIZATIONS_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const getOrganizationDetailsActionRequest = () => ({
  type: ActionTypes.GET_ORGANIZATION_DETAILS_REQUEST,
});

const getOrganizationDetailsActionSuccess = (details) => ({
  type: ActionTypes.GET_ORGANIZATION_DETAILS_SUCCESS,
  payload: {
    details,
  },
});

const getOrganizationDetailsActionFailure = (error) => ({
  type: ActionTypes.GET_ORGANIZATION_DETAILS_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const updateOrganizationNameActionRequest = () => ({
  type: ActionTypes.UPDATE_ORG_NAME_REQUEST,
});

const updateOrganizationNameActionSuccess = () => ({
  type: ActionTypes.UPDATE_ORG_NAME_SUCCESS,
  payload: {},
});

const updateOrganizationNameActionFailure = (error) => ({
  type: ActionTypes.UPDATE_ORG_NAME_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const updateOrganizationEmailActionRequest = () => ({
  type: ActionTypes.UPDATE_ORG_EMAIL_REQUEST,
});

const updateOrganizationEmailActionSuccess = () => ({
  type: ActionTypes.UPDATE_ORG_EMAIL_SUCCESS,
  payload: {},
});

const updateOrganizationEmailActionFailure = (error) => ({
  type: ActionTypes.UPDATE_ORG_EMAIL_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const updateOrganizationUserEmailActionRequest = () => ({
  type: ActionTypes.UPDATE_ORG_USER_EMAIL_REQUEST,
});

const updateOrganizationUserEmailActionSuccess = () => ({
  type: ActionTypes.UPDATE_ORG_USER_EMAIL_SUCCESS,
  payload: {},
});

const updateOrganizationUserEmailActionFailure = (error) => ({
  type: ActionTypes.UPDATE_ORG_USER_EMAIL_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const getUsersInOrganizationActionRequest = () => ({
  type: ActionTypes.GET_USERS_IN_ORGANIZATION_REQUEST,
});

const getUsersInOrganizationActionSuccess = (users) => ({
  type: ActionTypes.GET_USERS_IN_ORGANIZATION_SUCCESS,
  payload: {
    users,
  },
});

const getUsersInOrganizationActionFailure = (error) => ({
  type: ActionTypes.GET_USERS_IN_ORGANIZATION_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const addUserToOrganizationActionRequest = () => ({
  type: ActionTypes.ADD_USER_TO_ORGANIZATION_REQUEST,
});

const addUserToOrganizationActionSuccess = () => ({
  type: ActionTypes.ADD_USER_TO_ORGANIZATION_SUCCESS,
  payload: {},
});

const addUserToOrganizationActionFailure = (error) => ({
  type: ActionTypes.ADD_USER_TO_ORGANIZATION_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const addOrganizationActionRequest = () => ({
  type: ActionTypes.ADD_ORGANIZATION_REQUEST,
});

const addOrganizationActionSuccess = () => ({
  type: ActionTypes.ADD_ORGANIZATION_SUCCESS,
  payload: {},
});

const addOrganizationActionFailure = (error) => ({
  type: ActionTypes.ADD_ORGANIZATION_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const getAllChildrenForOrganizationRequest = () => ({
  type: ActionTypes.GET_ALL_CHILDREN_FOR_ORGANIZATION_REQUEST,
});

const getAllChildrenForOrganizationSuccess = (data) => ({
  type: ActionTypes.GET_ALL_CHILDREN_FOR_ORGANIZATION_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getAllChildrenForOrganizationFailure = (error) => ({
  type: ActionTypes.GET_ALL_CHILDREN_FOR_ORGANIZATION_FAILURE,
  payload: {
    error,
  },
});

const getAllChildrenWithRolesActionRequest = () => ({
  type: ActionTypes.GET_ALL_CHILDREN_WITH_ROLES_REQUEST,
});

const getAllChildrenWithRolesActionSuccess = (data) => ({
  type: ActionTypes.GET_ALL_CHILDREN_WITH_ROLES_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getAllChildrenWithRolesActionFailure = (error) => ({
  type: ActionTypes.GET_ALL_CHILDREN_WITH_ROLES_FAILURE,
  payload: {
    error,
  },
});

export default {
  getAllOrganizationsActionRequest,
  getAllOrganizationsActionSuccess,
  getAllOrganizationsActionFailure,
  queryOrganizationsActionRequest,
  queryOrganizationsActionSuccess,
  queryOrganizationsActionFailure,
  getOrganizationDetailsActionRequest,
  getOrganizationDetailsActionSuccess,
  getOrganizationDetailsActionFailure,
  updateOrganizationNameActionRequest,
  updateOrganizationNameActionSuccess,
  updateOrganizationNameActionFailure,
  updateOrganizationEmailActionRequest,
  updateOrganizationEmailActionSuccess,
  updateOrganizationEmailActionFailure,
  updateOrganizationUserEmailActionRequest,
  updateOrganizationUserEmailActionSuccess,
  updateOrganizationUserEmailActionFailure,
  getUsersInOrganizationActionRequest,
  getUsersInOrganizationActionSuccess,
  getUsersInOrganizationActionFailure,
  addUserToOrganizationActionRequest,
  addUserToOrganizationActionSuccess,
  addUserToOrganizationActionFailure,
  getAllChildrenForOrganizationRequest,
  getAllChildrenForOrganizationSuccess,
  getAllChildrenForOrganizationFailure,
  addOrganizationActionRequest,
  addOrganizationActionSuccess,
  addOrganizationActionFailure,
  getAllChildrenWithRolesActionRequest,
  getAllChildrenWithRolesActionSuccess,
  getAllChildrenWithRolesActionFailure,
};

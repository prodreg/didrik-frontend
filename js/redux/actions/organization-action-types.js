const OrganizationActionTypes = {
  GET_ALL_ORGANIZATIONS_REQUEST: "GET_ALL_ORGANIZATIONS_REQUEST",
  GET_ALL_ORGANIZATIONS_SUCCESS: "GET_ALL_ORGANIZATIONS_SUCCESS",
  GET_ALL_ORGANIZATIONS_FAILURE: "GET_ALL_ORGANIZATIONS_FAILURE",

  QUERY_ORGANIZATIONS_REQUEST: "QUERY_ORGANIZATIONS_REQUEST",
  QUERY_ORGANIZATIONS_SUCCESS: "QUERY_ORGANIZATIONS_SUCCESS",
  QUERY_ORGANIZATIONS_FAILURE: "QUERY_ORGANIZATIONS_FAILURE",

  GET_ORGANIZATION_DETAILS_REQUEST: "GET_ORGANIZATION_DETAILS_REQUEST",
  GET_ORGANIZATION_DETAILS_SUCCESS: "GET_ORGANIZATION_DETAILS_SUCCESS",
  GET_ORGANIZATION_DETAILS_FAILURE: "GET_ORGANIZATION_DETAILS_FAILURE",

  UPDATE_ORG_NAME_REQUEST: "UPDATE_ORG_NAME_REQUEST",
  UPDATE_ORG_NAME_SUCCESS: "UPDATE_ORG_NAME_SUCCESS",
  UPDATE_ORG_NAME_FAILURE: "UPDATE_ORG_NAME_FAILURE",

  UPDATE_ORG_EMAIL_REQUEST: "UPDATE_ORG_EMAIL_REQUEST",
  UPDATE_ORG_EMAIL_SUCCESS: "UPDATE_ORG_EMAIL_SUCCESS",
  UPDATE_ORG_EMAIL_FAILURE: "UPDATE_ORG_EMAIL_FAILURE",

  UPDATE_ORG_USER_EMAIL_REQUEST: "UPDATE_ORG_USER_EMAIL_REQUEST",
  UPDATE_ORG_USER_EMAIL_SUCCESS: "UPDATE_ORG_USER_EMAIL_SUCCESS",
  UPDATE_ORG_USER_EMAIL_FAILURE: "UPDATE_ORG_USER_EMAIL_FAILURE",

  GET_USERS_IN_ORGANIZATION_REQUEST: "GET_USERS_IN_ORGANIZATION_REQUEST",
  GET_USERS_IN_ORGANIZATION_SUCCESS: "GET_USERS_IN_ORGANIZATION_SUCCESS",
  GET_USERS_IN_ORGANIZATION_FAILURE: "GET_USERS_IN_ORGANIZATION_FAILURE",

  ADD_USER_TO_ORGANIZATION_REQUEST: "ADD_USER_TO_ORGANIZATION_REQUEST",
  ADD_USER_TO_ORGANIZATION_SUCCESS: "ADD_USER_TO_ORGANIZATION_SUCCESS",
  ADD_USER_TO_ORGANIZATION_FAILURE: "ADD_USER_TO_ORGANIZATION_FAILURE",
  GET_ALL_CHILDREN_FOR_ORGANIZATION_REQUEST:
    "GET_ALL_CHILDREN_FOR_ORGANZATION_REQUEST",
  GET_ALL_CHILDREN_FOR_ORGANIZATION_SUCCESS:
    "GET_ALL_CHILDREN_FOR_ORGANIZATION_SUCCESS",
  GET_ALL_CHILDREN_FOR_ORGANIZATION_FAILURE:
    "GET_ALL_CHILDREN_FOR_ORGANIZATION_FAILURE",
  ADD_ORGANIZATION_REQUEST: "ADD_ORGANIZATION_REQUEST",
  ADD_ORGANIZATION_SUCCESS: "ADD_ORGANIZATION_SUCCESS",
  ADD_ORGANIZATION_FAILURE: "ADD_ORGANIZATION_FAILURE",
  GET_ALL_CHILDREN_WITH_ROLES_REQUEST: "GET_ALL_CHILDREN_WITH_ROLES_REQUEST",
  GET_ALL_CHILDREN_WITH_ROLES_SUCCESS: "GET_ALL_CHILDREN_WITH_ROLES_SUCCESS",
  GET_ALL_CHILDREN_WITH_ROLES_FAILURE: "GET_ALL_CHILDREN_WITH_ROLES_FAILURE",
};

export default OrganizationActionTypes;

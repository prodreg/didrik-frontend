import ImportActionTypes from "./import-action-type";

const getImportsActionRequest = () => ({
  type: ImportActionTypes.GET_ALL_IMPORTS_REQUEST,
});

const getImportsActionSuccess = (data) => ({
  type: ImportActionTypes.GET_ALL_IMPORTS_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getImportsActionFailure = (error) => ({
  type: ImportActionTypes.GET_ALL_IMPORTS_FAILURE,
  payload: {
    error,
  },
});

const getImportForBankActionRequest = () => ({
  type: ImportActionTypes.GET_IMPORT_FOR_BANK_REQUEST,
});

const getImportForBankActionSuccess = (data) => ({
  type: ImportActionTypes.GET_IMPORT_FOR_BANK_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getImportForBankActionFailure = (error) => ({
  type: ImportActionTypes.GET_IMPORT_FOR_BANK_FAILURE,
  payload: {
    error,
  },
});

const getImportForOrganizationActionRequest = () => ({
  type: ImportActionTypes.GET_IMPORT_FOR_ORGANIZATION_REQUEST,
});

const getImportForOrganizationActionSuccess = (data) => ({
  type: ImportActionTypes.GET_IMPORT_FOR_ORGANIZATION_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getImportForOrganizationActionFailure = (error) => ({
  type: ImportActionTypes.GET_IMPORT_FOR_ORGANIZATION_FAILURE,
  payload: {
    error,
  },
});

const createImportForBankActionRequest = () => ({
  type: ImportActionTypes.CREATE_IMPORT_FOR_BANK_REQUEST,
});

const createImportForBankActionSuccess = (data) => ({
  type: ImportActionTypes.CREATE_IMPORT_FOR_BANK_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const createImportForBankActionFailure = (error) => ({
  type: ImportActionTypes.CREATE_IMPORT_FOR_BANK_FAILURE,
  payload: {
    error,
  },
});

const createImportForOrganizationActionRequest = () => ({
  type: ImportActionTypes.CREATE_IMPORT_FOR_ORGANIZATION_REQUEST,
});

const createImportForOrganizationActionSuccess = (data) => ({
  type: ImportActionTypes.CREATE_IMPORT_FOR_ORGANIZATION_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const createImportForOrganizationActionFailure = (error) => ({
  type: ImportActionTypes.CREATE_IMPORT_FOR_ORGANIZATION_FAILURE,
  payload: {
    error,
  },
});

const deleteBankImportActionRequest = () => ({
  type: ImportActionTypes.DELETE_BANK_IMPORT_REQUEST,
});

const deleteBankImportActionSuccess = () => ({
  type: ImportActionTypes.DELETE_BANK_IMPORT_SUCCESS,
  payload: {
    error: false,
  },
});

const deleteBankImportActionFailure = (error) => ({
  type: ImportActionTypes.DELETE_BANK_IMPORT_FAILURE,
  payload: {
    error,
  },
});

const deleteOrganizationImportActionRequest = () => ({
  type: ImportActionTypes.DELETE_ORGANIZATION_IMPORT_REQUEST,
});

const deleteOrganizationImportActionSuccess = () => ({
  type: ImportActionTypes.DELETE_ORGANIZATION_IMPORT_SUCCESS,
  payload: {
    error: false,
  },
});

const deleteOrganizationImportActionFailure = (error) => ({
  type: ImportActionTypes.DELETE_ORGANIZATION_IMPORT_FAILURE,
  payload: {
    error,
  },
});

const deleteBankErrorRowsActionRequest = () => ({
  type: ImportActionTypes.DELETE_BANK_ERROR_ROWS_REQUEST,
});

const deleteBankErrorRowsActionSuccess = () => ({
  type: ImportActionTypes.DELETE_BANK_ERROR_ROWS_SUCCESS,
  payload: {
    error: false,
  },
});

const deleteBankErrorRowsActionFailure = (error) => ({
  type: ImportActionTypes.DELETE_BANK_ERROR_ROWS_FAILURE,
  payload: {
    error,
  },
});

const deleteOrganizationErrorRowsActionRequest = () => ({
  type: ImportActionTypes.DELETE_ORGANIZATION_ERROR_ROWS_REQUEST,
});

const deleteOrganizationErrorRowsActionSuccess = () => ({
  type: ImportActionTypes.DELETE_ORGANIZATION_ERROR_ROWS_SUCCESS,
  payload: {
    error: false,
  },
});

const deleteOrganizationErrorRowsActionFailure = (error) => ({
  type: ImportActionTypes.DELETE_ORGANIZATION_ERROR_ROWS_FAILURE,
  payload: {
    error,
  },
});

export default {
  getImportsActionRequest,
  getImportsActionSuccess,
  getImportsActionFailure,
  getImportForBankActionRequest,
  getImportForBankActionSuccess,
  getImportForBankActionFailure,
  getImportForOrganizationActionRequest,
  getImportForOrganizationActionSuccess,
  getImportForOrganizationActionFailure,
  createImportForBankActionRequest,
  createImportForBankActionSuccess,
  createImportForBankActionFailure,
  createImportForOrganizationActionRequest,
  createImportForOrganizationActionSuccess,
  createImportForOrganizationActionFailure,
  deleteBankImportActionRequest,
  deleteBankImportActionSuccess,
  deleteBankImportActionFailure,
  deleteOrganizationImportActionRequest,
  deleteOrganizationImportActionSuccess,
  deleteOrganizationImportActionFailure,
  deleteBankErrorRowsActionRequest,
  deleteBankErrorRowsActionSuccess,
  deleteBankErrorRowsActionFailure,
  deleteOrganizationErrorRowsActionRequest,
  deleteOrganizationErrorRowsActionSuccess,
  deleteOrganizationErrorRowsActionFailure,
};

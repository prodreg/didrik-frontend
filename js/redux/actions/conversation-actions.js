import ConversationActionTypes from "./conversation-action-type";

const getConversationByIdActionRequest = () => ({
  type: ConversationActionTypes.GET_CONVERSATION_BY_ID_REQUEST,
});

const getConversationByIdActionSuccess = (data) => ({
  type: ConversationActionTypes.GET_CONVERSATION_BY_ID_REQUEST,
  payload: {
    data,
    error: false,
  },
});

const getConversationByIdActionFailure = (error) => ({
  type: ConversationActionTypes.GET_CONVERSATION_BY_ID_REQUEST,
  payload: {
    error,
  },
});

const getConversationForCaseAndOrganizationActionRequest = () => ({
  type: ConversationActionTypes.GET_CONVERSATION_FOR_CASE_AND_ORGANIZATION_REQUEST,
});

const getConversationForCaseAndOrganizationActionSuccess = (data) => ({
  type: ConversationActionTypes.GET_CONVERSATION_FOR_CASE_AND_ORGANIZATION_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getConversationForCaseAndOrganizationActionFailure = (error) => ({
  type: ConversationActionTypes.GET_CONVERSATION_FOR_CASE_AND_ORGANIZATION_FAILURE,
  payload: {
    error,
  },
});

const postConversationMessageActionRequest = () => ({
  type: ConversationActionTypes.POST_CONVERSATION_MESSAGE_REQUEST,
});

const postConversationMessageActionSuccess = (data) => ({
  type: ConversationActionTypes.POST_CONVERSATION_MESSAGE_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const postConversationMessageActionFailure = (error) => ({
  type: ConversationActionTypes.POST_CONVERSATION_MESSAGE_FAILURE,
  payload: {
    error,
  },
});

const postConversationMessageAsActionRequest = () => ({
  type: ConversationActionTypes.POST_CONVERSATION_MESSAGE_AS_REQUEST,
});

const postConversationMessageAsActionSuccess = (data) => ({
  type: ConversationActionTypes.POST_CONVERSATION_MESSAGE_AS_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const postConversationMessageAsActionFailure = (error) => ({
  type: ConversationActionTypes.POST_CONVERSATION_MESSAGE_AS_FAILURE,
  payload: {
    error,
  },
});

const getConversationMessagesActionRequest = () => ({
  type: ConversationActionTypes.GET_CONVERSATION_MESSAGES_REQUEST,
});

const getConversationMessagesActionSuccess = (data) => ({
  type: ConversationActionTypes.GET_CONVERSATION_MESSAGES_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getConversationMessagesActionFailure = (error) => ({
  type: ConversationActionTypes.GET_CONVERSATION_MESSAGES_FAILURE,
  payload: {
    error,
  },
});

const getConversationAttachmentsActionRequest = () => ({
  type: ConversationActionTypes.GET_CONVERSATION_ATTACHMENT_REQUEST,
});

const getConversationAttachmentsActionSuccess = (data) => ({
  type: ConversationActionTypes.GET_CONVERSATION_ATTACHMENT_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getConversationAttachmentsActionFailure = (error) => ({
  type: ConversationActionTypes.GET_CONVERSATION_ATTACHMENT_FAILURE,
  payload: {
    error,
  },
});

export default {
  getConversationByIdActionRequest,
  getConversationByIdActionSuccess,
  getConversationByIdActionFailure,
  getConversationForCaseAndOrganizationActionRequest,
  getConversationForCaseAndOrganizationActionSuccess,
  getConversationForCaseAndOrganizationActionFailure,
  postConversationMessageActionRequest,
  postConversationMessageActionSuccess,
  postConversationMessageActionFailure,
  postConversationMessageAsActionRequest,
  postConversationMessageAsActionSuccess,
  postConversationMessageAsActionFailure,
  getConversationMessagesActionRequest,
  getConversationMessagesActionSuccess,
  getConversationMessagesActionFailure,
  getConversationAttachmentsActionRequest,
  getConversationAttachmentsActionSuccess,
  getConversationAttachmentsActionFailure,
};

import CasesActionTypes from "./case-notice-action-types";

const getAllMyCaseNoticesRequest = () => ({
  type: CasesActionTypes.GET_ALL_MY_NOTICES_REQUEST,
});

const getAllMyCaseNoticesSuccess = (notices) => ({
  type: CasesActionTypes.GET_ALL_MY_NOTICES_SUCCESS,
  payload: {
    notices,
  },
});

const getAllMyCaseNoticesFailure = (error) => ({
  type: CasesActionTypes.GET_ALL_MY_NOTICES_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const getNoticeIdForOrganizationActionRequest = () => ({
  type: CasesActionTypes.GET_NOTICE_ID_FOR_ORGANIZATION_REQUEST,
});

const getNoticeIdForOrganizationActionSuccess = (data) => ({
  type: CasesActionTypes.GET_NOTICE_ID_FOR_ORGANIZATION_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getNoticeIdForOrganizationActionFailure = (error) => ({
  type: CasesActionTypes.GET_NOTICE_ID_FOR_ORGANIZATION_FAILURE,
  payload: {
    error,
  },
});

export default {
  getAllMyCaseNoticesRequest,
  getAllMyCaseNoticesSuccess,
  getAllMyCaseNoticesFailure,
  getNoticeIdForOrganizationActionRequest,
  getNoticeIdForOrganizationActionSuccess,
  getNoticeIdForOrganizationActionFailure,
};

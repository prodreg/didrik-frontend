import ActionTypes from "./user-action-types";

const loginRequest = () => ({
  type: ActionTypes.LOGIN_REQUEST,
});

const loginSuccess = (user) => ({
  type: ActionTypes.LOGIN_SUCCESS,
  payload: {
    user,
  },
});

const loginFailure = (error) => ({
  type: ActionTypes.LOGIN_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const logoutRequest = () => ({
  type: ActionTypes.LOGOUT_REQUEST,
});

const getUserInfoActionRequest = () => ({
  type: ActionTypes.GET_USER_INFO_REQUEST,
});

const getUserInfoActionSuccess = (userInfo) => ({
  type: ActionTypes.GET_USER_INFO_SUCCESS,
  payload: {
    userInfo,
  },
});

const getUserInfoActionFailure = (error) => ({
  type: ActionTypes.GET_USER_INFO_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const updateUserEmailActionRequest = () => ({
  type: ActionTypes.UPDATE_USER_EMAIL_REQUEST,
});

const updateUserEmailActionSuccess = () => ({
  type: ActionTypes.UPDATE_USER_EMAIL_SUCCESS,
  payload: {}
});

const updateUserEmailActionFailure = (error) => ({
  type: ActionTypes.UPDATE_USER_EMAIL_FAILURE,
  payload: {
    error
  },
  error: true
});

const changeUserPasswordActionRequest = () => ({
  type: ActionTypes.CHANGE_USER_PASSWORD_REQUEST,
});

const changeUserPasswordActionSuccess = () => ({
  type: ActionTypes.CHANGE_USER_PASSWORD_SUCCESS,
  payload: {}
});

const changeUserPasswordActionFailure = (error) => ({
  type: ActionTypes.CHANGE_USER_PASSWORD_FAILURE,
  payload: {
    error
  },
  error: true
});

const subscribeUserToEmailActionRequest = () => ({
  type: ActionTypes.SUBSCRIBE_USER_TO_EMAIL_REQUEST,
});

const subscribeUserToEmailActionSuccess = () => ({
  type: ActionTypes.SUBSCRIBE_USER_TO_EMAIL_SUCCESS,
  payload: {}
});

const subscribeUserToEmailActionFailure = (error) => ({
  type: ActionTypes.SUBSCRIBE_USER_TO_EMAIL_FAILURE,
  payload: {
    error
  },
  error: true
});

const updateUserStatusActionRequest = () => ({
  type: ActionTypes.UPDATE_USER_STATUS_REQUEST,
});

const updateUserStatusActionSuccess = () => ({
  type: ActionTypes.UPDATE_USER_STATUS_SUCCESS,
  payload: {}
});

const updateUserStatusActionFailure = (error) => ({
  type: ActionTypes.UPDATE_USER_STATUS_FAILURE,
  payload: {
    error
  },
  error: true
});

const requestResetUserPasswordActionRequest = () => ({
  type: ActionTypes.REQUEST_RESET_USER_PASSWORD_REQUEST,
});

const requestResetUserPasswordActionSuccess = () => ({
  type: ActionTypes.REQUEST_RESET_USER_PASSWORD_SUCCESS,
  payload: {}
});

const requestResetUserPasswordActionFailure = (error) => ({
  type: ActionTypes.REQUEST_RESET_USER_PASSWORD_FAILURE,
  payload: {
    error
  },
  error: true
});

const resetUserPasswordActionRequest = () => ({
  type: ActionTypes.RESET_USER_PASSWORD_REQUEST,
});

const resetUserPasswordActionSuccess = () => ({
  type: ActionTypes.RESET_USER_PASSWORD_SUCCESS,
  payload: {}
});

const resetUserPasswordActionFailure = (error) => ({
  type: ActionTypes.RESET_USER_PASSWORD_FAILURE,
  payload: {
    error
  },
  error: true
});

const getResetUserPasswordInfoActionRequest = () => ({
  type: ActionTypes.GET_RESET_USER_PASSWORD_INFO_REQUEST,
});

const getResetUserPasswordInfoActionSuccess = () => ({
  type: ActionTypes.GET_RESET_USER_PASSWORD_INFO_SUCCESS,
  payload: {}
});

const getResetUserPasswordInfoActionFailure = (error) => ({
  type: ActionTypes.GET_RESET_USER_PASSWORD_INFO_FAILURE,
  payload: {
    error
  },
  error: true
});

export default {
  loginRequest,
  loginSuccess,
  loginFailure,
  logoutRequest,
  getUserInfoActionRequest,
  getUserInfoActionSuccess,
  getUserInfoActionFailure,
  updateUserEmailActionRequest,
  updateUserEmailActionSuccess,
  updateUserEmailActionFailure,
  changeUserPasswordActionRequest,
  changeUserPasswordActionSuccess,
  changeUserPasswordActionFailure,
  subscribeUserToEmailActionRequest,
  subscribeUserToEmailActionSuccess,
  subscribeUserToEmailActionFailure,
  updateUserStatusActionRequest,
  updateUserStatusActionSuccess,
  updateUserStatusActionFailure,
  requestResetUserPasswordActionRequest,
  requestResetUserPasswordActionSuccess,
  requestResetUserPasswordActionFailure,
  resetUserPasswordActionRequest,
  resetUserPasswordActionSuccess,
  resetUserPasswordActionFailure,
  getResetUserPasswordInfoActionRequest,
  getResetUserPasswordInfoActionSuccess,
  getResetUserPasswordInfoActionFailure,
}
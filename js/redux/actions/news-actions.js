import NewsActionTypes from "./news-action-type";

const getAllNewsPostsActionRequest = () => ({
  type: NewsActionTypes.GET_ALL_NEWS_POSTS_REQUEST,
});

const getAllNewsPostsActionSuccess = (posts) => ({
  type: NewsActionTypes.GET_ALL_NEWS_POSTS_SUCCESS,
  payload: {
    posts,
    error: false,
  },
});

const getAllNewsPostsActionFailure = (error) => ({
  type: NewsActionTypes.GET_ALL_NEWS_POSTS_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const postNewsPostActionRequest = () => ({
  type: NewsActionTypes.POST_NEWS_POST_REQUEST,
});

const postNewsPostActionSuccess = () => ({
  type: NewsActionTypes.POST_NEWS_POST_SUCCESS,
  payload: {
    error: false,
  },
});

const postNewsPostActionFailure = (error) => ({
  type: NewsActionTypes.POST_NEWS_POST_FAILURE,
  payload: {
    error,
  },
});

const editNewsPostActionRequest = () => ({
  type: NewsActionTypes.EDIT_NEWS_POST_REQUEST,
});

const editNewsPostActionSuccess = () => ({
  type: NewsActionTypes.EDIT_NEWS_POST_SUCCESS,
  payload: {
    error: false,
  },
});

const editNewsPostActionFailure = (error) => ({
  type: NewsActionTypes.EDIT_NEWS_POST_FAILURE,
  payload: {
    error,
  },
});

const deleteNewsPostActionRequest = () => ({
  type: NewsActionTypes.DELETE_NEWS_POST_REQUEST,
});

const deleteNewsPostActionSuccess = () => ({
  type: NewsActionTypes.DELETE_NEWS_POST_SUCCESS,
  payload: {
    error: false,
  },
});

const deleteNewsPostActionFailure = (error) => ({
  type: NewsActionTypes.DELETE_NEWS_POST_FAILURE,
  payload: {
    error: error,
  },
});

export default {
  getAllNewsPostsActionRequest,
  getAllNewsPostsActionSuccess,
  getAllNewsPostsActionFailure,
  postNewsPostActionRequest,
  postNewsPostActionSuccess,
  postNewsPostActionFailure,
  editNewsPostActionRequest,
  editNewsPostActionSuccess,
  editNewsPostActionFailure,
  deleteNewsPostActionRequest,
  deleteNewsPostActionSuccess,
  deleteNewsPostActionFailure,
};

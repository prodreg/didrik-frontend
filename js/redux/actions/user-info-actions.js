import ActionTypes from "./user-info-action-types";

const getUserInfoActionRequest = () => ({
  type: ActionTypes.GET_USER_INFO_REQUEST,
});

const getUserInfoActionSuccess = (userInfo) => ({
  type: ActionTypes.GET_USER_INFO_SUCCESS,
  payload: {
    userInfo,
  },
});

const getUserInfoActionFailure = (error) => ({
  type: ActionTypes.GET_USER_INFO_FAILURE,
  payload: {
    error,
  },
  error: true,
});

export default {
  getUserInfoActionRequest,
  getUserInfoActionSuccess,
  getUserInfoActionFailure,
};
import CasesActionTypes from "./case-action-types";

const getAllCasesActionRequest = () => ({
  type: CasesActionTypes.GET_ALL_CASES_REQUEST,
});

const getAllCasesActionSuccess = (cases) => ({
  type: CasesActionTypes.GET_ALL_CASES_SUCCESS,
  payload: {
    cases,
  },
});

const getAllCasesActionFailure = (error) => ({
  type: CasesActionTypes.GET_ALL_CASES_FAILURE,
  payload: {
    error,
  },
  error: true,
});

const getAllActiveCasesByProductionActionRequest = () => {
  return {
    type: CasesActionTypes.GET_ALL_ACTIVE_CASES_REQUEST,
  };
};

const getAllActiveCasesByProductionActionSuccess = (cases) => {
  return {
    type: CasesActionTypes.GET_ALL_ACTIVE_CASES_SUCCESS,
    payload: {
      cases,
      error: false,
    },
  };
};

const getAllActiveCasesByProductionActionFailure = (error) => {
  return {
    type: CasesActionTypes.GET_ALL_ACTIVE_CASES_FAILURE,
    payload: {
      error,
    },
  };
};

export default {
  getAllCasesActionRequest,
  getAllCasesActionSuccess,
  getAllCasesActionFailure,
  getAllActiveCasesByProductionActionFailure,
  getAllActiveCasesByProductionActionRequest,
  getAllActiveCasesByProductionActionSuccess,
};

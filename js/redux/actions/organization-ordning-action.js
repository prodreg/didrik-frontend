import ActionTypes from "./organization-ordning-action-type";

const getAllOrdningActionRequest = () => ({
  type: ActionTypes.GET_ALL_ORDNING_REQUEST,
});

const getAllOrdningActionSuccess = (ordninger) => ({
  type: ActionTypes.GET_ALL_ORDNING_SUCCESS,
  payload: {
    ordninger
  }
});

const getAllOrdningActionFailure = (error) => ({
  type: ActionTypes.GET_ALL_ORDNING_FAILURE,
  payload: {
    error
  },
  error: true
});

const getOrdningActionRequest = () => ({
  type: ActionTypes.GET_ORDNING_REQUEST,
});

const getOrdningActionSuccess = (ordning) => ({
  type: ActionTypes.GET_ORDNING_SUCCESS,
  payload: {
    ordning
  }
});

const getOrdningActionFailure = (error) => ({
  type: ActionTypes.GET_ORDNING_FAILURE,
  payload: {
    error
  },
  error: true
});

const updateOrdningActionRequest = () => ({
  type: ActionTypes.UPDATE_ORDNING_REQUEST,
});

const updateOrdningActionSuccess = () => ({
  type: ActionTypes.UPDATE_ORDNING_SUCCESS,
  payload: {}
});

const updateOrdningActionFailure = (error) => ({
  type: ActionTypes.UPDATE_ORDNING_FAILURE,
  payload: {
    error
  },
  error: true
});

export default {
  getAllOrdningActionRequest,
  getAllOrdningActionSuccess,
  getAllOrdningActionFailure,
  getOrdningActionRequest,
  getOrdningActionSuccess,
  getOrdningActionFailure,
  updateOrdningActionRequest,
  updateOrdningActionSuccess,
  updateOrdningActionFailure
}
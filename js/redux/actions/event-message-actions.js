import ActionTypes from "./event-message-action-types";

const getAllEventMessagesActionRequest = () => ({
  type: ActionTypes.GET_ALL_EVENT_MESSAGES_REQUEST,
});

const getAllEventMessagesActionSuccess = (data) => ({
  type: ActionTypes.GET_ALL_EVENT_MESSAGES_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getAllEventMessagesActionFailure = (error) => ({
  type: ActionTypes.GET_ALL_EVENT_MESSAGES_FAILURE,
  payload: {
    error,
  },
});

const getEventMessageByIdActionRequest = () => ({
  type: ActionTypes.GET_EVENT_MESSAGE_BY_ID_REQUEST,
});

const getEventMessageByIdActionSuccess = (data) => ({
  type: ActionTypes.GET_EVENT_MESSAGE_BY_ID_SUCCESS,
  payload: {
    data,
    error: false,
  },
});

const getEventMessageByIdActionFailure = (error) => ({
  type: ActionTypes.GET_EVENT_MESSAGE_BY_ID_FAILURE,
  payload: {
    error,
  },
});

const updateEventMessageReadActionRequest = () => ({
  type: ActionTypes.UPDATE_EVENT_MESSAGE_READ_REQUEST,
});

const updateEventMessageReadActionSuccess = () => ({
  type: ActionTypes.UPDATE_EVENT_MESSAGE_READ_SUCCESS,
  payload: {
    error: false,
  },
});

const updateEventMessageReadActionFailure = (error) => ({
  type: ActionTypes.UPDATE_EVENT_MESSAGE_READ_FAILURE,
  payload: {
    error: error,
  },
});

const sendEventMessageToArchiveActionRequest = () => ({
  type: ActionTypes.SEND_EVENT_MESSAGE_TO_ARCHIVE_REQUEST,
});

const sendEventMessageToArchiveActionSuccess = () => ({
  type: ActionTypes.SEND_EVENT_MESSAGE_TO_ARCHIVE_SUCCESS,
  payload: {
    error: false,
  },
});

const sendEventMessageToArchiveActionFailure = (error) => ({
  type: ActionTypes.SEND_EVENT_MESSAGE_TO_ARCHIVE_FAILURE,
  payload: {
    error: error,
  },
});

const sendEventMessagesToArchiveActionRequest = () => ({
  type: ActionTypes.SEND_EVENT_MESSAGES_TO_ARCHIVE_REQUEST,
});

const sendEventMessagesToArchiveActionSuccess = () => ({
  type: ActionTypes.SEND_EVENT_MESSAGES_TO_ARCHIVE_SUCCESS,
  payload: {
    error: false,
  },
});

const sendEventMessagesToArchiveActionFailure = (error) => ({
  type: ActionTypes.SEND_EVENT_MESSAGES_TO_ARCHIVE_FAILURE,
  payload: {
    error: error,
  },
});

const setEventMessageToWorkInProgressActionRequest = () => ({
  type: ActionTypes.SET_EVENT_MESSAGE_TO_WORK_IN_PROGRESS_REQUEST,
});

const setEventMessageToWorkInProgressActionSuccess = () => ({
  type: ActionTypes.SET_EVENT_MESSAGE_TO_WORK_IN_PROGRESS_SUCCESS,
  payload: {
    error: false,
  },
});

const setEventMessageToWorkInProgressActionFailure = (error) => ({
  type: ActionTypes.SET_EVENT_MESSAGE_TO_WORK_IN_PROGRESS_FAILURE,
  payload: {
    error: error,
  },
});

const setNumberOfUnreadMessages = (counter) => ({
  type: ActionTypes.SET_NUMBER_OF_UNREAD_MESSAGES,
  payload: {
    counter,
  },
});

export default {
  getAllEventMessagesActionRequest,
  getAllEventMessagesActionSuccess,
  getAllEventMessagesActionFailure,
  getEventMessageByIdActionRequest,
  getEventMessageByIdActionSuccess,
  getEventMessageByIdActionFailure,
  updateEventMessageReadActionRequest,
  updateEventMessageReadActionSuccess,
  updateEventMessageReadActionFailure,
  sendEventMessageToArchiveActionRequest,
  sendEventMessageToArchiveActionSuccess,
  sendEventMessageToArchiveActionFailure,
  sendEventMessagesToArchiveActionRequest,
  sendEventMessagesToArchiveActionSuccess,
  sendEventMessagesToArchiveActionFailure,
  setEventMessageToWorkInProgressActionRequest,
  setEventMessageToWorkInProgressActionSuccess,
  setEventMessageToWorkInProgressActionFailure,
  setNumberOfUnreadMessages,
};

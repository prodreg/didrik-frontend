import ActionTypes from "./version-info-action-types";

const getVersionInfoActionRequest = () => ({
  type: ActionTypes.GET_VERSION_INFO_REQUEST,
});

const getVersionInfoActionSuccess = (versionInfo) => ({
  type: ActionTypes.GET_VERSION_INFO_SUCCESS,
  payload: {
    versionInfo,
  },
});

const getVersionInfoActionFailure = (error) => ({
  type: ActionTypes.GET_VERSION_INFO_FAILURE,
  payload: {
    error,
  },
  error: true,
});

export default {
  getVersionInfoActionRequest,
  getVersionInfoActionSuccess,
  getVersionInfoActionFailure,
};
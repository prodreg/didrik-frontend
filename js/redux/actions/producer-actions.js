import ProducerActionTypes from "./producer-action-types";

const getAllProducersActionRequest = () => {
  return {
    type: ProducerActionTypes.GET_ALL_PRODUCERS_REQUEST,
  };
};

const getAllProducersActionSuccess = (producers, totalResults) => {
  return {
    type: ProducerActionTypes.GET_ALL_PRODUCERS_SUCCESS,
    payload: {
      producers,
      totalResults,
      error: false,
    },
  };
};

const getAllProducersActionFailure = (error) => {
  return {
    type: ProducerActionTypes.GET_ALL_PRODUCERS_FAILURE,
    payload: {
      error,
    },
  };
};

const getActionRequest = (actionType) => {
  return {
    type: actionType,
  };
};

const getActionSuccess = (actionType, data) => {
  return {
    type: actionType,
    payload: {
      data,
      error: false,
    },
  };
};

const getActionFailure = (actionType, error) => {
  return {
    type: actionType,
    payload: {
      error,
    },
  };
};

export default {
  getAllProducersActionRequest,
  getAllProducersActionSuccess,
  getAllProducersActionFailure,
  getActionRequest,
  getActionSuccess,
  getActionFailure,
};

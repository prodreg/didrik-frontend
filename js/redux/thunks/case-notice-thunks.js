import Actions from "../actions/case-notice-actions";

export const getAllMyNotices = (orgnr) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getAllMyCaseNoticesRequest());
    return apis.caseNoticeApi
      .getAllMyNotices(orgnr)
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          dispatch(Actions.getAllMyCaseNoticesFailure(res.error));
        }
        res.forEach((element) => {
          element.noticeContent = JSON.parse(element.varselContent);
        });
        dispatch(Actions.getAllMyCaseNoticesSuccess(res));
        return res;
      })
      .catch((error) => {
        dispatch(Actions.getAllMyCaseNoticesFailure(error));
      });
  };
};

export const getNoticesForCase = (caseId) => {
  return (dispatch, getState, apis) => {
    return apis.caseNoticeApi
      .getNoticesForCase(caseId)
      .then((response) => response.json());
  };
};

export const getNoticeById = (noticeId) => {
  return (dispatch, getState, apis) => {
    return apis.caseNoticeApi
      .getNoticeById(noticeId)
      .then((response) => response.json());
  };
};

export const getNoticeIdForOrganization = (caseId, orgId) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getNoticeIdForOrganizationActionRequest());
    return apis.caseNoticeApi
      .getNoticeIdForOrganization(caseId, orgId)
      .then((response) => response.json())
      .then((response) =>
        dispatch(Actions.getNoticeIdForOrganizationActionSuccess(response))
      )
      .catch((error) =>
        dispatch(Actions.getNoticeIdForOrganizationActionFailure(error))
      );
  };
};

import ProducerActions from "../actions/producer-actions";
import ProducerActionTypes from "../actions/producer-action-types";

export const getAllProducers = (page, query) => {
  return (dispatch, getState, apis) => {
    dispatch(ProducerActions.getAllProducersActionRequest());

    return apis.producerApi
      .getAllProducers(page, query)
      .then((response) => response.json())
      .then((response) => {
        const producers = response.resultat;
        const totalResults = response.totalResults;
        return dispatch(
          ProducerActions.getAllProducersActionSuccess(producers, totalResults)
        );
      })
      .catch((error) => {
        return dispatch(ProducerActions.getAllProducersActionFailure(error));
      });
  };
};

export const getProducer = (prodnr) => {
  return (dispatch, getState, apis) => {
    dispatch(
      ProducerActions.getActionRequest(ProducerActionTypes.GET_PRODUCER_REQUEST)
    );
    return apis.producerApi
      .getProducer(prodnr)
      .then((response) => response.json())
      .then((response) =>
        dispatch(
          ProducerActions.getActionSuccess(
            ProducerActionTypes.GET_PRODUCER_SUCCESS,
            response
          )
        )
      )
      .catch((error) =>
        dispatch(
          ProducerActions.getActionFailure(
            ProducerActionTypes.GET_PRODUCER_FAILURE,
            error
          )
        )
      );
  };
};

export const getCreditHistory = (prodnr) => {
  return (dispatch, getState, apis) => {
    dispatch(
      ProducerActions.getActionRequest(
        ProducerActionTypes.GET_CREDIT_HISTORY_REQUEST
      )
    );

    return apis.producerApi
      .getCreditHistory(prodnr)
      .then((response) => response.json())
      .then((response) =>
        dispatch(
          ProducerActions.getActionSuccess(
            ProducerActionTypes.GET_CREDIT_HISTORY_SUCCESS,
            response
          )
        )
      )
      .catch((error) =>
        dispatch(
          ProducerActions.getActionFailure(
            ProducerActionTypes.GET_ALL_PRODUCERS_FAILURE,
            error
          )
        )
      );
  };
};

export const getProductionHistory = (prodnr, queryYear) => {
  return (dispatch, getState, apis) => {
    dispatch(
      ProducerActions.getActionRequest(
        ProducerActionTypes.GET_PRODUCTION_HISTORY_REQUEST
      )
    );
    return apis.producerApi
      .getProductionHistory(prodnr, queryYear)
      .then((response) => response.json())
      .then((response) =>
        dispatch(
          ProducerActions.getActionSuccess(
            ProducerActionTypes.GET_PRODUCTION_HISTORY_SUCCESS,
            response
          )
        )
      )
      .catch((error) =>
        dispatch(
          ProducerActions.getActionFailure(
            ProducerActionTypes.GET_PRODUCTION_HISTORY_FAILURE,
            error
          )
        )
      );
  };
};

export const getFarmProductionHistory = (prodnr, queryYear) => {
  return (dispatch, getState, apis) => {
    dispatch(
      ProducerActions.getActionRequest(
        ProducerActionTypes.GET_FARM_PRODUCTION_HISTORY_REQUEST
      )
    );
    return apis.producerApi
      .getFarmProductionHistory(prodnr, queryYear)
      .then((response) => response.json())
      .then(response => new Promise((resolve) => {
        Promise.all(response.map((production) =>
          apis.producerApi
            .getProducer(production.prodNummer)
            .then((response) => response.json())
            .then((response) => {
              production.produsent = response;
            })
            .catch((e) => {
              production.produsent = null
            })))
          .then(() => resolve(response));
        }
      ))
      .then((response) => {
        return dispatch(
          ProducerActions.getActionSuccess(
            ProducerActionTypes.GET_FARM_PRODUCTION_HISTORY_SUCCESS,
            response
          )
        )
      })
      .catch((error) =>
        dispatch(
          ProducerActions.getActionFailure(
            ProducerActionTypes.GET_FARM_PRODUCTION_HISTORY_FAILURE,
            error
          )
        )
      );
  };
};

export const getAllProducerNumberConnections = () => {
  return (dispatch, getState, apis) => {
    dispatch(
      ProducerActions.getActionRequest(
        ProducerActionTypes.GET_ALL_PRODUCER_NUMBER_CONNECTIONS_REQUEST
      )
    );
    return apis.producerApi
      .getAllProducerNumberConnections()
      .then((response) => response.json())
      .then((response) =>
        dispatch(
          ProducerActions.getActionSuccess(
            ProducerActionTypes.GET_ALL_PRODUCER_NUMBER_CONNECTIONS_SUCCESS,
            response
          )
        )
      )
      .catch((error) =>
        dispatch(
          ProducerActions.getActionFailure(
            ProducerActionTypes.GET_ALL_PRODUCER_NUMBER_CONNECTIONS_FAILURE,
            error
          )
        )
      );
  };
};

export const createProducerNumberConnection = (oldNumber, newNumber) => {
  return (dispatch, getState, apis) => {
    dispatch(
      ProducerActions.getActionRequest(
        ProducerActionTypes.CREATE_PRODUCER_NUMBER_CONNECTION_REQUEST
      )
    );
    return apis.producerApi
      .createProducerNumberConnection(oldNumber, newNumber)
      .then((response) => {
        if (!response.ok) {
          throw new Error(response.statusText);
        } else {
          return response;
        }
      })
      .then((response) => response.json())
      .then((response) => {
        dispatch(
          ProducerActions.getActionSuccess(
            ProducerActionTypes.CREATE_PRODUCER_NUMBER_CONNECTION_SUCCESS,
            response
          )
        )
      })
      .catch((error) =>
        dispatch(
          ProducerActions.getActionFailure(
            ProducerActionTypes.CREATE_PRODUCER_NUMBER_CONNECTION_FAILURE,
            error
          )
        )
      );
  };
};

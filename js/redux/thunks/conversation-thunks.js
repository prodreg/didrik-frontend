import ConversationActions from "../actions/conversation-actions";

export const getConversationById = (conversationId) => {
  return (dispatch, getState, apis) => {
    dispatch(ConversationActions.getConversationByIdActionRequest());
    return apis.conversationApi
      .getConversationById(conversationId)
      .then((response) => response.json())
      .then((response) =>
        dispatch(ConversationActions.getConversationByIdActionSuccess(response))
      )
      .catch((error) =>
        dispatch(ConversationActions.getConversationByIdActionFailure(error))
      );
  };
};

export const getConversationForCaseAndOrganization = (caseId, orgId) => {
  return (dispatch, getState, apis) => {
    dispatch(
      ConversationActions.getConversationForCaseAndOrganizationActionRequest()
    );
    return apis.conversationApi
      .getConversationForCaseAndOrganization(caseId, orgId)
      .then((response) => response.json())
      .then((response) =>
        dispatch(
          ConversationActions.getConversationForCaseAndOrganizationActionSuccess(
            response
          )
        )
      )
      .catch((error) =>
        dispatch(
          ConversationActions.getConversationForCaseAndOrganizationActionFailure(
            error
          )
        )
      );
  };
};

export const postConversationMessage = (conversationId, message) => {
  return (dispatch, getState, apis) => {
    dispatch(ConversationActions.postConversationMessageActionRequest());
    return apis.conversationApi
      .postConversationMessage(conversationId, message)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return response.json();
      })
      .then((response) =>
        dispatch(
          ConversationActions.postConversationMessageActionSuccess(response)
        )
      )
      .catch((error) =>
        dispatch(
          ConversationActions.postConversationMessageActionFailure(error)
        )
      );
  };
};

export const postConversationMessageAs = (conversationId, postAs, message) => {
  return (dispatch, getState, apis) => {
    dispatch(ConversationActions.postConversationMessageAsActionRequest());
    return apis.conversationApi
      .postConversationMessageAs(conversationId, postAs, message)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return response.json();
      })
      .then((response) =>
        dispatch(
          ConversationActions.postConversationMessageAsActionSuccess(response)
        )
      )
      .catch((error) =>
        dispatch(
          ConversationActions.postConversationMessageAsActionFailure(error)
        )
      );
  };
};

export const getConversationMessages = (conversationId, time) => {
  return (dispatch, getState, apis) => {
    dispatch(ConversationActions.getConversationMessagesActionRequest());
    return apis.conversationApi
      .getConversationMessages(conversationId, time)
      .then((response) => response.json())
      .then((response) =>
        dispatch(
          ConversationActions.getConversationMessagesActionSuccess(response)
        )
      )
      .catch((error) =>
        dispatch(
          ConversationActions.getConversationMessagesActionFailure(error)
        )
      );
  };
};

export const getConversationAttachments = (conversationId, messageId) => {
  return (dispatch, getState, apis) => {
    dispatch(ConversationActions.getConversationAttachmentsActionRequest());
    return apis.conversationApi
      .getConversationAttachments(conversationId, messageId)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return response.json();
      })
      .then((response) =>
        dispatch(
          ConversationActions.getConversationAttachmentsActionSuccess(response)
        )
      )
      .catch((error) =>
        dispatch(
          ConversationActions.getConversationAttachmentsActionFailure(error)
        )
      );
  };
};

import Actions from "../actions/user-info-actions";

export const getUserInfo = (username) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getUserInfoActionRequest());
    return apis.userInfoApi
    .getUserInfo(username)
    .then((res) => res.json())
    .then((res) => {
      if (res.error) {
        dispatch(Actions.getUserInfoActionFailure(res.error));
      }
      dispatch(Actions.getUserInfoActionSuccess(res));
      return res;
    })
    .catch((error) => {
      dispatch(Actions.getUserInfoActionFailure(error));
    });
  };
};
import AdminActions from "../actions/admin-actions";

export const postEventMessage = (eventMessage) => {
  return (dispatch, getState, apis) => {
    dispatch(AdminActions.sendMessageToUsersActionRequest());
    return apis.adminApi
      .postEventMessage(eventMessage)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        } else {
          return dispatch(AdminActions.sendMessageToUsersActionSuccess());
        }
      })
      .catch((error) =>
        dispatch(AdminActions.sendMessageToUsersActionFailure(error))
      );
  };
};

export const getBankFile = (orgnr, year, type) => {
  return (dispatch, getState, apis) => {
    dispatch(AdminActions.getBankFileActionRequest());
    return apis.adminApi
      .getBankFile(orgnr, year, type)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        } else {
          return response.text();
        }
      })
      .then((response) =>
        dispatch(AdminActions.getBankFileActionSuccess(response))
      )
      .catch((error) => dispatch(AdminActions.getBankFileActionFailure(error)));
  };
};

export const getReportedFiles = (year) => {
  return (dispatch, getState, apis) => {
    dispatch(AdminActions.getReportedFilesActionRequest());
    return apis.adminApi
      .getReportedFiles(year)
      .then((response) => response.json())
      .then((response) =>
        dispatch(AdminActions.getReportedFilesActionSuccess(response))
      )
      .catch((error) =>
        dispatch(AdminActions.getReportedFilesActionFailure(error))
      );
  };
};

export const getReportedFile = (id, type) => {
  return (dispatch, getState, apis) => {
    dispatch(AdminActions.getReportedFileActionRequest());
    return apis.adminApi
      .getReportedFile(id, type)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        } else {
          return response.text();
        }
      })
      .then((response) =>
        dispatch(AdminActions.getReportedFileActionSuccess(response))
      )
      .catch((error) =>
        dispatch(AdminActions.getReportedFileActionFailure(error))
      );
  };
};


export const updateProdusentIndex = () => {
  return (dispatch, getState, apis) => {
    return apis.adminApi.updateProdusentIndex();
  };
};

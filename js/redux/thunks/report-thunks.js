import ResponsibilityActions from "../actions/report-actions";

export const getResponsibilityReport = (year, filter, grouping, basis) => {
  return (dispatch, getState, apis) => {
    dispatch(ResponsibilityActions.getResponsibilityReportActionRequest());
    return apis.reportApi
      .getResponsibilityReport(year, filter, grouping, basis)
      .then((response) => response.json())
      .then((response) => {
        if (response.message) {
          throw new Error(true);
        }
        return dispatch(ResponsibilityActions.getResponsibilityReportActionSuccess(response));
      })
      .catch((error) =>
        dispatch(ResponsibilityActions.getResponsibilityReportActionFailure(error))
      );
  };
};

export const getWordResponsibilityReport = (year, type, orgnr) => {
  return (dispatch, getState, apis) => {
    dispatch(ResponsibilityActions.getWordFileResponsibilityActionRequest());
    return apis.reportApi
      .getWordResponsibilityReport(year, type, orgnr)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        } else {
          return response.blob();
        }
      })
      .then((response) =>
        dispatch(ResponsibilityActions.getWordFileResponsibilityActionSuccess(response))
      )
      .catch((error) =>
        dispatch(ResponsibilityActions.getWordFileResponsibilityActionFailure(error))
      );
  };
};

export const getExcelResponsibilityReport = (year, filter, grouping, type, id) => {
  return (dispatch, getState, apis) => {
    dispatch(ResponsibilityActions.getExcelFileResponsibilityActionRequest());
    return apis.reportApi
      .getExcelResponsibilityReport(year, filter, grouping, type, id)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        } else {
          return response.blob();
        }
      })
      .then((response) =>
        dispatch(ResponsibilityActions.getExcelFileResponsibilityActionSuccess(response))
      )
      .catch((error) =>
        dispatch(ResponsibilityActions.getExcelFileResponsibilityActionFailure(error))
      );
  };
};

export const getVaremottakerReport = (orgnr) => {
  return (dispatch, getState, apis) => {
    dispatch(ResponsibilityActions.getVaremottakerReportActionRequest());
    return apis.reportApi
      .getVaremottakerReport(orgnr)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        } else {
          return response.blob();
        }
      })
      .then((response) => {
        return dispatch(ResponsibilityActions.getVaremottakerReportActionSuccess(response));
      })
      .catch((error) =>
        dispatch(ResponsibilityActions.getVaremottakerReportActionFailure(error))
      );
  };
};

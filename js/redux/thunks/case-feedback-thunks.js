import Actions from "../actions/case-actions";

export const getFeedbackById = (feedbackId) => {
  return (dispatch, getState, apis) => {
    return apis.caseFeedbackApi
    .getFeedbackById(feedbackId)
    .then((response) => response.json())
  };
};

export const sendFeedback = (feedback) => {
  return (dispatch, getState, apis) => {
    return apis.caseFeedbackApi
    .sendFeedback(feedback)
  };
};


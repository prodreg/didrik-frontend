import Actions from "../actions/user-actions";
import userService from "../../service/user-service";

export const validateSession = () => {
  return (dispatch, getState, apis) => {
    return apis.userApi
    .validate()
    .then((response) => {
      if (response.status === 401) {
        dispatch(logout());
        return {
          valid: false
        }
      }
      else {
        return response.json().then(res => {
          if(!res.valid) {
            dispatch(logout());
          }
          return res;
        });
      }
    })
  };
}

export const login = (username, password) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.loginRequest());
    return apis.userApi
    .login(username, password)
    .then((res) => res.json())
    .then((res) => {
      if (!res.hasOwnProperty("token")) {
        throw new Error(res.statusText);
      }
      const jwtPart = res.token.split(".")[1];
      const parsedUser = JSON.parse(atob(jwtPart));

      const user = {
        username: parsedUser.username,
        orgNumber: parsedUser.userInfo.organization.orgNumber,
        orgType: parsedUser.userInfo.organization.organizationType,
        roles: parsedUser.roles,
        isAdmin: parsedUser.roles.includes('admin'),
        isBank: parsedUser.roles.includes('bank'),
        isVaremottaker: parsedUser.roles.includes('mottaker'),
        isGarantiutvalget: parsedUser.roles.includes('garantiutvalget'),
        token: res.token,
      };

      userService.loginUser(user);
      return dispatch(Actions.loginSuccess(user));
    })
    .catch((error) => {
      return dispatch(Actions.loginFailure(error));
    });
  };
};

export const logout = () => {
  return (dispatch, getState, apis) => {
    userService.logoutUser();
    dispatch(Actions.logoutRequest());
  };
};

export const updateUserEmail = (username, email) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.updateUserEmailActionRequest());
    return apis.userApi.updateUserEmail(username, email)
    .then(res => {
      if (!res.ok) {
        throw new Error(res.statusText);
      }
      return dispatch(Actions.updateUserEmailActionSuccess());
    })
    .catch(error => {
      return dispatch(Actions.updateUserEmailActionFailure(error));
    });
  }
}

export const changeUserPassword = (username, currentPassword, newPassword) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.changeUserPasswordActionRequest());
    return apis.userApi.changeUserPassword(username, currentPassword,
        newPassword)
    .then(res => {
      if (!res.ok) {
        throw new Error(res.statusText);
      }
      return dispatch(Actions.changeUserPasswordActionSuccess());
    })
    .catch(error => {
      return dispatch(Actions.changeUserPasswordActionFailure(error));
    });
  }
}

export const subscribeUserToEmail = (username, isSubscriber) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.subscribeUserToEmailActionRequest());
    return apis.userApi.subscribeUserToEmail(username, isSubscriber)
    .then(res => {
      if (!res.ok) {
        throw new Error(res.statusText);
      }
      return dispatch(Actions.subscribeUserToEmailActionSuccess());
    })
    .catch(error => {
      return dispatch(Actions.subscribeUserToEmailActionFailure(error));
    });
  }
}

export const updateUserStatus = (username, status) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.updateUserStatusActionRequest());
    return apis.userApi.updateUserStatus(username, status)
    .then(res => {
      if (!res.ok) {
        throw new Error(res.statusText);
      }
      return dispatch(Actions.updateUserStatusActionSuccess());
    })
    .catch(error => {
      return dispatch(Actions.updateUserStatusActionFailure(error));
    });
  }
}

export const requestResetUserPassword = (username) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.requestResetUserPasswordActionRequest());
    return apis.userApi.requestResetUserPassword(username)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return dispatch(Actions.requestResetUserPasswordActionSuccess());
      })
      .catch(error => {
        return dispatch(Actions.requestResetUserPasswordActionFailure(error));
      });
  }
}

export const resetUserPassword = (token, newPassword) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.resetUserPasswordActionRequest());
    return apis.userApi.resetUserPassword(token, newPassword)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return dispatch(Actions.resetUserPasswordActionSuccess());
      })
      .catch(error => {
        return dispatch(Actions.resetUserPasswordActionFailure(error));
      });
  }
}

export const getResetUserPasswordInfo = (token) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getResetUserPasswordInfoActionRequest());
    return apis.userApi.getResetUserPasswordInfo(token)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return dispatch(Actions.getResetUserPasswordInfoActionSuccess());
      })
      .catch(error => {
        return dispatch(Actions.getResetUserPasswordInfoActionFailure(error));
      });
  }
}

import Actions from "../actions/version-info-actions";

export const getVersionInfo = () => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getVersionInfoActionRequest());
    return apis.versionInfoApi
    .getVersionInfo()
    .then((res) => res.json())
    .then((res) => {
      if (res.error) {
        dispatch(Actions.getVersionInfoActionFailure(res.error));
      }
      dispatch(Actions.getVersionInfoActionSuccess(res));
      return res;
    })
    .catch((error) => {
      dispatch(Actions.getVersionInfoActionFailure(error));
    });
  };
};
import ImportActions from "../actions/import-actions";

export const getAllImports = () => {
  return (dispatch, getState, apis) => {
    dispatch(ImportActions.getImportsActionRequest());
    return apis.importApi
      .getAllImports()
      .then((response) => response.json())
      .then((response) =>
        dispatch(ImportActions.getImportsActionSuccess(response))
      )
      .catch((error) => dispatch(ImportActions.getImportsActionFailure(error)));
  };
};

export const getImportForBank = (importId) => {
  return (dispatch, getState, apis) => {
    dispatch(ImportActions.getImportForBankActionRequest());
    return apis.importApi
      .getImportForBank(importId)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(response.status);
        }
        return response.json();
      })
      .then((response) =>
        dispatch(ImportActions.getImportForBankActionSuccess(response))
      )
      .catch((error) =>
        dispatch(ImportActions.getImportForBankActionFailure(error.message))
      );
  };
};

export const getImportForOrganization = (importId) => {
  return (dispatch, getState, apis) => {
    dispatch(ImportActions.getImportForOrganizationActionRequest());
    return apis.importApi
      .getImportForOrganization(importId)
      .then((response) => response.json())
      .then((response) =>
        dispatch(ImportActions.getImportForOrganizationActionSuccess(response))
      )
      .catch((error) =>
        dispatch(ImportActions.getImportForOrganizationActionFailure(error))
      );
  };
};

export const createImportForBank = (data, orgnr, filename, year) => {
  return (dispatch, getState, apis) => {
    dispatch(ImportActions.createImportForBankActionRequest());
    return apis.importApi
      .createImportForBank(data, orgnr, filename, year)
      .then((response) => {
        if (!response.ok) {
          throw new Error(response.status);
        }
        return response.json();
      })
      .then((response) =>
        dispatch(ImportActions.createImportForBankActionSuccess(response))
      )
      .catch((error) =>
        dispatch(ImportActions.createImportForBankActionFailure(error))
      );
  };
};

export const createImportForOrganization = (data, orgnr, filename, year) => {
  return (dispatch, getState, apis) => {
    dispatch(ImportActions.createImportForOrganizationActionRequest());
    return apis.importApi
      .createImportForOrganization(data, orgnr, filename, year)
      .then((response) => {
        if (!response.ok) {
          throw new Error(response.status);
        }
        return response.json();
      })
      .then((response) =>
        dispatch(
          ImportActions.createImportForOrganizationActionSuccess(response)
        )
      )
      .catch((error) =>
        dispatch(ImportActions.createImportForOrganizationActionFailure(error))
      );
  };
};

export const deleteBankImport = (importId) => {
  return (dispatch, getState, apis) => {
    dispatch(ImportActions.deleteBankImportActionRequest());
    return apis.importApi
      .deleteBankImport(importId)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return dispatch(ImportActions.deleteBankImportActionSuccess());
      })
      .catch((error) =>
        dispatch(ImportActions.deleteBankImportActionFailure(error))
      );
  };
};

export const deleteOrganizationImport = (importId) => {
  return (dispatch, getState, apis) => {
    dispatch(ImportActions.deleteOrganizationImportActionRequest());
    return apis.importApi
      .deleteOrganizationImport(importId)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return dispatch(ImportActions.deleteOrganizationImportActionSuccess());
      })
      .catch((error) =>
        dispatch(ImportActions.deleteOrganizationImportActionFailure(error))
      );
  };
};

export const deleteBankErrorRowsByType = (importId, errorType) => {
  return (dispatch, getState, apis) => {
    dispatch(ImportActions.deleteBankErrorRowsActionRequest());
    return apis.importApi
      .deleteBankErrorRowsByType(importId, errorType)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return dispatch(ImportActions.deleteBankErrorRowsActionSuccess());
      })
      .catch((error) =>
        dispatch(ImportActions.deleteBankErrorRowsActionFailure(error))
      );
  };
};

export const deleteOrganizationErrorRowsByType = (importId, errorType) => {
  return (dispatch, getState, apis) => {
    dispatch(ImportActions.deleteOrganizationErrorRowsActionRequest());
    return apis.importApi
      .deleteOrganizationErrorRowsByType(importId, errorType)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return dispatch(
          ImportActions.deleteOrganizationErrorRowsActionSuccess()
        );
      })
      .catch((error) =>
        dispatch(ImportActions.deleteOrganizationErrorRowsActionFailure(error))
      );
  };
};

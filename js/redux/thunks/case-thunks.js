import Actions from "../actions/case-actions";

export const getAllCases = () => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getAllCasesActionRequest());
    return apis.caseApi
    .getAllCases()
    .then((res) => res.json())
    .then((res) => {
      if (res.error) {
        dispatch(Actions.getAllCasesActionFailure(res.error));
      }
      dispatch(Actions.getAllCasesActionSuccess(res.resultat));
      return res;
    })
    .catch((error) => {
      dispatch(Actions.getAllCasesActionFailure(error));
    });
  };
};

export const getAllActiveCasesByProducer = (producerNumber) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getAllActiveCasesByProductionActionRequest());

    return apis.caseApi
    .getAllActiveCasesByProducer(producerNumber)
    .then((response) => response.json())
    .then((response) => {
      const cases = response.resultat;
      return dispatch(
          Actions.getAllActiveCasesByProductionActionSuccess(cases)
      );
    })
    .catch((error) => {
      const errorMessage = error.message;
      return dispatch(
          Actions.getAllActiveCasesByProductionActionFailure(errorMessage)
      );
    });
  };
};

export const getCaseById = (caseId) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.getCaseById(caseId).then((response) => response.json());
  };
};

export const getCaseDraftById = (caseId) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi
    .getCaseDraftById(caseId)
    .then((response) => response.json());
  };
};

export const getOrCreateNewPreDraftAndRedirect = (
    bankOrg,
    producerNumber,
    producer,
    routerHistory
) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi
    .createNewPreDraftForBankAndProducer(bankOrg, producer)
    .then((response) =>
        response
        .json()
        .then((data) => ({ status: response.status, body: data }))
    )
    .then((res) => {
      if (res.status === 201) {
        routerHistory.push({
          pathname: "/create-case/" + res.body.id,
        });
      } else {
        throw new Error("");
      }
    });
  };
};

export const cancelCase = (caseId) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.cancelCase(caseId);
  };
};

export const endCase = (caseId) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.endCase(caseId);
  };
};

export const addGarantiutvalgetToCase = (caseId) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.addGarantiutvalget(caseId);
  };
};

export const getGarantiutvalgetDataForCase = (caseId) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.getGarantiutvalgetDataForCase(caseId)
    .then((response) => response.json());
  };
};

export const approveCaseGarantiutvalget = (caseId, justification) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.updateCaseGarantiutvalget(caseId, justification, "ACCEPTED");
  };
};

export const signOffCaseGarantiutvalget = (caseId, justification) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.updateCaseGarantiutvalget(caseId, justification, "SIGNED_OFF");
  };
};

export const rejectCaseGarantiutvalget = (caseId, justification) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.updateCaseGarantiutvalget(caseId, justification, "REJECTED");
  };
};

export const completeCase = (caseId) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.completeCase(caseId);
  };
};

export const createCase = (caseObject) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.createCase(caseObject);
  };
};

export const editCase = (caseObject) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi
    .createNewPreDraftForBankAndProducer(caseObject.bankId, caseObject.produsent)
    .then((response) =>
        response
        .json()
        .then((data) => ({ status: response.status, body: data }))
    )
    .then((res) => {
      if (res.status === 201) {
        caseObject.id = res.body.id;
        return apis.caseApi.createCase(caseObject)
      } else {
        throw new Error("");
      }
    });
  };
};

export const getCaseNote = (caseId) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.getCaseNote(caseId);
  };
};

export const updateCaseNote = (caseId, note) => {
  return (dispatch, getState, apis) => {
    return apis.caseApi.updateCaseNote(caseId, note);
  };
};

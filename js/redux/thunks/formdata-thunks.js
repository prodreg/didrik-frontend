import Actions from "../actions/formdata-actions";

export const getFormdataNoticeTypes = () => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getNoticeTypesActionRequest());
    return apis.formdataApi
    .getNoticeTypes()
    .then((res) => res.json())
    .then((res) => {
      if (res.error) {
        dispatch(Actions.getNoticeTypesActionFailure(res.error));
      }
      dispatch(Actions.getNoticeTypesActionSuccess(res));
      return res;
    })
    .catch((error) => {
      dispatch(Actions.getNoticeTypesActionFailure(error));
    });
  };
};

export const getFormdataProductTypes = () => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getProductTypesRequest());
    return apis.formdataApi
    .getProductTypes()
    .then((res) => res.json())
    .then((res) => {
      if (res.error) {
        dispatch(Actions.getProductTypesFailure(res.error));
      }
      dispatch(Actions.getProductTypesSuccess(res));
      return res;
    })
    .catch((error) => {
      dispatch(Actions.getProductTypesFailure(error));
    });
  };
};

export const getFormdataSchemeTypes = () => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getSchemeTypesRequest());
    return apis.formdataApi
    .getSchemeTypes()
    .then((res) => res.json())
    .then((res) => {
      if (res.error) {
        dispatch(Actions.getSchemeTypesFailure(res.error));
      }
      dispatch(Actions.getSchemeTypesSuccess(res));
      return res;
    })
    .catch((error) => {
      dispatch(Actions.getSchemeTypesFailure(error));
    });
  };
};

export const getOrganizationTypes = () => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getOrganizationTypesRequest());
    return apis.formdataApi
    .getOrganizationTypes()
    .then((res) => res.json())
    .then((res) => {
      if (res.error) {
        dispatch(Actions.getOrganizationTypesFailure(res.error));
      }
      dispatch(Actions.getOrganizationTypesSuccess(res));
      return res;
    })
    .catch((error) => {
      dispatch(Actions.getOrganizationTypesFailure(error));
    });
  };
};
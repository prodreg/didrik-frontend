import Actions from "../actions/event-message-actions";

export const getAllEventMessages = () => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getAllEventMessagesActionRequest());
    return apis.eventMessageApi
      .getAllEventMessages()
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return response.json();
      })
      .then((response) =>
        dispatch(Actions.getAllEventMessagesActionSuccess(response))
      )
      .catch((error) =>
        dispatch(Actions.getAllEventMessagesActionFailure(error))
      );
  };
};

export const getEventMessageById = (messageId) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getEventMessageByIdActionRequest());
    return apis.eventMessageApi
      .getEventMessageById(messageId)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return response.json();
      })
      .then((response) =>
        dispatch(Actions.getEventMessageByIdActionSuccess(response))
      )
      .catch((error) =>
        dispatch(Actions.getEventMessageByIdActionFailure(error))
      );
  };
};

export const setEventMessageIsRead = (messageId, isRead) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.updateEventMessageReadActionRequest());
    return apis.eventMessageApi
      .setEventMessageIsRead(messageId, isRead)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return dispatch(Actions.updateEventMessageReadActionSuccess());
      })
      .catch((error) =>
        dispatch(Actions.updateEventMessageReadActionFailure(error))
      );
  };
};

export const softDeleteEventMessage = (messageId, del) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.sendEventMessageToArchiveActionRequest());
    return apis.eventMessageApi
      .softDeleteEventMessage(messageId, del)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return dispatch(Actions.sendEventMessageToArchiveActionSuccess());
      })
      .catch((error) =>
        dispatch(Actions.sendEventMessageToArchiveActionFailure(error))
      );
  };
};

export const softDeleteEventMessages = (messageIds) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.sendEventMessagesToArchiveActionRequest());
    return apis.eventMessageApi
      .softDeleteEventMessages(messageIds)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return dispatch(Actions.sendEventMessagesToArchiveActionSuccess());
      })
      .catch((error) =>
        dispatch(Actions.sendEventMessagesToArchiveActionFailure(error))
      );
  };
};

export const setMessageToWorkInProgress = (messageId, workInProgess) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.setEventMessageToWorkInProgressActionRequest());
    return apis.eventMessageApi
      .setMessageToWorkInProgress(messageId, workInProgess)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        }
        return dispatch(Actions.setEventMessageToWorkInProgressActionSuccess());
      })
      .catch((error) =>
        dispatch(Actions.setEventMessageToWorkInProgressActionFailure(error))
      );
  };
};

import Actions from "../actions/organization-ordning-action";

export const getAllOrdning = () => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getAllOrdningActionRequest());
    return apis.organizationOrdningApi
      .getAllOrdning()
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          throw new Error(res.statusText);
        }
        dispatch(Actions.getAllOrdningActionSuccess(res));
        return res;
      })
      .catch((error) => {
        dispatch(Actions.getAllOrdningActionFailure(error));
      });
  };
};

export const getOrganizationOrdning = (orgNumber) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getOrdningActionRequest());
    return apis.organizationOrdningApi
      .getOrdning(orgNumber)
      .then((res) => {
        if (res.status === 404) {
          dispatch(Actions.getOrdningActionSuccess(res));
          return res;
        }

        res = res.json();

        if (res.error) {
          throw new Error(res.statusText);
        }
        dispatch(Actions.getOrdningActionSuccess(res));
        return res;
      })
      .catch((error) => {
        dispatch(Actions.getOrdningActionFailure(error));
      });
  };
};

export const updateOrdning = (orgNumber, ordningName) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.updateOrdningActionRequest());
    return apis.organizationOrdningApi
      .updateOrdning(orgNumber, ordningName)
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return dispatch(Actions.updateOrdningActionSuccess());
      })
      .catch((error) => {
        return dispatch(Actions.updateOrdningActionFailure(error));
      });
  };
};

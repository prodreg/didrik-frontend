import NewsActions from "../actions/news-actions";

export const getAllNewsPosts = () => {
  return (dispatch, getState, apis) => {
    dispatch(NewsActions.getAllNewsPostsActionRequest());
    return apis.newsApi
      .getAllNewsPosts()
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          return dispatch(NewsActions.getAllNewsPostsActionFailure(res.error));
        }
        return dispatch(NewsActions.getAllNewsPostsActionSuccess(res));
      })
      .catch((error) =>
        dispatch(NewsActions.getAllNewsPostsActionFailure(error))
      );
  };
};

export const postNewsPost = (newsPost) => {
  return (dispatch, getState, apis) => {
    dispatch(NewsActions.postNewsPostActionRequest());
    return apis.newsApi
      .postNewsPost(newsPost)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        } else {
          return dispatch(NewsActions.postNewsPostActionSuccess());
        }
      })
      .catch((error) => dispatch(NewsActions.postNewsPostActionFailure(error)));
  };
};

export const editNewsPost = (newsPost) => {
  return (dispatch, getState, apis) => {
    dispatch(NewsActions.editNewsPostActionRequest());
    return apis.newsApi
      .editNewsPost(newsPost)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        } else {
          return dispatch(NewsActions.editNewsPostActionSuccess(newsPost));
        }
      })
      .catch((error) => dispatch(NewsActions.editNewsPostActionFailure(error)));
  };
};

export const deleteNewsPost = (newsPostId) => {
  return (dispatch, getState, apis) => {
    dispatch(NewsActions.deleteNewsPostActionRequest());
    return apis.newsApi
      .deleteNewsPost(newsPostId)
      .then((response) => {
        let isFailed = response.ok;
        if (!isFailed) {
          throw new Error(!isFailed);
        } else {
          return dispatch(NewsActions.deleteNewsPostActionSuccess());
        }
      })
      .catch((error) =>
        dispatch(NewsActions.deleteNewsPostActionFailure(error))
      );
  };
};

import Actions from "../actions/organization-action";

export const getAllOrganizations = (types) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getAllOrganizationsActionRequest());
    return apis.organizationApi
      .getAllOrganizations(types)
      .then((response) => response.json())
      .then((response) =>
        dispatch(Actions.getAllOrganizationsActionSuccess(response))
      )
      .catch((error) =>
        dispatch(Actions.getAllOrganizationsActionFailure(error))
      );
  };
};

export const queryOrganizations = (query, types) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.queryOrganizationsActionRequest());
    return apis.organizationApi
      .queryOrganizations(query, types)
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          throw new Error(res.statusText);
        }
        dispatch(Actions.queryOrganizationsActionSuccess(res));
        return res;
      })
      .catch((error) => {
        dispatch(Actions.queryOrganizationsActionFailure(error));
      });
  };
};

export const getOrganizationDetails = (orgNumber) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getOrganizationDetailsActionRequest());
    return apis.organizationApi
      .getOrganizationDetails(orgNumber)
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          throw new Error(res.statusText);
        }
        dispatch(Actions.getOrganizationDetailsActionSuccess(res));
        return res;
      })
      .catch((error) => {
        dispatch(Actions.getOrganizationDetailsActionFailure(error));
      });
  };
};

export const updateOrganizationName = (orgNumber, name) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.updateOrganizationNameActionRequest());
    return apis.organizationApi
      .updateOrganizationName(orgNumber, name)
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return dispatch(Actions.updateOrganizationNameActionSuccess());
      })
      .catch((error) => {
        return dispatch(Actions.updateOrganizationNameActionFailure(error));
      });
  };
};

export const updateOrganizationEmail = (orgNumber, email) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.updateOrganizationEmailActionRequest());
    return apis.organizationApi
      .updateOrganizationEmail(orgNumber, email)
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return dispatch(Actions.updateOrganizationEmailActionSuccess());
      })
      .catch((error) => {
        return dispatch(Actions.updateOrganizationEmailActionFailure(error));
      });
  };
};

export const updateOrganizationUserEmail = (orgNumber, userId, email) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.updateOrganizationUserEmailActionRequest());
    return apis.organizationApi
      .updateOrganizationUserEmail(orgNumber, userId, email)
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return dispatch(Actions.updateOrganizationUserEmailActionSuccess());
      })
      .catch((error) => {
        return dispatch(Actions.updateOrganizationUserEmailActionFailure(error));
      });
  };
};

export const getUsersInOrganization = (orgNumber) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getUsersInOrganizationActionRequest());
    return apis.organizationApi
      .getUsersInOrganization(orgNumber)
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          throw new Error(res.statusText);
        }
        dispatch(Actions.getUsersInOrganizationActionSuccess(res));
        return res;
      })
      .catch((error) => {
        dispatch(Actions.getUsersInOrganizationActionFailure(error));
      });
  };
};

export const addUserToOrganization = (orgNumber, user) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.addUserToOrganizationActionRequest());
    return apis.organizationApi
      .addUserToOrganization(orgNumber, user)
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return dispatch(Actions.addUserToOrganizationActionSuccess());
      })
      .catch((error) => {
        return dispatch(Actions.addUserToOrganizationActionFailure(error));
      });
  };
};

export const addOrganization = (organization) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.addOrganizationActionRequest());
    return apis.organizationApi
      .addOrganization(organization)
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return dispatch(Actions.addOrganizationActionSuccess());
      })
      .catch((error) => {
        return dispatch(Actions.addOrganizationActionFailure(error));
      });
  };
};

export const getAllChildrenForOrganization = (orgNumber) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getAllChildrenForOrganizationRequest());
    return apis.organizationApi
      .getAllChildrenForOrganization(orgNumber)
      .then((response) => response.json())
      .then((response) =>
        dispatch(Actions.getAllChildrenForOrganizationSuccess(response))
      )
      .catch((error) =>
        dispatch(Actions.getAllChildrenForOrganizationFailure(error))
      );
  };
};

export const getAllChildrenWithRoles = (orgNumber, roles) => {
  return (dispatch, getState, apis) => {
    dispatch(Actions.getAllChildrenWithRolesActionRequest);
    return apis.organizationApi
      .getAllChildrenWithRoles(orgNumber, roles)
      .then((response) => response.json())
      .then((response) =>
        dispatch(Actions.getAllChildrenWithRolesActionSuccess(response))
      )
      .catch((error) =>
        dispatch(Actions.getAllChildrenWithRolesActionFailure(error))
      );
  };
};

import {addAuth, proxy} from "./api";

const getVersionInfo = () => {
  return fetch(
    `${proxy}/version`,
    addAuth({
      method: "GET",
    })
  );
};

export default {
  getVersionInfo,
};

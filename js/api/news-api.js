import { addAuth, proxy } from "./api";

const newsProxy = proxy + "/news";

const postNewsPost = (newsPost) => {
  return fetch(
    newsProxy,
    addAuth({
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(newsPost),
    })
  );
};

const editNewsPost = (newsPost) => {
  return fetch(
    newsProxy,
    addAuth({
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(newsPost),
    })
  );
};

const deleteNewsPost = (newsPostId) => {
  return fetch(
    `${newsProxy}/${newsPostId}`,
    addAuth({
      method: "DELETE",
    })
  );
};

const getAllNewsPosts = () => {
  return fetch(
    newsProxy,
    addAuth({
      method: "GET",
    })
  );
};

export default {
  postNewsPost,
  editNewsPost,
  deleteNewsPost,
  getAllNewsPosts,
};

import { addAuth, proxy } from "./api";

const reportProxy = proxy + "/reports";

const getResponsibilityReport = (year, filter, grouping, type) => {
  const responsibilityBasis =
    type === "Saldo" ? "KontiMedNegativSaldo" : "AlleKonti";
  return fetch(
    `${reportProxy}/${year}?ansvarsGrunnlag=${responsibilityBasis}&filter=${filter}&gruppering=${grouping}`,
    addAuth({
      method: "GET",
    })
  );
};

const getWordResponsibilityReport = (year, type, orgnr) => {
  return fetch(
    `${reportProxy}/${year}/word?orgNumber=${orgnr}&type=${type}`,
    addAuth({
      method: "GET",
    })
  );
};

const getExcelResponsibilityReport = (year, filter, grouping, type, id) => {
  const responsibilityBasis =
    type === "Saldo" ? "KontiMedNegativSaldo" : "AlleKonti";
  return fetch(
    `${reportProxy}/${year}/excel?ansvarsGrunnlag=${responsibilityBasis}&filter=${filter}&gruppering=${grouping}&type=${id}`,
    addAuth({ method: "GET" })
  );
};

const getVaremottakerReport = (year) => {
  return fetch(
    `${reportProxy}/${year}/word/mottaker`,
    addAuth({
      method: "GET",
    })
  );
};

export default {
  getResponsibilityReport,
  getWordResponsibilityReport,
  getExcelResponsibilityReport,
  getVaremottakerReport
};

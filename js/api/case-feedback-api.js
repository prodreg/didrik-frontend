import { addAuth, proxy } from "./api";

const getFeedbackById = (feedbackId) => {
  return fetch(
      `${proxy}/tilbakemelding/${feedbackId}`,
      addAuth({
        method: "GET",
      })
  );
};

const sendFeedback = (feedback) => {
  return fetch(
      `${proxy}/tilbakemelding/${feedback.tilbakemeldingId}`,
      addAuth({
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(feedback)
      })
  );
};

export default {
  getFeedbackById,
  sendFeedback
};

import { addAuth, proxy } from "./api";

const caseProxy = proxy + "/soeknad";

const getAllCases = () => {
  return fetch(
      caseProxy,
      addAuth({
        method: "GET",
      })
  );
};

const getAllActiveCasesByProducer = (producerNumber) => {
  return fetch(
    `${caseProxy}/prod/${producerNumber}/active`,
    addAuth({
      method: "GET",
    })
  );
};

const getCaseDraftForBankAndProducer = (bankOrgNumber, producerNumber) => {
  return fetch(
      `${caseProxy}/draft/bank/${bankOrgNumber}/produsent/${producerNumber}`,
      addAuth({
        method: "GET",
      })
  );
};

const getCaseDraftById = (caseId) => {
  return fetch(
      `${caseProxy}/draft/${caseId}`,
      addAuth({
        method: "GET",
      })
  );
};

const getCaseById = (caseId) => {
  return fetch(
      `${caseProxy}/${caseId}?includeVareleveranse=true`,
      addAuth({
        method: "GET",
      })
  );
};

const createCase = (caseObject) => {
  return fetch(
      `${caseProxy}/${caseObject.id}`,
      addAuth({
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(caseObject)
      })
  );
};

const createNewPreDraftForBankAndProducer = (bankOrgNumber, producer) => {
  return fetch(
      `${caseProxy}/draft/bank/${bankOrgNumber}`,
      addAuth({
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({produsent: producer})
      })
  );
};

const cancelCase = (caseId) => {
  return fetch(
      `${caseProxy}/${caseId}/cancel`,
      addAuth({
        method: "POST",
      })
  );
};

const endCase = (caseId) => {
  return fetch(
      `${caseProxy}/${caseId}/close`,
      addAuth({
        method: "POST",
      })
  );
};

const completeCase = (caseId) => {
  return fetch(
      `${caseProxy}/${caseId}/complete`,
      addAuth({
        method: "POST",
      })
  );
};

const addGarantiutvalget = (caseId) => {
  return fetch(
      `${caseProxy}/${caseId}/add-garanti-utvalget`,
      addAuth({
        method: "PUT",
      })
  );
};

const getGarantiutvalgetDataForCase = (caseId) => {
  return fetch(
      `${caseProxy}/${caseId}/garantiutvalget`,
      addAuth({
        method: "GET",
      })
  );
};

const updateCaseGarantiutvalget = (caseId, justification, status) => {
  return fetch(
    `${caseProxy}/${caseId}/garantiutvalget`,
    addAuth({
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        sakId: caseId,
        status: status,
        text: justification
      })
    })
  );
};

const getCaseNote = (caseId) => {
  return fetch(
    `${caseProxy}/${caseId}/note`,
    addAuth({
      method: "GET",
    })
  );
};

const updateCaseNote = (caseId, note) => {
  return fetch(
    `${caseProxy}/${caseId}/note`,
    addAuth({
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        text: note
      })
    })
  );
};

export default {
  getAllCases,
  getAllActiveCasesByProducer,
  getCaseDraftForBankAndProducer,
  getCaseDraftById,
  getCaseById,
  createNewPreDraftForBankAndProducer,
  createCase,
  cancelCase,
  endCase,
  completeCase,
  addGarantiutvalget,
  getGarantiutvalgetDataForCase,
  updateCaseGarantiutvalget,
  getCaseNote,
  updateCaseNote
};

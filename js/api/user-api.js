import { addAuth, proxy } from "./api";

const userProxy = proxy + "/users";

const validate = () => {
  return fetch(`${proxy}/jwt/validate` , addAuth({
    method: 'POST',
  }));
};

const login = (username, password) => {
  return fetch(`${proxy}/jwt/login`, {
    method: "POST",
    body: JSON.stringify({
      username: username,
      password: password,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
};

const getUserInfo = (username) => {
  return fetch(`${userProxy}/${username}` , addAuth({
    method: 'GET',
  }));
};

const updateUserEmail = (username, email) => {
  return fetch(`${userProxy}/${username}/email` , addAuth({
    method: 'PUT',
    body: email,
    headers: {
      "Content-Type": "application/json",
    }
  }));
};

const changeUserPassword = (username, currentPassword, newPassword) => {
  return fetch(`${userProxy}/${username}/password` , addAuth({
    method: 'PUT',
    body: JSON.stringify({
      currentPassword: currentPassword,
      newPassword: newPassword,
    }),
    headers: {
      "Content-Type": "application/json",
    }
  }));
};

const subscribeUserToEmail = (username, isSubscriber) => {
  return fetch(`${userProxy}/${username}/subscribe` , addAuth({
    method: 'PUT',
    body: isSubscriber,
    headers: {
      "Content-Type": "application/json",
    }
  }));
};

const updateUserStatus = (username, status) => {
  return fetch(`${userProxy}/${username}/status` , addAuth({
    method: 'PUT',
    body: status,
    headers: {
      "Content-Type": "application/json",
    }
  }));
};

const requestResetUserPassword = (username) => {
  return fetch(`${userProxy}/password-reset` , addAuth({
    method: 'POST',
    body: username,
    headers: {
      "Content-Type": "application/json",
    }
  }));
};

const resetUserPassword = (token, newPassword) => {
  return fetch(`${userProxy}/password-reset/password` , addAuth({
    method: 'POST',
    body: JSON.stringify({
      token: token,
      password: newPassword
    }),
    headers: {
      "Content-Type": "application/json",
    }
  }));
};

const getResetUserPasswordInfo = (token) => {
  return fetch(`${userProxy}/password-reset/${token}` , addAuth({
    method: 'GET'
  }));
};

export default {
  validate,
  login,
  getUserInfo,
  updateUserEmail,
  changeUserPassword,
  subscribeUserToEmail,
  updateUserStatus,
  requestResetUserPassword,
  resetUserPassword,
  getResetUserPasswordInfo
};
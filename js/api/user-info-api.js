import {addAuth, proxy} from "./api";

const getUserInfo = (username) => {
  return fetch(
    `${proxy}/users/${username}`,
    addAuth({
      method: "GET",
    })
  );
};

export default {
  getUserInfo,
};

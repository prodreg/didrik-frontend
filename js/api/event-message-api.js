import { addAuth, proxy } from "./api";

const messageProxy = proxy + "/event-message";

const getAllEventMessages = () => {
  return fetch(
    messageProxy,
    addAuth({
      method: "GET",
    })
  );
};

const getEventMessageById = (messageId) => {
  return fetch(
    `${messageProxy}/${messageId}`,
    addAuth({
      method: "GET",
    })
  );
};

const setEventMessageIsRead = (messageId, isRead) => {
  return fetch(
    `${messageProxy}/${messageId}?isRead=${isRead}`,
    addAuth({
      method: "PUT",
    })
  );
};

const softDeleteEventMessage = (messageId, del) => {
  return fetch(
    `${messageProxy}/${messageId}?delete=${del}`,
    addAuth({
      method: "DELETE",
    })
  );
};

const softDeleteEventMessages = (messageIds) => {
  return fetch(
    `${messageProxy}/batch-soft-delete?messageIds=${messageIds}`,
    addAuth({
      method: "DELETE",
    })
  );
};

const setMessageToWorkInProgress = (messageId, workInProgess) => {
  return fetch(
    `${messageProxy}/${messageId}/work-in-progress?status=${workInProgess}`,
    addAuth({
      method: "PUT",
    })
  );
};

export default {
  getAllEventMessages,
  getEventMessageById,
  setEventMessageIsRead,
  softDeleteEventMessage,
  softDeleteEventMessages,
  setMessageToWorkInProgress,
};

export const proxy = "/api";
import store from "../redux/store";

export const addAuth = (init) => {
  if (!init.headers) {
    init.headers = {};
  }
  init.headers["Authorization"] = `Bearer ${
    store.getState().user.data.token || ""
  }`;
  return init;
};

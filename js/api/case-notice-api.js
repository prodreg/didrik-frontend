import { addAuth, proxy } from "./api";

const getAllMyNotices = (orgnr) => {
  return fetch(
      `${proxy}/varsel/mottaker/${orgnr}`,
      addAuth({
        method: "GET",
      })
  );
};

const getNoticesForCase = (caseId) => {
  return fetch(
    `${proxy}/varsel/sak/${caseId}`,
    addAuth({
      method: "GET",
    })
  );
};

const getNoticeById = (noticeId) => {
  return fetch(
    `${proxy}/varsel/${noticeId}`,
    addAuth({
      method: "GET",
    })
  );
};

const getNoticeIdForOrganization = (caseId, orgId) => {
  return fetch(
    `${proxy}/varsel/sak/${caseId}/mottaker/${orgId}`,
    addAuth({
      method: "GET",
    })
  );
};

export default {
  getAllMyNotices,
  getNoticesForCase,
  getNoticeById,
  getNoticeIdForOrganization,
};

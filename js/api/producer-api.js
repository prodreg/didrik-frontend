import { addAuth, proxy } from "./api";

const producerProxy = proxy + "/produsent/";

const getAllProducers = (page, query) => {
  return fetch(
    `${producerProxy}?page=${page}&pageSize=10&query=${query}`,
    addAuth({
      method: "GET",
    })
  );
};

const getProducer = (prodnr) => {
  return fetch(
    `${producerProxy}${prodnr}`,
    addAuth({
      method: "GET",
    })
  );
};

const getCreditHistory = (prodnr) => {
  return fetch(
    `${producerProxy}${prodnr}/history/credit`,
    addAuth({
      method: "GET",
    })
  );
};

const getProductionHistory = (prodnr, queryYear) => {
  return fetch(
    `${producerProxy}${prodnr}/history/production?year=${queryYear}`,
    addAuth({
      method: "GET",
    })
  );
};

const getFarmProductionHistory = (prodnr, queryYear) => {
  return fetch(
    `${producerProxy}${prodnr}/farm/history/production?year=${queryYear}`,
    addAuth({
      method: "GET",
    })
  );
};

const getProductionHistoryForConsignee = (prodnr, mottakerId, queryYear) => {
  return fetch(
    `${producerProxy}${prodnr}/history/production/mottaker/${mottakerId}?year=${queryYear}`,
    addAuth({
      method: "GET",
    })
  );
};

const getFarmProductionHistoryForCosignee = (prodnr, mottakerId, queryYear) => {
  return fetch(
    `${producerProxy}${prodnr}/farm/history/production/mottaker/${mottakerId}?year=${queryYear}`,
    addAuth({
      method: "GET",
    })
  );
};

const getAllProducerNumberConnections = () => {
  return fetch(
    `${producerProxy}koblinger`,
    addAuth({
      method: "GET",
    })
  );
};

const createProducerNumberConnection = (oldNumber, newNumber) => {
  return fetch(
    `${producerProxy}${newNumber}/koblinger/${oldNumber}`,
    addAuth({
      method: "POST"
    })
  );
};

export default {
  getAllProducers,
  getProducer,
  getCreditHistory,
  getProductionHistory,
  getFarmProductionHistory,
  getProductionHistoryForConsignee,
  getFarmProductionHistoryForCosignee,
  getAllProducerNumberConnections,
  createProducerNumberConnection,
};

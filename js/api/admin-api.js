import { addAuth, proxy } from "./api";

const adminProxy = proxy + "/admin";

const postEventMessage = (eventMessage) => {
  return fetch(
    `${adminProxy}/event-message`,
    addAuth({
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(eventMessage),
    })
  );
};

const getBankFile = (orgnr, year, type) => {
  return fetch(
    `${adminProxy}/bank-file?orgnr=${orgnr}&year=${year}&type=${type}`,
    addAuth({
      method: "GET",
    })
  );
};

const getReportedFiles = (year) => {
  return fetch(
    `${adminProxy}/reported-file?year=${year}`,
    addAuth({
      method: "GET",
    })
  );
};

const getReportedFile = (id, type) => {
  return fetch(
    `${adminProxy}/reported-file/${id}?type=${type}`,
    addAuth({
      method: "GET",
    })
  );
};

const updateProdusentIndex = () => {
  return fetch(
    `${adminProxy}/cache/produsent/refresh`,
    addAuth({
      method: "POST",
    })
  );
};

export default {
  postEventMessage,
  getBankFile,
  getReportedFile,
  getReportedFiles,
  updateProdusentIndex
};

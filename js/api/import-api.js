import { addAuth, proxy } from "./api";

const importProxy = proxy + "/imports";

const getAllImports = () => {
  return fetch(
    importProxy,
    addAuth({
      method: "GET",
    })
  );
};

const getImportForOrganization = (importId) => {
  return fetch(
    `${importProxy}/mottaker-imports/${importId}?errors=true&rows=true`,
    addAuth({
      method: "GET",
    })
  );
};

const getImportForBank = (importId) => {
  return fetch(
    `${importProxy}/bank-imports/${importId}?errors=true&rows=true`,
    addAuth({
      method: "GET",
    })
  );
};

const createImportForBank = (data, orgnr, filename, year) => {
  return fetch(
    `${importProxy}/bank-imports?bank=${orgnr}&filename=${filename}&year=${year}`,
    addAuth({
      method: "POST",
      headers: { "Content-Type": "application/json;charset=UTF-8" },
      body: data,
    })
  );
};

const createImportForOrganization = (data, orgnr, filename, year) => {
  return fetch(
    `${importProxy}/mottaker-imports?mottaker=${orgnr}&filename=${filename}&year=${year}`,
    addAuth({
      method: "POST",
      headers: { "Content-Type": "application/json;charset=UTF-8" },
      body: data,
    })
  );
};

const deleteBankImport = (importId) => {
  return fetch(
    `${importProxy}/bank-imports/${importId}`,
    addAuth({
      method: "DELETE",
    })
  );
};

const deleteOrganizationImport = (importId) => {
  return fetch(
    `${importProxy}/mottaker-imports/${importId}`,
    addAuth({
      method: "DELETE",
    })
  );
};

const deleteOrganizationErrorRowsByType = (importId, errorType) => {
  return fetch(
    `${importProxy}/mottaker-imports/${importId}/errors/type/${errorType}`,
    addAuth({
      method: "DELETE",
    })
  );
};

const deleteBankErrorRowsByType = (importId, errorType) => {
  return fetch(
    `${importProxy}/bank-imports/${importId}/errors/type/${errorType}`,
    addAuth({
      method: "DELETE",
    })
  );
};

export default {
  getAllImports,
  getImportForOrganization,
  getImportForBank,
  createImportForBank,
  createImportForOrganization,
  deleteBankImport,
  deleteOrganizationImport,
  deleteOrganizationErrorRowsByType,
  deleteBankErrorRowsByType,
};

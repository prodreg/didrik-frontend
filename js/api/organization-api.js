import { addAuth, proxy } from "./api";
import querystring from "querystring";

const orgProxy = proxy + "/organizations";

const getAllOrganizations = (types) => {
  var formattedTypes = "";
  for (const type of types) {
    formattedTypes += "type=" + type + "&";
  }
  return fetch(
    `${orgProxy}?${formattedTypes}`,
    addAuth({
      method: "GET",
    })
  );
};

const queryOrganizations = (query, types) => {
  var formattedTypes = "";
  for (const type of types) {
    formattedTypes += "type=" + type + "&";
  }
  return fetch(
    `${orgProxy}/query/${query}?${formattedTypes}`,
    addAuth({
      method: "GET",
    })
  );
};

const getOrganizationDetails = (orgNumber) => {
  return fetch(
    `${orgProxy}/${orgNumber}`,
    addAuth({
      method: "GET",
    })
  );
};

const updateOrganizationName = (orgNumber, name) => {
  return fetch(
    `${orgProxy}/${orgNumber}/name`,
    addAuth({
      method: "PUT",
      body: name,
    })
  );
};

const updateOrganizationEmail = (orgNumber, email) => {
  return fetch(
    `${orgProxy}/${orgNumber}/email`,
    addAuth({
      method: "PUT",
      body: email,
    })
  );
};

const updateOrganizationUserEmail = (orgNumber, userId, email) => {
  return fetch(
    `${orgProxy}/${orgNumber}/${userId}/email`,
    addAuth({
      method: "PUT",
      body: email,
    })
  );
};

const getUsersInOrganization = (orgNumber) => {
  return fetch(
    `${orgProxy}/${orgNumber}/users`,
    addAuth({
      method: "GET",
    })
  );
};

const addUserToOrganization = (orgNumber, user) => {
  return fetch(
    `${orgProxy}/${orgNumber}/users`,
    addAuth({
      method: "POST",
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
      },
    })
  );
};

const getAllChildrenForOrganization = (orgNumber) => {
  return fetch(
    `${orgProxy}/${orgNumber}/children?includeAccessRoles=${true}`,
    addAuth({
      method: "GET",
    })
  );
};

const addOrganization = (organization) => {
  return fetch(
    `${orgProxy}`,
    addAuth({
      method: "POST",
      body: JSON.stringify(organization),
      headers: {
        "Content-Type": "application/json",
      },
    })
  );
};

const getAllChildrenWithRoles = (orgNumber, roles) => {
  const params = querystring.stringify({ roles: roles });

  return fetch(
    `${orgProxy}/${orgNumber}/children-with-roles?${params}`,
    addAuth({
      method: "GET",
    })
  );
};

export default {
  getOrganizationDetails,
  updateOrganizationName,
  updateOrganizationEmail,
  updateOrganizationUserEmail,
  getUsersInOrganization,
  addUserToOrganization,
  getAllChildrenForOrganization,
  getAllOrganizations,
  queryOrganizations,
  addOrganization,
  getAllChildrenWithRoles,
};

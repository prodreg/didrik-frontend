import { addAuth, proxy } from "./api";

const ordningProxy = proxy + "/organizations-ordning";

const getAllOrdning = () => {
  return fetch(`${ordningProxy}` , addAuth({
    method: 'GET',
  }));
};

const getOrdning = (orgNumber) => {
  return fetch(`${ordningProxy}/${orgNumber}` , addAuth({
    method: 'GET',
  }));
};


const updateOrdning = (orgNumber, ordningName) => {
  return fetch(`${ordningProxy}/${orgNumber}/ordning/${ordningName}` , addAuth({
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
    }
  }));
};

export default {
  getAllOrdning,
  getOrdning,
  updateOrdning
};
import { addAuth, proxy } from "./api";

const getNoticeTypes = () => {
  return fetch(
      proxy + "/options/varseltype",
      addAuth({
        method: "GET",
      })
  );
};

const getProductTypes = () => {
  return fetch(
      proxy + "/options/produktslag",
      addAuth({
        method: "GET",
      })
  );
};

const getSchemeTypes = () => {
  return fetch(
    proxy + "/options/ordning",
    addAuth({
      method: "GET",
    })
  );
};

const getOrganizationTypes = () => {
  return fetch(
    proxy + "/options/organization-type",
    addAuth({
      method: "GET",
    })
  );
};

export default {
  getNoticeTypes,
  getProductTypes,
  getSchemeTypes,
  getOrganizationTypes,
};

import { addAuth, proxy } from "./api";

const conversationProxy = proxy + "/conversations";

const getConversationById = (conversationId) => {
  return fetch(
    `${conversationProxy}/${conversationId}`,
    addAuth({
      method: "GET",
    })
  );
};

const getConversationForCaseAndOrganization = (caseId, orgId) => {
  return fetch(
    `${conversationProxy}?sakId=${caseId}&mottakerId=${orgId}`,
    addAuth({
      method: "GET",
    })
  );
};

const postConversationMessage = (conversationId, message) => {
  return fetch(
    `${conversationProxy}/${conversationId}/message`,
    addAuth({
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(message),
    })
  );
};

const postConversationMessageAs = (conversationId, postAs, message) => {
  return fetch(
    `${conversationProxy}/${conversationId}/message/as/${postAs}`,
    addAuth({
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(message),
    })
  );
};

const getConversationMessages = (conversationId, time) => {
  return fetch(
    `${conversationProxy}/${conversationId}/messages?time=${time}`,
    addAuth({ method: "GET" })
  );
};

const getConversationAttachments = (conversationId, messageId) => {
  return fetch(
    `${conversationProxy}/${conversationId}/message/${messageId}/attachment`,
    addAuth({
      method: "GET",
    })
  );
};

export default {
  getConversationById,
  getConversationForCaseAndOrganization,
  postConversationMessage,
  postConversationMessageAs,
  getConversationMessages,
  getConversationAttachments,
};

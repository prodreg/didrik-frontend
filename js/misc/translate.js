import {getTextColorForCaseGarantiutvalgetStatus} from "./interpret";
import React from "react";

export const translateCaseNoticeType = (status) => {
  if(status === 'NY') {
    return 'Innvilget kreditt';
  }
  else if(status === 'ENDRING') {
    return 'Endring av kreditt';
  }
  else if(status === 'SLETTING') {
    return 'Pantsettelse er slettet';
  }
  else {
    return status;
  }
}

export const translateCaseNoticeTypeExplanation = (status) => {
  if(status === 'NY') {
    return 'Varsel om innvilget kreditt og pant i enkle pengekrav';
  }
  else if(status === 'ENDRING') {
    return 'Varsel om endring av innvilget kreditt og pant i enkle pengekrav';
  }
  else if(status === 'SLETTING') {
    return 'Varslet er kansellert';
  }
  else {
    return status;
  }
}

export const translateShortCaseGarantiutvalgetStatus = (status) => {
  if (status === 'REJECTED') {
    return 'Avslått';
  } else if (status === 'ACCEPTED') {
    return 'Godkjent';
  } else if (status === 'SIGNED_OFF') {
    return 'Utmeldt';
  } else if (status === 'PENDING') {
    return 'Under behandling';
  } else {
    return status;
  }
}

export const translateLongCaseGarantiutvalgetStatus = (status) => {
  if (status === 'REJECTED') {
    return 'Saken er avslått av garantiutvalget';
  } else if (status === 'ACCEPTED') {
    return 'Saken er godkjent av garantiutvalget';
  } else if (status === 'SIGNED_OFF') {
    return 'Garantiutvalget er utmeldt av saken';
  } else if (status === 'PENDING') {
    return 'Saken er under behandling hos Garantiutvalget';
  } else {
    return status;
  }
}
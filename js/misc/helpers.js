export const formatCurrency = (input) => {
  if (!input) {
    return input;
  }

  return input.toLocaleString(
      'no', {
        style: 'currency',
        currency: 'NOK',
      })
}

export const formatAccountNumber = (input) => {
  if (!input) {
    return input;
  }

  if (input.length === 11) {
    return input.substring(0,4) + " " + input.substring(4, 6) + " " + input.substring(6, 11);
  }
  return input;
}

export const validateAccountNumber = (input) => {
  if (!input) {
    return false;
  }

  if (input.length !== 11) {
    return false;
  }

  if (isNaN(input)) {
    return false;
  }

  const weights = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
  const controlNumber = parseInt(input.charAt(10), 10);
  const accountNumberWithoutControl = input.substring(0, 10);
  let sum = 0;
  for (let index = 0; index < 10; index++) {
    sum += parseInt(accountNumberWithoutControl.charAt(index), 10) * weights[index];
  }
  const remainder = sum % 11;
  return controlNumber === (remainder === 0 ? 0 : 11 - remainder);
}

export const isNumberNotNegative = (input) => {
  if (!input) {
    return false;
  }

  if (isNaN(input)) {
    return false;
  }

  return parseInt(input) >= 0;
}

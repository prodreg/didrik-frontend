export const getTextColorForCaseStatus = (status) => {
  if(status === 'PENDING') {
    return 'text-info';
  }
  else if(status === 'ALL_VARSEL_HANDLED' || status === 'COMPLETED') {
    return 'text-success';
  }
  else if(status === 'CLOSED' || status === 'CANCELLED') {
    return 'text-danger';
  }
}

export const getTextColorForCaseGarantiutvalgetStatus = (status) => {
  if(status === 'PENDING') {
    return 'text-info';
  }
  else if(status === 'REJECTED') {
    return 'text-danger';
  }
  else if(status === 'ACCEPTED') {
    return 'text-success';
  }
  else if(status === 'SIGNED_OFF') {
    return 'text-warning';
  }
}
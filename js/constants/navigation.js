export const NAVIGATION_KEYS = {
	LOGIN: "/login",
	HOME: "/",
	CASES: "/case",
	CASE_NOTICES: "/case-notice",
	MESSAGES: "/event-message",
	PRODUCERS: "/producers",
	ADMINISTRATOR: "/administrator",
	ORGANIZATION: "/organizations/details",
	ORGANIZATIONS: "/organizations/search",
	RESPONSIBILITYREPORT: "/responsibility-report",
	REPORTING: "/reporting/imports",
	PRODUCER_NUMBER_CONNECTIONS: "/produsentnummer-koblinger",
	VAREMOTTAKER_REPORT: "/report/varemottaker",
	BANK_REPORTS: "/report/bank",
};

export const NAVIGATION = {
	["ADMIN"]: [
		{
			name: "Hjem",
			path: NAVIGATION_KEYS.HOME,
		},
		{ name: "Innrapportering", path: NAVIGATION_KEYS.REPORTING },
		{
			name: "Produsenter",
			path: NAVIGATION_KEYS.PRODUCERS,
		},
		{
			name: "Saker",
			path: NAVIGATION_KEYS.CASES,
		},
		{
			name: "Ansvarsrapporter",
			path: NAVIGATION_KEYS.RESPONSIBILITYREPORT,
		},
		{
			name: "Organisasjoner",
			path: NAVIGATION_KEYS.ORGANIZATIONS,
		},
		{
			name: "Meldinger",
			path: NAVIGATION_KEYS.MESSAGES,
		},
		{
			name: "Administrator",
			path: NAVIGATION_KEYS.ADMINISTRATOR,
		},
		{
			name: "Produsentnummer-koblinger",
			path: NAVIGATION_KEYS.PRODUCER_NUMBER_CONNECTIONS,
		},
	],
	["VAREMOTTAKER"]: [
		{
			name: "Hjem",
			path: NAVIGATION_KEYS.HOME,
		},
		{ name: "Innrapportering", path: NAVIGATION_KEYS.REPORTING },
		{
			name: "Produsenter",
			path: NAVIGATION_KEYS.PRODUCERS,
		},
		{
			name: "Varsler",
			path: NAVIGATION_KEYS.CASE_NOTICES,
		},
		{
			name: "Varemottaker rapporter",
			path: NAVIGATION_KEYS.VAREMOTTAKER_REPORT,
		},
		{
			name: "Organisasjon",
			path: NAVIGATION_KEYS.ORGANIZATION,
		},
		{
			name: "Meldinger",
			path: NAVIGATION_KEYS.MESSAGES,
		},
	],
	["BANK"]: [
		{
			name: "Hjem",
			path: NAVIGATION_KEYS.HOME,
		},
		{ name: "Innrapportering", path: NAVIGATION_KEYS.REPORTING },
		{
			name: "Produsenter",
			path: NAVIGATION_KEYS.PRODUCERS,
		},
		{
			name: "Saker",
			path: NAVIGATION_KEYS.CASES,
		},
		{
			name: "Bankrapporter",
			path: NAVIGATION_KEYS.BANK_REPORTS,
		},
		{
			name: "Organisasjon",
			path: NAVIGATION_KEYS.ORGANIZATION,
		},
		{
			name: "Meldinger",
			path: NAVIGATION_KEYS.MESSAGES,
		},
	],
	["GARANTIUTVALGET"]: [
		{
			name: "Hjem",
			path: NAVIGATION_KEYS.HOME,
		},
		{
			name: "Produsenter",
			path: NAVIGATION_KEYS.PRODUCERS,
		},
		{
			name: "Saker",
			path: NAVIGATION_KEYS.CASES,
		},
		{
			name: "Organisasjon",
			path: NAVIGATION_KEYS.ORGANIZATION,
		},
		{
			name: "Meldinger",
			path: NAVIGATION_KEYS.MESSAGES,
		},
	],
};

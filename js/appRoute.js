import "../sass/app.scss";

import { connect, Provider } from "react-redux";

import React from "react";
import { Redirect } from "react-router-dom";
import Header from "./components/header";
import {
	getStoreDataForUser,
	getStoreDataForUserInfo,
} from "./redux/selectors";
import Navbar from "./components/navbar";
import { NAVIGATION_KEYS } from "./constants/navigation";

class AppRoute extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const {
			component: Component,
			menu,
			redirect,
			requiredRoles,
			user,
			location,
			userInfo,
			...rest
		} = this.props;

		if (redirect) {
			//If uses is not logged in, redirect to login page
			if (!user.isLoggedIn) {
				return (
					<Redirect
						to={{
							pathname: "/login",
							state: { from: location },
						}}
					/>
				);
			}

			//If user is logged in, but does not have the required role redirect to home page
			if (
				requiredRoles.length > 0 &&
				requiredRoles.filter((role) => user.data.roles.includes(role))
					.length == 0
			) {
				return (
					<Redirect
						to={{
							pathname: "/",
							state: { from: location },
						}}
					/>
				);
			}
		}

		return (
			<div className="app">
				<Header />

				{!menu && (
					<div className="container" style={{ marginTop: "10px" }}>
						<Component {...rest} />
					</div>
				)}
				{menu && (
					<div
						className="container-fluid"
						style={{ marginTop: "10px" }}
					>
						<div
							className="d-flex flex-column flex-lg-row"
							style={{ gap: 20 }}
						>
							<div>
								<Navbar
									userRoles={user.data.roles}
									userInfo={userInfo.data}
									url={location.pathname}
								/>
							</div>
							<div className="flex-fill">
								<Component {...rest} />
							</div>
						</div>
					</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	const user = getStoreDataForUser(state);
	const userInfo = getStoreDataForUserInfo(state);
	return { user, userInfo };
};

AppRoute.defaultProps = {
	menu: true,
	redirect: true,
	requiredRoles: [],
};

export default connect(mapStateToProps)(AppRoute);

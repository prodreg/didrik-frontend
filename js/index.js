import "../sass/app.scss";

import $ from "jquery";
import Popper from "popper.js";
import bootstrap from "bootstrap";

import { Provider } from "react-redux";
import store from "./redux/store";

import React from "react";
import ReactDOM from "react-dom";
import App from "./app";

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);

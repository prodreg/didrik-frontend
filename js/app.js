import "../sass/app.scss";

import { HashRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import Home from "./page/home";
import Login from "./page/login";
import Cases from "./page/case/cases";

import EventMessages from "./page/event-message/event-messages";
import EventMessageDetail from "./page/event-message/event-message-detail";
import OrganizationDetails from "./page/organizations/organization-details";
import Producers from "./page/producer/producers";
import ProducerDetail from "./page/producer/producer-detail";
import UserProfile from "./page/user-profile/user-profile";
import ResetPassword from "./page/reset-password/reset-password";
import OrganizationSearch from "./page/organization-search/organization-search";

import React from "react";
import AppRoute from "./appRoute";
import Admin from "./page/admin/admin";
import EventMessage from "./page/admin/event-message/event-message";
import News from "./page/admin/news/news";
import Reporting from "./page/admin/reporting/reporting";
import Case from "./page/case/case";
import CaseNotice from "./page/case-notice/case-notice";
import CaseNotices from "./page/case-notice/case-notices";
import CreateCase from "./page/case/create-case";
import EditCase from "./page/case/edit-case";
import ResponsibilityReport from "./page/responsibility-report/responsibility-report";
import Imports from "./page/imports/imports";
import OrganizationImportDetailContainer from "./page/imports/organization-import/organization-import-detail-container";
import BankImportDetailContainer from "./page/imports/bank-import/bank-import-detail-container";
import OrganizationImport from "./page/imports/organization-import/organization-import";
import BankImport from "./page/imports/bank-import/bank-import";
import CaseFeedback from "./page/case-feedback/case-feedback";
import { useDispatch } from "react-redux";
import { validateSession } from "./redux/thunks/user-thunks";
import ProducerNumberConnections from "./page/producer/producer-number-connections";
import VaremottakerReport from "./page/varemottaker-report/varemottaker-report";
import { BankReportPage } from "./page/bank-report/bank-report";

const App = () => {
	const dispatch = useDispatch();
	dispatch(validateSession());

	return (
		<Router>
			<Switch>
				<Route
					exact
					path="/login"
					render={(props) => (
						<AppRoute
							menu={false}
							redirect={false}
							component={Login}
							{...props}
						/>
					)}
				/>
				<Route
					exact
					path="/"
					render={(props) => <AppRoute component={Home} {...props} />}
				/>
				<Route
					exact
					path="/case"
					render={(props) => (
						<AppRoute component={Cases} {...props} />
					)}
				/>
				<Route
					exact
					path="/case/:caseId"
					render={(props) => <AppRoute component={Case} {...props} />}
				/>
				<Route
					exact
					path="/create-case/:caseId"
					render={(props) => (
						<AppRoute component={CreateCase} {...props} />
					)}
				/>
				<Route
					exact
					path="/edit-case/:caseId"
					render={(props) => (
						<AppRoute component={EditCase} {...props} />
					)}
				/>
				<Route
					exact
					path="/case-notice/:noticeId"
					render={(props) => (
						<AppRoute component={CaseNotice} {...props} />
					)}
				/>
				<Route
					exact
					path="/case-notice"
					render={(props) => (
						<AppRoute component={CaseNotices} {...props} />
					)}
				/>
				<Route
					exact
					path="/case-feedback/:feedbackId"
					render={(props) => (
						<AppRoute component={CaseFeedback} {...props} />
					)}
				/>
				<Route
					exact
					path="/event-message"
					render={(props) => (
						<AppRoute component={EventMessages} {...props} />
					)}
				/>
				<Route
					exact
					path="/event-message/:id"
					render={(props) => (
						<AppRoute component={EventMessageDetail} {...props} />
					)}
				/>
				<Route
					exact
					path="/producers"
					render={(props) => (
						<AppRoute component={Producers} {...props} />
					)}
				/>
				<Route
					exact
					path="/producers/details/:prodnr"
					render={(props) => (
						<AppRoute component={ProducerDetail} {...props} />
					)}
				/>
				<Route
					exact
					path="/administrator"
					render={(props) => (
						<AppRoute component={Admin} {...props} />
					)}
				/>
				<Route
					exact
					path="/administrator/send-event-message"
					render={(props) => (
						<AppRoute component={EventMessage} {...props} />
					)}
				/>
				<Route
					exact
					path="/administrator/news"
					render={(props) => <AppRoute component={News} {...props} />}
				/>
				<Route
					exact
					path="/administrator/reporting"
					render={(props) => (
						<AppRoute component={Reporting} {...props} />
					)}
				/>
				<Route
					exact
					path="/organizations/details/:orgNumber"
					render={(props) => (
						<AppRoute component={OrganizationDetails} {...props} />
					)}
				/>
				<Route
					exact
					path="/organizations/search"
					render={(props) => (
						<AppRoute component={OrganizationSearch} {...props} />
					)}
				/>
				<Route
					exact
					path="/user/:username"
					render={(props) => (
						<AppRoute component={UserProfile} {...props} />
					)}
				/>
				<Route
					exact
					path="/password-reset/:token"
					render={(props) => (
						<AppRoute menu={false} redirect={false} component={ResetPassword} {...props} />
					)}
				/>
				<Route
					exact
					path="/responsibility-report"
					render={(props) => (
						<AppRoute component={ResponsibilityReport} {...props} />
					)}
				/>
				<Route
					exact
					path="/reporting/imports"
					render={(props) => (
						<AppRoute component={Imports} {...props} />
					)}
				/>
				<Route
					exact
					path="/reporting/imports/details/mottaker/:importId"
					render={(props) => (
						<AppRoute
							component={OrganizationImportDetailContainer}
							{...props}
						/>
					)}
				/>
				<Route
					exact
					path="/reporting/imports/details/bank/:importId"
					render={(props) => (
						<AppRoute
							component={BankImportDetailContainer}
							{...props}
						/>
					)}
				/>
				<Route
					exact
					path="/reporting/imports/mottaker"
					render={(props) => (
						<AppRoute component={OrganizationImport} {...props} />
					)}
				/>
				<Route
					exact
					path="/reporting/imports/bank"
					render={(props) => (
						<AppRoute component={BankImport} {...props} />
					)}
				/>
				<Route
					exact
					path="/produsentnummer-koblinger"
					render={(props) => (
						<AppRoute
							component={ProducerNumberConnections}
							{...props}
						/>
					)}
				/>
				<Route
					exact
					path="/report/varemottaker"
					render={(props) => (
						<AppRoute component={VaremottakerReport} {...props} />
					)}
				/>
				<Route
					exact
					path="/report/bank"
					render={(props) => (
						<AppRoute component={BankReportPage} {...props} />
					)}
				/>
				<Redirect exact from="/home/action/varsler/:noticeId" to='/case-notice/:noticeId'  />
				<Redirect exact from="/home/sak/details/:caseId" to='/case/:caseId'  />
			</Switch>
		</Router>
	);
};

export default App;

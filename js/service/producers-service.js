const removeQueryFromStorage = () => {
  localStorage.removeItem("query");
};

const removePageNumberFromStorage = () => {
  localStorage.removeItem("page_number_producer");
};

export default {
  removeQueryFromStorage,
  removePageNumberFromStorage,
};

import producersService from "./producers-service";

const isLoggedIn = () => {
  return !!localStorage.getItem("user");
};

const getLoggedInUser = () => {
  return JSON.parse(localStorage.getItem("user"));
};

const loginUser = (user) => {
  localStorage.setItem("user", JSON.stringify(user));
};

const logoutUser = () => {
  localStorage.removeItem("user");
  producersService.removeQueryFromStorage();
  producersService.removePageNumberFromStorage();
  localStorage.removeItem("page_number");
  localStorage.removeItem("radio_button_state");
  localStorage.removeItem("scroll_position");
};

export default {
  isLoggedIn,
  getLoggedInUser,
  loginUser,
  logoutUser,
};

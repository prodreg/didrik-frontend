import React from "react";
import { Modal, Row, Col, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import TertiaryButton from "./ui/button/tertiary-button";
import SecondaryButton from "./ui/button/secondary-button";

class NewName extends React.Component {
  state = {
    validName: {
      isValid: false,
      nonEmpty: false,
    },
    newName: "",
    saving: false,
    validated: false,
  };

  constructor(props) {
    super(props);
    this.validateName = this.validateName.bind(this);
  }

  closeDialog = () => {
    this.setState({
      validName: {
        isValid: false,
        nonEmpty: false,
      },
      newName: "",
      saving: false,
      validated: false,
    });

    this.props.toggleShowChangeName();
  };

  validateName = (event) => {
    const name = event.target.value;
    var isValid = true;
    var nonEmpty = true;
    if (!name) {
      isValid = false;
      nonEmpty = false;
    }

    const validName = {
      isValid: isValid,
      nonEmpty: nonEmpty,
    };
    this.setState({
      validName: validName,
      newName: name,
    });
  };

  handleSubmitNewName = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.state.validName.isValid) {
      this.setState({
        saving: true,
      });
      if (this.props.orgNumber) {
        this.props
          .updateTarget(this.props.orgNumber, this.props.targetId, this.state.newName)
          .then((res) => {
            this.handleError(res.payload.error);
            this.setState({
              saving: false,
            });
          });
      } else {
        this.props
          .updateTarget(this.props.targetId, this.state.newName)
          .then((res) => {
            this.handleError(res.payload.error);
            this.setState({
              saving: false,
            });
          });
      }
    }

    this.setState({
      validated: true,
    });
  };

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Navnet kunne ikke bli oppdatert. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("OK", "Navnet er oppdatert", "success").then(() => {
        this.props.history.go(0);
      });
    }
  };

  render() {
    return (
      <Modal show={this.props.showChangeName} onHide={this.closeDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Endre navn</Modal.Title>
        </Modal.Header>
        <Form noValidate onSubmit={this.handleSubmitNewName}>
          <Modal.Body>
            <Form.Group as={Row}>
              <Form.Label column sm={4}>
                Nytt navn
              </Form.Label>
              <Col sm={8}>
                <Form.Control
                  required
                  isInvalid={
                    !this.state.validName.isValid && this.state.validated
                  }
                  type="text"
                  onChange={this.validateName}
                />
                {this.state.validated && (
                  <>
                    <Row>
                      {!this.state.validName.nonEmpty && (
                        <Form.Text className="text-danger text-small">
                          Navn er påkrevd
                        </Form.Text>
                      )}
                    </Row>
                  </>
                )}
              </Col>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <TertiaryButton
              type="reset"
              title="Avbryt"
              onClick={this.closeDialog}
              disabled={this.state.saving}
            />
            <SecondaryButton
              type="submit"
              title="Lagre"
              disabled={
                (!this.state.validName.isValid && this.state.validated) ||
                this.state.saving
              }
            />
          </Modal.Footer>
        </Form>
      </Modal>
    );
  }
}

NewName.defaultProps = {
  showChangeName: false,
};

export default NewName;

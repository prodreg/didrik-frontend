import React from "react";
import { Modal, Row, Col, Form } from "react-bootstrap";
import { requestResetUserPassword } from "../redux/thunks/user-thunks";
import Swal from "sweetalert2";
import TertiaryButton from "./ui/button/tertiary-button";
import SecondaryButton from "./ui/button/secondary-button";
import { connect } from "react-redux";

class ForgotPassword extends React.Component {
  state = {
    validUsername: {
      isValid: false,
      nonEmpty: false
    },
    username: "",
    saving: false,
    validated: false,
  };

  constructor(props) {
    super(props);
    this.validateUsername = this.validateUsername.bind(this);
  }

  closeDialog = () => {
    this.setState({
      validUsername: {
        isValid: false,
        nonEmpty: false
      },
      username: "",
      saving: false,
      validated: false,
    });

    this.props.toggleShowForgottenPasswordDialog();
  };

  validateUsername = (event) => {
    const username = event.target.value;
    let isValid = true;
    let nonEmpty = true;
    if (!username) {
      isValid = false;
      nonEmpty = false;
    }

    const validUsername = {
      isValid: isValid,
      nonEmpty: nonEmpty
    };
    this.setState({
      validUsername: validUsername,
      username: username,
    });
  };

  handleSubmitForgotPassword = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    const validForm = this.state.validUsername.isValid;
    if (form.checkValidity() === false || !validForm) {
      event.stopPropagation();
    } else {
      this.setState({
        saving: true,
      });

      this.props.requestResetUserPassword(this.state.username)
        .then((res) => {
          this.handleError(res.payload.error);
        })
        .finally(() => {
          this.setState({
            saving: false,
          });
        });
    }

    this.setState({
      validated: true,
    });
  };

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Fant ingen bruker med dette brukernavnet",
        "error"
      );
    } else {
      Swal.fire("OK", "Instruksjoner har blitt sendt til din e-post adresse", "success").then(() => {
        this.props.history.go(0);
      });
    }
  };

  render() {
    return (
      <Modal show={this.props.showForgotPassword} onHide={this.closeDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Glemt Passord</Modal.Title>
        </Modal.Header>
        <Form noValidate onSubmit={this.handleSubmitForgotPassword}>
          <Modal.Body>
            <p>
              Hvis du har glemt passordet ditt kan du fylle inn ditt brukernavn i feltet under, så sender vi deg en e-post med instruksjoner for å gjenopprette passord.
            </p>
            <Form.Group as={Row}>
              <Form.Label column sm={4}>
                Brukernavn
              </Form.Label>
              <Col sm={8}>
                <Form.Control
                  required
                  onChange={this.validateUsername}
                />
                {this.state.validated && (
                  <>
                    <Row>
                      {!this.state.validUsername.nonEmpty && (
                        <Form.Text className="text-danger text-small">
                          Brukernavn er påkrevd
                        </Form.Text>
                      )}
                    </Row>
                  </>
                )}
              </Col>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <TertiaryButton
              type="reset"
              title="Avbryt"
              onClick={this.closeDialog}
              disabled={this.state.saving}
            />
            <SecondaryButton
              type="submit"
              title="Send"
              disabled={ (!this.state.validUsername.isValid && this.state.validated) || this.state.saving }
            />
          </Modal.Footer>
        </Form>
      </Modal>
    );
  }
}

ForgotPassword.defaultProps = {
  showForgotPassword: false,
};

const mapStateToProps = (state) => {
  return { };
}

const mapDispatchToProps = {
  requestResetUserPassword
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);

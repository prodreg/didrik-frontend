import { connect } from "react-redux";
import "./header.scss";

const React = require("react");

import { logout } from "../redux/thunks/user-thunks";
import userService from "../service/user-service";
import { getStoreDataForUser } from "../redux/selectors";
import { NavLink, Link } from "react-router-dom";
import Logo from "../../img/logo.svg";

class Header extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<nav className="main-header navbar navbar-expand-lg">
				<div className="container-fluid">
					<a
						className="navbar-brand d-flex align-items-center"
						href="#"
					>
						<img src={Logo} alt="Driftskreditt" />
						<div style={{ paddingLeft: "10px" }}>Driftskreditt</div>
					</a>
					<button
						className="navbar-toggler"
						type="button"
						data-bs-toggle="collapse"
						data-bs-target="#navbarNavDropdown"
						aria-controls="navbarNavDropdown"
						aria-expanded="false"
						aria-label="Toggle navigation"
					>
						<span className="navbar-toggler-icon">
							<div className="line"></div>
							<div className="line"></div>
							<div className="line"></div>
						</span>
					</button>
					<div
						className="collapse navbar-collapse"
						id="navbarNavDropdown"
					>
						<ul className="navbar-nav ms-auto mb-2 mb-lg-0">
							<li className="nav-item">
								{this.props.user.isLoggedIn && (
									<div className="dropdown">
										<a
											className="nav-link dropdown-toggle"
											href="#"
											id="navbarDarkDropdownMenuLink"
											role="button"
											data-bs-toggle="dropdown"
											aria-expanded="false"
										>
											{this.props.user.data.username}
										</a>
										<ul
											id="navbar-user-logout-button"
											className="dropdown-menu dropdown-menu-end"
											aria-labelledby="navbarDarkDropdownMenuLink"
										>
											<li>
												<Link
													to={
														"/user/" +
														this.props.user.data
															.username
													}
													className="dropdown-item"
												>
													Brukerprofil
												</Link>
											</li>
											<li className="dropdown-divider"></li>
											<li>
												<a
													className="dropdown-item"
													style={{
														cursor: "pointer",
													}}
													onClick={() => {
														this.props.logout();
													}}
												>
													Logg ut
												</a>
											</li>
										</ul>
									</div>
								)}
							</li>
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}

const mapStateToProps = (state) => {
	const user = getStoreDataForUser(state);
	return { user };
};

const mapDispatchToProps = {
	logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);

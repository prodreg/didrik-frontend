import React from "react";
import "./card.scss";

const Card = (props) => {
  return (
    <div
      className="card"
      key={props.id}
      style={{
        marginBottom: "15px",
        boxShadow:
          "rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 6px",
      }}
    >
      <div className="card-header" style={props.style}>
        {props.title}
      </div>
      <div className="card-body">{props.children}</div>
    </div>
  );
};

export default Card;

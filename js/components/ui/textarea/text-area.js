import React from "react";
import "./text-area.scss";

const TextArea = (props) => {
  return (
    <textarea
      name="textarea"
      id="textarea"
      cols="30"
      rows="10"
      className="textarea-input"
      onChange={props.onChange}
      onBlur={props.onBlur}
      value={props.value}
      disabled={props.disabled}
    ></textarea>
  );
};

export default TextArea;

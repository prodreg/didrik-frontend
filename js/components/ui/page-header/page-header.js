import React from "react";

const PageHeader = (props) => {
  return (
    <div className="header-container" style={{ display: "flex" }}>
      <div
        className="icon-container"
        style={{ lineHeight: "68.8px", marginRight: "5px" }}
      >
        {props.icon}
      </div>
      <div className="text-container" style={{ width: "100%" }}>
        <div className="title-container" style={{ fontSize: "30px" }}>
          {props.title}
        </div>
        <div
          className="description-container"
          style={{ fontWeight: "normal", marginTop: "-5px" }}
        >
          {props.description}
        </div>
      </div>
    </div>
  );
};

export default PageHeader;

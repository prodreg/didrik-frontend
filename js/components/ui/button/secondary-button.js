import React from "react";
import "./secondary-button.scss";

/**
 *
 * @param props.type Button type ["button" | "sumbit"]
 * @param props.onClick Callback called when the button is pressed
 * @param props.className Classname prop
 * @param props.icon Icon to display, should be JSX
 * @param props.title Text to display
 * @param props.children React children. Will be used if title isn't provided.
 */
export const SecondaryButton = ({
	type,
	className,
	onClick,
	disabled,
	icon,
	title,
	children,
	...rest
}) => {
	return (
		<button
			type={type}
			className={"secondary-button" + (className ? " " + className : "")}
			onClick={onClick}
			disabled={disabled}
			{...rest}
		>
			{icon && <div className="button-icon">{icon}</div>}
			{title || children}
		</button>
	);
};

export default SecondaryButton;

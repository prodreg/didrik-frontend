import React from "react";
import "./tertiary-button.scss";

const TertiaryButton = (props) => {
  return (
    <button
      type={props.type}
      className={"tertiary-button" + (props.className ? " " + props.className : "")}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.icon && (
        <div className="button-icon">
          {props.icon}
        </div>
      )}
      {props.title}
    </button>
  );
};

export default TertiaryButton;

import React from "react";
import "./checkbox.scss";

const CheckboxWithLabel = (props) => {
  return (
    <div className="form-check">
      <input
        className="form-check-input"
        type="checkbox"
        checked={props.checked}
        onChange={props.onChange}
      />
      <label className="form-check-label">{props.label}</label>
    </div>
  );
};

export default CheckboxWithLabel;

import React from "react";
import "./checkbox.scss";

class Checkbox extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <input
        disabled={this.props.disabled}
        checked={this.props.checked}
        type="checkbox"
        {...this.props}
      />
    );
  }
}

Checkbox.defaultProps = {
  checked: false,
  disabled: false,
};

export default Checkbox;

import React from "react";
import "./search-form.scss";
import PrimaryButton from "../button/primary-button";
import InputField from "../input-field/input-field";
import {MdOutlineSearch} from "react-icons/md";

const SearchForm = (props) => {
  return (
    <form className="form-container">
      <input
        type={props.type}
        className="form-control mb-3"
        id={props.id}
        value={props.value}
        onChange={props.onChange}
        placeholder={props.placeholder}
      />
      {props.onClick && (
        <PrimaryButton
          title={props.title}
          onClick={props.onClick}
          className={props.className}
          icon={<MdOutlineSearch/>}
        />
      )}
    </form>
  );
};

export default SearchForm;

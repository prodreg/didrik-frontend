import React from "react";
import Select from "react-select";
import "./dropdown-menu.scss";

const DropdownMenu = (props) => {
  return (
    <div className="container">
      <Select
        options={props.options}
        value={props.value}
        onInputChange={props.onInputChange}
        onChange={props.onChange}
        placeholder={props.placeholder}
        getOptionLabel={props.optionLabel}
        getOptionValue={props.optionValue}
        styles={{ menuPortal: (base) => ({ ...base, zIndex: 9999 }) }}
        menuPortalTarget={document.body}
        menuPosition={"fixed"}
        defaultValue={props.defaultValue}
      />
    </div>
  );
};

export default DropdownMenu;

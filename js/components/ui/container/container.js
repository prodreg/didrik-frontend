import React from "react";
import "./container.scss";
import PageHeader from "../page-header/page-header";

const Container = (props) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-lg">
          <div className="card">
            <div className="card-header">
              <PageHeader
                title={props.title}
                description={props.description}
                icon={props.icon}
              />
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-lg">{props.children}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Container;

import React from "react";
import "./input-field.scss";

const InputField = (props) => {
  return (
    <input
      type={props.type}
      id={props.id}
      value={props.value}
      name={props.name}
      className="input-field"
      onChange={props.onChange}
      onBlur={props.onBlur}
      placeholder={props.placeholder}
      min={props.min}
      disabled={props.disabled}
    />
  );
};

export default InputField;

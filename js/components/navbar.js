import { useSelector, useDispatch } from "react-redux";
import React, { useEffect, useCallback, useState } from "react";
import { NAVIGATION } from "../constants/navigation";
import { Link, Route } from "react-router-dom";
import "./navbar.scss";
import { getAllEventMessages } from "../redux/thunks/event-message-thunks";
import Actions from "../redux/actions/event-message-actions";
import userService from "../service/user-service";

const active = (url, path) => {
  if (!url.includes("/producers")) {
    localStorage.setItem("query", "");
  }
  if (!url.includes("/event-message")) {
    localStorage.setItem("radio_button_state", "innboks");
    localStorage.setItem("page_number", 0);
    localStorage.setItem("scroll_position", 0);
  }
  if ("/" === path) {
    return url.endsWith("/") ? "active" : "";
  }
  if (url.includes("/organizations")) {
    return path.includes("/organizations") ? "active" : "";
  }
  return url.includes(path) ? "active" : "";
};

const Navbar = ({ userRoles, url, userInfo }) => {
  const eventMessageCounter = useSelector((state) => state.counterReducer);
  const user = userService.getLoggedInUser();
  const dispatch = useDispatch();

  const getEventMessages = () => {
    dispatch(getAllEventMessages()).then((response) => {
      let counter = 0;
      response.payload.data.forEach((e) => {
        if (!e.read && !e.softDeleted) {
          counter++;
        }
      });
      dispatch(Actions.setNumberOfUnreadMessages(counter));
    });
  };

  useEffect(() => {
    getEventMessages();
  }, []);

  useEffect(() => {
    const timer = setInterval(() => getEventMessages(), 60000);
    return () => clearInterval(timer);
  }, []);

  let userNavigation = [];

  if (userRoles.includes("admin")) {
    userNavigation = NAVIGATION.ADMIN;
  } else if (userRoles.includes("bank")) {
    userNavigation = NAVIGATION.BANK;
  } else if (userRoles.includes("mottaker")) {
    userNavigation = NAVIGATION.VAREMOTTAKER;
  } else if (userRoles.includes("garantiutvalget")) {
    userNavigation = NAVIGATION.GARANTIUTVALGET;
  }

  const getPath = (path) => {
    if (userRoles.includes("bank") || userRoles.includes("mottaker") || userRoles.includes("garantiutvalget")) {
      if (path.includes("/organizations/details")) {
        path = path + "/" + user.orgNumber;
      }
    }
    return path;
  };

  return (
    <div id="navbar" className="navbar-container">
      <div className="row">
        <div className="col-lg">
          <div className="card">
            <div className="card-body">
              <div className="nav flex-column nav-pills">
                {userNavigation.map((nav) => (
                  <Link
                    key={nav.path}
                    className={"nav-link " + active(url, nav.path)}
                    to={getPath(nav.path)}
                  >
                    {nav.name.includes("Meldinger") ? (
                      <span>
                        {nav.name}
                        <span className="float-end">
                          <span className="counter-container">
                            {" "}
                            {eventMessageCounter > 0 ? eventMessageCounter : ""}
                          </span>
                        </span>
                      </span>
                    ) : (
                      nav.name
                    )}
                  </Link>
                ))}
                <a className={"nav-link"} href="https://produsentregisteret.zendesk.com/hc/no" target="_blank">
                  Hjelp
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Navbar.defaultProps = {
  userRoles: [],
  url: "",
};

export default Navbar;

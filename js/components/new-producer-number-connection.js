import React from "react";
import { Modal, Row, Col, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import TertiaryButton from "./ui/button/tertiary-button";
import SecondaryButton from "./ui/button/secondary-button";
import {createProducerNumberConnection} from "../redux/thunks/producer-thunk";
import { connect } from "react-redux";

class NewProducerNumberConnection extends React.Component {
  state = {
    oldNumber: "",
    newNumber: "",
    saving: false
  };

  constructor(props) {
    super(props);
  }

  closeDialog = () => {
    this.setState({
      oldNumber: "",
      newNumber: "",
      saving: false
    });

    this.props.toggleShowNewConnectionDialog();
  };

  handleSubmitNewConnection = (event) => {
    event.preventDefault();
    event.stopPropagation();
      this.setState({
        saving: true,
      });
      this.props.createProducerNumberConnection(this.state.oldNumber, this.state.newNumber)
        .then((res) => {
          if (res && res.payload) {
            this.handleError(res.payload.error);
          } else {
            this.handleError(res);
          }
          this.setState({
            saving: false,
          });
        });
  };

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Koblingen kunne ikke opprettes. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("OK", "Koblingen er opprettet", "success").then(() => {
        this.props.history.go(0);
      });
    }
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  render() {
    return (
      <Modal show={this.props.showAddProducerNumberConnection} onHide={this.closeDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Legg til en produsentnummerkobling</Modal.Title>
        </Modal.Header>
        <Form noValidate onSubmit={this.handleSubmitNewConnection}>
          <Modal.Body>
            <Form.Group as={Row}>
              <Form.Label column>
                Gammelt produsentnummer
              </Form.Label>
              <Col>
                <Form.Control
                  required
                  name="oldNumber"
                  onChange={this.handleChange}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column>
                Nytt produsentnummer
              </Form.Label>
              <Col>
                <Form.Control
                  required
                  name="newNumber"
                  onChange={this.handleChange}
                />
              </Col>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <TertiaryButton
              type="reset"
              title="Avbryt"
              onClick={this.closeDialog}
              disabled={this.state.saving}
            />
            <SecondaryButton
              type="submit"
              title="Lagre"
              disabled={this.state.saving}
            />
          </Modal.Footer>
        </Form>
      </Modal>
    );
  }
}

NewProducerNumberConnection.defaultProps = {
  showAddProducerNumberConnection: false,
};

const mapStateToProps = (state) => {
  return { };
}

const mapDispatchToProps = {
  createProducerNumberConnection
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProducerNumberConnection);

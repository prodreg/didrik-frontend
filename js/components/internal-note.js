import React from "react";
import Swal from "sweetalert2";
import { connect } from "react-redux";
import PrimaryButton from "./ui/button/primary-button";
import {getCaseNote, updateCaseNote} from "../redux/thunks/case-thunks";

class InternalNote extends React.Component {
  state = {
    note: "",
    saving: false
  };

  constructor(props) {
    super(props);
    this.props.getCaseNote(this.props.caseId)
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
        return null;
        })
      .then((res) => {
        if (res !== null) {
          this.setState({
            note: res.text
          });
        }
      })
  }

  handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.setState({
      saving: true,
    });
    this.props.updateCaseNote(this.props.caseId, this.state.note)
      .then((res) => {
        this.handleError(!res.ok);
        this.setState({
          saving: false,
        });
      });
  };

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Internt notat kunne ikke oppdateres. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("OK", "Internt notat har blitt oppdatert", "success");
    }
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  render() {
    return (
      <div>
        <h4>Internt notat</h4>
        <textarea
          className="form-control form-control-sm"
          rows="5"
          value={this.state.note}
          name="note"
          onChange={this.handleChange}
        />
        <br />
        <PrimaryButton
          title="Lagre notat"
          onClick={this.handleSubmit}
          disabled={this.state.saving}
        />
      </div>
    );
  }
}

InternalNote.defaultProps = {};

const mapStateToProps = (state) => {
  return { };
}

const mapDispatchToProps = {
  getCaseNote,
  updateCaseNote
}

export default connect(mapStateToProps, mapDispatchToProps)(InternalNote);

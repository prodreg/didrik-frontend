import React from "react";
import { Modal, Row, Col, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import TertiaryButton from "./ui/button/tertiary-button";
import SecondaryButton from "./ui/button/secondary-button";
import { connect } from "react-redux";

class GarantiutvalgetResponse extends React.Component {
  state = {
    justification: "",
    sending: false
  };

  constructor(props) {
    super(props);
  }

  closeDialog = () => {
    this.setState({
      justification: "",
      sending: false
    });

    this.props.toggleShowGarantiutvalgetResponseDialog();
  };

  handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.setState({
      sending: true,
    });
    this.props.sendGarantiutvalgetResponse(this.props.caseId, this.state.justification)
      .then((res) => {
        this.handleError(!res.ok);
        this.setState({
          saving: false,
        });
      });
  };

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "Responsen kunne ikke sendes. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("OK", "Responsen ble sendt", "success").then(() => {
        this.props.history.go(0);
      });
    }
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  render() {
    return (
      <Modal show={this.props.showGarantiutvalgetResponseDialog} onHide={this.closeDialog}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.title}</Modal.Title>
        </Modal.Header>
        <Form noValidate onSubmit={this.handleSubmit}>
          <Modal.Body>
            <Form.Group as={Row}>
              <Form.Label>
                Begrunnelse
              </Form.Label>
              <Col>
                <Form.Control
                  rows={6}
                  as="textarea"
                  name="justification"
                  onChange={this.handleChange}
                />
              </Col>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <TertiaryButton
              type="reset"
              title="Avbryt"
              onClick={this.closeDialog}
              disabled={this.state.saving}
            />
            <SecondaryButton
              type="submit"
              title={this.props.buttonTitle}
              disabled={this.state.saving}
            />
          </Modal.Footer>
        </Form>
      </Modal>
    );
  }
}

GarantiutvalgetResponse.defaultProps = {
  showGarantiutvalgetResponseDialog: false,
};

const mapStateToProps = (state) => {
  return { };
}

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(GarantiutvalgetResponse);

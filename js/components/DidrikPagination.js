import React from "react";
import {
	MdChevronLeft,
	MdChevronRight,
	MdOutlineChevronLeft,
	MdOutlineChevronRight,
} from "react-icons/md";
import ReactPaginate from "react-paginate";

/**
 * Pagination bar. Uses React Pagination, accepts any props it does.
 * @param outline whether to use outlined or full chevrons
 * @param rest any prop from react pagination
 * @returns
 */
export const DidrikPagination = ({ chevronOutline = false, ...rest }) => {
	return (
		<ReactPaginate
			previousLabel={
				chevronOutline ? <MdOutlineChevronLeft /> : <MdChevronLeft />
			}
			nextLabel={
				chevronOutline ? <MdOutlineChevronRight /> : <MdChevronRight />
			}
			breakLabel={"..."}
			containerClassName={"pagination"}
			pageClassName={"page-item"}
			pageLinkClassName={"page-link"}
			previousClassName={"page-item"}
			previousLinkClassName={"page-link"}
			nextLinkClassName={"page-link"}
			nextClassName={"page-item"}
			breakClassName={"page-item"}
			breakLinkClassName={"page-link"}
			activeClassName={"active"}
			{...rest}
		/>
	);
};

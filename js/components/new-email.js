import React from "react";
import { Modal, Row, Col, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import TertiaryButton from "./ui/button/tertiary-button";
import SecondaryButton from "./ui/button/secondary-button";

class NewEmail extends React.Component {
  state = {
    validEmail: {
      isValid: false,
      nonEmpty: false,
      validPattern: true,
    },
    newEmail: "",
    saving: false,
    validated: false,
  };

  constructor(props) {
    super(props);
    this.validateEmail = this.validateEmail.bind(this);
  }

  closeDialog = () => {
    this.setState({
      validEmail: {
        isValid: false,
        nonEmpty: false,
        validPattern: true,
      },
      newEmail: "",
      saving: false,
      validated: false,
    });

    this.props.toggleShowChangeEmail();
  };

  validateEmail = (event) => {
    const email = event.target.value;
    var isValid = true;
    var nonEmpty = true;
    var validPattern = true;
    if (!email) {
      isValid = false;
      nonEmpty = false;
    }

    let re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    validPattern = re.test(email);
    if (!validPattern) {
      isValid = false;
    }

    const validEmail = {
      isValid: isValid,
      nonEmpty: nonEmpty,
      validPattern: validPattern,
    };
    this.setState({
      validEmail: validEmail,
      newEmail: email,
    });
  };

  handleSubmitNewEmail = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.state.validEmail.isValid) {
      this.setState({
        saving: true,
      });
      if (this.props.orgNumber) {
        this.props
          .updateTarget(this.props.orgNumber, this.props.targetId, this.state.newEmail)
          .then((res) => {
            this.handleError(res.payload.error);
            this.setState({
              saving: false,
            });
          });
      } else {
        this.props
          .updateTarget(this.props.targetId, this.state.newEmail)
          .then((res) => {
            this.handleError(res.payload.error);
            this.setState({
              saving: false,
            });
          });
      }
    }

    this.setState({
      validated: true,
    });
  };

  handleError = (error) => {
    if (error) {
      Swal.fire(
        "En feil har oppstått",
        "E-post adressen kunne ikke bli oppdatert. Vennligst prøv igjen senere.",
        "error"
      );
    } else {
      Swal.fire("OK", "E-post adressen er oppdatert", "success").then(() => {
        this.props.history.go(0);
      });
    }
  };

  render() {
    return (
      <Modal show={this.props.showChangeEmail} onHide={this.closeDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Endre e-postadresse</Modal.Title>
        </Modal.Header>
        <Form noValidate onSubmit={this.handleSubmitNewEmail}>
          <Modal.Body>
            <Form.Group as={Row}>
              <Form.Label column sm={4}>
                Ny e-post adresse
              </Form.Label>
              <Col sm={8}>
                <Form.Control
                  required
                  isInvalid={
                    !this.state.validEmail.isValid && this.state.validated
                  }
                  type="email"
                  onChange={this.validateEmail}
                />
                {this.state.validated && (
                  <>
                    <Row>
                      {!this.state.validEmail.nonEmpty && (
                        <Form.Text className="text-danger text-small">
                          Epost er påkrevd
                        </Form.Text>
                      )}
                    </Row>
                    <Row>
                      {!this.state.validEmail.validPattern && (
                        <Form.Text className="text-danger text-small">
                          Du må skrive inn en gyldig e-post adresse
                        </Form.Text>
                      )}
                    </Row>
                  </>
                )}
              </Col>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <TertiaryButton
              type="reset"
              title="Avbryt"
              onClick={this.closeDialog}
              disabled={this.state.saving}
            />
            <SecondaryButton
              type="submit"
              title="Lagre"
              disabled={
                (!this.state.validEmail.isValid && this.state.validated) ||
                this.state.saving
              }
            />
          </Modal.Footer>
        </Form>
      </Modal>
    );
  }
}

NewEmail.defaultProps = {
  showChangeEmail: false,
};

export default NewEmail;

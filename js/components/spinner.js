const React = require('react');

const Spinner = () => {
  return (
      <div style={{margin:'auto', width: '64px', marginTop: '20px'}}>
        <div className="lds-ellipsis">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
  );
};

export default Spinner;
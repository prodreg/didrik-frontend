import React, { useState } from "react";

const useCheckbox = () => {
  const [checked, setChecked] = useState(false);
  const [selected, setSelected] = useState(0);

  const checkboxHandler = () => {
    setSelected(1);
    setChecked(!checked);
  };

  const resetCheckbox = () => {
    setChecked(false);
    setSelected(0);
  };
  return {
    checked,
    selected,
    checkboxHandler,
    resetCheckbox,
  };
};

export default useCheckbox;

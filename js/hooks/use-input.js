import { useState } from "react";

const useInput = (validateInput) => {
  const [enteredInput, setEnteredInput] = useState("");
  const [isTouched, setIsTouched] = useState(false);

  const inputIsValid = validateInput(enteredInput);
  const hasError = !inputIsValid && isTouched;

  const inputChangeHandler = (event) => {
    setEnteredInput(event.target.value);
  };

  const inputBlurHandler = () => {
    setIsTouched(true);
  };

  const resetForm = () => {
    setEnteredInput("");
    setIsTouched(false);
  };

  return {
    input: enteredInput,
    isValid: inputIsValid,
    hasError,
    inputChangeHandler,
    inputBlurHandler,
    resetForm,
  };
};

export default useInput;
